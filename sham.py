'''
Assign stellar mass or magnitude to [sub]halos via subhalo abundance matching (SHAM).

Masses in [log M_sun], luminosities in [log L_sun/h^2], distances in [Mpc comoving].

@author: Andrew Wetzel
'''


# system ----

import numpy as np
from numpy import log10, Inf
from scipy import integrate, interpolate, ndimage
# local ----
import utilities as ut
from visualize import plot_sm


def assign_to_catalog(
    subs, m_kind='star.mass', scatter=0, disrupt_mf=0.007, source='', sham_property='mass.peak',
    snapshot_indices=None):
    '''
    Assign stellar mass or absolute r-band magnitude to subhalos via abundance matching.

    Parameters
    ----------
    subs : dict or list : catalog of subhalos at snapshot or across snapshots
    m_kind : string : mass/mag kind: mag.r, star.mass
    scatter : float : mass/mag log-normal 1-sigma scatter at fixed subhalo property [dex]
    disrupt_mf : float : M_now / M_peak below which remove subhalos (both satellites and centrals)
    source : string : source of mass/mag function
    sham_property : string : subhalo property to abundance match to
    snapshot_indices : int or list : index[s] of snapshot[s]
    '''
    if isinstance(subs, list):
        if snapshot_indices is None:
            raise ValueError('subhalo catalog is a tree list, but no input snapshot index[s]')
    elif isinstance(subs, dict):
        if snapshot_indices is not None:
            raise ValueError('input snapshot index[s], but input catalog of subhalo at snapshot')
        subs = [subs]
        snapshot_indices = [0]

    sub = subs[snapshot_indices[0]]
    vol = (sub.info['box.length'] * ut.const.mega_per_kilo) ** 3  # [Mpc comoving]
    snapshot_indices = ut.array.arrayize(snapshot_indices)

    if m_kind == 'star.mass':
        if not source:
            source = 'li-drory-marchesini'
        redshift = sub.snapshot['redshift']
        if redshift < 0.1:
            redshift = 0.1
        MF = SMFClass(source, redshift, scatter, sub.Cosmology['hubble'])
    elif m_kind == 'mag.r':
        if not source:
            source = 'blanton'
        MF = LFClass(source, scatter, sub.Cosmology['hubble'])
    else:
        raise ValueError('not recognize m_kind = %s' % m_kind)

    for ti in snapshot_indices:
        sub = subs[ti]
        sub[m_kind] = np.zeros(sub[sham_property].size, np.float32)
        if m_kind == 'star.mass':
            z = sub.snapshot['redshift']
            if z < 0.1:
                z = 0.1
            MF.initialize_redshift(redshift)

        # maximum number of objects in volume to assign given SMF/LF threshold
        num_max = int(round(MF.get_number_density(MF.m_min) * vol))
        sis = ut.array.get_indices(sub[sham_property], [0.001, Inf])
        if disrupt_mf:
            sis = ut.array.get_indices(sub['mass.frac.min'], [disrupt_mf, Inf], sis)
        siis_sort = np.argsort(sub[sham_property][sis]).astype(sis.dtype)[::-1][:num_max]
        num_sums = ut.array.get_arange(num_max) + 1

        if scatter:
            scats = np.random.normal(np.zeros(num_max), MF.scatter).astype(np.float32)
            sub[m_kind][sis[siis_sort]] = MF.mass_scat(num_sums / vol) + scats
        else:
            sub[m_kind][sis[siis_sort]] = MF.m(num_sums / vol)


class SMFClass:
    '''
    Relate number density [dnumden / dlog(M_star/M_sun)] <-> stellar mass [log10(M_star/M_sun)]
    using fits to observed stellar mass functions.
    All SMFs assume input Hubble constant.
    '''
    def __init__(self, source='li-marchesini', redshift=0.1, scatter=0, hubble=0.7):
        '''
        Parameters
        ----------
        source : string : source of observed stellar mass function
        redshift : float : redshift of observed stellar mass function
        scatter : float : log-normal scatter in M_star at fixed M_peak
        hubble : float : dimensionless hubble parameter to assume (regardless of what was published)
        '''
        self.source = source
        self.scatter = scatter
        self.hubble = hubble

        if source == 'li':
            '''
            Li & White 2009. z = 0.1 from SDSS. Chabrier IMF. Complete to 1e8 M_sun/h^2.
            '''
            self.redshifts = np.array([0.1])
            self.m_chars = np.array([10.525]) - 2 * log10(hubble)  # [M_sun]
            self.amplitudes = np.array([0.0083]) * hubble ** 3  # [Mpc^-3 / log(M/M_sun)]
            self.slopes = np.array([-1.155])
            self.initialize_redshift(redshift)

        elif source == 'baldry':
            '''
            Baldry et al 2008. z = 0.1 from SDSS. diet Salpeter IMF = 0.7 Salpeter.
            Complete to 1e8 M_sun.
            '''
            h_them = 0.7  # their assumed hubble constant
            self.redshifts = np.array([0.1])
            # covert to Chabrier
            self.m_chars = (np.array([10.525]) + 2 * log10(h_them / hubble) + log10(1 / 1.6 / 0.7))
            self.amplitudes = np.array([0.00426]) * (hubble / h_them) ** 3
            self.amplitudes_2 = np.array([0.00058]) * (hubble / h_them) ** 3
            self.slopes = np.array([-0.46])
            self.slopes_2 = np.array([-1.58])
            self.initialize_redshift(redshift)

        elif source == 'cole-marchesini':
            '''
            Marchesini et al 2009. 1.3 < z < 4.0. Kroupa IMF.
            z = 0.1 from Cole et al 2001 (2dF), converting their Salpeter to Kroupa.
            *** In order to use out to z ~ 4, made evolution flat from z = 3.5 to 4.
            '''
            self.redshifts = np.array([0.1, 1.6, 2.5, 3.56, 4.03])
            self.m_chars = np.array([10.65, 10.60, 10.65, 11.07, 11.07]) - 2 * log10(hubble)
            # converted to [Mpc^-3 dex^-1]
            self.amplitudes = np.array([90.00, 29.65, 11.52, 1.55, 1.55]) * 1e-4 * hubble ** 3
            self.slopes = np.array([-1.18, -1.00, -1.01, -1.39, -1.39])
            self.make_splines()
            self.initialize_redshift(redshift)

        elif source == 'li-marchesini':
            '''
            Marchesini et al 2009, using Li & White at z = 0.1.
            '''
            self.redshifts = np.array([0.1, 1.6, 2.5, 3.56, 4.03])
            self.m_chars = np.array([10.525, 10.60, 10.65, 11.07, 11.07]) - 2 * log10(hubble)
            self.amplitudes = (np.array([0.0083, 0.002965, 0.00115, 0.000155, 0.000155]) *
                               hubble ** 3)
            self.slopes = np.array([-1.155, -1.00, -1.01, -1.39, -1.39])
            self.make_splines()
            self.initialize_redshift(redshift)

        elif source == 'fontana':
            '''
            Fontana et al 2006. 0.4 < z < 4 from GOODS-MUSIC. Salpeter IMF.
            z = 0.1 from Cole et al 2001.
            '''
            h_them = 0.7  # their assumed hubble constant
            self.redshifts = np.array([0.1, 4.0])  # store redshift range of validity
            self.amplitude_0 = 0.0035 * (hubble / h_them) ** 3  # to [Mpc^-3 / log10(M/M_sun)]
            self.amplitude_1 = -2.2
            self.slope_0 = -1.18
            self.slope_1 = -0.082
            self.m_char_0 = 11.16  # log10(M/M_sun)
            self.m_char_1 = 0.17  # log10(M/M_sun)
            self.m_char_2 = -0.07  # log10(M/M_sun)
            # convert to my hubble & Chabrier IMF
            self.m_char_0 += 2 * log10(h_them / hubble) - log10(1.6)
            self.initialize_redshift(redshift)

        elif source == 'li-drory-marchesini':
            '''
            Drory et al 2009. 0.3 < z < 1.0 from COSMOS.
            Chabrier IMF limited to 0.1 - 100 M_sun.
            Complete to (8.0, 8.6, 8.9, 9.1) M_sun/h^2 at z = (0.3, 0.5, 0.7, 0.9).
            Anchor to Li & White at z = 0.1, Marchesini et al at higher redshift.
            See Ilbert et al 2010 for alternate COSMOS version.
            '''
            h_them = 0.72  # their assumed hubble constant
            self.redshifts = np.array([0.3, 0.5, 0.7, 0.9])
            self.m_chars = np.array([10.90, 10.91, 10.95, 10.92]) + 2 * log10(h_them / hubble)
            # convert to [Mpc^-3 dex^-1]
            self.amplitudes = (np.array([0.00289, 0.00174, 0.00216, 0.00294]) *
                               (hubble / h_them) ** 3)
            self.slopes = np.array([-1.06, -1.05, -0.93, -0.91])
            self.m_chars_2 = np.array([9.63, 9.70, 9.75, 9.85]) + 2 * log10(h_them / hubble)
            self.amplitudes_2 = (np.array([0.00180, 0.00143, 0.00289, 0.00212]) *
                                 (hubble / h_them) ** 3)
            self.slopes_2 = np.array([-1.73, -1.76, -1.65, -1.65])
            # add li & white
            self.redshifts = np.append(0.1, self.redshifts)
            self.m_chars = np.append(10.525 - 2 * log10(hubble), self.m_chars)
            self.amplitudes = np.append(0.0083 * hubble ** 3, self.amplitudes)
            self.slopes = np.append(-1.155, self.slopes)
            self.m_chars_2 = np.append(self.m_chars_2[0], self.m_chars_2)
            self.amplitudes_2 = np.append(0, self.amplitudes_2)
            self.slopes_2 = np.append(self.slopes_2[0], self.slopes_2)
            # add marchesini et al
            h_them = 0.7  # their assumed hubble constant
            self.redshifts = np.append(self.redshifts, [1.6, 2.5, 3.56, 4.03])
            self.m_chars = np.append(
                self.m_chars, np.array([10.60, 10.65, 11.07, 11.07]) - 2 * log10(hubble))
            self.amplitudes = np.append(
                self.amplitudes, np.array([0.002965, 0.00115, 0.000155, 0.000155]) * hubble ** 3)
            self.slopes = np.append(self.slopes, [-1.00, -1.01, -1.39, -1.39])
            self.m_chars_2 = np.append(self.m_chars_2, np.zeros(4) + self.m_chars_2[0])
            self.amplitudes_2 = np.append(self.amplitudes_2, np.zeros(4))
            self.slopes_2 = np.append(self.slopes_2, np.zeros(4) + self.slopes_2[0])
            self.make_splines()
            self.initialize_redshift(redshift)

        elif source == 'li-drory-marchesini_sameshape':
            '''
            Apply low-mass slope from Drory et al 2009 to Li & White, Marchesini et al.
            '''
            self.redshifts = np.array([0.1, 0.3, 0.5, 0.7, 0.9, 1.6, 2.5, 3.56, 4.03])
            self.m_chars = np.array(
                [10.525, 10.61, 10.62, 10.66, 10.63, 10.60, 10.65, 11.07, 11.07] -
                2 * log10(hubble))
            self.amplitudes = np.array([0.0083, 0.00774, 0.00466, 0.00579, 0.00787, 0.00297,
                                        0.00115, 0.000155, 0.000155]) * hubble ** 3
            self.slopes = np.array([-1.155, -1.06, -1.05, -0.93, -0.91, -1.00, -1.01, -1.39, -1.39])
            self.m_chars_2 = np.array(
                [9.35, 9.34, 9.41, 9.46, 9.56, 9.41, 9.46, 9.83, 9.83]) - 2 * log10(hubble)
            self.amplitudes_2 = np.array(
                [0.00269, 0.00482, 0.00383, 0.00774, 0.00568, 0.000962, 0.000375, 0.0000503,
                 0.0000503]) * hubble ** 3
            self.slopes_2 = np.array(
                [-1.70, -1.73, -1.76, -1.65, -1.65, -1.72, -1.74, -2.39, -2.39])
            self.make_splines()
            self.initialize_redshift(redshift)

        elif source == 'perez':
            '''
            Perez-Gonzalez et al 2008. 0.1 < z < 4.0 from Spitzer, Hubble, Chandra.
            Salpeter IMF.
            Complete to (8, 9.5, 10, 11) M_star at z = (0, 1, 2, 3).
            '''
            h_them = 0.7  # their assumed hubble constant
            self.redshifts = np.array(
                [0.1, 0.3, 0.5, 0.7, 0.9, 1.15, 1.45, 1.8, 2.25, 2.75, 3.25, 3.75])
            self.m_chars = np.array([11.16, 11.20, 11.26, 11.25, 11.27, 11.31, 11.34, 11.40, 11.46,
                                     11.34, 11.33, 11.36]) + 2 * log10(h_them / hubble)
            # convert to Chabrier IMF
            self.m_chars -= log10(1.6)
            # convert to [Mpc ^ -3 dex ^ -1]
            self.amplitudes = (
                10 ** np.array([-2.47, -2.65, -2.76, -2.82, -2.91, -3.06, -3.27, -3.49, -3.69,
                                - 3.64, -3.74, -3.94]) * (hubble / h_them) ** 3)
            self.slopes = np.array(
                [-1.18, -1.19, -1.22, -1.26, -1.23, -1.26, -1.29, -1.27, -1.26, -1.20, -1.14,
                 - 1.23])
            self.make_splines()
            self.initialize_redshift(redshift)

        else:
            raise ValueError('not recognize source = %s' % source)

    def make_splines(self):
        '''
        Make spline fits to SMF fit parameters v redshift.
        Use 1st order spline (k) to avoid ringing.
        '''
        self.m_char_v_z_spl = interpolate.splrep(self.redshifts, self.m_chars, k=1)
        self.slope_v_z_spl = interpolate.splrep(self.redshifts, self.slopes, k=1)
        self.amplitude_v_z_spl = interpolate.splrep(self.redshifts, self.amplitudes, k=1)
        if self.source in ['li-drory-marchesini', 'li-drory-marchesini_sameslope']:
            self.m_char_2_v_z_spl = interpolate.splrep(self.redshifts, self.m_chars_2, k=1)
            self.slope_2_v_z_spl = interpolate.splrep(self.redshifts, self.slopes_2, k=1)
            self.amplitude_2_v_z_spl = interpolate.splrep(self.redshifts, self.amplitudes_2, k=1)

    def initialize_redshift(self, redshift=0.1):
        '''
        Make spline to get mass from number density.

        Find SMF fit parameters at redshift, correcting amplitude by * log(10) and slope by + 1
        to make get_dn_dm call faster.

        Parameters
        ----------
        redshift : float
        '''
        if redshift < self.redshifts.min() - 1e-5 or redshift > self.redshifts.max() + 1e-5:
            raise ValueError('z = %.2f out of range for %s' % (redshift, self.source))
        self.redshift = redshift
        if self.source in ['li']:
            self.m_char = self.m_chars[0]
            self.amplitude = self.amplitudes[0] * np.log(10)
            self.slope = self.slopes[0] + 1
        elif self.source in ['baldry']:
            self.m_char = self.m_chars[0]
            self.m_char_2 = self.m_chars[0]
            self.amplitude = self.amplitudes[0] * np.log(10)
            self.amplitude_2 = self.amplitudes_2[0] * np.log(10)
            self.slope = self.slopes[0] + 1
            self.slope_2 = self.slopes_2[0] + 1
        elif self.source in ['cole-marchesini', 'li-marchesini', 'perez']:
            self.m_char = interpolate.splev(redshift, self.m_char_v_z_spl)
            self.amplitude = interpolate.splev(redshift, self.amplitude_v_z_spl) * np.log(10)
            self.slope = interpolate.splev(redshift, self.slope_v_z_spl) + 1
        elif self.source == 'fontana':
            self.m_char = self.m_char_0 + self.m_char_1 * redshift + self.m_char_2 * redshift ** 2
            self.amplitude = (self.amplitude_0 * (1 + redshift) ** self.amplitude_1) * np.log(10)
            self.slope = (self.slope_0 + self.slope_1 * redshift) + 1
        elif self.source in ['li-drory-marchesini', 'li-drory-marchesini_sameslope']:
            self.m_char = interpolate.splev(redshift, self.m_char_v_z_spl)
            self.amplitude = interpolate.splev(redshift, self.amplitude_v_z_spl) * np.log(10)
            self.slope = interpolate.splev(redshift, self.slope_v_z_spl) + 1
            self.m_char_2 = interpolate.splev(redshift, self.m_char_2_v_z_spl)
            self.amplitude_2 = interpolate.splev(redshift, self.amplitude_2_v_z_spl) * np.log(10)
            self.slope_2 = interpolate.splev(redshift, self.slope_2_v_z_spl) + 1
        self.make_number_density_v_m_spline(self.redshift, self.scatter)

    def get_dn_dm(self, star_mass):
        '''
        Compute d(num-den) / d(log mass) = ln(10) * amplitude *
        (10^(star_mass - m_char)) ** (1 + slope) * exp(-10^(star_mass - m_char)).

        Parameters
        ----------
        star_mass : float : log stellar mass
        '''
        mass_ratios = 10 ** (star_mass - self.m_char)

        if 'drory' in self.source or self.source == 'baldry':
            dmass2s = 10 ** (star_mass - self.m_char_2)
            return (self.amplitude * mass_ratios ** self.slope * np.exp(-mass_ratios) +
                    self.amplitude_2 * dmass2s ** self.slope_2 * np.exp(-dmass2s))
        else:
            return self.amplitude * mass_ratios ** self.slope * np.exp(-mass_ratios)

    def get_number_density(self, mass_min, mass_max=14):
        '''
        Compute number density within range.

        Parameters
        ----------
        mass_min : float : minimum of log stellar mass
        mass_max : float : maximum of log stellar mass
        '''
        return integrate.quad(self.get_dn_dm, mass_min, mass_max)[0]

    def make_number_density_v_m_spline(self, redshift=0.1, scatter=0):
        '''
        Make splines to relate d(num-den) / d[log]mass & num-den(> mass) to mass.

        Parameters
        ----------
        redshift : float : use if want to change from default
        scatter : float : log-normal scatter [dex]
        '''
        iter_number = 30

        if redshift != self.redshift:
            self.initialize_redshift(redshift)
        if scatter != self.scatter:
            self.scatter = scatter

        dm = 0.01
        dm_scat_lo = 3 * scatter  # extend fit for deconvolve b.c.'s
        dm_scat_hi = 0.5 * scatter  # extend fit for deconvolve b.c.'s
        self.m_min = 7.3
        self.m_max = 12.3
        m_stars = np.arange(self.m_min - dm_scat_lo, self.m_max + dm_scat_hi, dm, np.float32)
        numdens = np.zeros(m_stars.size)
        dndms = np.zeros(m_stars.size)
        for mi in range(m_stars.size):
            # make sure numdens are monotonically decreasing even if = -infinity
            numdens[mi] = self.get_number_density(m_stars[mi]) + 1e-9 * (1 - mi * 0.001)
            dndms[mi] = self.get_dn_dm(m_stars[mi]) + 1e-9 * (1 - mi * 0.001)

        # make no scatter splines
        self.log_numden_m_spl = interpolate.splrep(m_stars, log10(numdens))
        self.mass_log_numden_spl = interpolate.splrep(log10(numdens)[::-1], m_stars[::-1])
        # at high z, smf not monotonically decreasing, so spline not work on below
        # self.m_log_dndm_spl = interpolate.splrep(log10(dndms)[::-1], m_stars[::-1])
        # make scatter splines
        if scatter:
            # deconvolve osbserved smf assuming scatter to find unscattered one
            dndms_scat = ut.math.deconvolve(dndms, scatter, dm, iter_number)
            # chop off lower boundaries, unreliable
            m_stars = m_stars[dm_scat_lo / dm:]
            dndms_scat = dndms_scat[dm_scat_lo / dm:]
            # find spline to integrate over
            self.dndm_m_scat_spl = interpolate.splrep(m_stars, dndms_scat)
            numdens_scat = np.zeros(m_stars.size)
            for mi in range(m_stars.size):
                numdens_scat[mi] = interpolate.splint(m_stars[mi], m_stars.max(),
                                                      self.dndm_m_scat_spl)
                numdens_scat[mi] += 1e-9 * (1 - mi * 0.001)
            self.log_numden_m_scat_spl = interpolate.splrep(m_stars, log10(numdens_scat))
            self.m_log_numden_scat_spl = interpolate.splrep(log10(numdens_scat)[::-1],
                                                            m_stars[::-1])

    def get_m(self, get_number_density):
        '''
        Get mass at threshold.

        Parameters
        ----------
        get_number_density : float : threshold number density
        '''
        return interpolate.splev(log10(get_number_density),
                                 self.mass_log_numden_spl).astype(np.float32)

    def get_m_scatter(self, get_number_density):
        '''
        Get mass at threshold, using de-scattered source.

        Parameters
        ----------
        get_number_density : float : threshold number density
        '''
        return interpolate.splev(log10(get_number_density),
                                 self.m_log_numden_scat_spl).astype(np.float32)

    def get_m_from_dn_dm(self, dn_dmass):
        '''
        Get mass at d(number-density)/d[log]mass.

        Parameters
        ----------
        dn_dmass : float : d(number-density) / d[log]mass
        '''
        return interpolate.splev(log10(dn_dmass), self.m_log_dndm_spl)

    def get_dn_dm_scatter(self, mass):
        '''
        Get d(number-density) / d[log]mass at stellar mass, using de-scattered source.

        Parameters
        ----------
        mass : float : log stellar mass
        '''
        return interpolate.splev(mass, self.dndm_m_scat_spl)

    def get_number_density_scatter(self, mass):
        '''
        Get number-density(> [log]mass) at mass, using de-scattered source.

        Parameters
        ----------
        mass : float : log stellar mass
        '''
        return 10 ** (interpolate.splev(mass, self.log_numden_m_scat_spl))


class LFClass(SMFClass):
    '''
    Relate number density [Mpc ^ -3] <-> magnitude/luminosity using spline fit to luminosity
    functions.

    Parameters
    ----------
    SMFClass : class : use for spline querying functions from SMFClass
    '''
    def __init__(self, source='blanton', scatter=0, hubble=0.7):
        '''
        Parameters
        ----------
        source : string : published source for luminosity function
        scatter : float : log-normal scatter in Mag_r at fixed M_peak
        hubble : float : dimensionless hubble parameter to assume (regardless of published value)
        '''
        if source == 'norberg':
            # Norberg et al 2002: 2dF r-band at z ~ 0.1.
            self.m_char = -19.66
            self.amplitude = 1.61e-2 * hubble ** 3  # Mpc ^ -3
            self.slope = -1.21
        elif source == 'blanton':
            # Blanton et al 03: SDSS r-band z ~ 0.1.
            self.m_char = -20.44
            self.amplitude = 1.49e-2 * hubble ** 3  # Mpc ^ -3
            self.slope = -1.05
        elif source == 'sheldon':
            # Sheldon et al 07: SDSS i-band z = 0.25. Valid for Mag < -19.08 (0.19L*).
            self.m_char = -20.9  # Hansen et al 09 catalog has -20.8
            self.amplitude = 1.02e-2 * hubble ** 3  # Mpc ^ -3
            self.slope = -1.21
        else:
            raise ValueError('not recognize source = %s in LFClass' % source)

        self.make_number_density_v_m_spline(scatter)

    def get_dn_dm(self, mag):
        '''
        Get d(number-density) / d(mag).

        Parameters
        ----------
        mag : float : absolute value of absolute magnitude
        '''
        mag = -mag
        return (np.log(10) / 2.5 * self.amplitude *
                10 ** ((self.slope + 1) / 2.5 * (self.m_char - mag)) *
                np.exp(-10 ** ((self.m_char - mag) / 2.5)))

    def get_number_density(self, mag_min, mag_max=25):
        '''
        Get number density within range.

        Parameters
        ----------
        mag_min : float : minimum of absolute value of absolute magnitude
        mag_max : float : maximum of absolute value of absolute magnitude
        '''
        return integrate.quad(self.get_dn_dm, mag_min, mag_max)[0]

    def make_number_density_v_m_spline(self, scatter=0):
        '''
        Make splines to relate d(number-density)/d(mag) and number-density(> mag) to mag.

        Parameters
        ----------
        scatter : float : log-normal 1-sigma scatter [dex]
        '''
        self.scatter = 2.5 * scatter  # convert scatter in log(luminosity) to scatter in magnitude
        deconvol_iter_number = 20

        dmag = 0.01
        dmag_scat_lo = 2 * self.scatter  # extend fit for boundary conditions of deconvolve
        dmag_scat_hi = 1 * self.scatter

        self.m_min = 17.0
        self.m_max = 23.3

        mags = np.arange(self.m_min - dmag_scat_lo, self.m_max + dmag_scat_hi, dmag, np.float32)
        numdens = np.zeros(mags.size)
        dndms = np.zeros(mags.size)
        for mag_i in range(len(mags)):
            numdens[mag_i] = self.get_number_density(mags[mag_i])
            dndms[mag_i] = self.get_dn_dm(mags[mag_i])

        # make no scatter splines
        self.log_numden_m_spl = interpolate.splrep(mags, log10(numdens))
        self.dndm_m_spl = interpolate.splrep(mags, dndms)
        self.mass_log_numden_spl = interpolate.splrep(log10(numdens)[::-1], mags[::-1])
        # make scatter splines
        if self.scatter:
            # deconvolve observed lf assuming scatter to find unscattered one
            dndms_scat = ut.math.deconvolve(dndms, self.scatter, dmag, deconvol_iter_number)
            # chop off boundaries, unreliable
            mags = mags[dmag_scat_lo / dmag:-dmag_scat_hi / dmag]
            dndms_scat = dndms_scat[dmag_scat_lo / dmag:-dmag_scat_hi / dmag]
            # find spline to integrate over
            self.dndm_m_scat_spl = interpolate.splrep(mags, dndms_scat)
            numdens_scat = np.zeros(mags.size)
            for mag_i in range(mags.size):
                numdens_scat[mag_i] = interpolate.splint(mags[mag_i], max(mags),
                                                         self.dndm_m_scat_spl)
                numdens_scat[mag_i] += 1e-9 * (1 - mag_i * 0.001)
            self.log_numden_m_scat_spl = interpolate.splrep(mags, log10(numdens_scat))
            self.m_log_numden_scat_spl = interpolate.splrep(log10(numdens_scat)[::-1], mags[::-1])


#===================================================================================================
# test/plot
#===================================================================================================
def plot_test_sham(
    subs, snapshot_index, m_kind, m_min, m_max, scatter=0.2, mass_frac_min=0, mass_width=0.1,
    source='', sham_kind='mass.peak'):
    '''
    Plot mass/mag functions.

    Parameters
    ----------
    subs : list : catalog of subhalos across snapshots
    snapshot_index : int : snapshot index
    m_kind : string : mass/mag kind: star.mass, mag.r
    m_min : float : min of m_kind
    m_max : flot : max of m_kind
    scatter : float : stellar mass/mag log-normal 1-sigma scatter at fixed subhalo property
    mass_frac_min : float : M_now / M_peak fraction to disrupt subhalos
    mass_width : float : mass/mag bin width
    source : string : source for mass/mag function
    sham_kind : string : subhalo property to SHAM against
    '''
    mass_width_scat = 3 * scatter
    mass_bins = (np.arange(m_min - mass_width_scat, m_max + mass_width_scat, mass_width, np.float32) +
              0.5 * mass_width)

    if m_kind == 'star.mass':
        if not source:
            source = 'li-marchesini'
        Sf = SMFClass(source, subs.snapshot['redshift'][snapshot_index], scatter,
                      subs.Cosmology['hubble'])
    elif m_kind == 'mag.r':
        if not source:
            source = 'blanton'
        Sf = LFClass(source, scatter, subs.Cosmology['hubble'])

    # analytic gmf, no scatter
    dndm_anal = Sf.get_dn_dm(mass_bins)
    if scatter:
        # convolve above gmf with scatter, then deconvolve, to see if can recover
        dndm_anal_conv = ndimage.filters.gaussian_filter1d(dndm_anal, Sf.scatter / mass_width)
        dndm_anal_decon = ut.math.deconvolve(dndm_anal_conv, Sf.scatter, mass_width, 30)
        # mean (underlying) relation
        dndm_anal_pre = Sf.get_dn_dm_scatter(mass_bins)
        # observed gmf after convolution (no random noise)
        dndm_anal_recov = ndimage.filters.gaussian_filter1d(dndm_anal_pre, Sf.scatter / mass_width)
        # cut out extremes, unreliable
        cutoff = int(round(mass_width_scat / mass_width))
        if cutoff > 0:
            mass_bins = mass_bins[cutoff:-cutoff]
            dndm_anal = dndm_anal[cutoff:-cutoff]
            dndm_anal_conv = dndm_anal_conv[cutoff:-cutoff]
            dndm_anal_pre = dndm_anal_pre[cutoff:-cutoff]
            dndm_anal_decon = dndm_anal_decon[cutoff:-cutoff]
            dndm_anal_recov = dndm_anal_recov[cutoff:-cutoff]

    mass_bins -= 0.5 * mass_width
    # assign mass to subhalo, with or without scatter (random noise at high mass end)
    assign_to_catalog(subs, snapshot_index, m_kind, scatter, mass_frac_min, source, sham_kind)
    ims = ut.binning.get_bin_indices(subs[snapshot_index][m_kind], mass_bins)
    gal_numbers = np.zeros(mass_bins.size)
    for mi in range(mass_bins.size):
        gal_numbers[mi] = ims[ims == mi].size
    print('bin count min %d' % np.min(gal_numbers))
    dndm_sham = gal_numbers / subs.info['box.length'] ** 3 / mass_width
    print('assign ratio ave %.3f' % np.mean(abs(dndm_sham / dndm_anal)))
    if scatter:
        print('recov ratio ave %.3f' % np.mean(abs(dndm_anal_recov / dndm_anal)))

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('linear', 'linear', [m_min, m_max], log10(dndm_anal))
    Plot.make_window()
    Plot.draw('c', mass_bins, log10(dndm_anal))
    Plot.draw('c', mass_bins, log10(dndm_sham), ct='red')
    if scatter:
        Plot.draw('c', mass_bins, log10(dndm_anal_pre), ct='green')
        Plot.draw('c', mass_bins, log10(dndm_anal_recov), ct='blue')


def plot_sources(
    sources=['li-marchesini', 'perez'], redshifts=0.1, mass_limits=[8.0, 11.7], mass_width=0.1,
    plot_kind='value'):
    '''
    Plot each source at each redshift.

    Parameters
    ----------
    sources : string or list : source[s] for mass/mag function
    redshifts : float or list : redshifts to plot
    mass_limits : list : min and max stellar mass/mag to plot
    mass_width : float : mass/mag bin width
    plot_kind : string : what to plot: value, ratio
    '''
    sources = ut.array.arrayize(sources)
    redshifts = ut.array.arrayize(redshifts)

    MassBin = ut.binning.BinClass(mass_limits, mass_width)

    log_dn_dlogms = []
    for src_i in range(sources.size):
        log_dn_dlogms_so = []
        for zi in range(redshifts.size):
            Smf = SMFClass(sources[src_i], redshifts[zi], scatter=0, hubble=0.7)
            log_dn_dlogms_so.append(log10(Smf.get_dn_dm(MassBin.mids)))
        log_dn_dlogms.append(log_dn_dlogms_so)

    # plot ----------
    Plot = plot_sm.PlotClass()
    if plot_kind == 'ratio':
        ys = 10 ** (log_dn_dlogms - log_dn_dlogms[0][0])
        Plot.axis.space_y = 'linear'
    elif plot_kind == 'value':
        ys = log_dn_dlogms
        Plot.axis.space_y = 'log'
    Plot.set_axis('log', '', MassBin.mids, ys, tick_label_kind='log')
    Plot.set_axis_label('star.mass', 'dn/dlog(M_star) [h^3 Mpc^-3]')
    Plot.make_window()
    Plot.set_label(position_y=0.4)
    for src_i in range(sources.size):
        for zi in range(redshifts.size):
            Plot.draw('c', MassBin.mids, log_dn_dlogms[src_i][zi], ct=src_i, lt=zi)
            Plot.make_label(sources[src_i] + ' z=%.1f' % redshifts[zi])
    # add cosmos at z = 0.35
    '''
    cosmos = [[8.8,    0.015216 , 1.250341e-03],
                        [9,         0.01257,    1.210321e-03],
                        [9.2,         0.01009,    1.047921e-03],
                        [9.4,        0.007941,    8.908445e-04],
                        [9.6,        0.006871,    7.681928e-04],
                        [9.8,        0.005688,    6.825634e-04],
                        [10,        0.005491,    6.136567e-04],
                        [10.2,        0.004989,    6.004422e-04],
                        [10.4,         0.00478,    5.917784e-04],
                        [10.6,         0.00423,    5.851342e-04],
                        [10.8,        0.003651,    4.919025e-04],
                        [11,        0.002253,    3.562664e-04],
                        [11.2,        0.001117,    2.006811e-04],
                        [11.4,     0.0004182,    8.486049e-05],
                        [11.6,     8.365e-05,    2.802892e-05],
                        [11.8,     1.195e-05,    8.770029e-06]]
    '''
    # cosmos at z = 0.87
    cosmos = [
        [9.8, 0.005377, 3.735001e-04],
        [10, 0.004206, 3.443666e-04],
        [10.2, 0.003292, 3.235465e-04],
        [10.4, 0.003253, 3.318173e-04],
        [10.6, 0.002985, 3.198681e-04],
        [10.8, 0.002994, 2.735925e-04],
        [11, 0.002218, 1.922526e-04],
        [11.2, 0.001202, 1.067172e-04],
        [11.4, 0.0005681, 3.983348e-05],
        [11.6, 0.0001837, 1.195015e-05],
        [11.8, 4.214e-05, 3.200856e-06],
        [12, 1.686e-06, 7.463160e-07]
    ]
    cosmos = np.array(cosmos)
    cosmos = cosmos.transpose()
    cosmos[1] = log10(cosmos[1] * 0.72 ** -3)
    # Plot.draw('pp', cosmos[0], cosmos[1], pt=123)
