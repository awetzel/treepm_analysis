'''
Plot tracks of [sub]halos, histories of [sub]halo properties.

Masses in {log M_sun}, distances in [kpc comoving].

@author: Andrew Wetzel
'''


# system ----
from __future__ import absolute_import, division, print_function
import numpy as np
from numpy import log10, Inf
# local ----
import utilities as ut
from visualize import plot_sm


#===================================================================================================
# history plot
#===================================================================================================
def plot_histories(cats, zi_start=1, zi_end=15, props=['mass.fof', 'vel.circ.max'],
                   axis_time_kind='time', cis=None):
    '''
    Loop over [sub]halos, plot history of input properties for each.

    Import [sub]halo catalog, starting & ending snapshot index, list of properties to plot,
    x-axis property to plot (t, z, a), catalog indices to loop over.
    '''
    if cis is not None:
        cis = ut.array.arrayize(cis, bit_number=32)
    else:
        cis = ut.array.get_arange(cats[zi_start][props[0]])
    for ci in cis:
        print(ci)
        plot_history(cats, zi_start, zi_end, ci, axis_time_kind, props)
        if raw_input('continue? [y/n] ') == 'n':
            return


def plot_histories_satellite(subs, zi_now, zi_inf_min=0, mass_kind='mass.peak', mass_limits=[],
                             props=['mass.bound', 'vel.circ.max']):
    '''
    Plot satellite history since just before infall.

    Import subhalo catalog, snapshot index, minimum snapshot for infall, mass range, what to plot.
    '''
    sis = ut.array.get_indices(subs[zi_now][mass_kind], mass_limits)
    sis = ut.catalog.get_indices_ilk(subs[zi_now], 'satellite', sis)
    axis_t_kind = 'time.inf'
    for si in sis:
        zi_inf = subs[zi_now]['sat.first.zi'][si]
        if zi_inf >= zi_inf_min:
            print(si)
            plot_history(subs, zi_now, zi_inf, si, axis_t_kind, props)
            if raw_input('continue? ') == 'n':
                return


def plot_history(cats, zi_start, zi_end, ci, axis_t_kind='time',
                 props=['mass.bound', 'mass.peak', 'halo.mass'], mass_limits=[1, Inf]):
    '''
    Plot history of input properties.

    Import [sub]halo catalog, starting & ending snapshot indices (forward or backward),
    [sub]halo index at zi_start, x-axis kind (t, z, a), properties to plot.
    '''
    def draw_ctypes(Plot, xs, ys, ctypes, lt):
        for pi in range(ys.size):
            Plot.draw('pp', xs[pi], ys[pi], ct=ctypes[pi])
        Plot.draw('c', xs, ys, ct='def', lt=lt)

    #Say = ut.io.SayClass(plot_history)
    zi_step, family_key = ut.catalog.get_tree_direction_info(zi_start, zi_end)
    props = ut.array.arrayize(props)
    prop_dict = {'time': [], 'redshift': [], 'ct': []}
    for k in props:
        prop_dict[k] = []
    zi, ci = zi_start, ci
    # go through history, append properties to arrays
    while zi != zi_end + zi_step and ci >= 0:
        prop_dict['redshift'].append(cats.snapshot['redshift'][zi])
        prop_dict['time'].append(abs(cats.snapshot['time'][zi] -
                                     cats.snapshot['time'][max(zi_start, zi_end)]))
        for k in props:
            if k in cats[zi]:
                prop_dict[k].append(cats[zi][k][ci])
        if 'sat.number' in props:
            cen_e = cats[zi]['central.index'][ci]
            if cen_e >= 0:
                print(zi, cats[zi]['central.index'][ci])
                prop_dict['sat.number'].append(ut.catalog.get_indices_in_halo(
                    cats[zi], cats[zi]['central.index'][ci], 'mass.peak', mass_limits,
                    'satellite').size)
            else:
                prop_dict['sat.number'].append(0)
        """
        if -1 <= cats[zi]['ilk'][ci] <= 0:
            rperi = su.get_distance_peri_sat(cats, zi, ci) * ut.const.kilo_per_mega
            prop_dict['radius.peri'].append(rperi)
        else:
            prop_dict['radius.peri'].append(zeros(10))
        prop_dict['time.peri'].append(arange(0, cats.snapshot['time.wid'][zi],
                                       cats.snapshot['time.wid'][zi] / 10))
        if zi != zi_start and prop_dict['central.distance'][-1] > prop_dict['central.distance'][-2]:
            prop_dict['time.peri'][-1] = prop_dict['time'][-1] + prop_dict['time.peri'][-1]
        else:
            prop_dict['time.peri'][-1] = prop_dict['time'][-1] - prop_dict['time.peri'][-1]
        """
        # set color
        if 'ilk' in cats[zi]:
            if cats[zi]['ilk'][ci] == 1:
                prop_dict['ct'].append('blue')
            elif cats[zi]['ilk'][ci] == 2:
                prop_dict['ct'].append('green')
            elif cats[zi]['ilk'][ci] == 0:
                prop_dict['ct'].append('red')
            elif cats[zi]['ilk'][ci] < 0:
                prop_dict['ct'].append('orange')
        else:
            prop_dict['ct'].append('blue')
        zi, ci = zi + zi_step, cats[zi][family_key][ci]
    for k in prop_dict:
        prop_dict[k] = np.array(prop_dict[k])

    # plot ----------
    panel_number_y = len(props)
    if panel_number_y > 1:
        panel_number_y *= -1
    Plot = plot_sm.PlotClass(panel_number_y=panel_number_y)
    if axis_t_kind == 'time':
        lab_x = 'Time [Gyr]'
        vals_x = prop_dict['time']
        space_x = 'linear'
        limits_x = vals_x
    elif axis_t_kind == 'redshift':
        lab_x = '1 + z'
        vals_x = log10(1 + prop_dict['redshift'])
        space_x = 'log'
        limits_x = [float(max(vals_x)), float(min(vals_x))]
    for k in props:
        # set y-axis
        if k == 'satellite.number':
            Plot.set_axis(space_x, 'linear', limits_x, [0, prop_dict['satellite.number'].max() + 1])
            Plot.set_axis_label(lab_x, 'N_{sat}')
        elif k == 'central.distance':
            Plot.set_axis(space_x, 'linear', limits_x, [0, prop_dict['central.distance'].max() * 1.2])
            Plot.set_axis_label(lab_x, 'distance [kpc]')
        else:
            Plot.set_axis(space_x, 'linear', limits_x, prop_dict[k], extra=0.05)
            Plot.set_axis_label(lab_x, k)
            #Plot.axis.lab_y = 'M/M_{max} & V_{c}^3/V_{c, max}^3'
        Plot.make_panel()
        # draw
        if k == 'sat.number':
            draw_ctypes(Plot, vals_x, prop_dict['sat-num'], prop_dict['ct'], lt='-')
        elif k == 'central.distance':
            draw_ctypes(Plot, vals_x, prop_dict['central.distance'], prop_dict['ct'], lt='-')
            #for ir in range(len(prop_dict['rperi'])):
            # Plot.draw('c', prop_dict['trperi'][jr], prop_dict['rperi'][jr], ct='purple')
        else:
            draw_ctypes(Plot, vals_x, prop_dict[k], prop_dict['ct'], lt='-')
            """
            Plot.make_label('log(M_{max})=%.2f' % log10(prop_dict['m'].max()))
            Plot.make_label('V_{circ,max}=%.0f km/s' % prop_dict['v.circ'].max())
            """


#===================================================================================================
# track plot
#===================================================================================================
def plot_track_virtual(subs, ti, mass_limits=[], ilk='all'):
    '''
    Import subhalo catalog, snapshot index, mass range, ilk (cen, sat, all).
    '''
    par_zi = ti + 1
    end_zi = ti + 3
    pos_range = 0.2
    if ilk == 'central':
        sis = ut.array.get_indices(subs[ti]['ilk'], 2)
        rad_scaling = ''
    elif ilk == 'satellite':
        sis = ut.array.get_indices(subs[ti]['ilk'], [-3, -0.9])
        rad_scaling = 'central'

    sis = ut.array.get_indices(subs[ti]['mass.peak'], mass_limits, sis)
    for si in sis:
        par_i = ut.catalog.get_indices_tree(subs, ti, par_zi, si)
        if par_i > 0:
            print('par_i %6d | m %.2e | sat num %2d' %
                  (par_i, subs[par_zi]['mass.bound'][par_i], ut.catalog.get_indices_in_halo(
                   subs[par_zi], par_i, 'mass.bound', [1, Inf], 'satellite').size))
            print('si    %6d | m.bound %.2e | sat.number %2d' %
                  (si, subs[ti]['mass.bound'][si],
                   ut.catalog.get_indices_in_halo(subs[ti], si, 'mass.bound', [1, Inf],
                                                  'satellite').size))
            chi_zi, chi_i = ti - 1, subs[ti]['child.index'][si]
            print('chi_i %6d | m.bound %.2e | sat.number %2d' %
                  (chi_i, subs[chi_zi]['mass.bound'][chi_i], ut.catalog.get_indices_in_halo(
                   subs[chi_zi], chi_i, 'mass.bound', [1, Inf], 'satellite').size))
            plot_track_group(subs, ti, end_zi, si, pos_range, 0, Inf, rad_scaling,
                             mass_kind='mass.bound')
            cont = raw_input('continue? ')
            if cont == 'n':
                return


def assign_plot_type(Plot, ilk):
    if ilk == 1:
        Plot.set_all(ct='red', pt=80)
    elif ilk == 2:
        Plot.set_all(ct='magenta', pt=80)
    elif ilk == 0:
        Plot.set_all(ct='blue', pt=30)
    elif ilk < 0:
        Plot.set_all(ct='cyan', pt=30)
    if ilk <= 0:
        Plot.set_all(ct='yellow', pt=30)


def plot_track_2d(Plot, subs, ti_start, ti_end, si, cen, pos_limits, pos_is, pos_scale, ilks):
    '''
    Import plot class, subhalo catalog, starting & final snapshot index (go forward or backward),
    id, position fam_ti central, plotting pos range, dimensions to plot.
    '''
    ti_step, family_key = ut.catalog.get_tree_direction_info(ti_start, ti_end)
    ti, si = ti_start, si
    Plot.set_lt(0)
    # loop over all direct parents/children
    while ti != ti_end and si > 0 and ilks[0] <= subs[ti]['ilk'][si] <= ilks[1]:
        assign_plot_type(Plot, subs[ti]['ilk'][si])
        if ti == ti_start:
            Plot.set_line(ex=2)
        else:
            Plot.set_line(ex=1.1)
        if pos_scale == 'central':
            p0, p1 = ut.coordinate.get_position_differences(
                subs[ti]['position'][si][pos_is] - cen[ti]['position'][pos_is],
                subs.info['box.length'])
        else:
            p0, p1 = subs[ti]['position'][si][pos_is]
        Plot.draw('p', p0, p1)
        # find its parent/child & connect
        fam_ti, fam_si = ti + ti_step, subs[ti][family_key][si]
        if fam_ti != ti_end and fam_si > 0:
            assign_plot_type(Plot, subs[fam_ti]['ilk'][fam_si])
            if pos_scale == 'central':
                fp0, fp1 = ut.coordinate.get_position_differences(
                    subs[fam_ti]['position'][pos_is] - cen[fam_ti]['position'][pos_is],
                    subs.info['box.length'])
            else:
                fp0, fp1 = subs[fam_ti]['position'][fam_si][pos_is]
            Plot.draw('c', [p0, fp0], [p1, fp1])

            # if going backward, find all parents
            if ti_step == 1:
                parn_zi, parn_i = fam_ti, subs[fam_ti]['parent.n.index'][fam_si]
                while (parn_zi != ti_end and parn_i > 0 and
                       ilks[0] <= subs[parn_zi]['ilk'][parn_i] <= ilks[1]):
                    assign_plot_type(Plot, subs[parn_zi]['ilk'][parn_i])
                    if pos_scale == 'central':
                        lp0, lp1 = ut.coordinate.get_position_differences(
                            subs[parn_zi]['position'][parn_i, pos_is] -
                            cen[parn_zi]['position'][parn_i, pos_is],
                            subs.info['box.length'])
                    else:
                        lp0, lp1 = subs[parn_zi]['position'][parn_i, pos_is]
                    Plot.draw('c', [p0, lp0], [p1, lp1])
                    plot_track_2d(Plot, subs, parn_zi, ti_end, parn_i, cen, pos_limits, pos_is,
                                  pos_scale, ilks)
                    parn_zi, parn_i = parn_zi, subs[parn_zi]['parent.n.index'][parn_i]
        ti, si = fam_ti, fam_si


def plot_track_3d(subs, zi_start, zi_end, sis, cen, pos_limits, pos_scale, ilks):
    '''
    Plot 3 slices on 1 plot.

    Import subhalo catalog, starting & final snapshot index (go forward or backward), ids,
    central dictionary, plotting pos range, position scaling (none, cen), ilks to plot.
    '''
    Plot = plot_sm.PlotClass(panxn=-2, panyn=-2)
    pos_is_all = [[0, 1], [2, 1], [0, 2]]
    p_names = ['x', 'y', 'z']
    sis = ut.array.arrayize(sis, bit_number=32)
    for ip in range(len(pos_is_all)):
        pos_is = pos_is_all[ip]
        if pos_scale == 'central':
            Plot.set_axis_limit((-pos_limits, pos_limits), (-pos_limits, pos_limits))
        else:
            Plot.set_axis_limit((cen['position'][zi_start][pos_is[0]] - pos_limits,
                                 cen['position'][zi_start][pos_is[0]] + pos_limits),
                                (cen['position'][zi_start][pos_is[1]] - pos_limits,
                                 cen['position'][zi_start][pos_is[1]] + pos_limits))
        Plot.set_axis('linear', 'linear')
        Plot.set_axis_label('%s [kpc]' % p_names[pos_is[0]], '%s [kpc]' % p_names[pos_is[1]])
        Plot.make_panel()
        if pos_scale == 'central':
            point_number = 100
            rad = cen['radius.vir'][zi_start]
            xx = range(-point_number, point_number + 1)
            xx = np.array(xx) * rad / point_number
            yy = (rad ** 2 - xx ** 2) ** 0.5
            Plot.set_lt(1)
            Plot.draw('c', xx, yy)
            Plot.draw('c', xx, -yy)
        for e in sis:
            plot_track_2d(Plot, subs, zi_start, zi_end, e, cen, pos_limits, pos_is, pos_scale, ilks)


def plot_track_group(subs, zi_start, zi_end, si, position_limits,
                     mass_kind='mass.peak', mass_limits=[11, Inf], pos_scale='', ilks=[-3, 2]):
    '''
    Plot tracks of all subhalos in si's halo.

    Import subhalo catalog, starting & final snapshot index (go forward or backward), id,
    plotting pos range, satellite mass kind & range, postion scaling (none, cen), ilks to plot.
    '''
    c200 = 4

    snapshot_number = max(zi_start, zi_end)
    cen = {'position': np.zeros((snapshot_number + 1, 3)),
           'radius.vir': np.zeros(snapshot_number + 1)}
    zi_step, family_key = ut.catalog.get_tree_direction_info(zi_start, zi_end)
    HaloProperty = ut.halo_property.HaloPropertyClass(subs.Cosmology)
    cen_is = subs[zi_start]['central.index'][si]
    zi = zi_start
    si = cen_is
    # compile central/halo properties across snapshot range
    while zi != zi_end + zi_step and si > 0:
        cen['position'][zi] = subs[zi]['position'][si]
        cen['radius.vir'][zi] = HaloProperty.get_radius_virial(
            'fof.100m', 10 ** subs[zi]['halo.mass'][si], c200,
            redshift=subs.snapshot['redshift'][zi])
        zi, si = zi + zi_step, subs[zi][family_key][si]
    # compling ids to plot
    sis = ut.array.get_indices(subs[zi_start]['central.index'], cen_is)
    sis = ut.array.get_indices(subs[zi_start][mass_kind], mass_limits, sis)
    print('cen %7d | m %.2f' % (cen_is, subs[zi_start][mass_kind][cen_is]))
    for si in sis:
        if si != cen_is:
            print('sat %7d | m %.2f' % (si, subs[zi_start][mass_kind][si]))
    plot_track_3d(subs, zi_start, zi_end + zi_step, sis, cen, position_limits, pos_scale, ilks)


def plot_track_compare(
    subs_1, subs_2, hals_1, hals_2, zi_start, zi_end, mass_rank, mass_limits=[],
    radius_scaling='', ilks=[-3, 2], sat_i=0):
    '''
    Plot tracks of 2 subhalo catalogs.
    Requires halo catalogs to be the same.
    '''
    his_sort = np.argsort(hals_1[zi_start]['mass.fof'])[::-1]
    halo_i = his_sort[mass_rank]
    print('halo.m %.2f' % log10(hals_1[zi_start]['mass.fof'][halo_i]))

    snapshot_number = max(zi_start, zi_end)
    cen = {
        'position': np.zeros((snapshot_number + 1, 3)),
        'radius.vir': np.zeros(snapshot_number + 1)
    }
    zi_step, family_key = ut.catalog.get_tree_direction_info(zi_start, zi_end)
    zi = zi_start
    hi = halo_i
    # compile central/halo properties across snapshot range
    HaloProperty = ut.halo_property.HaloPropertyClass(subs_1.Cosmology)
    c200 = 4
    while zi != zi_end + zi_step and hi > 0:
        cen['position'][zi] = hals_1['position'][zi][hi]
        cen['radius.vir'][zi] = HaloProperty.get_radius_virial(
            'fof.100m', hals_1['mass.fof'][zi][hi], c200, redshift=hals_1.snapshot['redshift'][zi])
        zi, hi = zi + zi_step, hals_1[family_key][zi][hi]

    pos_range = cen['radius.vir'][zi_start] * 1.3
    # start plotting
    sn = hals_1['central.index'][zi_start][halo_i]
    sfn = hals_2['central.index'][zi_start][halo_i]
    print('subfind cen ', sn)
    print('fof6d     cen ', sfn)
    sat_number = 0
    parn_i = hals_1[zi_start]['parent.n.index'][halo_i]
    while parn_i > 0:
        if mass_limits[0] < hals_1[zi_start]['mass.fof'][parn_i] < mass_limits[1]:
            sat_number += 1
            if sat_number == sat_i:
                #plot_sm.sm.device('x11 -device 0')
                cen_i = hals_1['central.index'][zi_start][parn_i]
                if cen_i > 0:
                    print('subfind inf cen ', cen_i)
                    plot_track_3d(subs_1, zi_start, zi_end + zi_step, cen_i, cen, pos_range,
                                  radius_scaling, ilks)
                #plot_sm.sm.device('x11 -device 1')
                cen_i = hals_2['central.index'][zi_start][parn_i]
                if cen_i > 0:
                    print('fof6d     inf cen ', cen_i)
                    plot_track_3d(subs_2, zi_start, zi_end + zi_step, cen_i, cen, pos_range,
                                  radius_scaling, ilks)
                return sat_number + 1
        parn_i = hals_1['parent.n.index'][zi_start][parn_i]
