'''
Analyze of catalogs of halos and subhalos from Martin White's TreePM simulations.

Masses in {log M_sun}, positions and distances in [kpc comoving].

@author: Andrew Wetzel
'''


# system ----

import numpy as np
from numpy import log10, Inf, int32, float32
from scipy import special, stats
import copy
# local ----
import utilities as ut
from visualize import plot_sm
from . import sham


Fraction = ut.math.FractionClass()
Function = ut.math.FunctionClass()


#===================================================================================================
# diagnostic
#===================================================================================================
def compare_catalogs(cat_1, cat_2, props='all'):
    '''
    Print objects whose properties do not agree.

    Import catalogs of [sub]halo at snapshot, properties to compare ('all').
    '''
    Say = ut.io.SayClass(compare_catalogs)
    if len(cat_1['parent.index']) != len(cat_2['parent.index']):
        Say.say('! h1 has size %d, cat_2 has size %d' %
                (len(cat_1['parent.index']), len(cat_2['parent.index'])))
        return
    if props == 'all':
        props = list(cat_1.keys())
    props = ut.array.arrayize(props)
    cis_all = ut.array.get_arange(cat_1['parent.index'])
    for k in props:
        if isinstance(cat_1[k], ut.cosmology.CosmologyClass):
            continue
        elif np.isscalar(cat_1[k]):
            if cat_1[k] != cat_2[k]:
                Say.say(k, cat_1[k], cat_2[k])
        elif np.isscalar(cat_1[k]) and np.isscalar(cat_2[k]):
            if cat_1[k] != cat_2[k]:
                Say.say(k, cat_1[k], cat_2[k])
        elif len(cat_1[k]) != len(cat_2[k]):
            Say.say(k, '! cat_1 has size %d, cat_2 has size %d' % (len(cat_1[k]), len(cat_2[k])))
        elif not len(cat_1[k]):
            continue
        else:
            if (cat_1[k] == cat_2[k]).min():
                continue
            else:
                cis = cis_all[cat_1[k] != cat_2[k]]
                if len(cis):
                    Say.say(k, cis)
                    Say.say(cat_1[k][cis])
                    Say.say(cat_2[k][cis])


def print_offset_cen_halo(subs, hals, mass_limits=[]):
    '''
    Print central - halo position offset count and average.

    Import catalog of subhalo & halo at snapshot, mass range.
    '''
    Say = ut.io.SayClass(print_offset_cen_halo)
    offset = []
    mass = []
    sis = ut.array.get_indices(subs['mass.peak'], mass_limits)
    cen_is = hals['central.index'][sis]
    for sii, si in enumerate(sis):
        if cen_is[sii] > 0:
            if hals['x'][si] != subs['x'][cen_is[sii]]:
                offset.append(ut.coordinate.get_distances(
                    'scalar', hals['position'][si], subs['position'][cen_is[sii]],
                    subs.info['box.length']))
                mass.append(hals['mass.fof'][si])
    Say.say('halo %d | offset %d (%.2f) | offset.ave %.3f' %
            (sis.size, len(offset), len(offset) / len(sis), np.mean(offset)))


# global / population properties ----------
def get_number_density(cat, mass_limits=[1, Inf]):
    '''
    Get number density {kpc comoving ^ -3}.

    Import catalog of [sub]halo at snapshot, mass range.
    '''
    return (ut.array.get_indices(cat[cat.info['mass.kind']], mass_limits).size /
            cat.info['box.length'] ** 3)


def print_number_density(cat, mass_limits=[1, Inf]):
    '''
    Print count and number density {kpc comoving ^ -3}.

    Import catalog of [sub]halo at snapshot, mass range.
    '''
    obj_number = ut.array.get_indices(cat[cat.info['mass.kind']], mass_limits).size
    get_number_density = obj_number / cat.info['box.length'] ** 3
    print('# number %d | number density %.3e' % (obj_number, get_number_density))


def print_number_density_ilk(sub, mass_limits=[1, Inf], disrupt_mf=0):
    '''
    Print count and number density {kpc comoving ^ -3} for halos, centrals, and satellites.

    Import catalog of subhalo at snapshot, mass range, disruption mass fraction.
    '''
    volume = sub.info['box.length'] ** 3
    sis = ut.array.get_indices(sub['mass.peak'], mass_limits)
    if disrupt_mf > 0:
        sis = ut.array.get_indices(sub['mass.frac.min'], [disrupt_mf, Inf], sis)
    sis_cen = ut.array.get_indices(sub['ilk'], [1, 2.1], sis)
    sat_number = ut.array.get_indices(sub['ilk'], [-3, 0.1], sis).size
    sis = ut.array.get_indices(sub['halo.mass'], mass_limits, sis_cen)
    halo_number = ut.array.get_indices(sub, 'ilk', [1, 2.1], sis).size
    print('# halo %d %.3e | cen %d %.3e | sat %d %.3e | all %d %.3e' % (
          halo_number, halo_number / volume, sis_cen.size, sis_cen.size / volume, sat_number,
          sat_number / volume, sat_number + sis_cen.size, (sat_number + sis_cen.size) / volume))


def print_number_ilk(sub, mass_limits=[], mass_width=0.5):
    '''
    Count different kinds of subhalos in mass bins.

    Import catalog of subhalo at snapshot, mass range & bin width.
    '''
    Say = ut.io.SayClass(print_number_ilk)
    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    for m in MassBin.mins:
        Say.say('m %s' % (MassBin.get_bin_limits(m),))
        ilk_numbers = np.zeros(6)
        sis = ut.array.get_indices(sub['mass.peak'], [m, m + mass_width])
        for ilk in range(-3, len(ilk_numbers) - 3):
            ilk_numbers[ilk] = sub['ilk'][sis][sub['ilk'][sis] == ilk].size
        sat_number = sub['ilk'][sis][sub['ilk'][sis] <= 0].size
        cen_number = ilk_numbers[1] + ilk_numbers[2]
        Say.say('sub %d' % sis.size)
        Say.say('cen %d | norm %d %.2f | virt %d %.2f' %
                (cen_number, ilk_numbers[1], ilk_numbers[1] / cen_number, ilk_numbers[2],
                 ilk_numbers[2] / cen_number))
        Say.say('sat %d %.1f' % (sat_number, 100 * sat_number / sis.size))
        Say.say('norm %d %.2f | virt %d %.2f | no cen %d %.2f | no halo %d %.2f' %
                (ilk_numbers[0], ilk_numbers[0] / sat_number, ilk_numbers[-1],
                 ilk_numbers[-1] / sat_number, ilk_numbers[-2], ilk_numbers[-2] / sat_number,
                 ilk_numbers[-3], ilk_numbers[-3] / sat_number))
        sis = ut.array.get_indices(sub['v.circ.max'], 0)
        Say.say('v.circ.max = 0 %d' % sis.size)


def print_neighbor_distance(
    sub, gm_kind='mass.peak', gm_limits=[1, Inf], hm_limits=[1, Inf], ilk='all',
    neig_gm_kind='mass.peak', neig_gm_limits=[1, Inf], neig_hm_limits=[1, Inf], neig_ilk='all',
    disrupt_mf=0, neig_number_max=2000, neig_distance_max=10, space='real',
    neig_velocity_dif_max=None):
    '''
    Tally up to neig_number_max neighbors within neig_distance_max, print statistics for sample.

    Parameters
    ----------
    catalog of [sub]halo at snapshot: dict
    galaxy mass kind: string
    galaxy mass limits: list
    halo mass limits: list
    ilk: string
    neighbor mass kind: string
    neighbor mass limits: list
    neihbor halo mass limits: list
    neighb ilk: string
    disrupt mass fraction for both (ignore for neighbor if just cut on its halo mass): float
    maximum number of neighbors per center: int
    neighbor maximum distance [kpc comoving]: float
    distance space: string
        options: real, red, proj
    neighbor line-of-sight velocity difference maximum [km / s] (if space = proj): float
    '''
    Say = ut.io.SayClass(print_neighbor_distance)

    neig = ut.catalog.get_catalog_neighbor(
        sub, gm_kind, gm_limits, hm_limits, ilk, neig_gm_kind, neig_gm_limits, neig_hm_limits,
        neig_ilk, disrupt_mf, neig_number_max, neig_distance_max, space, neig_velocity_dif_max)
    distances = np.zeros(len(neig['distances']), dtype=float32) + neig_distance_max

    for nii, neig_dists in enumerate(neig['distances']):
        if neig_dists.size:
            distances[nii] = neig_dists[0]

    neig_number_density = (neig['info']['neig.number.tot'] /
                           (sub.info['box.length'] * sub.snapshot['scalefactor']) ** 3)
    distance_ave = (3 / (4 * np.pi * neig_number_density)) ** (1 / 3)
    no_neig_number = np.sum(distances == neig_distance_max)

    Say.say('has no neighbor within distance max = %d (%.2f)' %
            (no_neig_number, no_neig_number / neig['self.index'].size))
    Say.say('distance average from number density = %.1f kpc comoving' % distance_ave)
    Say.say('distance median, average = %.1f, %.1f kpc comoving' %
            (np.median(distances), np.mean(distances)))


# satellites in single halo ----------
def print_satellites(sub, index, mass_kind='mass.peak', mass_limits=[11, Inf]):
    '''
    Print satellites in mass range in halo (include self).

    Import catalog of subhalo at snapshot, snapshot index, subhalo index, mass kind & range.
    '''
    sat_is = ut.catalog.get_indices_in_halo(sub, index, mass_kind, mass_limits, 'satellite')
    print('# sat number %d' % sat_is.size)
    for sat_i in sat_is:
        print('  m_max %6.3f | m_bound %6.3f' %
              (index, sub['mass.peak'][sat_i], sub['mass.bound'][sat_i]))


#===================================================================================================
# utility
#===================================================================================================
def assign_neighbor_history(
    subs, ti_now=1, ti_max=15, mass_kind='history', mass_limits=[9.7, Inf],
    neig_mass_limits=[8.5, Inf], ilk='sat ejected', disrupt_mf=0, neig_number_max=200,
    neig_distance_max=2.5):
    '''
    For up to neig_number_max neighbors within neig_distance_max, assign counts, indices,
    and distances for all objects within mass range at ti_now and their parents.
    *** Need to assign stellar mass at all snapshots.

    Parameters
    ----------
    [sub]halo catalog across snapshots: list
    snapshot limits: int
    mass kind: string
    mass limits: list
    neighbor mass limits: list
    ilk: string
        options: cen, sat, ejected
    disrupt mass fraction: float
    neighbor maximum number: int
    neighbor distance maximum [kpc physical]: float
    '''
    # neig objects in mass range at ti_now to track back
    if mass_kind == 'history':
        sis = subs.sathistory['self.index']
        mass_kind = 'star.mass'
    else:
        if 'ejected' in ilk:
            ilk = ilk.split()
            sis = ut.catalog.get_indices_subhalo(
                subs[ti_now], mass_kind, mass_limits, ilk[0], disrupt_mf)
            sis_ejected = get_indices_ejected(
                subs[ti_now], ti_max, mass_kind, mass_limits, ilk[0], disrupt_mf, 'sat.first')
            sis = np.union1d(sis, sis_ejected)
        else:
            sis = ut.catalog.get_indices_subhalo(
                subs[ti_now], mass_kind, mass_limits, ilk, disrupt_mf, ti_max,
                mgrow_max='sat.first')

    neig = {
        'param': {
            'mass.kind': mass_kind, 'mass.limits': mass_limits,
            'neig.mass.kind': mass_kind, 'neig.m.limits': mass_limits,
            'distance.max': neig_distance_max, 'number.max': neig_number_max, 'neig.number.tot': 0},
        # 'number.ave': [],    # average number of neighbors within dist_max, if random distribution
        'self.index': [],  # index of self in [sub]halo/galaxy catalog
        # 'self.index.inv': [],    # go from index in [sub]halo/galaxy catalog to index in this list
        'number': [],  # number of neighbors within mass & distance range, up to neig_number_max
        'distances': [],  # distances of neighbors, sorted
        'indices': []  # indices of neighbors, sorted by distance
    }

    Say = ut.io.SayClass(assign_neighbor_history)
    for k in neig:
        if not neig[k]:
            neig[k] = [[] for _ in range(len(subs))]

    neig['number.ave'] = np.zeros(len(subs), float32)
    Neighbor = ut.neighbor.NeighborClass()
    for zi in range(ti_now, ti_max + 1):
        Say.say('t_i = %d' % zi)
        neig_is = ut.catalog.get_indices_subhalo(
            subs[zi], mass_kind, neig_mass_limits, disrupt_mf=disrupt_mf)
        neig['distances'][zi], neig['indices'][zi] = Neighbor.get_neighbors(
            subs[zi]['position'][sis], subs[zi]['position'][neig_is], neig_number_max,
            [5e-5, neig_distance_max], subs.info['box.length'], subs.snapshot['scalefactor'],
            neig_ids=neig_is)

        neig['self.index'][zi] = sis
        if 'self.index.inv' in neig:
            neig['self.index.inv'][zi] = ut.array.get_array_null(subs[zi]['parent.index'].size)
            neig['self.index.inv'][zi][sis] = ut.array.get_arange(sis)

        # keep *all* subhalos with a parent (if central, might be sat at previous snapshot)
        sis_haspar = ut.array.get_indices(subs[zi]['parent.index'], [1, Inf], sis)
        sis = subs[zi]['parent.index'][sis_haspar]  # shift to previous snapshot
    subs[zi]['neig'] = neig


def assign_neighbor_distance(
    sub, neig, mass_kind='star.mass', neig_mass_ratio_min=0.25, neig_number=5):
    '''
    Assign distance [taken from get_catalog_neighbor] to n'th nearest neighbor above mass ratio at
    each snapshot.
    If not neig to n'th nearest neighbor above mass ratio within dist_max, assign dist_max.

    Import catalog of subhalo at snapshot & neighbor, snapshot index, mass kind,
    minimum neighbor mass ratio, n'th nearest neighbor to neig distance of.
    '''
    Say = ut.io.SayClass(assign_neighbor_distance)
    neig_log_mrat_min = log10(neig_mass_ratio_min)
    neig_dist_name = 'neig.%d.distance' % neig_number
    sub[neig_dist_name] = np.zeros(sub[mass_kind].size, float32)
    sub[neig_dist_name] += Inf
    sis = neig['self.index']
    not_enough_neig_number = 0
    for sii, si in enumerate(sis):
        if len(neig['indices'][sii]):
            neig_is = neig['indices'][sii]
            above_mrats = sub[mass_kind][neig_is] > sub[mass_kind][si] + neig_log_mrat_min
            neig_dists = neig['distances'][sii][above_mrats]
            if neig_dists.size < neig_number:
                not_enough_neig_number += 1
                sub[neig_dist_name][si] = neig['distance.max']
            else:
                sub[neig_dist_name][si] = neig_dists[neig_number - 1]
    Say.say('%d of %d (%.2f) centers not have enough neig above m.rat within distance.max' %
            (not_enough_neig_number, sis.size, not_enough_neig_number / sis.size))


def assign_neig_distance_history(
    subs, ti_now=1, ti_max=15, mass_kind='star.mass', neig_mass_ratio_min=0.25, neig_number=5):
    '''
    Assign distance [taken from assign_neighbor_history] to nth nearest neighbor above mass ratio at
    each snapshot.
    *** Need to assign_neighbor_history() first.

    Import subhalo catalog, snapshot range, mass kind, minimum neighbor mass ratio,
    nth nearest neighbor to neig distance of.
    '''
    Say = ut.io.SayClass(assign_neig_distance_history)
    neig_distance_name = 'neig.%d.distance' % neig_number

    for zi in range(len(subs)):
        subs[zi][neig_distance_name] = []

    for zi in range(ti_now, ti_max + 1):
        Say.say('t_i = %d' % zi)
        assign_neighbor_distance(
            subs[zi], subs[zi]['neig'], mass_kind, neig_mass_ratio_min, neig_number)


def assign_harassment(subs, ti_now=1, ti_max=15):
    '''
    Assign tidal velocity and energy kick from nearest neighbors,
    assuming impulse and distant encounter approximations.

    Import [sub]halo catalog, snapshot range.
    '''
    tid_props = ('tidal.energy.star.m', 'tidal.energy.m.bound')  # tid.vel.star.m')
    Say = ut.io.SayClass(assign_harassment)
    for ti in range(ti_now, ti_max + 1):
        Say.say('t_i = %d' % ti)
        sub = subs[ti]
        close_number = 0
        sis = subs[ti]['neig']['self.index']
        for k in tid_props:
            sub[k] = np.zeros(sub['star.mass'].size, float32)
        for sii, si in enumerate(sis):
            if len(subs[ti]['neig']['indices'][sii]):
                neig_sis = subs[ti]['neig']['indices'][sii]
                distance_vectors = ut.coordinate.get_distances(
                    'vector', sub['position'][neig_sis], sub['position'][si],
                    subs.info['box.length'])
                distance_vectors *= subs.snapshot['scalefactor'][ti]  # [physical]
                velocity_vectors = ut.catalog.get_velocity_differences(
                    sub, sub, neig_sis, si, 'vector', include_hubble_flow=True)
                vels = (velocity_vectors ** 2).sum(1) ** 0.5  # dot product
                vel_vecs_norm = (velocity_vectors.transpose() / vels).transpose()
                # scalar cross product yields impact parameter
                dist_mins = (np.cross(distance_vectors, vel_vecs_norm) ** 2).sum(1) ** 0.5
                if dist_mins.min() < 0.01:
                    close_number += 1
                denom = (vels * dist_mins ** 2)
                tid_vels_m_star = 10 ** sub['star.mass'][neig_sis] / denom
                tid_vels_m_bound = tid_vels_m_star + 10 ** sub['mass.bound'][neig_sis] / denom
                if 'tidal.energy.star.m' in tid_props:
                    sub['tidal.energy.star.m'][si] = log10((tid_vels_m_star ** 2).sum())
                if 'tidal.energy.m.bound' in tid_props:
                    sub['tidal.energy.m.bound'][si] = log10((tid_vels_m_bound ** 2).sum())
                if 'tidal.vel.star.m' in tid_props:
                    sub['tidal.vel.star.m'][si] = log10(tid_vels_m_star.sum())
                if 'tidal.vel.m.bound' in tid_props:
                    sub['tidal.vel.m.bound'][si] = log10(tid_vels_m_bound.sum())

        Say.say('of % d, % d (% .2f) have an distance_min < 10 kpc' %
                (sis.size, close_number, close_number / sis.size))


class NearestHaloClass(ut.catalog.NearestNeighborClass):
    '''
    Find nearest neighbor halo (minimum in terms of distance or d/R_neig) that is more massive
    than self and store its properties.
    '''
    def assign_to_self(
        self, sub,
        gm_kind='star.mass', gm_limits=[9.7, Inf], hm_kind='mass.fof', hm_limits=[1, Inf],
        ilk='', disrupt_mf=0, neig_hm_limits=[12, Inf], neig_number_max=100, neig_distance_max=10,
        distance_kind='comoving', distance_scaling='virial', virial_kind='200m', hal=None):
        '''
        Store dictionary of nearest halo's central index, distance, distance/R_neig, mass.

        Parameters
        ----------
        catalog of [subs]halo at snapshot: dict
        galaxy mass kind: string
        galaxy mass limits: list
        halo mass kind: string
        halo mass limits: list
        ilk: string
        neighbor halo mass limits: list
        maximum number of neighbor per center: int
        neighbor distance maximum [kpc comoving]: float
        distance kind: string
            options: physical, comoving
        distance scaling kind: string
            options: virial, center
        virial kind: string
        halo catalog: dict
        '''
        # make sure neighbor halo mass minimum is above that of centers
        sis = ut.catalog.get_indices_subhalo(sub, gm_kind, gm_limits, hm_limits, ilk, disrupt_mf)
        hm_min = sub['halo.mass'][sis].min()
        if hm_min > neig_hm_limits[0]:
            self.say('input neighbor halo mass min = %.1f but using %.1f' %
                     (neig_hm_limits[0], hm_min))
            neig_hm_limits[0] = hm_min
        neig = ut.catalog.get_catalog_neighbor(
            sub, gm_kind, gm_limits, hm_limits, ilk, 'mass.peak', [1, Inf], neig_hm_limits,
            'central', 0, neig_number_max, neig_distance_max, distance_kind, 'real',
            center_indices=sis)

        nearest = {
            'self.index': neig['self.index'],
            'central.index': ut.array.get_array_null(neig['self.index'].size),
            'central.distance': np.zeros(neig['self.index'].size, float32) + np.array(Inf, float32),
            'central.distance/Rhost': (np.zeros(neig['self.index'].size, float32) +
                                       np.array(Inf, float32)),
            hm_kind: np.zeros(neig['self.index'].size, float32)
        }

        HaloProperty = ut.halo_property.HaloPropertyClass(
            sub.Cosmology, redshift=sub.snapshot['redshift'])

        for sii in range(neig['self.index'].size):
            masks = (sub['halo.mass'][neig['indices'][sii]] >
                     sub['halo.mass'][neig['self.index'][sii]])
            if len(masks) and masks.max():
                cen_is = neig['indices'][sii][masks]
                halo_is = sub['halo.index'][cen_is]
                cen_dists = neig['distances'][sii][masks]
                vir_dists = cen_dists / HaloProperty.radius_virial_catalog(
                    virial_kind, hal, halo_is)

                if distance_scaling == 'virial':
                    iinear = np.argmin(vir_dists)
                elif distance_scaling == 'center':
                    iinear = np.argmin(cen_dists)
                else:
                    raise ValueError('not recognize distance_scaling = %s' % distance_scaling)

                nearest['central.distance/Rhost'][sii] = vir_dists[iinear]
                nearest['central.distance'][sii] = cen_dists[iinear]
                nearest['central.index'][sii] = cen_is[iinear]
                nearest[hm_kind][sii] = hal['mass.fof'][halo_is[iinear]]
        nearest_number = nearest['central.index'][nearest['central.index'] >= 0].size
        self.say('%d of %d (%.2f) have nearby more massive halo' %
                 (nearest_number, neig['self.index'].size,
                  nearest_number / neig['self.index'].size))
        self.nearest = nearest
        self.mass_kind = gm_kind

NearestHalo = NearestHaloClass()


def get_distance_min(cats, zi, cis_1, cis_2):
    '''
    Get minimum distance [kpc comoving] between objects, interpolating between current &
    previous snapshot positions.
    Assume no snapshots are skipped (subhalo interpolation).

    Import catalog of [sub]halo, snapshot index, catalog indices,
    Require len cis_1 <= len cis_2.
    '''
    step_number = 5

    cis_1, cis_2 = ut.array.arrayize((cis_1, cis_2), bit_number=32)
    if (cis_1.size != cis_2.size and cis_1.size != 1 and cis_2.size != 1):
        raise ValueError('input indices are arrays but have different size')
    obj_number = max(cis_1.size, cis_2.size)
    par_zi = zi + 1
    cat = cats[zi]
    dist_mins = np.zeros(obj_number, float32)
    dist_mins += Inf

    # for objects with no parent, use current separation
    ciis = ut.array.get_indices(cat['parent.index'][cis_1], [-Inf, 0.1])
    if len(ciis):
        if cis_1.size == 1:
            dist_mins = ut.coordinate.get_distances(
                'scalar', cat['position'][cis_1], cat['position'][cis_2],
                cats.info['box.length'])
        elif cis_2.size == 1:
            dist_mins[ciis] = ut.coordinate.get_distances(
                'scalar', cat['position'][cis_1[ciis]], cat['position'][cis_2],
                cats.info['box.length'])
        else:
            dist_mins[ciis] = ut.coordinate.get_distances(
                'scalar', cat['position'][cis_1[ciis]], cat['position'][cis_2[ciis]],
                cats.info['box.length'])

    # for objects with parent, interpolate positions
    ciis = ut.array.get_indices(cat['parent.index'][cis_2], [-Inf, 0.1])
    if len(ciis):
        if cis_1.size == 1:
            dist_mins[ciis] = ut.coordinate.get_distances(
                'scalar', cat['position'][cis_1], cat['position'][cis_2[ciis]],
                cats.info['box.length'])
        elif cis_2.size == 1:
            dist_mins = ut.coordinate.get_distances(
                'scalar', cat['position'][cis_1], cat['position'][cis_2],
                cats.info['box.length'])
        else:
            dist_mins[ciis] = ut.coordinate.get_distances(
                'scalar', cat['position'][cis_1[ciis]] - cat['position'][cis_2[ciis]],
                cats.info['box.length'])
    cis_1_haspar, iis = ut.array.get_indices(
        cat['parent.index'], [1, Inf], cis_1, cis_2, get_masks=True)
    cis_2_haspar = cis_2[iis]
    cis_2_haspar, ciis_temp = ut.array.get_indices(
        cat['parent.index'], [1, Inf], cis_2_haspar, get_masks=True)
    cis_1_haspar = cis_1_haspar[ciis_temp]
    iis = iis[ciis_temp]
    par_cis_1 = cat['parent.index'][cis_1_haspar]
    par_cis_2 = cat['parent.index'][cis_2_haspar]
    positions_1 = cat['position'][cis_1_haspar]
    positions_2 = cat['position'][cis_2_haspar]
    position_difs_1 = ut.coordinate.get_position_differences(
        positions_1 - cats[par_zi]['position'][par_cis_1], cats.info['box.length'])
    position_difs_2 = ut.coordinate.get_position_differences(
        positions_2 - cats[par_zi]['position'][par_cis_2], cats.info['box.length'])
    for ds in np.arange(0, 1, 1 / step_number):
        dists = ut.coordinate.get_distances(
            'scalar', (positions_1 - position_difs_1 * ds), (positions_2 - position_difs_2 * ds),
            cats.info['box.length'])
        ciis_temp = ut.array.get_indices(dists, [0, dist_mins[iis]])
        dist_mins[iis[ciis_temp]] = dists[ciis_temp]

    if cis_1.size == 1 and cis_2.size == 1:
        return dist_mins[0]
    else:
        return dist_mins


# formation ----------
class FormationClass(ut.io.SayClass):
    '''
    Get formation time for [sub]halos.
    '''
    def get_times(
        self, cats, ti_now, ti_max, cis, form_time_kind='t.index', form_prop_kind='mass.peak',
        thresh=0.5):
        '''
        Get formation kind value when first fell below threshold, going back.
        Use linear extrapolation between snapshots, rounding to nearest if form_time_kind == ti.
        *** for m ~ v_c ^ 3, m = 0.5 --> v_c = 0.79.

        Import [sub]halo catalog, current & maximum snapshot for history, index[s],
        formation time kind (ti, z, a, t) & defining property (m, m.max, v.circ.max) &
        threshold (fraction of current form prop if < 1, fixed value if > 1).
        '''
        cis = ut.array.arrayize(cis)
        if thresh <= 1:
            form_threshs = cats[ti_now][form_prop_kind][cis] + log10(thresh)
        else:
            form_threshs = np.zeros(cis.size, float32) + thresh
        form_times = np.zeros(cis.size, float32) - 1
        for ciis, ci in enumerate(cis):
            ti = ti_now
            par_ti, par_ci = ti + 1, cats[ti]['parent.index'][ci]
            while 0 <= par_ti < ti_max and par_ci > 0:
                if cats[par_ti][form_prop_kind][par_ci] < form_threshs[ciis]:
                    break
                ti, ci = par_ti, par_ci
                par_ti, par_ci = ti + 1, cats[ti]['parent.index'][ci]
            else:
                # not clear how to deal with not having parent, but still above property threshold
                # if not find parent, assign as snapshot when lose track
                ti = par_ti
                if form_time_kind == 't.index':
                    form_times[ciis] = ti
                else:
                    form_times[ciis] = cats.snapshot[ti][form_time_kind]
            if form_times[ciis] < 0:
                if form_time_kind == 't.index':
                    time_now, time_par = ti, par_ti
                else:
                    time_now = cats.snapshot[ti][form_time_kind]
                    time_par = cats.snapshot[par_ti][form_time_kind]
                val_now = cats[ti][form_prop_kind][ci]
                val_par = cats[par_ti][form_prop_kind][par_ci]
                form_times[ciis] = (time_par + (time_now - time_par) / (val_now - val_par + 1e-10) *
                                    (form_threshs[ciis] - val_par))
        if form_time_kind == 't.index':
            form_times = int(round(form_times))

        return ut.array.scalarize(form_times)

    def assign_to_catalog(
        self, cats, ti_now, ti_max, mass_kind='mass.peak', mass_limits=[11, Inf],
        form_time_kind='z.index', form_prop_kind='mass.peak', thresh=0.5, cis=None):
        '''
        Assign formation time to each object in mass range.

        Import [sub]halo catalog, current & maximum snapshot to look back, mass kind & range,
        formation time kind (zi, z, a, t) & property kind (m, m.max, v.circ.max) & threshold
        (fraction of current value if < 1, fixed value if > 1).
        '''
        form_name = 'form.' + form_time_kind
        cats[ti_now][form_name] = np.zeros(cats[ti_now][mass_kind].size, float32) - 1
        if form_time_kind == 'z.index':
            cats[ti_now][form_name] = cats[ti_now][form_name].astype(int32)
        cis = ut.array.get_indices(cats[ti_now][mass_kind], mass_limits, cis)
        cats[ti_now][form_name][cis] = self.get_times(cats, ti_now, ti_max, cis, form_time_kind,
                                                      form_prop_kind, thresh)
        self.Say('assigned formation at t_i = %d back to t_i = %d' % (ti_now, ti_max))

Formation = FormationClass()


# satellite properties ----------
def assign_first_infall_znow(
    subs, ti_now=1, ti_max=34, mass_kind='star.mass', mass_limits=[9.7, Inf], disrupt_mf=0,
    mass_grow_max=1.5):
    '''
    For satellites + ejecteds (cutting on m.grow.max) in mass range at ti_now, walk back histories,
    assign snapshot & subhalo index prior to first infall, requiring to be satellite for two
    consecutive snapshots afterwards.
    Keeps following back even if parent is a central (for ejected satellites).

    Import subhalo catalog, snapshot, maximum snapshot to go back, galaxy mass kind & range,
    disrupt mass fraction, maximum M_peak(now) / M_peak(inf) to include
    (here, for centrals/ejecteds inf = inf.dif, for satellites inf = inf.first).
    '''
    inf_name = 'sat.first'
    # increase mass growth tolerance for satellites to avoid selecting first infall as when
    # satellite experiences merger mass jump
    log_mgrow_max_sat = log10(mass_grow_max + 0.5)
    tis = np.arange(ti_now, ti_max + 1)
    # assign only to subhalos in mass ranges at zi.now
    sis_m = ut.array.get_indices(subs[ti_now][mass_kind], mass_limits)
    if disrupt_mf:
        sis_m = ut.array.get_indices(subs[ti_now]['mass.frac.min'], [disrupt_mf, Inf], sis_m)
    sis_sat = ut.catalog.get_indices_ilk(subs[ti_now], 'satellite', sis_m)
    # add centrals that have been ejected since zi.max
    sis_cen = get_indices_ejected(
        subs, ti_now, ti_max - 1, mass_kind, mass_limits, 'central', disrupt_mf, mass_grow_max)
    sis = np.union1d(sis_sat, sis_cen)
    sis_now = sis.copy()
    siis_now = ut.array.get_arange(sis_now)  # index to point back to zi.now
    inf_tis = np.zeros((tis.size, sis.size), int32) + ti_now + 1
    for zii, zi in enumerate(tis):
        # only tag as satellite at zi if in real halo & not grown by more than mgrow.max
        sis_am_sat, iis = ut.array.get_indices(subs[zi]['ilk'], [-2, 0.1], sis, get_masks=True)
        siis_now_am_sat = siis_now[iis]
        masks = (subs[zi]['mass.peak'][sis_am_sat] >
                 subs[ti_now]['mass.peak'][sis_now[siis_now_am_sat]] - log_mgrow_max_sat)
        sis_am_sat = sis_am_sat[masks]
        siis_now_am_sat = siis_now_am_sat[masks]
        # assign subhalo index & snapshot
        inf_tis[zii][siis_now_am_sat] = np.zeros(siis_now_am_sat.size, int32) + zi + 1
        # keep *all* subhalos with a parent (if central, might be satellite at previous snapshot)
        sis_has_par, iis = ut.array.get_indices(
            subs[zi]['parent.index'], [1, Inf], sis, get_masks=True)
        siis_now = siis_now[iis]
        sis = subs[zi]['parent.index'][sis_has_par]  # shift to earlier snapshot
    # assign to subhalos at current snapshot
    inf_tis_tran = inf_tis.transpose()
    subs[ti_now][inf_name + '.ti'] = np.zeros(subs[ti_now][mass_kind].size, int32) - 1
    subs[ti_now][inf_name + '.ti'][sis_now] = ti_now + 1
    subs[ti_now][inf_name + '.index'] = ut.array.get_array_null(subs[ti_now][mass_kind].size)
    # just use maximum snapshot was a satellite
    # subs[ti_now][k][sis_now] = inf_tis_tran.max(1)
    # require stay satellite for >1 snapshot: move towards earliest snapshot & overwrite
    inf_tis_tran_sort = np.sort(inf_tis_tran, 1)
    for zii in range(min(4, ti_max - ti_now), -1, -1):
        siis_now = (inf_tis_tran_sort[:, -zii] == (inf_tis_tran_sort[:, -zii - 1] + 1))
        subs[ti_now][inf_name + '.ti'][sis_now[siis_now]] = inf_tis_tran_sort[:, -zii][siis_now]
    # identify subhalo index at first infall
    sis = sis_now.copy()
    siis_now = ut.array.get_arange(sis_now)  # index to point back to zi.now
    inf_tis_znow = subs[ti_now][inf_name + '.ti'][sis_now]
    for zi in range(ti_now, ti_max + 1):
        inf_tis = inf_tis_znow[siis_now]
        am_inf = (inf_tis == zi)
        subs[ti_now][inf_name + '.index'][sis_now[siis_now[am_inf]]] = sis[am_inf]
        # keep *all* subhalos with a parent (if central, might be satellite at previous snapshot)
        sis_has_par, iis = ut.array.get_indices(
            subs[zi]['parent.index'], [1, Inf], sis, get_masks=True)
        siis_now = siis_now[iis]
        sis = subs[zi]['parent.index'][sis_has_par]  # shift to earlier snapshot


def assign_first_infall(subs, ti_limits=[0, 34], mass_kind='mass.peak', mass_limits=[10.5, Inf]):
    '''
    Working forward in time from ti_limits[1], assign snapshot and subhalo index prior to first
    infall, child inherits value, imposing *no* mass growth cut on ejected satellites.
    Require to be satellite for two consecutive snapshots (except at final snapshot).

    Import subhalo catalog, snapshot range to assign, galaxy mass kind & range to assign to.
    '''
    inf_name = 'sat.first'
    inf_props = [inf_name + '.ti', inf_name + '.index']
    Say = ut.io.SayClass(assign_first_infall)

    for ti in range(ti_limits[0], ti_limits[1]):
        subs[ti][inf_name + '.ti'] = np.zeros(subs[ti][mass_kind].size, int32) - 1
        subs[ti][inf_name + '.index'] = ut.array.get_array_null(subs[ti][mass_kind].size)
    for ti in range(ti_limits[1] - 1, ti_limits[0] - 1, -1):
        Say.say('t_i = %d' % ti)
        sis_m = ut.array.get_indices(subs[ti][mass_kind], mass_limits)

        # inherit from parent, if possible
        if ti < ti_limits[1] - 1:
            # not have infall
            sis_no_inf = ut.array.get_indices(subs[ti][inf_name + '.ti'], [-Inf, 0.1], sis_m)
            # has parent
            sis_has_par = ut.array.get_indices(subs[ti]['parent.index'], [1, Inf], sis_no_inf)
            sis_par = subs[ti]['parent.index'][sis_has_par]  # index of parent
            sis_par_has_inf, iis = ut.array.get_indices(
                subs[ti + 1][inf_name + '.ti'], [1, Inf], sis_par, get_masks=True)
            sis_has_par = sis_has_par[iis]
            for inf_prop in inf_props:
                subs[ti][inf_prop][sis_has_par] = subs[ti + 1][inf_prop][sis_par_has_inf]
        else:
            sis_has_par = np.array([])

        # if still without defined infall, check if should assign
        # not have infall
        sis_no_inf = ut.array.get_indices(subs[ti][inf_name + '.ti'], [-Inf, 0.1], sis_m)
        sis_sat = ut.array.get_indices(subs[ti]['ilk'], [-2, 0.1], sis_no_inf)
        # is satellite in real halo
        sis_sat_has_par = ut.array.get_indices(subs[ti]['parent.index'], [1, Inf], sis_sat)
        sis_par = subs[ti]['parent.index'][sis_sat_has_par]  # index of parent
        # parent is central
        sis_par_cen_c, siis_par_cen_c = ut.catalog.get_indices_ilk(
            subs[ti + 1], 'central', sis_par, get_masks=True)
        sis_sat_has_par_c = sis_sat_has_par[siis_par_cen_c]
        # or parent is ilk = -3
        sis_par_cen_3, iis = ut.array.get_indices(subs[ti + 1]['ilk'], -3, sis_par, get_masks=True)
        sis_sat_has_par_3 = sis_sat_has_par[iis]
        sis_par_cen = np.union1d(sis_par_cen_c, sis_par_cen_3)
        sis_sat_has_par = np.union1d(sis_sat_has_par_c, sis_sat_has_par_3)

        if ti >= 2:
            # has child
            sis_sat_has_chi, iis = ut.array.get_indices(
                subs[ti]['child.index'], [1, Inf], sis_sat_has_par, get_masks=True)
            sis_par_cen = sis_par_cen[iis]
            sis_chi = subs[ti]['child.index'][sis_sat_has_chi]  # index of child
            # child is satellite in real halo
            indices = ut.array.get_indices(
                subs[ti - 1]['ilk'], [-2, 0.1], sis_chi, get_masks=True)[1]
            sis_sat_has_chi = sis_sat_has_chi[indices]
            sis_par_cen = sis_par_cen[indices]
        else:
            sis_sat_has_chi = sis_sat_has_par

        subs[ti][inf_name + '.ti'][sis_sat_has_chi] = ti + 1
        subs[ti][inf_name + '.index'][sis_sat_has_chi] = sis_par_cen
        Say.say('inherit values for %d, assign new values to %d' %
                (sis_has_par.size, sis_sat_has_chi.size))


def assign_dynfric_time_infall(subs, ti_now=1, ti_max=15, mass_kind='history', inf_kind='sat.first',
                               dynfric_fac=0.1):
    '''
    Assign dynamical friction lifetime to satellites at their time of infall.

    Import subhalo catalog, snapshot now & max to track infall, infall kind,
    dynamical friction prefactor.
    '''
    from . import satellite_lifetime as sl

    subs[ti_now]['dyn-fric.t'] = np.zeros(subs[ti_now][list(subs.keys())[0]].size, float32)
    if mass_kind == 'history':
        sis = subs[ti_now].sathistory['self.index']
    else:
        sis = None
    subs[ti_now]['dyn-fric.t'][sis] = sl.get_dynfric_time_infall(
        subs, ti_now, ti_max, sis, inf_kind, dynfric_fac)


def get_distance_cen_min(sub, hal, sis):
    '''
    Get minimum distance [kpc comoving] for each satellite, integrating orbit towards smaller
    distance across timestep length.

    Import subhalo and halo catalogs at snapshot, subhalo indices.
    '''
    Orbit = ut.orbit.OrbitClass(sub.Cosmology)
    HaloProperty = ut.halo_property.HaloPropertyClass(sub.Cosmology)
    sis = ut.array.arrayize(sis)
    halo_indices = sub['halo.index'][sis]
    c200cs = hal['c.200c'][halo_indices]
    m200cs = 10 ** hal['mass.200c'][halo_indices]
    # physical R_halo
    halo_radiuss = (HaloProperty.radius_virial_catalog('200c', hal, halo_indices) *
                    sub.snapshot['scalefactor'])
    cen_indices = sub['central.index'][sis]
    orb = ut.orbit.get_orbit_dictionary_catalog(sub, sis, cen_indices)
    # integrals of orbit
    # CHECK UNITS
    enes_kin = 0.5 * (orb['velocity.tot'] * ut.const.kpc_per_km / ut.const.Gyr_per_sec) ** 2
    enes_pot = Orbit.get_potential(orb['distance'], m200cs, halo_radiuss, c200cs)
    enes = enes_kin + enes_pot
    ang_moms = orb['distance'] * (orb['velocity.tan'] * ut.const.kpc_per_km / ut.const.Gyr_per_sec)
    distances_min = Orbit.dist_integrate(
        sub.info['time.width'], orb['distance'], enes, ang_moms, m200cs, halo_radiuss, c200cs)

    return distances_min / sub.snapshot['scalefactor']


def get_satellite_end(subs, hals, zi, si):
    '''
    Get snapshot when subhalo is last identified as satellite
    (-1 if always a satellite, -2 if ejected into a central).
    Get end kind (normal, different halo along the way, dissolve into nothing).
    ! PARALLELIZE THIS.

    Import subhalo & halo catalog, snapshot index, satellite index.
    '''
    end_kind = 'no-end'
    while zi > 0 and si > 0:
        chi_zi, chi_i = zi - 1, subs[zi]['child.index'][si]
        # child dissolves somehow, into nothing?
        if chi_i <= 0:
            return -1, 'dissolve'
        # check if remains in same halo
        if ut.catalog.is_in_same_halo(subs, hals, chi_zi, chi_i, zi, si):
            if subs[chi_zi]['central.distance'][chi_i] > subs[zi]['central.distance'][si] > 0:
                return (subs[zi]['central.distance'][si] * subs.snapshot['scalefactor'][zi],
                        'normal')
            elif subs[chi_zi]['ilk'][chi_i] >= 1:
                return -1, 'noperi'
        # is in different halo
        else:
            if subs[chi_zi]['ilk'][chi_i] >= 1:
                halo_i = subs[chi_zi]['halo.index'][chi_i]
                # child is ejected 'normally'
                if hals[chi_zi]['parent.index'][halo_i] <= 0:
                    return -1, 'eject'
                # child falls into large halo, merges immediately
                else:
                    return -1, 'different-merge'
            # remains satellite, flag as different halo
            else:
                end_kind = -1, 'different'
        zi, si = chi_zi, chi_i

    return -1, end_kind


def assign_central_distance_integrate(subs, hals, ti_now=1, ti_max=15, mass_kind='star.mass',
                                      mass_limits=[9.7, Inf]):
    '''
    For subhalos in mass range at ti_now, walk back history & assign minimum halo-centric distance
    [kpc comoving] via numerical integration at each snapshot.
    Keeps following back even if parent is a central (for ejected satellites).

    Import subhalo & halo catalog, snapshot range, mass kind & range.
    '''
    def is_sat_infalling(sub, sis):
        '''
        Get normalized direction vector of satellite from central.

        Import catalog of subhalo at snapshot, subhalo indices.
        '''
        sis = ut.array.arrayize(sis)
        cen_indices = sub['central.index'][sis]
        distances = ut.coordinate.get_distances(
            sub['position'][cen_indices], sub['position'][sis], 'vector', sub.info['box.length'])
        velocity_vectors = ut.catalog.get_velocity_differences(
            sub, sub, sis, cen_indices, 'vector', include_hubble_flow=True)
        distances_dot_velocitys = (distances * velocity_vectors).sum(1)
        return distances_dot_velocitys < 0

    distance_cen_int_name = 'central.distance.int'
    Say = ut.io.SayClass(assign_central_distance_integrate)
    # assign only to subhalos in mass range at ti.now
    sis = ut.array.get_indices(subs[ti_now][mass_kind], mass_limits)
    for ti in range(ti_now, ti_max + 1):
        sub = subs[ti]
        sub[distance_cen_int_name] = np.zeros(sub[list(sub.keys())[0]].size, float32) - 1
        # properties only defined if sat at this snapshot, select only those with a central
        sis_sat = ut.array.get_indices(sub['ilk'], [-1, 0.1], sis)
        Say.say('t_i = %d' % ti)
        # if current snapshot, assign current central.distance for those on infall orbit
        if ti == ti_now:
            am_inf = is_sat_infalling(sub, sis_sat)
            sis_inf = sis_sat[am_inf]
            sub[distance_cen_int_name][sis_inf] = sub['central.distance'][sis_inf]
            Say.say('assign %s = central.distance for %d on infalling orbit' %
                    (distance_cen_int_name, sis_inf.size))
            sis_sat = sis_sat[am_inf == False]
        # assign comoving central.distance.int
        sub[distance_cen_int_name][sis_sat] = get_distance_cen_min(
            subs[ti], hals[ti], sis_sat).astype(sub[distance_cen_int_name].dtype)
        Say.say('assign minimum central.distance.int via integration for %d' % sis_sat.size)
        # ensure central.distance.int < central.distance
        sis_fix = sis_sat[sub[distance_cen_int_name][sis_sat] > sub['central.distance'][sis_sat]]
        sub[distance_cen_int_name][sis_fix] = sub['central.distance'][sis_fix]
        Say.say('re-assign %s = central.distance for %d with min %s > central.distance' %
                (distance_cen_int_name, sis_fix.size, distance_cen_int_name))
        # ensure central.distance.int > resolution
        dist_res = 0.0025
        sub[distance_cen_int_name][sis_sat] = sub[distance_cen_int_name][sis_sat].clip(dist_res)
        # keep *all* subhalos with a parent (if central, might be sat at previous snapshot)
        sis_haspar = ut.array.get_indices(sub['parent.index'], [1, Inf], sis)
        sis = sub['parent.index'][sis_haspar]  # shift to earlier snapshot


def assign_density_sat(sub, hal, mass_kind='star.mass', mass_limits=[9.7, Inf]):
    '''
    Assign halo local density {M_sun / kpc physical ^ 3} from NFW to satellites.

    Import catalog of subhalo and halo at snapshot, mass kind and range.
    '''
    sub['density'] = np.zeros(sub[list(sub.keys())[0]].size, float32) - 1
    HaloProperty = ut.halo_property.HaloPropertyClass(sub.Cosmology)
    sis_m = ut.array.get_indices(sub[mass_kind], mass_limits)
    sis_sat = ut.array.get_indices(sub['ilk'], [-1, 0.1], sis_m)
    halo_is = sub['halo.index'][sis_sat]
    sub['density'][sis_sat] = HaloProperty.density(
        '200c', hal['mass.200c'][halo_is], hal['c.200c'][halo_is], sub['central.distance'][sis_sat],
        redshift=hal.snapshot['redshift']) - 3 * log10(sub.snapshot['scalefactor'])


def get_prop_extrema_sat(subs, hals=None, ti_now=1, ti_max=15, mass_kind='star.mass',
                         mass_limits=[9.7, Inf], disrupt_mf=0, virial_kind='200m'):
    '''
    For satellites + ejected satellites in mass range at ti_now, walk back histories,
    assign satellite dynamical properties.
    Keep following back even if parent is a central (for ejected satellites).

    Import subhalo [and halo] catalog, snapshot current and max to go back, mass kind and range,
    disrupt mass fraction, virial kind ('', fof, 200c/m).
    '''
    prop = {
        # if comment out, will not compute it
        'central.distance.min': [],  # central.distance (using central.distance.int) minimum
        # 'central.distance/Rneig.min': [],    # distance / R_halo (using distance.int) minimum
        # 'neig.5.distance.min': [],    # distance to nth nearest neighbor (physical) minimum
        # 'mass.frac.min': [],    # M_now / M_peak minimum
        # 'mass.tid.min': [] # tidal radius mass minimum
        # 'tidal.energy.star.m.max': [],    # change in internal energy maximum
        # 'tidal.energy.m.bound.max': [],    # change in internal energy maximum
        # 'tidal.energy.star.m.cum': [],    # change in internal energy maximum
        # 'tidal.energy.m.bound.cum': [],    # change in internal energy maximum
        # 'density.max': [],    # density in halo (from central.distance.int) maximum
        # 'gas.density.max': [],    # gas density in halo (using f_gas & distance.int) maximum
        # 'gas.density*vel2.max': [],    # gas density * velocity ^ 2 maximum
        # 'velocity.max': [],    # velocity wrt central (using central.distance.int) maximum
    }
    extrema_number = 5  # nth highest extrumum to keep that occurs prior to extremum
    mass_grow_max = 1.5

    Say = ut.io.SayClass(get_prop_extrema_sat)
    tis = np.arange(ti_now, ti_max + 1)
    Orbit = ut.orbit.OrbitClass(subs.Cosmology)
    HaloProperty = ut.halo_property.HaloPropertyClass(subs.Cosmology)
    # assign only to subhalos in mass ranges at ti_now
    sis_m = ut.catalog.get_indices_subhalo(
        subs[ti_now], mass_kind, mass_limits, disrupt_mf=disrupt_mf)
    sis_sat = ut.catalog.get_indices_ilk(subs[ti_now], 'satellite', sis_m)
    # add centrals that are ejected since ti_max
    sis_cen = get_indices_ejected(
        subs, ti_now, ti_max, mass_kind, mass_limits, 'central', disrupt_mf, mass_grow_max)
    sis = np.union1d(sis_sat, sis_cen)
    sis_znow = sis.copy()
    siis_znow = ut.array.get_arange(sis.size)
    siss_sat = ut.array.get_array_null((tis.size, sis.size))  # store indices of extreme values

    # initialize arrays for property dictionary
    for k in prop:
        prop[k] = np.zeros((tis.size, sis.size), float32)
        if '.min' in k:
            prop[k] += Inf

    for tii, ti in enumerate(tis):
        Say.say('t_i = %d' % ti)
        redshift = subs.snapshot['redshift'][ti]
        aexp = subs.snapshot['scalefactor'][ti]
        sub = subs[ti]
        hal = hals[ti]
        # properties only defined if sat at this snapshot, select only those with a central
        sis_am_sat, iis = ut.array.get_indices(sub['ilk'], [-1, 0.1], sis, get_masks=True)
        siis_am_sat = siis_znow[iis]
        cen_indices = sub['central.index'][sis_am_sat]
        if hals is not None:
            halo_is = sub['halo.index'][sis_am_sat]
            m200cs = hal['mass.200c'][halo_is]
            c200cs = hal['c.200c'][halo_is]
            # compile halo virial radii
            if virial_kind in ['200c', '200m']:
                vir_radiuss = HaloProperty.radius_virial_catalog(virial_kind, hals[ti], halo_is)
                rad_200cs = HaloProperty.radius_virial_catalog('200c', hals[ti], halo_is)

        # assign subhalo index at snapshot
        siss_sat[tii][siis_am_sat] = sis_am_sat
        # compute minimum central.distance / R_halo
        if 'central.distance.int' in sub:
            central_distance_mins = sub['central.distance.int'][sis_am_sat]
            if central_distance_mins.min() <= 0:
                Say.say('! %d satellites have central.distance.int <= 0' %
                        (central_distance_mins <= 0).sum())
        else:
            Say.say('subhalos not have ! no central.distance.int')

        k = 'central.distance.min'
        if k in prop:
            prop[k][tii][siis_am_sat] = central_distance_mins
        k = 'central.distance/Rneig.min'
        if k in prop:
            prop[k][tii][siis_am_sat] = central_distance_mins
            if virial_kind in ['200c', '200m']:
                prop[k][tii][siis_am_sat] /= vir_radiuss
        # compute bound mass
        k = 'mass.frac.min'
        if k in prop:
            prop[k][tii][siis_am_sat] = 10 ** (sub['mass.bound'][sis_am_sat] -
                                               sub['mass.peak'][sis_am_sat])
        #    prop[k][tii][siis_am_sat] = (
        #        central_distance_mins * 10 ** ((sub['mass.peak'][sis_am_sat] -
        # sub['halo.mass'][sis_am_sat]) / 6))
        k = 'tidal.energy.star.m.max'
        if k in prop:
            prop[k][tii][siis_am_sat] = sub['tidal.energy.star.m'][sis_am_sat]
        k = 'tidal.energy.star.m.cum'
        if k in prop:
            prop[k][tii][siis_am_sat] = sub['tidal.energy.star.m'][sis_am_sat]
        k = 'tidal.energy.m.bound.max'
        if k in prop:
            prop[k][tii][siis_am_sat] = sub['tidal.energy.m.bound'][sis_am_sat]
        k = 'tidal.energy.m.bound.cum'
        if k in prop:
            prop[k][tii][siis_am_sat] = sub['tidal.energy.m.bound'][sis_am_sat]
        k = 'neig.5.distance.min'
        if k in prop:
            prop[k][tii][siis_am_sat] = sub['neig.5.distance'][sis_am_sat]
        # compute physical local halo density {M_sun / (kpc physical) ^ 3}
        k = 'density.max'
        if k in prop:
            prop[k][tii][siis_am_sat] = HaloProperty.get_density_at_radius(
                '200c', m200cs, c200cs, central_distance_mins, redshift=redshift) - 3 * log10(aexp)
        k = 'gas.density.max'
        if k in prop:
            prop[k][tii][siis_am_sat] = (prop['density.max'][tii][siis_am_sat] +
                                         log10(HaloProperty.gas_fraction('200c', m200cs, c200cs,
                                                                         redshift)))
        # compute velocity, density * velocity^2
        if 'gas.density*velocity2.max' in prop or 'velocity.max' in prop:
            # physical velocity
            velocity2s = ut.catalog.get_velocity_differences(
                sub, sub, sis_am_sat, cen_indices, 'scalar', include_hubble_flow=True) ** 2
            # increase velocity ^ 2 given the potential at central.distance.int
            dvel2s = 2 * (Orbit.get_potential(sub['central.distance'][sis_am_sat] * aexp, 10 ** m200cs,
                                          rad_200cs * aexp, c200cs) -
                          Orbit.get_potential(central_distance_mins * aexp, 10 ** m200cs,
                                          rad_200cs * aexp, c200cs))
            velocity2s += dvel2s
            if 'gas.density*velocity2.max' in prop:
                prop['gas.den*vel2.max'][tii][siis_am_sat] = \
                    prop['gas.den.max'][tii][siis_am_sat] + log10(velocity2s)
            if 'velocity.max' in prop:
                prop['velocity.max'][tii][siis_am_sat] = 0.5 * log10(velocity2s)

        # keep *all* subhalos with a parent (if central, might be sat at previous snapshot)
        sis_haspar, iis = ut.array.get_indices(sub['parent.index'], [1, Inf], sis, get_masks=True)
        siis_znow = siis_znow[iis]
        sis = sub['parent.index'][sis_haspar]  # shift to earlier snapshot

    # assign extrema of props, including snapshot and index, to subhalos at current snapshot
    subhistory = {}
    subhistory['self.index'] = sis_znow
    subhistory['hist.index'] = ut.array.get_array_null(subs[ti_now][mass_kind].size)
    subhistory['hist.index'][sis_znow] = ut.array.get_arange(sis_znow)
    for k in prop:
        if '.cum' in k:
            prop[k] -= 0.0001  # penalize if stay at value, to find first crossing
            # neig cumulative value, starting at ti_max
            prop[k] = prop[k][::-1].cumsum(0)[::-1]
        prop_tran = prop[k].transpose()
        # initialize arrays for subhalos
        for kk in [k, k + '.ti', k + '.index']:
            subhistory[kk] = []
            if kk[-3] == '.ti' or kk[-2] == '.index':
                subhistory[kk] = ut.array.get_array_null((sis_znow.size, extrema_number))
            else:
                subhistory[kk] = np.zeros((sis_znow.size, extrema_number), float32)
                if '.min' in kk:
                    subhistory[kk] += Inf
        if '.min' in k:
            siss_sort = np.argsort(prop_tran)
        elif '.max' in k or '.cum' in k:
            siss_sort = np.argsort(prop_tran)[:, ::-1]

        # assign main extremum
        ziis_ext = siss_sort[:, 0]
        subhistory[k][:, 0] = prop[k][ziis_ext, ut.array.get_arange(ziis_ext)]
        subhistory[k + '.ti'][:, 0] = ziis_ext + ti_now
        subhistory[k + '.index'][:, 0] = siss_sat[ziis_ext, ut.array.get_arange(ziis_ext)]
        # assign ith extrema that occurred before main extremum
        for ii in range(prop_tran.shape[0]):
            ziis_ii = siss_sort[ii]
            for exti in range(1, extrema_number):
                ziis_ii = ziis_ii[ziis_ii > ziis_ii[0]]
                if ziis_ii.size and np.isfinite(prop[k][ziis_ii[0], ii]):
                    subhistory[k][ii][exti] = prop[k][ziis_ii[0], ii]
                    subhistory[k + '.ti'][ii][exti] = ziis_ii[0] + ti_now
                    subhistory[k + '.index'][ii][exti] = siss_sat[ziis_ii[0], ii]
                else:
                    break

    return subhistory


def assign_mass_frac_min_catalog(
    sub_200, sub_250, mass_peak_limits=[11.2, 14], mass_peak_width=0.2,
    hm_limits=[10, 15], hm_width=0.5, distance_cen_limits=[0.03, 2]):
    '''
    Transfer mass frac histories from L200 to L250 subhalos in bins of mass and distance.

    Import subhalo catalogs from L200 & L250, snapshot index, m_peak range and bin width,
    halo mass range & bin width, distance range.
    '''
    DistanceBin = ut.binning.BinClass(log10(distance_cen_limits), number=10)

    Say = ut.io.SayClass(assign_mass_frac_min_catalog)
    MPeakBin = ut.binning.BinClass(mass_peak_limits, mass_peak_width)
    MHaloBin = ut.binning.BinClass(hm_limits, hm_width)
    sis_200 = sub_200.sathistory['self.index']
    sis_250 = sub_250.sathistory['self.index']
    hist200_is = ut.array.get_array_null(sub_250.sathistory['self.index'].size)
    # satellites with defined distance
    sis_200_sat, siis_200_sat = ut.array.get_indices(
        sub_200['central.distance'], [1e-10, Inf], sis_200, get_masks=True)
    sis_250_sat, siis_250_sat = ut.array.get_indices(
        sub_250['central.distance'], [1e-10, Inf], sis_250, get_masks=True)
    mass_peak_bin_is = MPeakBin.get_bin_indices(sub_200['mass.peak'][sis_200_sat]) + 1
    hm_bin_is = MHaloBin.get_bin_indices(sub_200['halo.mass'][sis_200_sat]) + 1
    distance_bin_indices = (
        DistanceBin.get_bin_indices(log10(sub_200['central.distance'][sis_200_sat])) + 1)
    hist200_iss_in_bin = [[[[] for _ in range(DistanceBin.number + 2)] for _ in
                           range(MHaloBin.number + 2)] for _ in range(MPeakBin.number + 2)]
    for siis in range(sis_200_sat.size):
        m_bin = mass_peak_bin_is[siis]
        hist200_iss_in_bin[m_bin][hm_bin_is[siis]][distance_bin_indices[siis]].append(
            siis_200_sat[siis])

    # assign to sub_250
    mass_peak_bin_is = MPeakBin.get_bin_indices(sub_250['mass.peak'][sis_250_sat]) + 1
    hm_bin_is = MHaloBin.get_bin_indices(sub_250['halo.mass'][sis_250_sat]) + 1
    distance_bin_indices = \
        DistanceBin.get_bin_indices(log10(sub_250['central.distance'][sis_250_sat])) + 1
    random_number = 0
    for siis in range(sis_250_sat.size):
        hist200_is_in_bin = \
            hist200_iss_in_bin[mass_peak_bin_is[siis]][hm_bin_is[siis]][distance_bin_indices[siis]]
        if len(hist200_is_in_bin):
            hist200_i = hist200_is_in_bin[np.random.randint(0, len(hist200_is_in_bin))]
        else:
            # try one bin down in M_peak
            m_bin = mass_peak_bin_is[siis] - 1
            hist200_is_in_bin = \
                hist200_iss_in_bin[m_bin][hm_bin_is[siis]][distance_bin_indices[siis]]

            if len(hist200_is_in_bin):
                hist200_i = hist200_is_in_bin[np.random.randint(0, len(hist200_is_in_bin))]
            else:
                hist200_i = siis_200_sat[np.random.randint(0, len(siis_200_sat))]
                random_number += 1

        hist200_is[siis_250_sat[siis]] = hist200_i

    Say.say('%d sat have no corresponding bin - assign randomly' % random_number)
    # satellites without defined distance
    sis_200_sat, siis_200_sat = ut.array.get_indices(
        sub_200['central.distance'], -1, sis_200, get_masks=True)
    sis_250_sat, siis_250_sat = ut.array.get_indices(
        sub_250['central.distance'], -1, sis_250, get_masks=True)
    # assign to sub_250
    for siis in range(sis_250_sat.size):
        hist200_is[siis_250_sat[siis]] = siis_200_sat[np.random.randint(0, len(siis_200_sat))]
    # currently centrals
    sis_200_cen, siis_200_cen = ut.array.get_indices(
        sub_200['central.distance'], 0, sis_200, get_masks=True)
    sis_250_cen, siis_250_cen = ut.array.get_indices(
        sub_250['central.distance'], 0, sis_250, get_masks=True)
    mass_peak_bin_is = MPeakBin.get_bin_indices(sub_200['mass.peak'][sis_200_cen]) + 1
    hm_bin_is = MHaloBin.get_bin_indices(sub_200['halo.mass'][sis_200_cen]) + 1
    hist200_iss_in_bin = [[[] for _ in range(MHaloBin.number + 2)]
                          for _ in range(MPeakBin.number + 2)]

    for siis in range(sis_200_cen.size):
        hist200_iss_in_bin[mass_peak_bin_is[siis]][hm_bin_is[siis]].append(siis_200_cen[siis])
    # assign to sub_250
    mass_peak_bin_is = MPeakBin.get_bin_indices(sub_250['mass.peak'][sis_250_cen]) + 1
    hm_bin_is = MHaloBin.get_bin_indices(sub_250['halo.mass'][sis_250_cen]) + 1
    random_number = 0
    for siis in range(sis_250_cen.size):
        hist200_is_in_bin = hist200_iss_in_bin[mass_peak_bin_is[siis]][hm_bin_is[siis]]
        if len(hist200_is_in_bin):
            hist200_i = hist200_is_in_bin[np.random.randint(0, len(hist200_is_in_bin))]
        else:
            hist200_i = siis_200_cen[np.random.randint(0, len(siis_200_cen))]
            random_number += 1
        hist200_is[siis_250_cen[siis]] = hist200_i
    Say.say('%d cen have no corresponding bin - assign randomly' % random_number)
    # assign to history dictionary
    sub_250.sathistory['mass.frac.min'] = np.zeros(
        sub_250.sathistory['central.distance/Rneig.min'].shape, float32)
    sub_250.sathistory['mass.frac.min.zi'] = np.zeros(
        sub_250.sathistory['central.distance/Rneig.min'].shape, int32)
    sub_250.sathistory['mass.frac.min.index'] = np.zeros(
        sub_250.sathistory['central.distance/Rneig.min'].shape, int32)
    for siis in range(sub_250.sathistory['mass.frac.min'].shape[0]):
        sub_250.sathistory['mass.frac.min'][siis] = \
            sub_200.sathistory['mass.frac.min'][hist200_is[siis]]
        sub_250.sathistory['mass.frac.min.zi'][siis] = \
            sub_200.sathistory['mass.frac.min.zi'][hist200_is[siis]]
        sub_250.sathistory['mass.frac.min.index'][siis] = \
            sub_200.sathistory['mass.frac.min.index'][hist200_is[siis]]


# satellite catalog ----------
def get_catalog_satellite(
    sub, hal=None, gm_kind='mass.peak', gm_limits=[1, Inf], hm_limits=[-Inf, Inf],
    distance_halo_limits=None, dimension_number=3, virial_kind='', disrupt_mf=0,
    sis_ejected=None, sis=None):
    '''
    Compile dictionary of satellites.

    Import catalog of subhalo [& halo, if scale to R_halo] at snapshot, galaxy mass kind & range,
    halo mass range, satellite distance / R_halo range,
    number of dimensions for distance (if 0, not compute),
    halo virial kind ('' = no distance scaling, fof, 200c/m),
    disrupt mass fraction, indices of ejected satellites if want to include, prior indices.
    '''
    sat = ut.array.DictClass()
    Say = ut.io.SayClass(get_catalog_satellite)

    if gm_kind == 'history':
        # use neighbor indices from previously complied satellite history, including ejecteds
        gm_kind = 'star.mass'
        if 'sathistory' in sub.__dict__ and 'self.index' in sub.sathistory:
            sis_history = sub.sathistory['self.index']
            sis, sat_is = ut.array.get_indices(sub[gm_kind], gm_limits, sis_history, get_masks=True)
            sis, iis = ut.array.get_indices(sub['halo.mass'], hm_limits, sis, get_masks=True)
            sat_is = sat_is[iis]
            if sis_ejected is None:
                # excise ejected satellites that were compiled in history
                sis, iis = ut.catalog.get_indices_ilk(sub, 'satellite', sis, get_masks=True)
                sat_is = sat_is[iis]
            for k in sub.sathistory:
                if k[-4:] in ['.min', '.max', '.cum']:
                    # deal with multiplicity factor
                    sat[k] = sub.sathistory[k][sat_is % sub.sathistory[k][:, 0].size]
        else:
            raise ValueError('history dictionary not defined properly')
    else:
        sis = ut.catalog.get_indices_subhalo(
            sub, gm_kind, gm_limits, hm_limits, disrupt_mf=disrupt_mf)
        sis = ut.array.get_indices(sub['ilk'], [-9, 0.1], sis)  # select all satellites
        if sis_ejected is not None and not isinstance(sis_ejected, bool) and len(sis_ejected):
            # add centrals that are ejected satellites
            sis = np.union1d(sis, sis_ejected)

    # assign properties to satellite dictionary
    for k in list(sub.keys()):
        if len(sub[k]):
            sat[k] = sub[k][sis]
    if dimension_number in [1, 2]:
        # compute halo-centric distance [kpc comoving] given number of dimensions
        sat['central.distance'] = ut.coordinate.get_distances(
            'scalar', sub['position'][sis], sub['position'][sub['central.index'][sis]],
            sub.info['box.length'], list(range(dimension_number)))

    if virial_kind and dimension_number > 0:
        if 'central.distance/Rneig' in sub and len(sub['central.distance/Rneig']):
            pass
        else:
            # scale distance by R_halo
            if not hal:
                raise ValueError('cannot scale to R_halo, no halo catalog')
            if sis_ejected is not None:
                Say.say('! including ejected satellites, but they are not scaled by R_halo')
            his = sub['halo.index'][sis]
            if 'c.200c' in hal:
                sat['c.200c.ave'] = np.mean(hal['c.200c'][his])
            HaloProperty = ut.halo_property.HaloPropertyClass(
                sub.Cosmology, sub.snapshot['redshift'])
            vir_rads = HaloProperty.radius_virial_catalog(virial_kind, hal, his)
            sat['central.distance/Rneig'] = sat['central.distance'] / vir_rads

    if (distance_halo_limits is not None and len(distance_halo_limits) and
            (distance_halo_limits[0] > 0 or distance_halo_limits[1] < Inf)):
        # cut satellites on distance / R_halo limit
        sat_is, iis = ut.array.get_indices(
            sat['central.distance/Rneig'], distance_halo_limits, get_masks=True)
        sis = sis[iis]
        for k in sat:
            if not np.isscalar(sat[k]) and len(sat[k]):
                sat[k] = sat[k][sat_is]
        # print '# kept %5d of %5d (%.2f) %7s sats in r-range' % (
        #    sat['central.distance/Rneig'].size, sis.size,
        # sat['central.distance/Rneig'].size / sis.size, sub.info['catalog.kind'])
    else:
        sis = sis

    for infall_kind in ['sat.last', 'sat.first', 'ilk.dif']:
        if infall_kind + '.ti' in sub and len(sub[infall_kind + '.ti']):
            inf_tis = sub[infall_kind + '.ti'][sis]
            # uniformly distribute infall times betweqen snapshots, for matching qu fraction
            """
            sat[infall_kind + '.t'] = 0.5 * (sub.snapshots['time'][inf_tis] +
                                          sub.snapshots['time'][inf_tis - 1])
            """
            sat[infall_kind + '.t'] = np.random.uniform(
                sub.snapshots['time'][inf_tis - 1],
                sub.snapshots['time'][inf_tis], inf_tis.size).astype(float32)
            sat[infall_kind + '.z'] = 0.5 * (sub.snapshots['redshift'][inf_tis] +
                                             sub.snapshots['redshift'][inf_tis - 1])
        else:
            Say.say('%s.zi not in subhalo, so not assign to sat' % infall_kind)

    sat['self.index'] = sis
    sat['satellite.index'] = ut.array.get_array_null(sub[gm_kind].size)
    sat['satellite.index'][sis] = ut.array.get_arange(sis)

    sat.info = copy.copy(sub.info)
    sat.info['catalog.kind'] = sub.info['catalog.kind'] + '.sat'
    sat.info['halo.number'] = ut.catalog.get_indices_subhalo(
        sub, ilk='central', hal_mass_limits=hm_limits).size

    sat.snapshot = sub.snapshot
    sat.snapshots = sub.snapshots

    sat.Cosmology = sub.Cosmology

    return sat


def get_indices_ejected(
    subs, ti_now=1, ti_max=34, mass_kind='mass.peak', mass_limits=[1, Inf], ilk='central',
    disrupt_mf=0, mass_grow_max=1.5, sis=None):
    '''
    Get subhalo indices at ti_now that have been ejected.
    *** Cannot use inf.first for m.grow.max > 0 because this function used to assign inf.first.

    Parameters
    ----------
    subhalo catalog
    current & maximum snapshot to track ejection
    galaxy mass kind & range
    halo mass range
    ilk (all, cen, sat)
    disrupt mass fraction
    maximum M_peak(now) / M_peak(inf.dif) to include centrals as ejecteds
        (inf.first = use cut on inf.first, effectively = 1.5)
    '''
    sub = subs[ti_now]
    sis = ut.catalog.get_indices_subhalo(
        sub, mass_kind, mass_limits, disrupt_mf=disrupt_mf, indices=sis)
    # track back to one snapshot prior to zi.max because need transient check at prior shapshot
    sis = ut.array.get_indices(sub['ilk.dif.zi'], [ti_now + 1, ti_max - 1 + 0.1], sis)
    if not sis.size:
        return sis

    if ilk in ['satellite', 'all']:
        # only track satellites in a halo
        sis_sat = ut.array.get_indices(sub['ilk'], [-2, 0.1], sis)
        if sis_sat.size:
            sis_sat = sis_sat[sub['ilk.dif.zi'][sis_sat] == sub['sat.last.zi'][sis_sat]]
            sis_sat = sis_sat[sub['sat.first.zi'][sis_sat] > sub['ilk.dif.zi'][sis_sat]]
        if ilk == 'satellite':
            return sis_sat

    if ilk in ['central', 'all']:
        sis_cen = ut.catalog.get_indices_ilk(sub, 'central', sis)
        if sis_cen.size:
            if mass_grow_max == 'sat.first':
                # shortcut using first infall to avoid tracking full mass growth history
                # equates to using mgrow.max from first infall assignmant (1.5)
                sis_cen = sis_cen[sub['sat.first.zi'][sis_cen] > sub['ilk.dif.zi'][sis_cen]]
            elif mass_grow_max >= 1:
                infd_zibins = ut.array.get_arange(sub['ilk.dif.zi'][sis_cen].min(),
                                                     sub['ilk.dif.zi'][sis_cen].max() + 1)
                sis_keep = np.array([], np.int32)
                for infd_zi in infd_zibins:
                    sis_infd_zi = ut.array.get_indices(sub['ilk.dif.zi'], infd_zi, sis_cen)
                    infd_sis = sub['ilk.dif.index'][sis_infd_zi]
                    # enforce that was satellite in real halo
                    infd_sis, iis = ut.array.get_indices(
                        subs[infd_zi]['ilk'], [-2, 0.1], infd_sis, get_masks=True)
                    sis_infd_zi = sis_infd_zi[iis]
                    # enforce that satellite parent was satellite (not transient fly-by)
                    infd_sis, iis = ut.array.get_indices(
                        subs[infd_zi]['parent.index'], [1, Inf], infd_sis, get_masks=True)
                    sis_infd_zi = sis_infd_zi[iis]
                    par_sis = subs[infd_zi]['parent.index'][infd_sis]
                    # not include satellites with no halo
                    par_sis, siis = ut.array.get_indices(
                        subs[infd_zi + 1]['ilk'], [-2, 0.1], par_sis, get_masks=True)
                    infd_sis = infd_sis[siis]
                    sis_infd_zi = sis_infd_zi[siis]
                    sis_infd_zi = sis_infd_zi[
                        sub['mass.peak'][sis_infd_zi] < subs[infd_zi]['mass.peak'][infd_sis] +
                        log10(mass_grow_max)]
                    sis_keep = np.append(sis_keep, sis_infd_zi)
                sis_cen = np.sort(sis_keep)
            else:
                raise ValueError('m-grow.max = %s not valid' % mass_grow_max)

        if ilk == 'central':
            return sis_cen

    if ilk == 'all':
        sis_all = np.union1d(sis_cen, sis_sat)
        return sis_all


def get_central_ejected_catalog(
    subs, ti_now=1, ti_max=34, mass_kind='mass.peak', mass_limits=[10.8, Inf], disrupt_mf=0.007,
    mass_grow_max=1.5, sis=None):
    '''
    Get catalog of central subhalo indices at ti_now, M_peak(now) / M_peak(inf.dif),
    index of & distance to subhalo at ti_now that is child of central from which ejected.
    '''
    ilk = 'central'

    eje = ut.array.DictClass()
    eje['self.index'] = get_indices_ejected(
        subs, ti_now, ti_max, mass_kind, mass_limits, ilk, disrupt_mf, mass_grow_max, sis=sis)
    eje['mass.grow'] = np.zeros(eje['self.index'].size, float32) - 1
    eje['central.distance'] = np.zeros(eje['self.index'].size, float32) - 1
    eje['central.index'] = ut.array.get_array_null(eje['self.index'].size, eje['self.index'].dtype)
    sub = subs[ti_now]
    infd_zibins = ut.array.get_arange(sub['ilk.dif.zi'][eje['self.index']].min(),
                                         sub['ilk.dif.zi'][eje['self.index']].max() + 1)
    for infd_zi in infd_zibins:
        # get indices to indices at zi.now of those that last were satellite at zi
        siis_infd = ut.array.get_indices(sub['ilk.dif.zi'][eje['self.index']], infd_zi)
        sis_infd = eje['self.index'][siis_infd]  # indices at zi.now
        infd_sis = sub['ilk.dif.index'][sis_infd]  # indices at zi
        # compute mass growth
        # eje['halo.m.infd'][siis_infd] = subs[infd_zi]['halo.mass'][infd_sis]
        # eje['mass.peak.infd'][siis_infd] = subs[infd_zi]['mass.peak'][infd_sis]
        eje['mass.grow'][siis_infd] = 10 ** (sub['halo.mass'][sis_infd] -
                                             subs[infd_zi]['mass.peak'][infd_sis])
        # get central subhalo of host halo just before ejection
        infd_sis_cen = subs[infd_zi]['central.index'][infd_sis]
        masks = ut.array.arrayize(infd_sis_cen > 0)
        infd_sis_cen = infd_sis_cen[masks]
        siis_infd = siis_infd[masks]
        # get central host's child at zi.now
        sis_cen_host_infdz = ut.array.arrayize(
            ut.catalog.get_indices_tree(subs, infd_zi, ti_now, infd_sis_cen))
        masks = ut.array.arrayize(sis_cen_host_infdz > 0)
        sis_cen_host_infdz = sis_cen_host_infdz[masks]
        siis_infd = siis_infd[masks]
        # get its central at zi.now (in case became a satellite)
        masks = (sub['central.index'][sis_cen_host_infdz] > 0)
        sis_cen_host_infdz = sub['central.index'][sis_cen_host_infdz[masks]]
        siis_infd = siis_infd[masks]
        eje['central.index'][siis_infd] = sis_cen_host_infdz
        eje['central.distance'][siis_infd] = ut.coordinate.get_distances(
            'scalar', sub['position'][eje['self.index'][siis_infd]],
            sub['position'][eje['central.index'][siis_infd]], sub.info['box.length'])
    eje.info = sub.info
    eje.info['catalog.kind'] = sub.info['catalog.kind'] + '.ejected'
    eje.snapshot = sub.snapshot
    eje.Cosmology = sub.Cosmology

    return eje


#===================================================================================================
# properties v halo-centric distance
#===================================================================================================
# satellite number density v halo-centric distance ----------
def plot_numdensity_v_distance_halo_sat(
    subs, hal=None, gm_kind='mass.peak', gm_limits=[12, Inf],
    hm_kind='halo.mass', hm_limits=[12.0, 12.5], disrupt_mf=0,
    distance_scaling='log', distance_limits=[0.02, 1.0], distance_bin_number=30, dimension_number=3,
    virial_kind='vir', prop_vary='galaxy', prop_width=1,
    plot_prop='density-per-host', axis_y_limits=[], eje=None, plot_ratio=False, plot_nfw=False):
    '''
    Plot number density of satellites v halo-centric distance.

    Import catalog of subhalo & halo at snapshot, galaxy mass kind & range, halo mass kind & range,
    distruption mass fraction, distance scaling & range & number of bins,
    number of spatial dimensionsm, virial kind, property to vary (m.frac.min, sat, halo, prop).
    '''
    plot_prop_err = plot_prop.strip('log ') + '.err'
    DistanceBin = ut.binning.DistanceBinClass(
        distance_scaling, distance_limits, number=distance_bin_number,
        dimension_number=dimension_number)

    if prop_vary in ['galaxy', 'halo']:
        # vary satellite or halo m
        GHMassBin = ut.binning.GalaxyHaloMassBinClass(
            gm_kind, gm_limits, prop_width, hm_kind, hm_limits, prop_width, prop_vary)
        prop_bins = GHMassBin.mins
    else:
        gm_bin_limits, hm_bin_limits = gm_limits, hm_limits
        prop_bins = [0]

    if prop_vary == 'mass.frac.min':
        # vary disruption mass fraction
        prop_bins = [0.0, 0.007]  # , 0.03, 0.05, 0.10]
    else:
        thresh = disrupt_mf

    # cut on median infall time
    if prop_vary in ['sat.last.zi', 'ilk.dif.zi', 'sat.first.zi']:
        sis = ut.catalog.get_indices_subhalo(subs, gm_kind, gm_limits, hm_limits, 'satellite')
        props = subs[prop_vary][sis]
        prop_bins = [[0, Inf], [0, np.median(props) - 1], [np.median(props), Inf]]
        print('# median prop %.1f' % np.median(props))
    else:
        sis_p = None

    # satellite profile
    pro = {}
    if eje is not None:
        pro_cen_ej = {}
    for pi in range(len(prop_bins)):
        if prop_vary in ['galaxy', 'halo']:
            gm_bin_limits, hm_bin_limits = GHMassBin.bins_limits(pi)
        elif prop_vary == 'mass.frac.min':
            thresh = prop_bins[pi]
        elif prop_vary in ['sat.last.zi', 'ilk.dif.zi', 'sat.first.zi']:
            sis_p = ut.array.get_indices(subs[prop_vary], [prop_bins[pi][0], prop_bins[pi][1]])
        sat = get_catalog_satellite(
            subs, hal, gm_kind, gm_bin_limits, hm_bin_limits, [0, Inf], dimension_number,
            virial_kind, thresh, sis_p)
        halo_number = ut.catalog.get_indices_subhalo(
            subs, hm_limits=hm_limits, ilk='central', disrupt_mf=0).size
        # restrict to satellites within virial radius
        # sat_is = ut.array.get_indices(sat['central.distance/Rneig'], [0, 1])
        sat_is = ut.array.get_arange(sat['central.distance/Rneig'])
        pro_prop = DistanceBin.get_sum_profile(sat['central.distance/Rneig'][sat_is])
        pro_prop['density-per-host'] = pro_prop['density'] / halo_number
        pro_prop['density-per-host.err'] = pro_prop['density.err'] / halo_number
        ut.array.append_dictionary(pro, pro_prop)
        if pi == 0 and plot_nfw:
            HaloProperty = ut.halo_property.HaloPropertyClass(
                subs.Cosmology, subs.snapshot['redshift'])
            concen_vir = HaloProperty.convert_concentration(virial_kind, '200c', sat['c.200c.ave'])
            concen_vir = 4
            pro_nfw = HaloProperty.get_density_v_radius(virial_kind, concen_vir, DistanceBin)
        if eje is not None:
            sis = eje['self.index']
            sis, siis = ut.array.get_indices(subs[gm_kind], gm_bin_limits, sis, get_masks=True)
            siis = ut.array.get_indices(eje['mass.grow'], [0, 1.5], siis)
            cen_is = eje['central.index'][siis]
            siis = ut.array.get_indices(subs[hm_kind][cen_is], hm_bin_limits)
            cen_is = cen_is[siis]
            sis = sis[siis]
            halo_is = subs['halo.index'][cen_is]
            HaloProperty = ut.halo_property.HaloPropertyClass(subs.Cosmology)
            vir_radiuss = HaloProperty.radius_virial_catalog(virial_kind, hal, halo_is)
            distances_vir = eje['central.distance'][siis] / vir_radiuss
            # restrict to ejecteds beyond virial radius
            distances_vir = distances_vir[distances_vir > 1]
            # add satellites beyond virial radius
            sat_is = ut.array.get_indices(sat['central.distance/Rneig'], [1, Inf])
            distances_vir = np.concatenate((distances_vir, sat['central.distance/Rneig'][sat_is]))
            pro_prop = DistanceBin.get_sum_profile(distances_vir)
            pro_prop['density-per-host'] = pro_prop['density'] / halo_number
            pro_prop['density-per-host.err'] = pro_prop['density.err'] / halo_number
            ut.array.append_dictionary(pro_cen_ej, pro_prop)

    # plot ----------
    if plot_ratio:
        panel_number_y = -2
    else:
        panel_number_y = 1
    if not axis_y_limits:
        axis_y_limits = pro['log ' + plot_prop]
    Plot = plot_sm.PlotClass(panel_number_y=panel_number_y)
    Plot.set_axis('log', 'log', DistanceBin.log_limits, axis_y_limits, tick_label_kind=[0, 0, 1, 1],
                  extra=0.05)
    Plot.set_axis_label(
        ut.plot.get_label_distance(
            'central.distance/Rneig', None, virial_kind, dimension_number), 'dN_{sat}/dV')
    if plot_ratio:
        Plot.make_panel(panel_grid=[1, -5, 1, 2, 1, 5])
    else:
        Plot.make_window()

    Plot.set_label(0.05, 0.5)
    Plot.make_label('z=%.2f' % subs.snapshot['redshift'])
    # Plot.make_label(gm_kind, gm_limits)
    Plot.make_label('halo.mass', hm_limits)
    for pi in range(len(prop_bins)):
        Plot.draw('c', pro['distance'][pi], pro['log ' + plot_prop][pi], pro[plot_prop_err][pi],
                  pro[plot_prop][pi], ct='blue', lt=pi)
        if prop_vary == 'galaxy':
            Plot.make_label(gm_kind, [prop_bins[pi], prop_bins[pi] + prop_width])
        elif prop_vary == 'halo':
            Plot.make_label('halo.mass', [prop_bins[pi], prop_bins[pi] + prop_width])
        elif prop_vary == 'mass.frac.min':
            if prop_bins[pi] == 0:
                label = 'no f_{max}'
            else:
                label = 'f_{max}>%.2f' % prop_bins[pi]
            Plot.make_label(label)
        elif prop_vary in ['sat.last.zi', 'ilk.dif.zi', 'sat.first.zi']:
            if pi == 0:
                label = 'all sats'
            if pi == 1:
                label = 't_{inf}<%.1f\,Gyr' % (subs.snapshots['time'][np.median(props)])
            if pi == 2:
                label = 't_{inf}>%.1f\,Gyr' % (subs.snapshots['time'][np.median(props)])
            Plot.make_label(label)
        if eje is not None:
            Plot.draw('c', pro_cen_ej['distance'][pi], pro_cen_ej['log ' + plot_prop][pi],
                      pro_cen_ej[plot_prop_err][pi], pro_cen_ej[plot_prop][pi], ct='green', lt=pi)
    if plot_nfw:
        Plot.draw('c', pro_nfw['radius'], pro_nfw['density'] - 1, ct='red', lt='.')
        Plot.make_label('NFW')
    if plot_ratio:
        Plot.pan_grid = [1, -5, 1, 1, 1, 1]
        Plot.ykind = 'linear'
        Plot.tic_loc = [-1, 10, 0.1, 0.5]
        Plot.tic_labkind = [-1, 10, 0, 0]
        # Plot.make_panel()
        Plot.set_axis('log', 'linear', DistanceBin.log_limits, [0, 1.25])
        Plot.set_axis_label(
            ut.plot.get_label_distance(
                'central.distance/Rneig', None, virial_kind, dimension_number), 'ratio')
        Plot.make_window(erase=None)
        for pi in range(1, len(prop_bins)):
            Plot.draw('c', pro['distance'][0], pro[plot_prop][pi] / pro[plot_prop][0],
                      draw_if=pro[plot_prop][pi], ct='blue', lt=pi)


def plot_numdensities_v_distance_halo_sat(
    subs, hals=None, gm_kind='mass.peak', gm_lims=[0, 10], hm_lims=[12.0, 12.5], disrupt_mfs=0,
    distance_scaling='log', distance_limits=[0.05, 1], distance_bin_number=20, dimension_number=3,
    virial_kind='vir', plot_prop='den-per-host', axis_y_limits=[], plot_nfw=False):
    '''
    Plot satellite number density v halo-centric distance, for various catalogs.

    Import subhalo & halo catalogs at snapshots, galaxy mass kind & ranges, halo mass ranges,
    disruption mass fraction, distance scaling & range & number of bins, number of dimensions,
    virial kind.
    '''
    DistanceBin = ut.binning.DistanceBinClass(
        distance_scaling, distance_limits, distance_bin_number, dimension_number=dimension_number)

    if gm_kind in subs:
        subs = [subs]
        hals = [hals]
    if np.isscalar(gm_lims[0]):
        gm_lims = [gm_lims]
        hm_lims = [hm_lims]
    disrupt_mfs = ut.array.arrayize(disrupt_mfs, repeat_number=len(subs))
    pro = {}
    if plot_nfw:
        pro_nfw = {}
    for sub_i in range(len(subs)):
        pro_sub = {}
        for mi in range(len(gm_lims)):
            sat = get_catalog_satellite(
                subs[sub_i], hals[sub_i], gm_kind, gm_lims[mi], hm_lims[mi], distance_limits,
                dimension_number, virial_kind, disrupt_mfs[sub_i])
            halo_number = ut.catalog.get_indices_subhalo(
                subs[sub_i], hal_mass_limits=hm_lims[mi], ilk='central', disrupt_mf=0).size
            pro_m = DistanceBin.get_sum_profile(sat['central.distance/Rneig'])
            pro_m['density-per-host'] = pro_m['density'] / halo_number
            pro_m['density-per-host.err'] = pro_m['density.err'] / halo_number
            if plot_nfw and mi == 0:
                HaloProperty = ut.halo_property.HaloPropertyClass(
                    subs[sub_i].Cosmology, subs[sub_i].snapshot['redshift'])
                concen_vir = HaloProperty.convert_concentration(
                    virial_kind, '200c', sat['c.200c.ave'])
                pro_nfw_sub = HaloProperty.get_density_v_radius(virial_kind, concen_vir, DistanceBin)
            ut.array.append_dictionary(pro_sub, pro_m)
        ut.array.append_dictionary(pro, pro_sub)
        if plot_nfw:
            ut.array.append_dictionary(pro_nfw, pro_nfw_sub)

    # plot ----------
    Plot = plot_sm.PlotClass()
    if not axis_y_limits:
        axis_y_limits = pro['log ' + plot_prop][0][0]
    Plot.set_axis('log', 'log', DistanceBin.log_limits, axis_y_limits, tick_label_kind=[0, 0, 1, 1],
                  extra=0.05)
    Plot.set_axis_label(
        ut.plot.get_label_distance('central.distance/Rneig', None, virial_kind, dimension_number),
        'dN_{sat}/dV')
    Plot.make_window()
    Plot.set_label(0.6)
    Plot.set_point(color_number=len(subs), line_number=len(gm_lims))
    for sub_i in range(len(subs)):
        Plot.make_label(subs[sub_i].info['pair.kind'], point_kind='c', ct=sub_i)
    for sub_i in range(len(subs)):
        for mi in range(len(gm_lims)):
            # if mi == 0:
            #    Plot.make_label('z=%.1f' % subs[sub_i].snapshot['redshift'], 'c', ct=sub_i, lt='-')
            Plot.draw('c', pro['distance'][sub_i][mi], pro['log ' + plot_prop][sub_i][mi],
                      pro[plot_prop + '.err'][sub_i][mi], pro['log ' + plot_prop][sub_i][mi],
                      ct=sub_i, lt=mi)
            if sub_i == 0:
                Plot.make_label(gm_kind, gm_lims[mi], ct='def', lt=mi)
                # if hm_lims[mi]:
                #    Plot.make_label('halo.mass', hm_lims[mi])
        if plot_nfw:
            # plot NFW
            Plot.draw('c', pro_nfw['radius'][sub_i], pro_nfw['log density'][sub_i],
                      draw_if=pro_nfw['density'][sub_i], lt='.')
            # Plot.make_label('NFW')
    if len(subs) > 1:
        for mi in range(len(gm_lims)):
            print(pro[plot_prop][1][mi] / pro[plot_prop][0][mi])
            print(np.mean(pro[plot_prop][1][mi] / pro[plot_prop][0][mi]))


# prop extremum v prop now ----------
def plot_property_extreme_v_property_now_sat(
    sub, gm_kind='history', gm_limits=[9.7, Inf], hm_limits=[12, Inf],
    prop='central.distance/Rneig'):
    '''
    Plot satellite property extrema v property now.
    *** assign_density_sat()

    Import subhalo catalog, snapshot index, galaxy mass kind & range, halo mass range, ilk,
    disrupt mass fraction, plot kind.
    '''
    dimension_number = 3

    sat = get_catalog_satellite(sub, None, gm_kind, gm_limits, hm_limits)
    if gm_kind == 'history':
        gm_kind = 'star.mass'
    for exttype in ['.min', '.max']:
        if prop + exttype in sat:
            prop_ext = prop + exttype
    if prop_ext is None:
        raise ValueError('no extrema for prop = %s' % prop)
    if prop == 'density':
        axis_x_limits = [1.5, 7.5]
        axis_x_width = 0.5
        # convert so M_sun / kpc ^ 3
        units = -3 * dimension_number + log10(sub.Cosmology['hubble']) * dimension_number
    y_values = sat[prop_ext][:, 0]
    x_values = sat[prop]
    # for those that experience extremum since last snapshot, assign that value to now
    ext_tis = sat[prop_ext + '.ti'][:, 0]
    iis = ut.array.get_arange(x_values)[ext_tis == sub.snapshot['index']]
    x_values[iis] = y_values[iis]
    y_values += units
    x_values += units
    PBin = ut.binning.BinClass(axis_x_limits, axis_x_width)
    binstat = PBin.get_statistics_of_array(x_values, y_values)

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'log', axis_x_limits, [axis_x_limits[0], axis_x_limits[1] + 1],
                  tick_label_kind='log')
    Plot.set_axis_label('Current density [M_\odot\,kpc^{-3}]', 'Max density [M_\odot\,kpc^{-3}]')
    Plot.make_window()
    Plot.set_label(0.6, 0.25)
    Plot.make_label(gm_kind, gm_limits)
    Plot.make_label('halo.mass', hm_limits)
    Plot.fill(PBin.mids, [binstat['percent.2'], binstat['percent.98']], ct='blue.lite2')
    Plot.fill(PBin.mids, [binstat['percent.16'], binstat['percent.84']], ct='blue.lite')
    Plot.draw('c', PBin.mids, binstat['median'], ct='blue.dark')
    Plot.draw('c', [-10, 10], [-10, 10], ct='def', lt='.')
    Plot.make_window(erase=False)


def plot_at_extreme_frac_sat(
    sub, hal=None, gm_kind='history', gm_limits=[9.7, 11.3], gm_width=0.2,
    hm_limits=[12, 15], hm_width=1, vary_kind='galaxy', prop='central.distance/Rneig'):
    '''
    Plot fraction of current satellites that are at (or just experienced since last snapshot) the
    extremum value of prop.

    Import subhalo & halo catalog, snapshot index, galaxy mass kind & limit & bin width,
    halo mass range & bin width, which mass kind to vary, property to compute extrumum of.
    '''
    evoldict = {
        # 'mass.frac.min': {'label': 'min M_{now}/M_{max}'},
        # 'mass.tid.min': {'label': 'min M_{tid}/M_{max}'},
        # 'vel.circ.frac.min': {'label': 'min V_{now}/V_{max}'},
        'central.distance/Rneig.min': {'label': 'min d/R_{200m}'},
        'neig.5.distance.min': {'label': 'min d_{5th\,neig}'},
        'tidal.energy.star.m.max': {'label': 'max E_{tid}'},
        'tidal.energy.m.bound.max': {'label': 'max E_{tid}'},
        'density.max': {'label': 'max \\rho_{m}'},
        'gas.density.max': {'label': 'max \\rho_{gas}'},
        'gas.density.vel2.max': {'label': 'max \\rho_{gas}\,v^2'},
        'velocity.max': {'label': 'max velocity'},
    }
    extreme_tolerance = 0.1
    Say = ut.io.SayClass(plot_at_extreme_frac_sat)
    sat = get_catalog_satellite(sub, hal, gm_kind, gm_limits, hm_limits, virial_kind='200m')
    if gm_kind == 'history':
        gm_kind = 'star.mass'
    for ext_kind in ['.min', '.max']:
        if prop + ext_kind in sat:
            prop_ext = prop + ext_kind

    if prop_ext is None:
        raise ValueError('no extrema for prop = %s' % prop)
    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    Fraction = ut.math.FractionClass([GHMassBin.fix.number, GHMassBin.vary.number], 'beta')
    for fmi in range(GHMassBin.fix.number):
        sat_is_mfix = ut.array.get_indices(
            sat[GHMassBin.fix.mass_kind], GHMassBin.fix.get_bin_limits(fmi))
        for vmi in range(GHMassBin.vary.number):
            sat_is_mfix_mv = ut.array.get_indices(
                sat[GHMassBin.vary.mass_kind], GHMassBin.vary.get_bin_limits(vmi), sat_is_mfix)
            # test against values at o_now
            vals_ext = sat[prop_ext][:, 0][sat_is_mfix_mv]
            vals_now = sat[prop][sat_is_mfix_mv]
            if '.max' in prop_ext:
                at_extreme_number = (vals_now >= vals_ext * (1 - extreme_tolerance)).sum()
            elif '.min' in prop_ext:
                at_extreme_number = (vals_now <= vals_ext * (1 + extreme_tolerance)).sum()
            # test against whether o_ext == o_now
            # at_extreme_number = (sat[prop_ext + '.ti'][:, 0][sat_is_mfix_mv] == zi).sum()
            Fraction.assign_to_dict([fmi, vmi], at_extreme_number, sat_is_mfix_mv.size)
    Say.say('overall frac = %.2f' % (np.sum(Fraction.numberer) / np.sum(Fraction.denom)))

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', GHMassBin.vary.limits, [0, 1])
    Plot.set_axis_label(GHMassBin.vary.mass_kind, 'Fraction at ' + evoldict[prop_ext]['label'])
    Plot.make_window()
    Plot.set_point(color_number=GHMassBin.fix.number)
    imvs = np.arange(-1, GHMassBin.vary.number - 1).clip(0)
    for fmi in range(GHMassBin.fix.number):
        Plot.fill_err(GHMassBin.vary.mids, Fraction['value'][fmi], Fraction['error'][fmi],
                      draw_if=((Fraction['error'][fmi][0] < 0.05) * Fraction['value'][fmi] *
                               (Fraction['value'][fmi] >= Fraction['value'][fmi][imvs])),
                      ct=fmi - 0.1)
        Plot.draw('c', GHMassBin.vary.mids, Fraction['value'][fmi],
                  draw_if=((Fraction['error'][fmi][0] < 0.05) * Fraction['value'][fmi] *
                           (Fraction['value'][fmi] >= Fraction['value'][fmi][imvs])), ct=fmi)
        Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.get_bin_limits(fmi))


# sat property v radius ----------
def plot_property_v_distance_sat(
    subs, hals, tis=1, gm_kind='star.mass', gm_limits=[9.7, Inf], gm_width=None,
    hm_limits=[12, 15], hm_width=1, vary_kind='halo', disrupt_mf=0,
    prop_x='central.distance/Rneig', prop_y='sat.first.t',
    distance_halo_limits=[0.04, 1], distance_halo_bin_number=11, dimension_number=2,
    virial_kind='200m', scale_tdyn='', draw_vir_rad=False):
    '''
    Plot satellite median property v distance to central (or vice versa).

    Import subhalo & halo catalog, snapshot index, galaxy mass kind & range & bin width,
    halo mass range & bin width, which mass to vary, disrupt mass fraction,
    x-axis property, y-axis property,
    distance / R_halo range & bin number, number of spatial dimensions (for projection),
    virial kind (fof, 200c/m), whether to plot halo R_halo growth.
    '''
    if not tis:
        tis = np.arange(1, 21)  # for distance/Rneig v inf.first.t or computing halo R_halo growth
    if np.isscalar(tis):
        tis = [tis]

    Say = ut.io.SayClass(plot_property_v_distance_sat)
    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        'star.mass', gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    DistanceBin = ut.binning.DistanceBinClass(
        'log', distance_halo_limits, distance_halo_bin_number, dimension_number=dimension_number)
    DistanceBin_massive = ut.binning.DistanceBinClass(
        'log', distance_halo_limits, distance_halo_bin_number - 4,
        dimension_number=dimension_number)
    DistanceBin_lin = ut.binning.BinClass(DistanceBin.log_limits, DistanceBin.log_width)
    DistanceBin_lin_massive = ut.binning.BinClass(
        DistanceBin_massive.log_limits, DistanceBin_massive.log_width)

    Prop = ut.statistic.StatisticClass()

    for zi in tis:
        sat = get_catalog_satellite(
            subs[zi], hals[zi], gm_kind, gm_limits, hm_limits, DistanceBin.limits, dimension_number,
            virial_kind, disrupt_mf)

        if gm_kind == 'history':
            gm_kind = 'star.mass'
            for k in list(sat.keys()):
                if np.ndim(sat[k]) > 1:
                    sat[k] = sat[k][:, 0]
                    if k == 'central.distance/Rneig.min':
                        sat[k] = log10(sat[k])
        if '.t' in prop_y:
            sat[prop_y] = sat.snapshot['time'] - sat[prop_y]
            if scale_tdyn:
                HaloProperty = ut.halo_property.HaloPropertyClass(subs.Cosmology)
                dyn_ts = HaloProperty.get_dynamical_time(virial_kind, subs.snapshot['redshift'])

                if scale_tdyn == 'now':
                    sat[prop_y] /= dyn_ts[zi]
                elif scale_tdyn == 'inf':
                    sat[prop_y] /= dyn_ts[sat[prop_y.replace('.t', '.ti')] - 1]

        sat['central.distance/Rneig'] = log10(sat['central.distance/Rneig'])

        # compile statistics at each snapshot
        PropZ = ut.statistic.StatisticClass()
        for vmi in range(GHMassBin.vary.number):
            his = ut.array.get_indices(
                sat[GHMassBin.vary.mass_kind], GHMassBin.vary.get_bin_limits(vmi))
            PropMass = ut.statistic.StatisticClass()
            if prop_x == 'central.distance/Rneig':
                if zi > 8 and GHMassBin.vary.get_bin_limits(vmi)[0] >= 14:
                    PropMass.stat = DistanceBin_lin_massive.get_statistics_of_array(
                        sat[prop_x][his], sat[prop_y][his])
                else:
                    PropMass.stat = DistanceBin_lin.get_statistics_of_array(
                        sat[prop_x][his], sat[prop_y][his])
            else:
                for ti_2 in tis:
                    ti_range = ti_2
                    if ti_2 > 9:
                        ti_range = [ti_2 - 1, ti_2 + 1.1]
                    sis_zi = ut.array.get_indices(sat[prop_x.replace('.t', '.ti')], ti_range, his)
                    PropMass.append_to_dictionary(sat['central.distance/Rneig'][sis_zi])
                PropMass.stat['x'] = (
                    subs.snapshot['time'][zi] -
                    0.5 * (subs.snapshot['time'][tis] + subs.snapshot['time'][tis - 1]))
            ut.array.append_dictionary(PropZ.stat, PropMass.stat)
            Say.say('satellite number in x-bin % s' % PropZ.stat['number'][vmi])
        ut.array.append_dictionary(Prop.stat, PropZ.stat)
        if prop_x != 'central.distance/Rneig':
            break

    # compute R_halo growth over time
    if prop_y == 'central.distance/Rneig' and draw_vir_rad:
        HaloProperty = ut.halo_property.HaloPropertyClass(hals.Cosmology)
        vir_radiuss = np.zeros(tis.size)
        tsinfs = hals.snapshot['time'][tis[0]] - hals.snapshot['time'][tis]
        rad_vir_difs = []
        for hmi in range(GHMassBin.hal.number):
            his = ut.array.get_indices(hals[tis[0]]['mass.fof'], GHMassBin.hal.get_bin_limits(hmi))
            for zii, zi in enumerate(tis):
                vir_radiuss[zii] = HaloProperty.get_radius_virial(
                    virial_kind, np.median(hals[zi]['mass.fof'][his]),
                    redshift=hals.snapshot['redshift'][zi]) * hals.snapshot['scalefactor'][zi]
                # keep halos with parent
                sis_haspar = ut.array.get_indices(hals[zi]['parent.index'], [1, Inf], his)
                his = hals[zi]['parent.index'][sis_haspar]  # shift to earlier snapshot
            rad_vir_difs.append(vir_radiuss / vir_radiuss[0])
    # neighbor middle m_vary bin (for drawing scatter)
    immid = int(round(GHMassBin.vary.number / 2 - 0.5))

    # plot ----------
    Plot = plot_sm.PlotClass()
    #Plot.set_label(pos_y=0.32)
    Plot.set_label(position_y=0.27)
    if prop_x == 'central.distance/Rneig':
        Plot.axis.space_x = 'log'
        Plot.axis.lim_x = DistanceBin.log_limits
        Plot.axis.lab_x = ut.plot.get_label_distance(
            'central.distance/Rneig', None, virial_kind, dimension_number)
        if 'sat.' in prop_y:
            if prop_y == 'sat.first.t':
                inf_name = 'first'
            elif prop_y == 'sat.last.t':
                prop_y = 'sat.last.t'
                inf_name = 'recent'
            Plot.axis.space_y = 'linear'

            if scale_tdyn == 'now':
                Plot.axis.lim_y = [0, 1.7]  # {t / t_dyn}
                Plot.axis.lab_y = 't_{since\,inf} / t_{dyn}(z)'
            elif scale_tdyn == 'inf':
                y_max = np.ceil(np.max(Prop.stat['percent.84'][0][immid]))
                if y_max - np.max(Prop.stat['percent.84'][0][immid]) > 0.6:
                    y_max -= 0.5
                Plot.axis.lim_y = [0, y_max]  # {t / t_dyn}
                Plot.axis.lab_y = 't_{since\,inf} / t_{dyn}(z_{inf})'
                Plot.set_label(0.41, 0.88)
            else:
                #y_max = np.ceil(np.max(Prop.stat['percent.84'][0][immid]))
                y_max = 8.3
                if y_max - np.max(Prop.stat['percent.84'][0][immid]) > 0.5:
                    y_max -= 0.5
                Plot.axis.lim_y = [0, y_max]  # [Gyr]
                Plot.axis.lab_y = 'Time\,since\,%s\,infall\,[Gyr]' % inf_name

            if scale_tdyn:
                if scale_tdyn == 'now':
                    # right y-axis variables
                    if tis[0] > 14:
                        time_width = 0.5
                    else:
                        time_width = 1
                    lab2_values = np.arange(0, 10, time_width)
                    ts_lab2 = lab2_values / dyn_ts[tis[0]]
                    Plot.axis.lab_y2 = 'Time\,since\,%s\,infall\,[Gyr]' % inf_name
            #else:
                # right y-axis variables
                #lab2_values = np.array([0.1, 0.5, 1, 1.5, 2, 2.5, 3.0])
                #ts_lab2 = [subs.snapshot['time'][tis[0]] -
                #subs.Cosmology.get_time(lab2_values[i])
                #           for i in range(lab2_values.size)]
                #Plot.axis.lab_y2 = 'Redshift of %s infall' % inf_name
        elif prop_y == 'central.distance/Rneig.min':
            Plot.axis.lim_y = log10(np.array([0.007, 1]))
            Plot.axis.space_y = 'log'
            Plot.axis.lab_y = (
                'minimum\,' +
                ut.plot.get_label_distance(
                    'central.distance/Rneig', None, virial_kind, dimension_number))
            Plot.set_label(0.45)
        elif prop_y == 'den.max':
            Plot.axis.lim_y = [12.0, 17.0]
            Plot.axis.space_y = 'log'
            Plot.axis.lab_y = 'Density [M_\odot/kpc^3]'
        elif prop_y == 'neig.5.distance.min':
            Plot.axis.lim_y = [0, 1.5]
            Plot.axis.space_y = 'linear'
            Plot.axis.lab_y = 'd_{neig,5} [kpc]'
    elif 'inf' in prop_x:
        if prop_x == 'sat.first.t':
            inf_name = 'first'
        elif prop_x == 'sat.last.t':
            inf_name = 'recent'
        tis = np.arange(tis[0] + 1, tis[-1] + 1)
        Plot.set_axis('linear', 'log', [0, 9], log10(np.array([0.1, 1])))
        Plot.axis.lab_x = 'Time since %s infall [Gyr]' % inf_name
        Plot.axis.lab_y = ut.plot.get_label_distance(
            'central.distance/Rneig', None, virial_kind, dimension_number)
    else:
        raise ValueError('not recognize prop_x = %s' % prop_x)

    # plot ----------
    Plot.set_axis()
    if prop_x == 'central.distance/Rneig' and 'sat.' in prop_y and scale_tdyn != 'inf':
        Plot.set_axis_2('y', Plot.axis.lim_y, ts_lab2, [], lab2_values)
    Plot.make_window()
    #Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.limits)
    #if GHMassBin.vary.number == 1:
    #    Plot.make_label(GHMassBin.vary.mass_kind, GHMassBin.vary.limits)
    # draw scatter for middle m_vary bin
    #Plot.fill(Prop.stat['x'][0][immid],
    #          [Prop.stat['percent.16'][0][immid], Prop.stat['percent.84'][0][immid]],
    #ct='grey.lite2')
    line_number = max(len(tis), GHMassBin.vary.number)
    Plot.set_point(color_number=line_number, line_number=line_number)
    vmis = list(range(GHMassBin.vary.number))
    if 'sat.' in prop_y:
        vmis.reverse()

    for zii, zi in enumerate(tis):
        for vmi in vmis:
            if GHMassBin.vary.number >= len(tis):
                lt = vmi
            else:
                lt = zii
            Plot.draw('c', Prop.stat['x'][zii][vmi], Prop.stat['median'][zii][vmi],
                      draw_if=Prop.stat['median'][zii][vmi], ct=lt, lt=lt)
            if GHMassBin.vary.number > 1 and zii == 0:
                Plot.make_label(GHMassBin.vary.mass_kind, GHMassBin.vary.get_bin_limits(vmi))
            elif len(tis) > 1 and vmi == 0:
                if zii == 1:
                    label = 'z=%.1f' % (subs.snapshot['redshift'][zi] - 0.01)
                else:
                    label = 'z=%.1f' % (subs.snapshot['redshift'][zi] + 0.0158)
                Plot.make_label(label)
    if prop_y == 'central.distance/Rneig' and draw_vir_rad:
        for hmi in range(GHMassBin.hal.number):
            Plot.draw('c', tsinfs, log10(rad_vir_difs[hmi]), ct=hmi, lt='.')
    if prop_y == 'central.distance/Rneig.min':
        Plot.draw('c', [-2, 2], [-2, 2], ct='def', lt='.')
    Plot.make_window(erase=False)

    if len(tis) == 1:
        zi = tis[0]
        Plot.set_label(position_x=0.75, position_y=0.9)
        if zi == 1:
            label = 'z=%.2f' % subs.snapshot['redshift'][zi]
        else:
            label = 'z=%.1f' % (subs.snapshot['redshift'][zi] + 0.0158)
        Plot.make_label(label, point_kind='')


def plot_mass_frac_v_dist_vir_sat(
    sub, hal, gm_kind='mass.peak', gm_limits=[11.5, 13.5], gm_width=0.5,
    hm_limits=[14.5, 15.5], hm_width=1, vary_kind='galaxy', disrupt_mf=0, dimension_number=3,
    distance_bin_limits=[0.05, 2], distance_bin_number=7,
    stat_kind='median', axis_y_limits=[0, 0.6], virial_kind='200m'):
    '''
    Plot fraction of mass / vel.circ.max retained v halo-centric distance.

    Import catalog of subhalo & halo at snapshot, galaxy mass kind & range & bin width,
    halo mass range & bin width, which mass to vary, disruption mass fraction,
    number of spatial dimensions (for projection),
    distance limit & number of bins, statistic to plot, virial kind to scale distances to.
    '''
    mass_kind = 'mass.bound'
    bin_number_min = 4

    DistanceBin = ut.binning.DistanceBinClass(
        'log', distance_bin_limits, distance_bin_number, dimension_number=dimension_number)
    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    m_frac_stat = {}
    # m_frac_stat_anal = {}
    for vmi in range(GHMassBin.vary.number):
        gm_bin_limits, hm_bin_limits = GHMassBin.bins_limits(vmi)
        sat = get_catalog_satellite(
            sub, hal, gm_kind, gm_bin_limits, hm_bin_limits, DistanceBin.limits, dimension_number,
            virial_kind, disrupt_mf)
        m_fracs = 10 ** (sat[mass_kind] - sat[mass_kind + '.max'])
        if gm_kind == 'history':
            sub_is = sat['self.index']
            hist_is = sub.sathistory['self.index'][sub_is]
            m_fracs = sub.sathistory['mass.frac.min'][hist_is, 0]
        stats_m = DistanceBin.get_statistics_of_array(sat['central.distance/Rneig'], m_fracs)
        ut.array.append_dictionary(m_frac_stat, stats_m)
        # m_fracs_anal = 0.6 * 10 ** ((sat['mass.peak'] -
        #sat['halo.mass']) * 1 / 6) * sat['central.distance/Rneig']
        # m_fracs_anal = (10 ** ((12 - sat['mass.peak']) / 6) *
        #10 ** ((12 - sat['halo.mass']) / 6) *  sat['central.distance/Rneig'])
        # mchar = 12
        # m_fracs_anal = sat['central.distance/Rneig'] * (10 ** (0.3 * (mchar - sat['mass.peak'])) *
        # 10 ** (0.14 * (mchar - sat['halo.mass'])))
        # ut.array.append_dictionary(m_frac_stat_anal,
        # DistanceBin.get_statistics_of_array(sat['central.distance/Rneig'], m_fracs_anal))

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', DistanceBin.log_limits, axis_y_limits)
    Plot.set_axis_label(
        ut.plot.get_label_distance('central.distance/Rneig', None, virial_kind, dimension_number),
        '%s M/M_{max}' % stat_kind)
    Plot.make_window()
    Plot.set_point(color_number=GHMassBin.vary.number)
    Plot.make_label('z=%.2f' % sub.snapshot['redshift'])
    Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.limits)
    for vmi in range(GHMassBin.vary.number):
        Plot.draw('c', DistanceBin.log_mids, m_frac_stat[stat_kind][vmi],
                  draw_if=(m_frac_stat['number'][vmi] > bin_number_min), ct=vmi, lt='-')
        # Plot.draw('c', DistanceBin.log_mids, m_frac_stat_anal[stat_kind][vmi], lt='.')
        Plot.make_label(GHMassBin.vary.mass_kind, GHMassBin.vary.get_bin_limits(vmi), lt='-')
        print(m_frac_stat['number'][vmi])


def plot_property_v_distance_halo_neig(
    sub, hal, gm_kind='star.mass', gm_limits=[9.7, 10.9], gm_width=0.4, ilks='each', disrupt_mf=0,
    hm_limits=[14, 15], distance_halo_limits=[0, 5], distance_halo_width=1 / 3, dimension_number=3,
    virial_kind='200m', neig_number_max=1200, prop='halo.mass', stat='median'):
    '''
    Plot median property of neighbors v halo-centric distance.

    Import subhalo & halo catalog, snapshot index, galaxy mass kind & range & bin width, ilks[s],
    disrupt mass fraction, halo mass range, neighbor distance / R_halo limit & bin width,
    virial kind, max neighbor count in finder, neighbor property.
    '''
    space = 'real'

    if ilks == 'each':
        ilks = ['all', 'central', 'satellite']
    ilks = ut.array.arrayize(ilks)
    Say = ut.io.SayClass(plot_property_v_distance_halo_neig)
    HaloProperty = ut.halo_property.HaloPropertyClass(sub.Cosmology)
    GMBin = ut.binning.BinClass(gm_limits, gm_width)
    DistanceBin = ut.binning.BinClass(distance_halo_limits, distance_halo_width)
    # neig all centrals in halo mass range, regardless of central properties
    sis_cen = ut.catalog.get_indices_ilk(sub, 'central')
    sis_cen_hm = ut.array.get_indices(sub['halo.mass'], hm_limits, sis_cen)
    halo_is = sub['halo.index'][sis_cen_hm]
    rad_virs = HaloProperty.radius_virial_catalog(virial_kind, hal, halo_is)
    Say.say('halo number = %d' % sis_cen_hm.size)
    Say.say('R_halo max = %.2f kpc: find neighbors out to %.1f kpc' %
            (rad_virs.max(), rad_virs.max() * distance_halo_limits[1]))
    neig = ut.catalog.get_catalog_neighbor(
        sub, neig_gm_kind=gm_kind, neig_gm_limits=gm_limits, neig_hm_limits=None, neig_ilk='all',
        disrupt_mf=disrupt_mf, neig_number_max=neig_number_max,
        neig_distance_max=rad_virs.max() * distance_halo_limits[1], distance_space=space,
        center_indices=sis_cen_hm)
    prop_stat = {'all': {}, 'central': {}, 'satellite': {}}
    Statistic = ut.statistic.StatisticClass()
    for hi in range(sis_cen_hm.size):
        neig['distances'][hi] /= rad_virs[hi]
    neig_sis, neig_dists = np.concatenate(neig['indices']), np.concatenate(neig['distances'])
    for gmi in range(GMBin.number):
        neig_is_gm, iis = ut.array.get_indices(
            sub[gm_kind], GMBin.get_bin_limits(gmi), neig_sis, get_masks=True)
        neig_dists_gm = neig_dists[iis]
        neig_prop_gm = {'all': {}, 'central': {}, 'satellite': {}}
        for dist_i in range(DistanceBin.number):
            neig_sis_gm_d = {}
            neig_sis_gm_d['all'] = neig_is_gm[
                ut.array.get_indices(
                    neig_dists_gm, rad_virs[hi] * np.array(DistanceBin.get_bin_limits(dist_i)))]
            neig_sis_gm_d['satellite'] = ut.catalog.get_indices_ilk(
                sub, 'satellite', neig_sis_gm_d['all'])
            neig_sis_gm_d['central'] = ut.catalog.get_indices_ilk(
                sub, 'central', neig_sis_gm_d['all'])
            for ilk in ilks:
                ut.array.append_dictionary(
                    neig_prop_gm[ilk], Statistic.get_statistic_dict(sub[prop][neig_sis_gm_d[ilk]]))
        for ilk in ilks:
            ut.array.append_dictionary(prop_stat[ilk], neig_prop_gm[ilk])

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('linear', 'linear', distance_halo_limits, [11.5, 13.2])  # [13, 14.5])
    Plot.set_axis_label(
        ut.plot.get_label_distance('central.distance/Rneig', None, virial_kind, dimension_number),
        'log\,M_{halo} [M_\odot]')
    Plot.make_window()
    Plot.set_point(color_number=GMBin.number, line_number=GMBin.number)
    Plot.set_label(line_length=1.3)
    Plot.make_label('halo.mass', hm_limits, position_x=0.45, small=1)
    # for gmi in reversed(range(GMBin.number)):
    #    for ilk in ilks:
    #        Plot.fill_err(DistanceBin.mids, prop_stat[ilk][stat][gmi], prop_stat[ilk][''][gmi]
    # draw_if=prop_stat[ilk][stat][gmi],ct=gmi - 0.2)
    for gmi in reversed(list(range(GMBin.number))):
        for ilk in ilks:
            lt = gmi
            Plot.draw('c', DistanceBin.mids, prop_stat[ilk][stat][gmi],
                      draw_if=prop_stat[ilk][stat][gmi], ct=gmi, lt=lt)
    for gmi in reversed(list(range(GMBin.number))):
        lt = gmi
        Plot.make_label(gm_kind, GMBin.get_bin_limits(gmi), small=2, ct=gmi, lt=lt)
    Plot.make_window(erase=False)
    Plot.draw('c', [1, 1], [-1, 20], lt='--')


# satellite minimum distance ----------
def plot_distance_min_distribution_sat(
    sub, hal, gm_kind='history', gm_limits=[9.7, Inf], gm_width=None,
    hm_limits=[12, 15], hm_width=1, vary_kind='halo', disrupt_mf=0,
    distance_kind='central.distance/Rneig.min', distance_limits=[0.01, 1], distance_bin_number=30,
    distr_kind='prob.cum'):
    '''
    Plot distribution of minimum of central distance / R_halo.

    Import catalog of subhalo & halo at snapshot, galaxy mass kind & range & bin width,
    halo mass range & bin width, which mass to vary, disrupt mass fraction,
    current distance / R_halo limit & number of bins,
    distribution statistic to plot (prob, prob.cum).
    '''
    dimension_number = 3
    virial_kind = '200m'
    dist_max = distance_limits[1] + 0.1

    DistanceBin = ut.binning.DistanceBinClass(
        'log', distance_limits, distance_bin_number, dimension_number=dimension_number)
    sat = get_catalog_satellite(sub, hal, gm_kind, gm_limits, [1, Inf], virial_kind=virial_kind,
                                disrupt_mf=disrupt_mf, sis_ejected=True)
    if gm_kind == 'history':
        gm_kind = 'star.mass'
    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    DistMin = ut.statistic.StatisticClass()
    DistNow = ut.statistic.StatisticClass()
    DistMinRvir = ut.statistic.StatisticClass()
    DistMinEj = ut.statistic.StatisticClass()
    for vmi in range(GHMassBin.vary.number):
        # sats within a halo
        gm_bin_limits, hm_bin_limits = GHMassBin.bins_limits(vmi)
        sat_is_gm = ut.array.get_indices(sat[gm_kind], gm_bin_limits)
        sat_is_hm = ut.array.get_indices(sat['halo.mass'], hm_bin_limits, sat_is_gm)
        sat_is = ut.array.get_indices(sat['central.distance/Rneig'], [1e-10, dist_max], sat_is_hm)
        sat_is = ut.array.get_indices(sat[distance_kind][:, 0], [1e-10, dist_max], sat_is)
        DistMin.append_to_dictionary(
            log10(sat[distance_kind][sat_is, 0]), DistanceBin.log_limits,
            bin_number=DistanceBin.number)
        DistNow.append_to_dictionary(
            log10(sat['central.distance/Rneig'][sat_is]), DistanceBin.log_limits,
            bin_number=DistanceBin.number)
        # satellites near R_halo
        sat_is = ut.array.get_indices(sat['central.distance/Rneig'], [0.9, dist_max], sat_is_hm)
        sat_is = ut.array.get_indices(sat[distance_kind][:, 0], [1e-10, 0.9], sat_is)
        DistMinRvir.append_to_dictionary(
            log10(sat[distance_kind][sat_is, 0]), DistanceBin.log_limits,
            bin_number=DistanceBin.number)
        # centrals that are ejected satellites
        sat_is = ut.array.get_indices(sat['central.distance/Rcen'], 0, sat_is_gm)
        sat_is = ut.array.get_indices(sat['nearest.mass'], hm_bin_limits, sat_is)
        sat_is = ut.array.get_indices(sat[distance_kind][:, 0], [1e-10, dist_max], sat_is)
        DistMinEj.append_to_dictionary(
            log10(sat[distance_kind][sat_is, 0]), DistanceBin.log_limits,
            bin_number=DistanceBin.number)

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', DistanceBin.log_limits, [0, 1])
    Plot.set_axis_label(
        'minimum ' + ut.plot.get_label_distance('central.distance/Rneig', None, virial_kind,
                                                dimension_number),
        'P(< %s)' % ut.plot.get_label_distance('central.distance/Rneig', None, virial_kind,
                                               dimension_number))
    Plot.make_window()
    Plot.set_point(color_number=GHMassBin.vary.number, line_number=GHMassBin.vary.number)
    Plot.set_label(line_length=1.3)

    Plot.make_label('ejected\,satellite', ct='def')
    Plot.make_label('satellite\,(<R_{%s})' % virial_kind, ct='def')

    Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.limits)
    if GHMassBin.vary.number == 1:
        Plot.make_label(GHMassBin.vary.mass_kind, GHMassBin.vary.get_bin_limits(vmi))
    for vmi in range(GHMassBin.vary.number):
        #Plot.draw('c', DistNow.distr['bin.mid'][vmi], DistNow.distr[distr_kind][vmi], lt='.')
        Plot.draw('c', DistMin.distr['bin.mid'][vmi], DistMin.distr[distr_kind][vmi], ct=vmi,
                  lt=vmi, lw=3.5)
        #Plot.draw('c', DistMinRvir.distr['bin.mid'][vmi], DistMinRvir.distr[distr_kind][vmi],
        #ct=vmi, lt='.')
        Plot.draw('c', DistMinEj.distr['bin.mid'][vmi], DistMinEj.distr[distr_kind][vmi], ct=vmi,
                  lt=vmi, lw=7.5)
        if GHMassBin.vary.number > 1:
            Plot.make_label(GHMassBin.vary.mass_kind, GHMassBin.vary.get_bin_limits(vmi),
                            point_kind='c', lw=4.5)
    # Plot.make_label('satellite [0.9,1]R_{%s}' % virial_kind, point_kind='c', ct='def', lt='.')
    # Plot.make_label('minimum', point_kind='c', ct='def', lt='-')
    # Plot.make_label('current', point_kind='c', ct='def', lt='--')


# ram pressure v radius ----------
def plot_ram_pressue_profile(
    hal, mass_limits=[12, 13, 14, 15], mass_width=0.5, distance_limits=[0.05, 1],
    distance_bin_number=10, axis_kind='central.distance/Rneig', virial_kind='200c'):
    '''
    Plot density_nfw * velocity ^ 2 v axis_kind.

    Import catalog of halo at snapshot, mass range & bin width, distance/R_halo limit & bin counts,
    what to plot on x-axis (m, r), virial kind (200c/m).
    '''
    dimension_number = 3

    Orbit = ut.orbit.OrbitClass(hal.Cosmology)
    if '200' in virial_kind:
        mass_kind = 'mass.200c'
    if axis_kind == 'm':
        distance_bin_number = len(distance_limits) - 1
    elif axis_kind == 'central.distance/Rneig':
        mass_width = mass_limits[1] - mass_limits[0]

    HaloProperty = ut.halo_property.HaloPropertyClass(
        hal.Cosmology, redshift=hal.snapshot['redshift'])
    DistanceBin = ut.binning.DistanceBinClass(
        'log', distance_limits, distance_bin_number, dimension_number=3)
    masses = ut.array.get_arange_safe(mass_limits, mass_width, True)
    rads = np.append(DistanceBin.mins, distance_limits[1])
    Plot = plot_sm.PlotClass()
    if axis_kind == 'm':
        xbins = masses
        xlim = mass_limits
        Plot.axis.lab_x = 'M_{halo} [M_\odot]'
    elif axis_kind == 'central.distance/Rneig':
        xbins = np.append(DistanceBin.log_mins, log10(distance_limits[1]))
        xlim = log10(distance_limits)
        Plot.axis.lab_x = ut.plot.get_label_distance(
            'central.distance/Rneig', None, virial_kind, dimension_number)
    denpots = np.zeros((masses.size, rads.size), float32)
    denvel2s = np.zeros((masses.size, rads.size), float32)
    gas_fracs = 10 ** ((masses - masses[-1]) * 0.2)
    for mi in range(masses.size):
        concen = np.mean(hal['c.200c'][
            ut.array.get_indices(
                hal[mass_kind], [masses[mi] - 0.5 * mass_width, masses[mi] + 0.5 * mass_width])])
        concen = HaloProperty.convert_concentration(virial_kind, '200c', concen)
        rad_vir = HaloProperty.get_radius_virial(virial_kind, masses[mi])
        den_rs = HaloProperty.density(
            virial_kind, masses[mi], concen, rads * rad_vir) * gas_fracs[mi]
        pot_rs = Orbit.get_potential(rads * rad_vir * hal.snapshot['scalefactor'], 10 ** masses[mi],
                                 rad_vir * hal.snapshot['scalefactor'], concen)
        mint_rs = HaloProperty.get_mass_within_radius(virial_kind, masses[mi], concen, rads * rad_vir)
        v2_r = Orbit.grav_sim_units * mint_rs / (rads * rad_vir * hal.snapshot['scalefactor'])
        denpots[mi] = log10(-pot_rs * den_rs)
        denvel2s[mi] = log10(v2_r * den_rs)

    # plot ----------
    Plot.set_axis('log', 'log', xlim, [denpots, denvel2s])
    Plot.set_axis_label('', 'ram-pressure')
    Plot.make_window()
    if axis_kind == 'm':
        denpots = denpots.transpose()
        denvel2s = denvel2s.transpose()
    elif axis_kind == 'central.distance/Rneig':
        Plot.set_label(position_y=0.3)
    for pi in range(denpots.shape[0] - 1, -1, -1):
        Plot.draw('c', xbins, denpots[pi], ct=pi, lt='-')
        Plot.draw('c', xbins, denvel2s[pi], ct=pi, lt='--')
        if axis_kind == 'm':
            Plot.make_label('%s=%.1f' %
                            (ut.plot.get_label_distance('central.distance/Rneig', None, virial_kind),
                             rads[pi]))
        elif axis_kind == 'central.distance/Rneig':
            Plot.make_label('log\,M_{halo}=%.1f' % masses[pi], lt='-')
            Plot.draw('c', [-2, 2], [denpots[-1][-1], denpots[-1][-1]], ct='def', lt='-')


# satellite neighbor ----------
def plot_distance_neig_v_distance_neig(
    sub, gm_kind='star.mass', gm_limits=[9.7, Inf], hm_limits=[12, Inf], ilk='satellite',
    disrupt_mf=0, neig_mass_ratio_min=0.25, neig_number=5, neig_velocity_dif_max=1000,
    neig_number_max=200, neig_distance_max=1.5, plot_kind='phys_v_proj'):
    '''
    Plot distance to nth nearest neighbor above mass ratio, physical v projected.
    *** Need to run:  pickle_prop(subs, 'star.mass')

    Import catalog of subhalo at snapshot, galaxy mass kind & range, centers ilk, disrupt threshold,
    neighbor mass ratio minimum, maximum distance to nth [kpc physical],
    maximum velocity difference [km / s] (for projected distance), maximum number & distance.
    '''
    neig_distance_width = 0.06  # [kpc comoving]
    neig_distance_name = 'neig.%d.distance' % neig_number

    neig_distance_limits = np.array([0, neig_distance_max])
    plot_kinds = plot_kind.split('_v_')
    plot_kinds.reverse()
    if 'proj' in plot_kinds:
        proj_to_phys_fac = 2.4
    else:
        proj_to_phys_fac = 1

    neig_log_mrat_min = log10(neig_mass_ratio_min)
    neig_m_limits = [gm_limits[0] + neig_log_mrat_min, Inf]
    axis_lims = [[], []]
    axis_labs = [[], []]
    neig_distss = [[], []]
    if 'min' in plot_kinds:
        # minimum physical neighbor distance
        sat = get_catalog_satellite(sub, None, 'history', gm_limits, hm_limits, sis_ejected=True)
        sat_is = sat['self.index']
        neig_distss[plot_kinds.index('min')] = sat[neig_distance_name + '.min'][:, 0]
        axis_lims[plot_kinds.index('min')] = proj_to_phys_fac * neig_distance_limits
        axis_labs[plot_kinds.index('min')] = 'min d_{neig,%d} [kpc comoving]' % neig_number
    else:
        sat_is = None

    if 'physical' in plot_kinds:
        # physical neighbor distance
        neig = ut.catalog.get_catalog_neighbor(
            sub, gm_kind, gm_limits, hm_limits, ilk, gm_kind, neig_m_limits, disrupt_mf=disrupt_mf,
            neig_number_max=neig_number_max, neig_distance_max=proj_to_phys_fac * neig_distance_max,
            distance_space='real', center_indices=sat_is)
        assign_neighbor_distance(sub, neig, gm_kind, neig_mass_ratio_min, neig_number)
        neig_distss[plot_kinds.index('phys')] = sub[neig_distance_name][neig['self.index']]
        axis_lims[plot_kinds.index('phys')] = proj_to_phys_fac * neig_distance_limits
        axis_labs[plot_kinds.index('phys')] = 'physical d_%d [kpc comoving]' % neig_number

    if 'proj' in plot_kinds:
        # projected neighbor distance
        neig = ut.catalog.get_catalog_neighbor(
            sub, gm_kind, gm_limits, hm_limits, ilk, gm_kind, neig_m_limits, disrupt_mf=disrupt_mf,
            neig_number_max=neig_number_max, neig_distance_max=neig_distance_max,
            distance_space='proj', neig_velocity_dif_maxs=neig_velocity_dif_max,
            center_indices=sat_is)
        assign_neighbor_distance(sub, neig, gm_kind, neig_mass_ratio_min, neig_number)
        neig_distss[plot_kinds.index('proj')] = sub[neig_distance_name][neig['self.index']]
        axis_lims[plot_kinds.index('proj')] = neig_distance_limits
        axis_labs[plot_kinds.index('proj')] = ('projected d_{neig,%d}(z=0) [kpc comoving]' %
                                               neig_number)
    # bin statistics
    DistanceBin = ut.binning.BinClass(axis_lims[0], neig_distance_width)
    binstat = DistanceBin.get_statistics_of_array(neig_distss[0], neig_distss[1])

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('linear', 'linear', axis_lims[0], axis_lims[1])
    Plot.set_axis_label(axis_labs[0], axis_labs[1])
    Plot.make_window()
    Plot.fill(DistanceBin.mids, [binstat['percent.2'], binstat['percent.98']],
              draw_if=(binstat['median'] > 0), ct='blue.lite2')
    Plot.fill(DistanceBin.mids, [binstat['percent.16'], binstat['percent.84']],
              draw_if=(binstat['median'] > 0), ct='blue.lite')
    Plot.draw('c', DistanceBin.mids, binstat['median'], draw_if=(binstat['median'] > 0),
              ct='blue.dark')
    Plot.draw('c', [-10, 10], [-10, 10], lt='.', ct='def')
    Plot.make_window(erase=False)
    if hm_limits[0] <= 12 and plot_kind == 'phys_v_proj':
        Plot.set_label(0.66, 0.17)
    Plot.make_label(gm_kind, gm_limits, '')
    Plot.make_label('halo.mass', hm_limits, '')


#===================================================================================================
# infall time
#===================================================================================================
def plot_infall_v_mass_sat(
    sub, gm_kind='star.mass', gm_limits=[9.7, Inf], gm_width=0.25,
    hm_limits=[12, 15], hm_width=0.25, vary_kind='halo', disrupt_mf=0):
    '''
    Plot median infall property v mass.

    Parameters
    ----------
    subhalo catalog at snapshot: dict
    galaxy mass kind: string
    galaxy mass limits: list
    galaxy mass bin width: float
    halo mass limits: list
    halo mass bin width: float
    which mass to vary: string
    disruption mass fraction: float
    '''
    Say = ut.io.SayClass(plot_infall_v_mass_sat)
    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    sat = get_catalog_satellite(sub, None, gm_kind, gm_limits, hm_limits, disrupt_mf=disrupt_mf)
    infr_stat = GHMassBin.get_statistics_of_array(
        sat[GHMassBin.vary.mass_kind], sat.snapshot['time'] - sat['sat.last.t'])
    inff_stat = GHMassBin.get_statistics_of_array(
        sat[GHMassBin.vary.mass_kind], sat.snapshot['time'] - sat['sat.first.t'])
    for inf_i in range(infr_stat['x'].size):
        Say.say('m %.2f | sat number %5d' % (infr_stat['x'][inf_i], infr_stat['number'][inf_i]))

    # right y-axis variables
    redshifts = np.array([0.1, 0.5, 1, 1.5, 2])
    ts_z = [sub.snapshot['time'] - sub.Cosmology.get_time(redshifts[zi])
            for zi in range(redshifts.size)]

    # plot ----------
    Plot = plot_sm.PlotClass(window_mov_x=-0.01)
    axis_y_limits = [0, 9]
    if vary_kind == 'halo':
        Plot.label.pos_x = 0.05
    else:
        Plot.label.pos_x = 0.56
    Plot.set_axis('log', 'linear', GHMassBin.limits, axis_y_limits)
    Plot.set_axis_label(GHMassBin.vary.mass_kind + '.z0', 'Time since infall [Gyr]')
    Plot.set_axis_2('y', axis_y_limits, ts_z, [], redshifts, 'Redshift of infall')
    Plot.make_window()
    Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.limits, position_y=0.88, draw_log=True)
    Plot.set_label(0.5, 0.17, line_length=1.4)
    Plot.fill(inff_stat['x'], [inff_stat['percent.16'], inff_stat['percent.84']], ct='blue.lite2')
    Plot.draw('c', inff_stat['x'], inff_stat['median'], ct='blue', lt='-')
    # Plot.draw('c', inff_stat['x'], inff_stat['percent.16'], lt='--)
    # Plot.draw('c', inff_stat['x'], inff_stat['percent.84'])
    Plot.make_label('first\,infall', lt='-', small=2)
    Plot.draw('c', infr_stat['x'], infr_stat['median'], ct='red', lt='__')
    # Plot.draw('c', infr_stat['x'], infr_stat['percent.16'], lt='--)
    # Plot.draw('c', infr_stat['x'], infr_stat['percent.84'])
    Plot.make_label('recent\,infall', small=2)
    Plot.make_window(erase=False)


def plot_infall_v_masses_sat(
    sub, gm_kind='star.mass', gm_limits=[9.7, 11.3], gm_width=0.2, hm_limits=[12, 15], hm_width=1.5,
    vary_kind='galaxy', disrupt_mf=0):
    '''
    Plot median infall prop v mass.

    Import catalog of subhalo at snapshot, galaxy mass kind & range & bin width,
    halo mass range & bin width, which mass for x-axis, disrupt mass fraction.
    '''
    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    sat = get_catalog_satellite(sub, None, gm_kind, gm_limits, hm_limits, disrupt_mf=disrupt_mf)
    infr_stat = {}
    inff_stat = {}

    for fmi in range(GHMassBin.fix.number):
        sat_is = ut.array.get_indices(
            sat[GHMassBin.fix.mass_kind], GHMassBin.fix.get_bin_limits(fmi))
        infr_stat_m = GHMassBin.get_statistics_of_array(
            sat[GHMassBin.vary.mass_kind][sat_is], sat.snapshot['time'] - sat['sat.last.t'][sat_is])
        ut.array.append_dictionary(infr_stat, infr_stat_m)
        inff_stat_m = GHMassBin.get_statistics_of_array(
            sat[GHMassBin.vary.mass_kind][sat_is], sat.snapshot['time'] -
            sat['sat.first.t'][sat_is])
        ut.array.append_dictionary(inff_stat, inff_stat_m)

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', GHMassBin.limits, [0, 9])
    Plot.set_axis_label(GHMassBin.kind, 'Time since infall [Gyr]')
    Plot.make_window()
    for fmi in range(GHMassBin.fix.number):
        Plot.draw('c', inff_stat['x'][fmi], inff_stat['median'][fmi], ct='blue', lt=fmi)
        # Plot.draw('c', inff_stat['x'], inff_stat['sp.25'], lt=2)
        # Plot.draw('c', inff_stat['x'], inff_stat['sp.75'])
        if fmi == 0:
            Plot.make_label('first infall', lt='-')
        Plot.draw('c', infr_stat['x'][fmi], infr_stat['median'][fmi], ct='red', lt=fmi)
        # Plot.draw('c', infr_stat['x'], infr_stat['sp.25'], lt=2)
        # Plot.draw('c', infr_stat['x'], infr_stat['sp.75'])
        if fmi == 0:
            Plot.make_label('recent infall', lt='-')
        Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fixbinlim(fmi), ct='def')


def plot_infall_distribution(
    sub, gm_kind='star.mass', gm_limits=[9.7, 11.3], gm_width=0.8,
    hm_limits=[12, Inf], hm_width=None, vary_kind='galaxy', disrupt_mf=0, ilk='satellite',
    inf_prop='time', stat='cum', axis_x_limits=[]):
    '''
    Plot distribution of infall z or t of satellites at snapshot.
    *** Set ilk to cen & hm_min to 0 for central inf distribution.

    Import catalog of subhalo at snapshot, galaxy mass kind & range & bin width,
    halo mass range & bin width, which mass to vary, disrupt mass fraction, ilk,
    infall property (t, z), statistic (prob, prob.cum).
    '''
    Say = ut.io.SayClass(plot_infall_distribution)
    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    sis = ut.catalog.get_indices_subhalo(sub, ilk=ilk, disrupt_mf=disrupt_mf)
    ti_max = sub['ilk.dif.zi'][sis].max()
    x_bin_number = ti_max - sub.snapshot['index'] + 1
    inf_distr = {'sat.last': {}, 'ilk.dif': {}, 'sat.first': {}}
    Stat = ut.statistic.StatisticClass()
    for m in GHMassBin.mins:
        gm_bin_limits, hm_bin_limits = GHMassBin.bins_limits(m)
        sis_m = ut.array.get_indices(sub[gm_kind], gm_bin_limits, sis)
        sis_m = ut.array.get_indices(sub['halo.mass'], hm_bin_limits, sis_m)
        for k in ['sat.last', 'ilk.dif', 'sat.first']:
            inf_tis = sub[k + '.ti'][sis_m]
            ut.array.append_dictionary(
                inf_distr[k],
                Stat.get_distribution_dict(
                    inf_tis, (sub.snapshot['index'] - 0.5, ti_max + 0.5), bin_number=x_bin_number))
            if ilk == 'central' and k == 'ilk.dif':
                dif_number, m_number = inf_tis[inf_tis > 0].size, sis_m.size
                Say.say('%d of %d (%.2f) have inf.dif.zi > 0' %
                        (dif_number, m_number, dif_number / m_number))

    # plot ----------
    Plot = plot_sm.PlotClass(window_mov_x=0.01, window_mov_y=0.04)
    if inf_prop == 'time':
        tis = inf_distr['sat.last']['x'][0].astype(int)
        xs = sub.snapshot['time'] - sub.snapshots['time'][tis]
        Plot.axis.lab_x = 'Time since infall [Gyr]'
        # upper x-axis variables
        redshifts = np.array([0.1, 0.5, 1, 1.5, 2])
        ts_z = [sub.snapshot['time'] - sub.Cosmology.get_time(redshifts[zi])
                for zi in range(redshifts.size)]
    elif inf_prop == 'redshift':
        xs = sub.snapshot['redshift'][inf_distr['dif']['x'][0].astype(int)]
        Plot.axis.lab_x = 'redshift'
    if not axis_x_limits:
        axis_x_limits = ut.array.get_limits(xs)
        if inf_prop == 'time':
            axis_x_limits[1] = 10

    if stat == 'bin':
        Plot.axis.lim_y = inf_distr['dif'][stat]
        Plot.axis.lab_y = 'P (bin)'
    elif stat == 'cum':
        Plot.axis.lim_y = [0, 1]
        Plot.axis.lab_y = 'P (cumulative)'
    Plot.set_axis('linear', 'linear', axis_x_limits)
    if inf_prop == 'time':
        Plot.set_axis_2('x', axis_x_limits, ts_z, [], redshifts, 'Redshift of infall')
    Plot.make_window()
    '''
    Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.limits, '')
    for vmi in range(GHMassBin.vary.number:
        Plot.make_label(GHMassBin.vary.mass_kind, GHMassBin.vary.get_bin_limits(vmi), lt=vmi)
    '''
    for vmi in range(GHMassBin.vary.number):
        Plot.draw('c', xs, inf_distr['sat.first'][stat][vmi], ct='blue', lt=vmi)
        if vmi == 0:
            Plot.make_label('first\,infall', lt='-')
        Plot.draw('c', xs, inf_distr['sat.last'][stat][vmi], ct='red', lt=vmi)
        if vmi == 0:
            Plot.make_label('recent\,infall', lt='-')
    Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.limits, '', position_x=0.42,
                    position_y=0.25, ct='def')
    for vmi in range(GHMassBin.vary.number):
        Plot.make_label(GHMassBin.vary.mass_kind, GHMassBin.vary.get_bin_limits(vmi), ct='def',
                        lt=vmi)


#===================================================================================================
# host halo mass of progenitor
#===================================================================================================
def plot_halo_mass_distribution_v_redshift_sat(
    subs, hals=None, gm_kind='star.mass', gm_limits_0=[9, Inf], hm_limits_0=[14, 15],
    distance_halo_limits_0=[], dimension_number=2, hm_limits=[12, 15], hm_width=0.25,
    disrupt_mf=0.007, redshifts=[0.05, 0.5, 1, 1.5], distr_kind='prob.cum'):
    '''
    Plot distribution of host halo masses of parents/children at given redshifts.

    Import subhalo catalog, halo catalog, galaxy mass kind & limit,
    host halo mass limit at redshifts[0],
    disrupt mass fraction, redshifts to plot (select at redshifts[0]).
    '''
    virial_kind = '200m'

    Say = ut.io.SayClass(plot_halo_mass_distribution_v_redshift_sat)
    HmBin = ut.binning.BinClass(hm_limits, hm_width)
    tis = ut.binning.get_bin_indices(
        redshifts, subs.snapshot['redshift'], round_kind='near', clip_to_bin_limits=True,
        warn_outlier=False, scalarize=False)
    ti_ref = tis[0]
    redshift_ref = subs.snapshot['redshift'][ti_ref]
    if 0.05 < redshift_ref < 0.06:
        redshift_ref = 0.049
    tis = tis[1:]

    if gm_kind == 'star.mass':
        sham.assign_to_catalog(subs[ti_ref], gm_kind, 0.15, disrupt_mf)

    sat = get_catalog_satellite(
        subs[ti_ref], hals[ti_ref], gm_kind, gm_limits_0, hm_limits_0, distance_halo_limits_0,
        dimension_number, virial_kind, disrupt_mf)
    sis = sat['self.index']
    #sis = ut.catalog.get_indices_subhalo(subs[ti_ref], gm_kind, gm_limits_0, hm_limits_0,
    #'satellite',disrupt_mf)

    haloM = ut.statistic.StatisticClass()
    for zi in tis:
        sis_fam = ut.catalog.get_indices_tree(subs, ti_ref, zi, sis)
        halo_ms = np.zeros(sis_fam.size)  # default to 0, if not exist or no host halo
        masks = sis_fam > 0
        sis_fam = sis_fam[masks]
        halo_ms[masks] = subs[zi]['halo.mass'][sis_fam]
        Say.say('%d (%.2f) have tree link at zi = %d, z = %.2f' %
                (sis_fam.size, sis_fam.size / sis.size, zi, subs.snapshot['redshift'][zi]))
        _, siis_fam_cen = ut.catalog.get_indices_ilk(
            subs[zi], 'central', sis_fam, get_masks=True)
        halo_ms[masks[siis_fam_cen]] = 1
        haloM.append_dictionary(halo_ms, HmBin.limits, HmBin.number)

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', HmBin.limits, [0, 1])
    if distr_kind == 'probability':
        label = 'df/dM_{halo}'
    elif distr_kind == 'prob.cum':
        label = 'f(<M_{halo})'
    Plot.set_axis_label('M_{halo}(z)', label)
    Plot.make_window()
    Plot.set_point(color_number=tis.size)

    Plot.make_label(gm_kind, gm_limits_0, redshift=redshift_ref, position_y=0.9)
    Plot.make_label('halo.mass', hm_limits_0, redshift=redshift_ref)
    if distance_halo_limits_0:
        Plot.make_label(
            ut.plot.get_label_distance('central.distance/Rneig', dimension_number, virial_kind) +
            '=[%.1f,%.1f]' % (distance_halo_limits_0[0], distance_halo_limits_0[1]))
    Plot.set_label(position_x=0.7, position_y=0.25)
    for zii, zi in enumerate(tis):
        Plot.draw('c', haloM.distr['bin.mid'][zii], haloM.distr[distr_kind][zii], ct=zii, lt=zii)
        Plot.make_label('z=%.1f' % subs.snapshot['redshift'][zi])


#===================================================================================================
# infall type & ejected satellites
#===================================================================================================
def plot_infall_type_frac_v_mass_sat(
    sub, gm_kind='star.mass', gm_limits=[9.7, Inf], gm_width=0.25,
    hm_limits=[12, 15.25], hm_width=0.25, vary_kind='halo', disrupt_mf=0):
    '''
    Plot fraction of satellites that fell in as various kinds (sat, cen, ejected).

    Import subhalo catalog, snapshot index, galaxy mass kind & range & bin width,
    halo mass range & bin width, which mass to vary, disrupt mass fraction.
    '''
    error_kind = 'beta'

    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    sis = ut.catalog.get_indices_subhalo(sub, ilk='satellite', disrupt_mf=disrupt_mf)
    FieldFrac = ut.math.FractionClass(GHMassBin.number, error_kind)
    FieldEjectFrac = ut.math.FractionClass(GHMassBin.number, error_kind)
    GroupFrac = ut.math.FractionClass(GHMassBin.number, error_kind)
    FieldDirectFrac = ut.math.FractionClass(GHMassBin.number, error_kind)
    for vmi in range(GHMassBin.vary.number):
        gm_bin_limits, hm_bin_limits = GHMassBin.bins_limits(vmi)
        sis_m = ut.catalog.get_indices_subhalo(sub, gm_kind, gm_bin_limits, hm_bin_limits, sis=sis)
        sis_field = sis_m[sub['sat.last.zi'][sis_m] == sub['ilk.dif.zi'][sis_m]]
        sis_field_eject = sis_field[sub['sat.last.zi'][sis_field] <
                                    sub['sat.first.zi'][sis_field]]
        # assume those infall before max inf.first.zi came in from field
        sis_field_direct = sis_field[sub['sat.last.zi'][sis_field] >=
                                     sub['sat.first.zi'][sis_field]]
        sis_group = sis_m[sub['sat.last.zi'][sis_m] < sub['ilk.dif.zi'][sis_m]]
        FieldFrac.assign_to_dict(vmi, sis_field.size, sis_m.size)
        FieldEjectFrac.assign_to_dict(vmi, sis_field_eject.size, sis_m.size)
        FieldDirectFrac.assign_to_dict(vmi, sis_field_direct.size, sis_m.size)
        GroupFrac.assign_to_dict(vmi, sis_group.size, sis_m.size)

    # first infall only goes so far, some have inf.last.zi > inf.first.zi, assume are direct infall
    print('# direct =', FieldDirectFrac['value'])
    FieldDirectFrac['value'] = 1 - (FieldEjectFrac['value'] + GroupFrac['value'])
    print('# direct =', FieldDirectFrac['value'])
    print('# eject =', FieldEjectFrac['value'])
    print('# group =', GroupFrac['value'])

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', GHMassBin.limits, [0, 1])
    Plot.set_axis_label(GHMassBin.vary.mass_kind + '.z0', 'Satellite infall type fraction')
    Plot.make_window()
    Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.limits, position_y=0.59, draw_log=True)
    Plot.fill_err(GHMassBin.mids, FieldDirectFrac['value'], FieldDirectFrac.err, ct='red.lite2')
    Plot.draw('c', GHMassBin.mids, FieldDirectFrac['value'], ct='red', lt='-')
    Plot.make_label('first\,infall\,central', small=2, line_length=1.2)
    Plot.fill_err(GHMassBin.mids, GroupFrac['value'], GroupFrac.err, ct='blue.lite2')
    Plot.draw('c', GHMassBin.mids, GroupFrac['value'], ct='blue', lt='__')
    Plot.make_label('satellite\,in\,group', small=2)
    Plot.fill_err(GHMassBin.mids, FieldEjectFrac['value'], FieldEjectFrac.err, ct='green.lite2')
    Plot.draw('c', GHMassBin.mids, FieldEjectFrac['value'], ct='green', lt='--')
    Plot.make_label('re-infall\,ejected\,satellite', small=2)


def plot_ejected_frac_v_mass(
    subs, ti_now=1, ti_max=15, gm_kind='star.mass', gm_limits=[9, 11.2], gm_width=0.2,
    ilk='central', hm_limits=[12, 15], hm_width=1, mgrow_max='sat.first', disrupt_mf=0,
    distance_halo_max=2.5, plot_stripped=False, bin_inff=False, eje=None):
    '''
    Plot ejected fraction v galaxy mass kind.
    Also plot stripped fraction (halo.m < halo.m.max - 0.1).
    *** no significant difference using ti_max = 15 v 23

    Import subhalo catalog, snapshot index, max snapshot to track ejection,
    galaxy mass kind & range & bin width, halo mass range & bin width,
    maximum M_peak(now) / M_peak(inf.dif) to include centrals as ejecteds
    (inf.first = use cut on inf.first, effectively = 1.5),
    disrupt mass fraction, distance / R_halo range to count,
    whether to bin by redshift of first infall, whether to plot fraction of stripped subhalos,
    ejected central catalog.

    '''
    strip_log_m_dif = 0.1

    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, 'galaxy')
    if bin_inff:
        inff_tis = np.array([5, 8, 12, 14, 19])  # , 22])
        ti_lo, ti_hi = 1, 1
        # break down above by inf.first
        ej_fracs_inff = np.zeros((inff_tis.size, GHMassBin.vary.number))
        ej_fracs_inff_unc = np.zeros((inff_tis.size, 2, GHMassBin.vary.number))
    sub = subs[ti_now]
    sis = ut.catalog.get_indices_subhalo(sub, disrupt_mf=disrupt_mf, ilk=ilk)
    # fraction of centrals that are ejected
    EjFracCen = ut.math.FractionClass(GHMassBin.vary.number, 'beta')
    EjFracCenMh = ut.math.FractionClass([GHMassBin.fix.number, GHMassBin.vary.number], 'beta')
    # fraction of centrals whose halo.m is below m.max (for comparison)
    MstripFracCen = ut.math.FractionClass(GHMassBin.vary.number, 'beta')
    # fraction of satellites that have experienced ejection
    EjFracSat = ut.math.FractionClass([GHMassBin.fix.number, GHMassBin.vary.number], 'beta')
    # fraction of all that fell in that currently are ejected
    EjFracInf = ut.math.FractionClass(GHMassBin.vary.number, 'beta')
    for gmi in range(GHMassBin.vary.number):
        gm_bin_limits = GHMassBin.vary.get_bin_limits(gmi)
        if ilk == 'central':
            # all centrals
            sis_cen_m = ut.array.get_indices(sub[gm_kind], gm_bin_limits, sis)
            if eje:
                sis_ejected = np.intersect1d(eje['self.index'], sis_cen_m)
            else:
                sis_ejected = get_indices_ejected(subs, ti_now, ti_max, ilk='central',
                                                  mass_grow_max=mgrow_max, sis=sis_cen_m)
            EjFracCen.assign_to_dict(gmi, sis_ejected.size, sis_cen_m.size)
            sis_stripped = sis_cen_m[sub['halo.mfass'][sis_cen_m] <
                                     sub['mass.peak'][sis_cen_m] - strip_log_m_dif]
            MstripFracCen.assign_to_dict(gmi, sis_stripped.size, sis_cen_m.size)
        for hmi in range(GHMassBin.fix.number):
            hm_bin_limits = GHMassBin.fix.get_bin_limits(hmi)
            if ilk == 'satellite':
                # current satellites
                sis_sat_m = ut.catalog.get_indices_subhalo(
                    sub, gm_kind, gm_bin_limits, hm_bin_limits, sis=sis)
                sis_ejected = get_indices_ejected(
                    subs, ti_now, ti_max, ilk='satellite', sis=sis_sat_m)
                EjFracSat.assign_to_dict([hmi, gmi], sis_ejected.size, sis_sat_m.size)
            elif ilk == 'central' and 'nearest.distance/Rneig' in sub:
                sis_cen_m = ut.catalog.get_indices_subhalo(sub, gm_kind, gm_bin_limits, sis=sis)
                sis_cen_m = ut.array.get_indices(
                    sub['nearest.distance/Rneig'], [0, distance_halo_max], sis_cen_m)
                sis_cen_m = ut.array.get_indices(sub['nearest.mass'], hm_bin_limits, sis_cen_m)
                sis_ejected = get_indices_ejected(
                    subs, ti_now, ti_max, ilk='central', mass_grow_max=mgrow_max, sis=sis_cen_m)
                EjFracCenMh.assign_to_dict([hmi, gmi], sis_ejected.size, sis_cen_m.size)
            else:
                raise ValueError('ilk = %s, nearest.distance/Rneig not in subs[ti_now]')
        if bin_inff:
            # those fell in at u that currently are ejected
            sis_m = ut.array.get_indices(sub[gm_kind], gm_bin_limits, sis)
            sis_inf = ut.array.get_indices(sub['sat.first.zi'], [ti_now, Inf], sis_m)
            sis_ejected = get_indices_ejected(
                subs, ti_now, ti_max, ilk='central', mass_grow_max='sat.first', sis=sis_inf)
            EjFracInf.assign_to_dict(gmi, sis_ejected.size, sis_inf.size)
            for zii in range(inff_tis.size):
                sis_inff = ut.array.get_indices(
                    sub['sat.first.zi'],
                    [inff_tis[zii] - ti_lo - 0.1, inff_tis[zii] + ti_hi + 0.1], sis_m)
                sis_ejected = get_indices_ejected(
                    subs, ti_now, ti_max, ilk='central', mass_grow_max='sat.first', sis=sis_inff)
                ej_fracs_inff[zii, gmi], ej_fracs_inff_unc[zii, :, gmi] = \
                    Fraction.get_fraction(sis_ejected.size, sis_inff.size, 'beta')

    # plot ----------
    err_limit = 0.037
    Plot = plot_sm.PlotClass()
    if bin_inff:
        Plot.axis.lim_y = [0, ej_fracs_inff.max() * 1.05]
    else:
        Plot.axis.lim_y = [0, 0.15]  # np.max(ej_fracs_sat[-1]['value']) * 1.05]
    Plot.set_axis('log', 'linear', gm_limits)
    if ilk == 'satellite':
        Plot.set_axis_label(gm_kind, 'fraction were ejected')
    elif ilk == 'central':
        Plot.set_axis_label(gm_kind, 'ejected fraction')
        Plot.set_axis_label(gm_kind, 'fraction of centrals')
    Plot.make_window()
    Plot.set_label(0.55, 0.92, line_length=1.3)
    Plot.set_point(color_number=GHMassBin.fix.number, line_number=GHMassBin.fix.number)
    if ilk == 'satellite':
        for hmi in reversed(list(range(GHMassBin.fix.number))):
            Plot.fill_err(GHMassBin.vary.mids, EjFracSat['value'][hmi], EjFracSat['error'][hmi],
                          draw_if=(EjFracSat['error'][hmi][:, 1] < err_limit) *
                                  (EjFracSat['value'][hmi] > 0), ct=hmi - 0.2)
        for hmi in reversed(list(range(GHMassBin.fix.number))):
            Plot.draw('c', GHMassBin.vary.mids, EjFracSat['value'][hmi],
                      draw_if=(EjFracSat['error'][hmi][:, 1] < err_limit), ct=hmi, lt=hmi)
            Plot.make_label('halo.mass', GHMassBin.fix.get_bin_limits(hmi))
    elif ilk == 'central':
        for hmi in reversed(list(range(GHMassBin.fix.number))):
            Plot.fill_err(GHMassBin.vary.mids, EjFracCenMh['value'][hmi], EjFracCenMh['error'][hmi],
                          draw_if=(EjFracCenMh['error'][hmi][:, 1] < err_limit), ct=hmi - 0.2)
        for hmi in reversed(list(range(GHMassBin.fix.number))):
            Plot.draw('c', GHMassBin.vary.mids, EjFracCenMh['value'][hmi],
                      draw_if=(EjFracCenMh['error'][hmi][:, 1] < err_limit), ct=hmi, lt=hmi)
            Plot.make_label('halo.mass', GHMassBin.fix.get_bin_limits(hmi))
            # Plot.make_label('<%.1fR_{vir}\,of\,%s' %
            #                (distance_halo_max,
            #plot.get_m_label('halo.mass', GHMassBin.fix.get_bin_limits(hmi))))
        Plot.fill_err(GHMassBin.vary.mids, EjFracCen['value'], EjFracCen['error'], ct='blue.lite')
        Plot.draw('c', GHMassBin.vary.mids, EjFracCen['value'], ct='blue', lt='-')
        # Plot.make_label('all centrals')
        Plot.make_label('ejected')
        if plot_stripped:
            # add all stripped centrals
            Plot.fill_err(GHMassBin.vary.mids, MstripFracCen['value'], MstripFracCen['error'],
                          ct='red.lite')
            Plot.draw('c', GHMassBin.vary.mids, MstripFracCen['value'], ct='red', lt='--')
            Plot.make_label('stripped')
    if bin_inff:
        Plot.draw('c', GHMassBin.vary.mids, EjFracInf['value'], EjFracInf['error'], lt='-')
        Plot.make_label('all z_{inf}')
        for zii in range(len(inff_tis)):
            Plot.draw('c', GHMassBin.vary.mids, ej_fracs_inff[zii],  # ej_fracs_inff_unc[zii],
                      draw_if=(ej_fracs_inff_unc[zii][:][1] < 0.05), lt='-', ct=zii)
            Plot.make_label('z_{inf}=%.1f' % subs.snapshot['redshift'][inff_tis[zii]])
    Plot.make_window(erase=False)


def plot_ejected_frac_v_distance_halo(
    subs, hals, ti_now=1, ti_max=15,
    gm_kind='star.mass', gm_limits=[9.7, 10.9], gm_width=0.4, disrupt_mf=0,
    hm_limits=[14, 15], distance_halo_limits=[0, 5], distance_halo_width=1 / 3, dimension_number=3,
    virial_kind='200m', mass_grow_max='sat.first', neig_number_max=1200, draw_ej_from_cen=False,
    frac_to_draw='ejected'):
    '''
    Plot ejected fraction v halo-centric distance.
    *** no significant difference between ti_max = 23 & 15.

    Import subhalo & halo catalog, snapshot current & max to track ejection,
    galaxy mass kind & range & bin width, disrupt mass fraction, halo mass range,
    neighbor distance / R_halo limit & bin width & number of spatial dimensions, virial kind,
    max M_peak growth to count as ejected, maximum neighbor count in finder,
    whether to draw curve for those ejected just from central halo,
    which fraction to draw (ejected, sat).
    '''
    space = 'real'

    sub, hal = subs[ti_now], hals[ti_now]
    Say = ut.io.SayClass(plot_ejected_frac_v_distance_halo)
    HaloProperty = ut.halo_property.HaloPropertyClass(subs.Cosmology)
    GMBin = ut.binning.BinClass(gm_limits, gm_width)
    DistanceBin = ut.binning.BinClass(distance_halo_limits, distance_halo_width)
    # all centrals in halo mass range, regardless of central properties
    sis_cen = ut.catalog.get_indices_ilk(sub, 'central')
    sis_cen_hm = ut.array.get_indices(sub['halo.mass'], hm_limits, sis_cen)
    halo_is = sub['halo.index'][sis_cen_hm]
    rad_virs = HaloProperty.radius_virial_catalog(virial_kind, hals[ti_now], halo_is)
    Say.say('halo nuber = %d' % sis_cen_hm.size)
    Say.say('R_halo max = %.2f kpc comoving: find neighbors out to %.1f kpc comoving' %
            (rad_virs.max(), rad_virs.max() * distance_halo_limits[1]))
    neig = ut.catalog.get_catalog_neighbor(
        sub, neig_gm_kind=gm_kind, neig_gm_limits=gm_limits, neig_hm_limits=None, neig_ilk='all',
        disrupt_mf=disrupt_mf, neig_number_max=neig_number_max,
        neig_distance_max=rad_virs.max() * distance_halo_limits[1],
        distance_space=space, center_indices=sis_cen_hm)
    # total ejected fraction
    #ej_fracs, ej_fracs_err = np.zeros((2, GMBin.number, DistanceBin.number))
    # fraction in bin
    #ej_fracs_bin = np.zeros((GMBin.number, DistanceBin.number, sis_cen_hm.size)) - 1
    EjFrac = ut.math.FractionClass([GMBin.number, DistanceBin.number], 'beta')
    ej_numbers = np.zeros((GMBin.number, DistanceBin.number))
    total_numbers = np.zeros((GMBin.number, DistanceBin.number))
    # sat_fracs, sat_fracs_err = np.zeros((2, GMBin.number, DistanceBin.number))
    # sat_fracs_bin = np.zeros((GMBin.number, DistanceBin.number, sis_cen_hm.size)) - 1
    SatFrac = ut.math.FractionClass([GMBin.number, DistanceBin.number], 'beta')
    sat_numbers = np.zeros((GMBin.number, DistanceBin.number))

    if draw_ej_from_cen:
        # ej_halo_fracs, ej_halo_fracs_err = np.zeros((2, GMBin.number, DistanceBin.number))
        # ej_halo_fracs_bin = np.zeros((GMBin.number, DistanceBin.number, sis_cen_hm.size)) - 1
        EjFrachalo = ut.math.FractionClass([GMBin.number, DistanceBin.number], 'beta')
        ej_halo_numbers = np.zeros((GMBin.number, DistanceBin.number))

    for hi in range(sis_cen_hm.size):
        neig_sis, neig_ds = neig['indices'][hi], neig['distances'][hi]
        for dist_i in range(DistanceBin.number):
            neig_sis_d = neig_sis[
                ut.array.get_indices(
                    neig_ds, rad_virs[hi] * np.array(DistanceBin.get_bin_limits(dist_i)))]
            neig_sis_d_ej = get_indices_ejected(
                subs, ti_now, ti_max, ilk='all', mass_grow_max=mass_grow_max, sis=neig_sis_d)

            for gmi in range(GMBin.number):
                neig_sis_d_mg = ut.array.get_indices(
                    sub[gm_kind], GMBin.get_bin_limits(gmi), neig_sis_d)

                if neig_sis_d_mg.size:
                    neig_sis_d_mg_ej = ut.array.get_indices(
                        sub[gm_kind], GMBin.get_bin_limits(gmi), neig_sis_d_ej)
                    neig_sis_d_mg_sat = ut.catalog.get_indices_ilk(sub, 'satellite', neig_sis_d_mg)

                    if frac_to_draw == 'satellite':
                        # halo mass minimum for satellites
                        neig_sis_d_mg_sat = ut.array.get_indices(
                            sub['halo.mass'], [1, Inf], neig_sis_d_mg_sat)
                    else:
                        # require that satellites be in selected halo
                        neig_sis_d_mg_sat = ut.array.get_indices(
                            sub['halo.index'], sub['halo.index'][sis_cen_hm[hi]], neig_sis_d_mg_sat)

                    neig_sis_d_gm_sat_ej = np.intersect1d(neig_sis_d_mg_sat, neig_sis_d_mg_ej)
                    neig_sis_d_gm_cen = ut.catalog.get_indices_ilk(sub, 'central', neig_sis_d_mg)
                    neig_sis_d_gm_cen_ej = np.intersect1d(neig_sis_d_gm_cen, neig_sis_d_mg_ej)
                    num_d_gm = neig_sis_d_gm_cen.size + neig_sis_d_mg_sat.size
                    num_d_mg_ej = neig_sis_d_gm_cen_ej.size + neig_sis_d_gm_sat_ej.size

                    # count only bins with galaxies
                    if num_d_gm > 0:
                        # ej_fracs_bin[gmi, dist_i, hi] = ut.math.fraction(num_d_mg_ej, num_d_gm)
                        # sat_fracs_bin[gmi, dist_i, hi] = ut.math.fraction(neig_sis_d_mg_sat.size,
                        # num_d_gm)
                        ej_numbers[gmi, dist_i] += num_d_mg_ej
                        total_numbers[gmi, dist_i] += num_d_gm
                        sat_numbers[gmi, dist_i] += neig_sis_d_mg_sat.size

                    # count centrals ejected from centered halo
                    if draw_ej_from_cen:
                        num_d_gm_ej_hi = 0
                        if neig_sis_d_gm_cen_ej.size:
                            infd_sis = sub['ilk.dif.index'][neig_sis_d_gm_cen_ej]
                            infd_tis = sub['ilk.dif.zi'][neig_sis_d_gm_cen_ej]
                            halo_i_z = hal['parent.index'][halo_is[hi]]
                            for zi in range(ti_now + 1, ti_max + 1):
                                if halo_i_z <= 0:
                                    break
                                infd_sis_z = infd_sis[
                                    ut.array.get_arange(infd_tis)[zi == infd_tis]]
                                num_d_gm_ej_hi += np.sum(
                                    subs[zi]['halo.index'][infd_sis_z] == halo_i_z)
                                num_d_gm_ej_hi += (infd_tis > ti_max).sum()
                                halo_i_z = hals[zi]['parent.index'][halo_i_z]
                        # add current satellites in centered halo
                        num_d_gm_ej_hi += neig_sis_d_gm_sat_ej.size
                        # count only bins with galaxies
                        if num_d_gm > 0:
                            # ej_halo_fracs_bin[gmi, dist_i, hi] = gen.fraction(num_d_gm_ej_hi,
                            # num_d_gm)
                            ej_halo_numbers[gmi, dist_i] += num_d_gm_ej_hi

    # compute halo average - ignore bins with no objects
    for gmi in range(GMBin.number):
        for dist_i in range(DistanceBin.number):
            # compute halo-to-halo scatter
            '''
            is_defined = ej_fracs_bin[gmi, dist_i, :] >= 0
            if np.max(is_defined):
                ej_fracs_ok = ej_fracs_bin[gmi, dist_i, :][is_defined]
                ej_fracs[gmi, dist_i] = np.mean(ej_fracs_ok)
                #ej_fracs_err[gmi, dist_i] = stats.sem(ej_fracs_ok)
                ej_fracs_err[gmi, dist_i] = np.std(ej_fracs_ok, ddof=1)
                sat_fracs_ok = sat_fracs_bin[gmi, dist_i, :][is_defined]
                sat_fracs[gmi, dist_i] = np.mean(sat_fracs_ok)
                #sat_fracs_err[gmi, dist_i] = stats.sem(sat_fracs_ok)
                sat_fracs_err[gmi, dist_i] = np.std(sat_fracs_ok, ddof=1)
            '''
            EjFrac.assign_to_dict(
                [gmi, dist_i], ej_numbers[gmi, dist_i], total_numbers[gmi, dist_i])
            SatFrac.assign_to_dict(
                [gmi, dist_i], sat_numbers[gmi, dist_i], total_numbers[gmi, dist_i])
            if draw_ej_from_cen:
                # compute halo-to-halo scatter
                '''
                ej_fracs_ok = ej_halo_fracs_bin[gmi, dist_i, :][ej_halo_fracs_bin[gmi, dist_i, :]
                                                               >= 0]
                if ej_fracs_ok.size:
                    ej_halo_fracs[gmi, dist_i] = np.mean(ej_fracs_ok)
                    #ej_halo_fracs_err[gmi, dist_i] = stats.sem(ej_fracs_ok)
                    ej_halo_fracs_err[gmi, dist_i] = np.std(ej_fracs_ok, ddof=1)
                '''
                EjFrachalo.assign_to_dict(
                    [gmi, dist_i], ej_halo_numbers[gmi, dist_i], total_numbers[gmi, dist_i])

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('linear', 'linear', distance_halo_limits, [0, 0.45])
    Plot.set_axis_label(
        ut.plot.get_label_distance('central.distance/Rneig', dimension_number, virial_kind),
        'ejected fraction')
    Plot.make_window()
    Plot.set_point(color_number=GMBin.number, line_number=GMBin.number)
    Plot.set_label(line_length=1.3)
    Plot.make_label('halo.mass', hm_limits, position_x=0.45, small=1)
    if frac_to_draw == 'satellite':
        for gmi in reversed(list(range(GMBin.number))):
            Plot.fill_err(DistanceBin.mids, SatFrac['value'][gmi], SatFrac['error'][gmi],
                          SatFrac['value'][gmi], ct=gmi - 0.2)
        for gmi in reversed(list(range(GMBin.number))):
            Plot.draw('c', DistanceBin.mids, SatFrac['value'][gmi], draw_if=SatFrac['value'][gmi],
                      ct=gmi, lt=gmi)
        for gmi in reversed(list(range(GMBin.number))):
            Plot.make_label(gm_kind, GMBin.get_bin_limits(gmi), small=2, ct=gmi, lt=gmi)
    else:
        for gmi in reversed(list(range(GMBin.number))):
            Plot.fill_err(DistanceBin.mids, EjFrac['value'][gmi], EjFrac['error'][gmi],
                          EjFrac['value'][gmi], ct=gmi - 0.2)
        for gmi in reversed(list(range(GMBin.number))):
            Plot.draw('c', DistanceBin.mids, EjFrac['value'][gmi], draw_if=EjFrac['value'][gmi],
                      ct=gmi, lt=gmi, lw=7)
            if draw_ej_from_cen:
                Plot.draw('c', DistanceBin.mids, EjFrachalo['value'][gmi],
                          draw_if=EjFrachalo['value'][gmi], ct=gmi, lt=gmi, lw=4.5)
        for gmi in range(GMBin.number):
            Plot.make_label(gm_kind, GMBin.get_bin_limits(gmi), small=2, ct=gmi, lt=gmi, lw=4.5)
    Plot.make_window(erase=False)
    Plot.draw('c', [1, 1], [-1, 20], lt='--')
    # Plot.make_label('ejected\,from\,any\,halo', lt='__', small=3, pos_x=0.35)
    # Plot.make_label('ejected\,from\,centered\,halos', lt='-', small=3)


def plot_ejected_times(
    subs, ti_now=1, ti_max=23, gm_kind='star.mass', gm_limits=[9.7, Inf], gm_width=Inf,
    ilk='central', hm_limits=[12, 15], hm_width=1, vary_kind='halo', disrupt_mf=0,
    get_infalling=False, mass_grow_max='sat.first', to_draw='t_v_m', time_limits=[0, 10],
    time_bin_number=10):
    '''
    For ejected satellites, plot time
        since first infall, spent as ejected, spent as satellite.

    Import subhalo catalog, snapshot index, max snapshot to track ejection,
    galaxy mass kind & range & bin width, ilk, halo mass range & bin width,
    which mass to vary, disrupt mass fraction, whether to neig infalling ejected (child is sat),
    maximum M_peak(now) / M_peak(inf.dif) to include centrals as ejected
    (inf.first = use cut on inf.first, effectively = 1.5),
    what to draw (t_v_m, prob, prob.cum), time range & number of bins.
    '''
    time_now = subs.snapshot['time'][ti_now]
    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    sis = ut.catalog.get_indices_subhalo(subs[ti_now], gm_kind, gm_limits, disrupt_mf=disrupt_mf)
    TsInff = ut.statistic.StatisticClass()
    TsEj = ut.statistic.StatisticClass()
    TasSat = ut.statistic.StatisticClass()
    for vmi in range(GHMassBin.vary.number):
        gm_bin_limits, hm_bin_limits = GHMassBin.bins_limits(vmi)
        sis_ejected = get_indices_ejected(
            subs, ti_now, ti_max, gm_kind, gm_limits, 'central', disrupt_mf, mass_grow_max, sis)
        sat = get_catalog_satellite(
            subs[ti_now], None, gm_kind, gm_bin_limits, disrupt_mf=disrupt_mf,
            sis_ejected=sis_ejected)
        sat_is = ut.catalog.get_indices_ilk(sat, ilk)
        if ilk == 'central' and vary_kind == 'halo':
            sat_is = ut.array.get_indices(sat['nearest.distance/Rneig'], [0, 2.5], sat_is)
            sat_is = ut.array.get_indices(sat['nearest.mass'], hm_bin_limits, sat_is)
        if get_infalling:
            # neig ejecteds that are about to fall back in
            sis = sat['self.index'][sat_is]
            chi_is = subs[ti_now]['child.index'][sis]
            chi_is, siis = ut.catalog.get_indices_ilk(
                subs[ti_now - 1], 'satellite', chi_is, get_masks=True)
            sat_is = sat_is[siis]
        # return sat['self.index'][sat_is]
        TsInff.append_dictionary(
            time_now - sat['sat.first.t'][sat_is], time_limits, time_bin_number)
        TsEj.append_dictionary(time_now - sat['ilk.dif.t'][sat_is], time_limits, time_bin_number)
        TasSat.append_dictionary(
            subs.snapshot['time'][sat['ilk.dif.zi'][sat_is]] -
            subs.snapshot['time'][sat['sat.first.zi'][sat_is]], time_limits, time_bin_number)

    # plot ----------
    Plot = plot_sm.PlotClass()
    if to_draw == 't_v_m':
        Plot.set_axis('log', 'linear', GHMassBin.vary.limits, time_limits)
        Plot.set_axis_label(GHMassBin.vary.mass_kind, 'Time [Gyr]')
        Plot.make_window()
        Plot.draw('c', GHMassBin.mids, TsEj.stat['median'], ct='blue', lt='-')
        # Plot.draw('c', GHMassBin.mids, TsEj.stat['percent.16'], lt='.')
        # Plot.draw('c', GHMassBin.mids, TsEj.stat['percent.84'], lt='.')
        Plot.draw('c', GHMassBin.mids, TsInff.stat['median'], ct='red', lt='--')
        # Plot.draw('c', GHMassBin.mids, TsInff.stat['percent.16'], lt='.')
        # Plot.draw('c', GHMassBin.mids, TsInff.stat['percent.84'], lt='.')
        Plot.draw('c', GHMassBin.mids, TasSat.stat['median'], ct='red', lt='-.')
        Plot.make_label('since ejection', ct='def', lt='-')
        Plot.make_label('since first infall', ct='def', lt='--')
        Plot.make_label('as satellite', ct='def', lt='-.')
        print(TsEj.stat['median'])
        print(TsInff.stat['median'])
        print(TasSat.stat['median'])
    elif to_draw in ['probability', 'prob.cum']:
        Plot = plot_sm.PlotClass()
        Plot.set_axis('linear', 'linear', time_limits, [0, TsEj.distr[to_draw][-1].max() * 1.03])
        Plot.set_axis_label('Time [Gyr]')
        if to_draw == 'probability':
            Plot.axis.lab_y = 'dP/dt'
        elif to_draw == 'prob.cum':
            Plot.axis.lab_y = 'P(<t)'
        Plot.make_window()
        Plot.set_point(color_number=GHMassBin.vary.number, line_number=GHMassBin.vary.number)
        Plot.set_label(position_x=0.12)
        Plot.make_label('since\,ejection', ct='def', lt=None)
        Plot.make_label('since\,first\,infall', ct='def', lt=None)
        # Plot.make_label('as satellite', ct='def', lt='.')
        Plot.set_label(position_x=0.54, position_y=0.9, line_length=1.3)
        Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.limits)
        for vmi in range(GHMassBin.vary.number):
            Plot.draw('c', TsEj.distr['bin.mid'][vmi], TsEj.distr[to_draw][vmi], ct=vmi, lt=vmi,
                      lw=4)
            Plot.draw('c', TsInff.distr['bin.mid'][vmi], TsInff.distr[to_draw][vmi], ct=vmi, lt=vmi,
                      lw=7)
            #Plot.draw('c', TasSat.distr['bin.mid'][vmi], TasSat.distr[to_draw][vmi], ct=vmi,
            #lt='.')
            Plot.make_label(GHMassBin.vary.mass_kind, GHMassBin.vary.get_bin_limits(vmi), lw=4.5)


class MassDistrEjectedClass(ut.io.SayClass):
    '''
    For ejected satellites, plot distribution of fraction of current halo mass relative to that
    at ejection & infall.
    '''
    def __init__(self):
        pass

    def assign_to_self(
        self, subs, ti_now=1, ti_max=15, gm_kind='star.mass', gm_limits=[9.7, Inf], gm_width=None,
        hm_limits=[12, 15], hm_width=1, vary_kind='halo', disrupt_mf=0, bin_number=50,
        mass_grow_max=1.5):
        '''
        Import subhalo catalog, current & maximum snapshot for ejection,
        galaxy mass kind & range & bin width, halo mass range & bin width, which mass to vary,
        disrupt mass fraction, number of x-bins for plot,
        maximum m_peak growth fraction to include as ejected.
        '''
        mass_ratio_limits = [0, mass_grow_max]
        self.GHMassBin = ut.binning.GalaxyHaloMassBinClass(
            gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
        subs = subs[ti_now]
        sis = get_indices_ejected(
            subs, ti_now, ti_max, gm_kind, gm_limits, 'central', disrupt_mf, mass_grow_max)
        # neig centrals that have been ejected for > 1 snapshot to neig non-zero mass evolution
        sis = ut.array.get_indices(subs['ilk.dif.zi'], [ti_now + 2, Inf], sis)
        sis = ut.array.get_indices(subs['halo.mass'], [1, Inf], sis)
        mass_ratio_t = [[] for _ in range(self.GHMassBin.vary.number)]
        self.mass_ratio = {
            'mass.now': copy.deepcopy(mass_ratio_t),
            'mass.peak': copy.deepcopy(mass_ratio_t),
            'mass.inf': copy.deepcopy(mass_ratio_t)
        }
        for vmi in range(self.GHMassBin.vary.number):
            gm_bin_limits, hm_bin_limits = self.GHMassBin.bins_limits(vmi)
            infd_weird_number = inff_weird_number = infd_tot_number = inff_tot_number = 0
            sis_m = ut.array.get_indices(subs[gm_kind], gm_bin_limits, sis)
            sis_m = ut.array.get_indices(subs['nearest.mass'], hm_bin_limits, sis_m)
            for inf_zi in range(ti_now + 1, ti_max + 1):
                # indices of those that were last satellite at zi
                sis_infd_z = ut.array.get_indices(subs['ilk.dif.zi'], inf_zi, sis_m)
                if sis_infd_z.size:
                    infd_sis = subs['ilk.dif.index'][sis_infd_z]
                    # select by mass of host halo just before ejection
                    # infd_sis, sis_infd_z = gen.get_indices(subs[inf_zi]['halo.mass'],
                    #hm_bin_limits, infd_sis, sis_infd_z)
                    eject_sis = subs[inf_zi]['child.index'][infd_sis]
                    eject_zi = inf_zi - 1
                    '''
                    if eject_zi > 2:
                        eject_sis = subs[eject_zi]['child.index'][eject_sis]
                        eject_zi = eject_zi - 1
                    '''
                    eject_sis, iis = ut.array.get_indices(
                        subs[eject_zi]['halo.mass'], [1, Inf], eject_sis, get_masks=True)
                    sis_infd_z = sis_infd_z[iis]
                    halo_m_now = subs['halo.mass'][sis_infd_z]
                    self.mass_ratio['mass.now'][vmi].extend(
                        halo_m_now - subs[eject_zi]['halo.mass'][eject_sis])
                    self.mass_ratio['mass.peak'][vmi].extend(
                        halo_m_now - subs[eject_zi]['mass.peak'][eject_sis])
                    infd_weird_number += eject_sis[subs[eject_zi]['halo.mass'][eject_sis] >
                                                   subs[eject_zi]['mass.peak'][eject_sis]].size
                    infd_tot_number += eject_sis.size
                # ids of those that were last central at zi
                sis_inff_zi = ut.array.get_indices(subs['sat.first.zi'], inf_zi, sis_m)
                if sis_inff_zi.size:
                    inff_sis = subs['sat.first.index'][sis_inff_zi]
                    inff_sis, iis = ut.array.get_indices(
                        subs[inf_zi]['halo.mass'], [1, Inf], inff_sis, get_masks=True)
                    sis_inff_zi = sis_inff_zi[iis]
                    halo_m_now = subs['halo.mass'][sis_inff_zi]
                    self.mass_ratio['mass.inf'][vmi].extend(
                        halo_m_now - subs[inf_zi]['halo.mass'][inff_sis])
                    inff_weird_number += inff_sis[subs[inf_zi]['halo.mass'][inff_sis] >
                                                  subs[inf_zi]['mass.peak'][inff_sis]].size
                    inff_tot_number += inff_sis.size
            self.say('M(eject) > M_peak(eject): %d of %d (%.2f)' %
                     (infd_weird_number, infd_tot_number, infd_weird_number / infd_tot_number))
            self.say('M(inf.first) > M_peak(inf.first): %d of %d (%.2f)' %
                     (inff_weird_number, inff_tot_number, inff_weird_number / inff_tot_number))

        Statistic = ut.statistic.StatisticClass()
        self.mass_ratio_stat = {}
        for k in self.mass_ratio:
            self.mass_ratio_stat[k] = {}
            for vmi in range(self.GHMassBin.vary.number):
                self.mass_ratio[k][vmi] = 10 ** np.array(self.mass_ratio[k][vmi], float32)
                ut.array.append_dictionary(
                    self.mass_ratio_stat[k], Statistic.get_distribution_dict(
                        self.mass_ratio[k][vmi], mass_ratio_limits, bin_number))
        self.plot()

    def plot(self, stat_kind='prob.cum', mass_ratio_limits=[0, 1.5]):
        '''
        Import mass statistic to plot, x-axis limit.
        '''
        Plot = plot_sm.PlotClass()
        if stat_kind == 'prob.cum':
            Plot.axis.lim_y = [0, 1]
            # Plot.label.pos_y = 0.7
        elif stat_kind == 'probability':
            Plot.axis.lim_y = self.mass_ratio_stat['mass.now'][stat_kind]
            # Plot.label.pos_y = 0.9
        else:
            raise ValueError('not recognize stat kind = %s' % stat_kind)
        Plot.set_axis('linear', 'linear', mass_ratio_limits, y_value_min=0)
        Plot.set_axis_label('M_{halo}(z=0)/M_{halo}(z_{eject})', 'P(<\,ratio)')  # 'dP/dM')
        Plot.make_window()
        Plot.set_point(
            color_number=self.GHMassBin.vary.number, line_number=self.GHMassBin.vary.number)
        Plot.set_label(line_length=1.3)
        Plot.make_label(self.GHMassBin.fix.mass_kind, self.GHMassBin.fix.limits)
        # if hm_limits[0] > 1:
        #    Plot.make_label('halo.mass', hm_limits)
        for vmi in range(self.GHMassBin.vary.number):
            Plot.draw('c', self.mass_ratio_stat['mass.now']['value'][vmi],
                      self.mass_ratio_stat['mass.now'][stat_kind][vmi], ct=vmi, lt=vmi)
            Plot.make_label(self.GHMassBin.vary.mass_kind, self.GHMassBin.vary.get_bin_limits(vmi))
            Plot.draw('c', self.mass_ratio_stat['mass.peak']['value'][vmi],
                      self.mass_ratio_stat['mass.peak'][stat_kind][vmi], ct=vmi, lt=vmi)

MassDistrEjected = MassDistrEjectedClass()


def plot_mass_offset_v_mass_sat(
    subs, ti_now=1, ti_max=15, axis_mass_kind='star.mass', mass_limits=[9, 11], mass_width=0.25,
    ratio_mass_kind='halo.mass', ilk='ejected', mass_grow_max='sat.first', use_history=True,
    MassGrow=None):
    '''
    Plot fraction by which satellites are offset in mass at zi.now compared with centrals.
    ** M_peak = 11.3, 14.5 -> M_star = 9.5, 11.5
    ** M_peak = 11, 14.6 -> M_star = 8.7, 11.5, dm = 0.2
    ** MassGrow = su.MassGrowClass(subs, 1, 15, 'mass.peak', [10, 15], 0.1, ilk='central')

    Import subhalo catalog, snapshot current & maximum, x-axis mass kind & range & bin width,
    mass kind to neig ratio of, ilk to track (sat, ejected),
    maximum M_peak(now) / M_peak(inf.dif) to include centrals as ejected
    (inf.first = use cut on inf.first, effectively = 1.5),
    whether to track history to neig M_peak at infall.
    '''
    inf_kind = 'sat.first'
    ref_mass_kind = 'mass.peak'
    scatter = 0
    disrupt_mf = 0.007

    Say = ut.io.SayClass(plot_mass_offset_v_mass_sat)
    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    if axis_mass_kind == ref_mass_kind:
        mref_width = mass_width
    else:
        mref_width = 0.1
    mass_limits_exp = [mass_limits[0] - 2 * mass_width, Inf]
    if MassGrow is None:
        MassGrow = MassGrowClass(subs, ti_now, ti_max, ref_mass_kind, [10, 15], mref_width,
                                 ilk='central')
        Say.say('assign mass growth class')
    if 'star.mass' in axis_mass_kind or 'star.mass' in ratio_mass_kind:
        sham.assign_to_catalog(
            subs[ti_now], 'star.mass', scatter, disrupt_mf, sham_property=ref_mass_kind)
        Say.say('assign m_star: {} limits = [{:.1f}, {:.1f}]'.format(
                ref_mass_kind,
                ut.catalog.convert_mass(
                    subs, ti_now, axis_mass_kind, mass_limits[0], ref_mass_kind),
                ut.catalog.convert_mass(
                    subs, ti_now, axis_mass_kind, mass_limits[1], ref_mass_kind)))

    if ilk == 'ejected':
        sis = get_indices_ejected(
            subs, ti_now, ti_max, axis_mass_kind, mass_limits_exp, 'central', disrupt_mf,
            mass_grow_max)
    elif ilk == 'satellite':
        sis = ut.catalog.get_indices_subhalo(
            subs[ti_now], axis_mass_kind, mass_limits_exp, ilk='satellite', disrupt_mf=disrupt_mf)
        sis_ej = get_indices_ejected(
            subs, ti_now, ti_max, axis_mass_kind, mass_limits_exp, 'central', disrupt_mf,
            mass_grow_max)
        sis = np.union1d(sis, sis_ej)

    ref_masses_now = subs[ti_now][ref_mass_kind][sis]
    halo_masses_now = subs[ti_now]['halo.mass'][sis]
    # neigbhor mass and t_i at infall
    if not use_history:
        # assume mass_peak has not changed
        inf_tis = subs[ti_now][inf_kind + '.ti'][sis]
        ref_masses_inf = ref_masses_now
    elif use_history:
        # track history to neig M_peak at infall
        inf_tis = subs[ti_now][inf_kind + '.ti'][sis]
        inf_sis = subs[ti_now][inf_kind + '.index'][sis]
        ref_masses_inf = np.zeros(halo_masses_now.size)
        for zi in range(ti_now + 1, ti_max + 1):
            siis = ut.array.get_indices(inf_tis, zi)
            if siis.size:
                ref_masses_inf[siis] = subs[zi][ref_mass_kind][inf_sis[siis]]
        # for those that fell in before zi.max, use current M_peak
        siis = ut.array.get_indices(inf_tis, [ti_max + 1, Inf])
        ref_masses_inf[siis] = subs[ti_now][ref_mass_kind][sis[siis]]
        Say.say('%d of %d (%.2f) fell in before z_i_max' %
                (siis.size, sis.size, siis.size / sis.size))
    # neighbor log ratio m_ref_sat(t_i_now) / m_ref_cen(t_i_now)
    log_mass_halo_ratios = np.zeros(halo_masses_now.size)
    log_mass_ref_ratios = np.zeros(halo_masses_now.size)
    for zi in range(ti_now + 1, ti_max + 1):
        siis = ut.array.get_indices(inf_tis, zi)
        if siis.size:
            log_mass_ref_ratios[siis] = (ref_masses_now[siis] - ref_masses_inf[siis] -
                                         MassGrow.value(zi, ref_masses_inf[siis]))
            log_mass_halo_ratios[siis] = (halo_masses_now[siis] - ref_masses_inf[siis] -
                                          MassGrow.value(zi, ref_masses_inf[siis]))
    # for those that fell in before zi.max, use median central mass growth since zi.max
    siis = ut.array.get_indices(inf_tis, [ti_max + 1, Inf])
    log_mass_ref_ratios[siis] = (ref_masses_now[siis] - ref_masses_inf[siis] -
                                 MassGrow.value(ti_max, ref_masses_inf[siis]))
    log_mass_halo_ratios[siis] = (halo_masses_now[siis] - ref_masses_inf[siis] -
                                  MassGrow.value(ti_max, ref_masses_inf[siis]))
    # compile bin stats ----------
    # re-do SHAM, given reference mass growth
    if 'star.mass' in axis_mass_kind or 'star.mass' in ratio_mass_kind:
        masses_star_now_def = subs[ti_now]['star.mass'][sis]
        ref_mass_kind_mod = ref_mass_kind + '.mod'
        for zi in range(ti_now + 1):
            subs[zi][ref_mass_kind_mod] = []
        subs[ti_now][ref_mass_kind_mod] = subs[ti_now][ref_mass_kind].copy()
        subs[ti_now][ref_mass_kind_mod][sis] -= log_mass_ref_ratios
        sham.assign_to_catalog(
            subs[ti_now], 'star.mass', scatter, disrupt_mf, sham_property=ref_mass_kind_mod)
        masses_star_now_mod = subs[ti_now]['star.mass'][sis]
        del(subs[zi][ref_mass_kind_mod])
    if axis_mass_kind == 'star.mass':
        axis_ms = masses_star_now_def
        # axis_ms = masse_star_now_mod
    elif axis_mass_kind == ref_mass_kind:
        axis_ms = ref_masses_now
    elif axis_mass_kind == 'halo.mass':
        axis_ms = halo_masses_now
    if ratio_mass_kind == 'star.mass':
        mass_ratios = 10 ** (masses_star_now_def - masses_star_now_mod)
        # mass_ratios = 10 ** (masses_star_now_mod - masses_star_now_def) - 1
    elif ratio_mass_kind == ref_mass_kind:
        mass_ratios = 10 ** log_mass_ref_ratios
        # mass_ratios = 10 ** -log_mass_ref_ratios - 1
    elif ratio_mass_kind == 'halo.mass':
        mass_ratios = 10 ** log_mass_halo_ratios
    elif ratio_mass_kind == 'star.mass/halo.mass':
        mass_ratios = 10 ** (masses_star_now_mod - halo_masses_now)
        # mass_ratios = masses_star_now_mod
        # all centrals for comparison
        sis_cen = ut.catalog.get_indices_subhalo(
            subs[ti_now], axis_mass_kind, mass_limits, ilk='central', disrupt_mf=disrupt_mf)
        mass_ratios_cen = 10 ** (subs[ti_now]['star.mass'][sis_cen] -
                                 subs[ti_now]['halo.mass'][sis_cen])
        # mass_ratios_cen = subs[ti_now]['star.mass'][sis_cen]
        axis_masses_cen = subs[ti_now][axis_mass_kind][sis_cen]
        stat_cen = MassBin.get_statistics_of_array(axis_masses_cen, mass_ratios_cen)
    stat = MassBin.get_statistics_of_array(axis_ms, mass_ratios)
    print('number in bin = %s' % stat['number'])
    print(stat['median'])

    # plot ----------
    if ratio_mass_kind == 'star.mass/halo.mass':
        Plot = plot_sm.PlotClass(window_mov_x=0.05)
        Plot.set_axis('log', 'linear', mass_limits, [0, 0.07])
        # Plot.set_axis('log', 'log', mass_limits, [8, 11])
        Plot.set_axis_label(axis_mass_kind + '.z0', 'M_{star}/M_{halo}(z=0)')
        # Plot.set_axis_label(axis_mass_kind + '.z0', 'star.mass.z0')
        Plot.make_window()
        Plot.set_label()  # 0.4)
        Plot.draw('c', stat['x'], stat['median'], ct='blue')
        Plot.make_label('ejected satellite')
        Plot.draw('c', stat_cen['x'], stat_cen['median'], ct='red', lt='--')
        Plot.make_label('all central')
    else:
        Plot = plot_sm.PlotClass()
        Plot.set_axis('log', 'linear', mass_limits, [0, 1])
        if ratio_mass_kind == 'halo.mass':
            label_m_kind = 'halo'
        elif ratio_mass_kind == 'mass.peak':
            label_m_kind = 'max'
        elif ratio_mass_kind == 'star.mass':
            label_m_kind = 'star'
        Plot.set_axis_label(axis_mass_kind + '.z0',
                            'M_{%s,ejected}/M_{%s,cen}' % (label_m_kind, label_m_kind))
        # '\Delta %s/%s(z=0)' % (plot.get_m_label(mass_kind), ut.plot.get_m_label(mass_kind))
        Plot.make_window()
        Plot.fill(stat['x'], [stat['percent.16'], stat['percent.84']], ct='blue.lite2')
        Plot.draw('c', stat['x'], stat['median'], ct='blue')
        Plot.make_window(erase=False)


def plot_mass_function_ejected(
    subs, hals, ti_now=1, ti_max=23, hm_limits=[14.9, 15.1], mass_ratio_limits=[-4.5, 0],
    mass_ratio_width=0.25):
    '''
    Plot unevolved mass function of ejected satellites.
    '''
    sis = ut.array.get_indices(hals[ti_now]['mass.fof'], hm_limits)
    sat_ms = []  # mass of satellite halo at infall
    sat_in_halos = []  # index of host halo of satellite at zi.now
    for si in sis:
        hi_par = hals[ti_now]['parent.index'][si]
        for zi in range(ti_now + 1, ti_max + 1):
            if hi_par <= 0:
                break
            hi_parn = hals[zi]['parent.n.index'][hi_par]
            while hi_parn >= 0:
                si_cen = hals[zi]['central.index'][hi_parn]
                if si_cen > 0:
                    sat_ms.append(hals[zi]['mass.fof'][hi_parn] - hals[ti_now]['mass.fof'][si])
                    si_chi_now = ut.catalog.get_indices_tree(subs[zi], ti_now, si_cen)
                    if si_chi_now > 0:
                        if subs[ti_now]['halo.index'][si_chi_now] == si:
                            sat_in_halos.append(1)
                        else:
                            sat_in_halos.append(0)
                    else:
                        sat_in_halos.append(-1)
                hi_parn = hals[zi]['parent.n.index'][hi_parn]
            hi_par = hals[zi]['parent.index'][hi_par]
    sat_ms = np.array(sat_ms)
    sat_in_halos = np.array(sat_in_halos)
    MassBin = ut.binning.BinClass(mass_ratio_limits, mass_ratio_width)
    sat_numbers = np.histogram(sat_ms, MassBin.number, mass_ratio_limits, False)[0]
    dndlogms = Fraction.get_fraction(sat_numbers, sis.size) / mass_ratio_width
    log_dndlogms = log10(dndlogms + 1e-10)
    sat_ms_in_halos = sat_ms[sat_in_halos != 0]
    sat_numbers_in_halo = np.histogram(sat_ms_in_halos, MassBin.number, mass_ratio_limits, False)[0]
    dndlogms_in_halo = Fraction.get_fraction(sat_numbers_in_halo, sis.size) / mass_ratio_width
    log_dndlogms_in_halo = log10(dndlogms_in_halo + 1e-10)

    # plot ----------
    Plot = plot_sm.PlotClass(panel_number_y=-2)
    Plot.set_axis('log', 'log', mass_ratio_limits, [-1, 4])
    Plot.make_panel()
    Plot.draw('c', MassBin.mids, log_dndlogms, ct='blue')
    Plot.draw('c', MassBin.mids, log_dndlogms_in_halo, ct='red')
    # ratio
    Plot.set_axis('log', 'linear', mass_ratio_limits, [0.5, 1.1])
    Plot.set_axis_label('Mass ratio', 'Fraction')
    Plot.tick.loc = [-1, 10, 0, 0]
    Plot.make_panel()
    Plot.draw('c', MassBin.mids, dndlogms_in_halo / dndlogms, ct='blue')


class MassDistanceHaloNeigClass(ut.io.SayClass):
    '''
    Get mass statistics for neighbor galaxies in bins of galaxy mass & halo-centric distance.
    '''
    def assign_to_self(
        self, subs, hals, gal=None, gam=None, ti_now=1, ti_max=15,
        gm_kind='star.mass', gm_limits=[9.7, 10.5], gm_width=0.8, hm_limits=[14, 15], hm_width=1,
        vary_kind='galaxy', ilk='satellite', disrupt_mf=0.007, measure_m_kind='halo.mass',
        distance_halo_limits=[1, 5], dis_tvir_width=0.25, distance_halo_scaling='linear',
        dimension_number=3, space='real', neig_vel_dif_factor=0.25, neig_number_max=-1,
        evol_values=-2, sfr_pop='lo', correct_m_ej=True, use_history=True, MassGrow=None):
        '''
        Store mass v halo-centric distance.

        Import subhalo & halo catalog, galaxy & galaxy mock catalog, snapshot index,
        neighbor galaxy mass kind & range & bin width,
        center halo mass range & bin width, which mass to vary, neighbor ilk, disrupt threshold,
        which mass to measure,
        distance / R_200mass range & bin width & scaling (lin, log) & number of spatial dimensions,
        position space (real, red, proj),
        maximum neighbor redshift difference in terms of halo virial velocity (if space = proj),
        neighbor number maximum, central v satellite mass growth class.
        '''
        from observation.sdss import sdss_analysis

        scatter = 0.0
        m_bin_number = 10
        virial_kind = '200m'
        if neig_number_max <= 0:
            if hm_limits[0] >= 14:
                neig_number_max = 2000
            elif hm_limits[0] >= 13:
                neig_number_max = 1500
            elif hm_limits[0] >= 12:
                neig_number_max = 250
            else:
                neig_number_max = 200

        subs = subs[ti_now]
        hal = hals[ti_now]
        self.redshift = subs.snapshot['redshift']
        # compile relevant classes
        HaloProperty = ut.halo_property.HaloPropertyClass(subs.Cosmology)
        self.DistanceBin = ut.binning.DistanceBinClass(
            distance_halo_scaling, distance_halo_limits, width=dis_tvir_width,
            dimension_number=dimension_number)
        self.GHMassBin = ut.binning.GalaxyHaloMassBinClass(
            gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)

        # assign SFRs
        if gam is not None and gal is not None:
            # from observation import galaxy
            redshift_limits = [0, 0.06]
            sat_distance_halo_limits = [0.04, 1]  # for obtaining empirical evolution value
            sfr_kind = 'ssfr'
            sfr_bimod_limits = ut.catalog.get_sfr_bimodal_limits(sfr_kind)
            sfr_ini_kind = 'sat.first'
            evol_qufrac_sat = False
            evol_kinds = 'sat.first.t'
            quench_taus = 'default'
            GHMassBin_nohm = ut.binning.GalaxyHaloMassBinClass(
                gm_kind, gm_limits, gm_width, 'halo.mass', [1, Inf], Inf, vary_kind)
            SfrSat = sdss_analysis.SfrSatelliteClass(
                gal, subs, sfr_kind, gm_kind, redshift_limits=redshift_limits)
            if evol_values == 'default' and vary_kind == 'galaxy':
                evol_values = SfrSat.quench_times_default(
                    't-delay', evol_kinds, quench_taus, self.GHMassBin)
            if np.isscalar(evol_values) and evol_values < 0:
                # neig evolution value by matching
                SfrSat.assign_to_self(
                    gal, gam, subs, hal, self.GHMassBin, sfr_ini_kind, evol_qufrac_sat, evol_kinds,
                    evol_values, quench_taus, sat_distance_halo_limits, evol_ejected=True)
                evol_values = ut.array.scalarize(SfrSat.evol_values)
            # apply evolution value to all galaxies in galaxy mass range
            SfrSat.assign_to_self(
                gal, gam, subs, hal, GHMassBin_nohm, sfr_ini_kind, evol_qufrac_sat, evol_kinds,
                evol_values, quench_taus, sat_distance_halo_limits, evol_ejected=True)

        if gm_kind == 'star.mass' and correct_m_ej:
            ref_mass_kind = 'mass.peak'
            mass_stars_input = subs[gm_kind]
            sis_sat = ut.catalog.get_indices_subhalo(
                subs[ti_now], 'mass.peak', [10.8, Inf], None, 'satellite', disrupt_mf)
            sis_ej = get_indices_ejected(
                subs, ti_now, ti_max, 'mass.peak', [10.8, Inf], 'central', disrupt_mf,
                mass_grow_max=1.5)
            sis_sat = np.unique(np.concatenate([sis_sat, sis_ej]))
            ref_masses_now = subs[ref_mass_kind][sis_sat]

            # neighbor mass and t_i at infall
            if not use_history:
                # assume that mass_peak has not changed
                inf_tis = subs['sat.first.zi'][sis_sat]
                ref_masses_inf = ref_masses_now
            else:
                # track history to neighbor mass_peak at infall
                inf_tis = subs['sat.first.zi'][sis_sat]
                inf_sis = subs['sat.first.index'][sis_sat]
                ref_masses_inf = np.zeros(ref_masses_now.size)
                for zi in range(ti_now + 1, ti_max + 1):
                    siis = ut.array.get_indices(inf_tis, zi)
                    ref_masses_inf[siis] = subs[zi][ref_mass_kind][inf_sis[siis]]
                # for those that fell in before t_i_max, use current mass_peak
                siis = ut.array.get_indices(inf_tis, [ti_max + 1, Inf])
                ref_masses_inf[siis] = subs[ref_mass_kind][sis_sat[siis]]
                self.say('%d of %d (%.2f) fell in before zi.max' %
                         (siis.size, sis_sat.size, siis.size / sis_sat.size))

            # update stellar mass of ejected satellites
            if MassGrow is None:
                MassGrow = MassGrowClass(
                    subs, ti_now, ti_max, 'mass.peak', [10, 15], 0.1, ilk='central')
            # neig log ratio M_ref_sat(zi.now) / M_ref_cen(zi.now)
            log_mass_ref_ratios = np.zeros(ref_masses_now.size)
            for zi in range(ti_now + 1, ti_max + 1):
                siis = ut.array.get_indices(inf_tis, zi)
                if siis.size:
                    log_mass_ref_ratios[siis] = (ref_masses_now[siis] - ref_masses_inf[siis] -
                                                 MassGrow.value(zi, ref_masses_inf[siis]))
            # for those that fell in before zi.max, use median central mass growth since zi.max
            siis = ut.array.get_indices(inf_tis, [ti_max + 1, Inf])
            log_mass_ref_ratios[siis] = (ref_masses_now[siis] - ref_masses_inf[siis] -
                                         MassGrow.value(ti_max, ref_masses_inf[siis]))
            # sham
            ref_mass_kind_mod = ref_mass_kind + '.mod'
            subs[ref_mass_kind_mod] = subs[ref_mass_kind].copy()
            subs[ref_mass_kind_mod][sis_sat] -= log_mass_ref_ratios
            sham.assign_to_catalog(
                subs[ti_now], gm_kind, scatter, disrupt_mf, sham_property=ref_mass_kind_mod)
            del(subs[ref_mass_kind_mod])
        # neighbor number in mass range
        sis_cen = ut.catalog.get_indices_subhalo(subs, gm_kind, [1, Inf], hm_limits, 'central')
        self.say('halo number = %d' % sis_cen.size)
        self.MassDistanceHalo = ut.statistic.StatisticClass()
        self.MassRatDistanceHalo = ut.statistic.StatisticClass()
        self.MassAll = ut.statistic.StatisticClass()
        self.MassIso = ut.statistic.StatisticClass()
        for vmi in range(self.GHMassBin.vary.number):
            gm_bin_limits, hm_bin_limits = self.GHMassBin.bins_limits(vmi)
            # mass for all central galaxies
            sis_ref = ut.catalog.get_indices_subhalo(
                subs, gm_kind, gm_bin_limits, None, 'central', disrupt_mf=disrupt_mf)
            # just those that are isolated or never have been a satellite
            sis_iso = ut.array.get_indices(subs['ilk.dif.zi'], -1, sis_ref)
            # sis_iso = gen.get_indices(subs['nearest.distance/Rneig'], [3, Inf], sis_ref)
            if gam is not None and gal is not None:
                sis_ref = ut.array.get_indices(subs[sfr_kind], sfr_bimod_limits[sfr_pop], sis_ref)
                sis_iso = ut.array.get_indices(subs[sfr_kind], sfr_bimod_limits[sfr_pop], sis_iso)
            self.MassAll.append_dictionary(
                10 ** subs[measure_m_kind][sis_ref], bin_number=m_bin_number)
            self.MassIso.append_dictionary(
                10 ** subs[measure_m_kind][sis_iso], bin_number=m_bin_number)
            # self.say('%s ave of isolated = %.2f' % (gm_kind, np.mean(subs[gm_kind][sis_ref])))
            # neighbors in distance bins
            rad_vir_max = HaloProperty.get_radius_virial(
                virial_kind, hm_bin_limits[1], redshift=subs.snapshot['redshift'])
            self.say('R_halo max = %.2f kpc comoving: find neighbors out to %.1f kpc comoving' %
                     (rad_vir_max, rad_vir_max * distance_halo_limits[1]))
            center_sis = ut.catalog.get_indices_subhalo(
                subs, gm_kind, [1, Inf], hm_bin_limits, 'central')
            neig_sis = ut.catalog.get_indices_subhalo(
                subs, gm_kind, gm_bin_limits, [1, Inf], ilk, disrupt_mf)
            if gam is not None and gal is not None:
                neig_sis = ut.array.get_indices(subs[sfr_kind], sfr_bimod_limits[sfr_pop], neig_sis)
            # scale redshift cut to virial velocity of halo mass upper limit
            if space == 'proj' and neig_vel_dif_factor > 0:
                # vel_vir_max = halo.velocity_vir(virial_kind, hm_bin_limits[1],
                #redshift=subs.snapshot['redshift'])
                # neig_vel_dif_maxs = neig_vel_dif_factor * vel_vir_max
                # self.say('use neighbor velocity difference maximum = %.1f' % neig_vel_dif_maxs)
                halo_is = subs['halo.index'][center_sis]
                vel_virs = HaloProperty.velocity_vir_catalog(virial_kind, hal, halo_is)
                neig_vel_dif_maxs = neig_vel_dif_factor * vel_virs
            else:
                neig_vel_dif_maxs = None

            # neighbor distance maximum of sample
            neig_distance_max = self.DistanceBin.limits[1] * rad_vir_max
            neig = ut.catalog.get_catalog_neighbor(
                subs, gm_kind, [1, Inf], hm_bin_limits, 'central', neig_number_max=neig_number_max,
                neig_distance_max=neig_distance_max, distance_kind='comoving', distance_space=space,
                neig_velocity_dif_maxs=neig_vel_dif_maxs, center_indices=center_sis,
                neig_indices=neig_sis)
            halo_is = subs['halo.index'][neig['self.index']]
            rad_virs = HaloProperty.radius_virial_catalog(virial_kind, hal, halo_is)
            # scale distances to halo virial radius
            neig_dists = []
            for ni, neig_dists in enumerate(neig['indices']):
                if neig_dists.size:
                    neig_dists.extend(neig_dists / rad_virs[ni])
            dist_bin_is = self.DistanceBin.get_bin_indices(neig_dists, warn_outlier=False)
            neig_sis = np.concatenate(neig['indices'])
            MassDistanceHaloM = ut.statistic.StatisticClass()
            MassRatioDistanceHaloM = ut.statistic.StatisticClass()
            for dist_i in range(self.DistanceBin.number):
                sis_d = neig_sis[dist_bin_is == dist_i]
                MassDistanceHaloM.append_dictionary(
                    10 ** subs[measure_m_kind][sis_d], bin_number=m_bin_number)
                MassRatioDistanceHaloM.append_dictionary(
                    10 ** (subs['mass.bound'][sis_d] - subs['mass.peak'][sis_d]),
                    bin_number=m_bin_number)
                # self.say('%s ave in dist bin = %.2f' % (gm_kind, np.mean(subs[gm_kind][sis_d])))
            self.MassDistanceHalo.append_class_to_dictionary(MassDistanceHaloM)
            self.MassRatDistanceHalo.append_class_to_dictionary(MassRatioDistanceHaloM)
        if gm_kind == 'star.mass' and correct_m_ej:
            # assign back input stellar masses
            subs[gm_kind] = mass_stars_input
        self.plot_satellite()

    def plot_ejected(
        self, MassDistanceHaloNeig=None, C2=None, stat_kind='average', axis_y_limits=[0.45, 1]):
        '''
        Plot.

        Import class storing mass v halo-centric distance for neighbors.
        '''
        if MassDistanceHaloNeig is not None:
            self.__dict__ = MassDistanceHaloNeig.__dict__
        self.MassRef = self.MassIso
        if stat_kind == 'average':
            error_kind = 'sem'
        elif stat_kind == 'median':
            error_kind = 'sem'

        Plot = plot_sm.PlotClass()
        if 'log' in self.DistanceBin.scaling:
            Plot.set_axis(
                self.DistanceBin.scaling, 'linear', self.DistanceBin.log_limits, axis_y_limits)
            xs = self.DistanceBin.log_mids
        else:
            Plot.set_axis(
                self.DistanceBin.scaling, 'linear', self.DistanceBin.limits, axis_y_limits,
                tick_location=[0.5, 1, 0, 0])
            xs = self.DistanceBin.mids

        Plot.set_axis_label(
            ut.plot.get_label_distance(
                'central.distance/Rneig', self.DistanceBin.dimension_number, '200m'),
            '\\bar{M}_{halo}(d)/\\bar{M}_{halo}(isolated)')
        Plot.make_window()
        Plot.set_label(0.35, 0.4, line_length=1.5, move_value=0.09)
        Plot.set_point(color_number=self.GHMassBin.vary.number)
        # Plot.draw('c', [1, 1], [-2, 2], ct='def', lt='--')
        # Plot.make_label(self.GHMassBin.fix.mass_kind, self.GHMassBin.fix.limits, point_kind='')
        Plot.make_label('host halo', self.GHMassBin.fix.limits, point_kind='')

        for vmi in range(self.GHMassBin.vary.number):
            Plot.fill_err(
                xs, self.MassDistanceHalo.stat[stat_kind][vmi] / self.MassRef.stat[stat_kind][vmi],
                self.MassDistanceHalo.stat[error_kind][vmi] / self.MassRef.stat[stat_kind][vmi],
                ct=vmi - 0.2)
            if C2:
                Plot.fill_err(
                    xs, C2.MassDistanceHalo.stat[stat_kind][vmi] / C2.MassRef.stat[stat_kind][vmi],
                    C2.MassDistanceHalo.stat[error_kind][vmi] / C2.MassRef.stat[stat_kind][vmi],
                    ct='red.lite2')

        for vmi in range(self.GHMassBin.vary.number):
            ratios = self.MassDistanceHalo.stat[stat_kind][vmi] / self.MassRef.stat[stat_kind][vmi]
            Plot.draw('c', xs, ratios, ct=vmi, lt='-')
            if self.GHMassBin.vary.number == 1:
                point_kind = ''
                ct = 'def'
            else:
                point_kind = 'self'
                ct = None
            Plot.make_label(self.GHMassBin.vary.mass_kind, self.GHMassBin.vary.get_bin_limits(vmi),
                            point_kind=point_kind, ct=ct)
            if C2:
                Plot.make_label('real-space')
                ratios = C2.MassDistanceHalo.stat[stat_kind][vmi] / C2.MassRef.stat[stat_kind][vmi]
                Plot.draw('c', xs, ratios, ct='red', lt='__')
                Plot.make_label('projected')

        # Plot.make_label('all\,centrals')
        # Plot.make_label('quiescent\,centrals')
        Plot.make_window(erase=False)
        """
        for vmi in range(self.GHMassBin.vary.number):
            for dist_i in range(self.DistanceBin.mids.size):
                print '%.3f %.4f %.4f' % (
                    self.DistanceBin.mids[dist_i],
                    self.MassDistanceHalo.stat['average'][vmi][dist_i] /
                    MassRef.stat['average'][vmi],
                    self.MassDistanceHalo.stat[error_kind][vmi][dist_i] /
                    MassRef.stat['average'][vmi])
        """

    def plot_satellite(
        self, MassDistanceHaloNeig=None, stat_kind='average', axis_y_limits=[0, 1], fit=False):
        '''
        Plot.

        Import class storing mass v halo-centric distance for neighbors.
        '''
        bin_number_min = 4

        if MassDistanceHaloNeig is not None:
            self.__dict__ = MassDistanceHaloNeig.__dict__
        self.MassRef = self.MassIso
        if stat_kind == 'average':
            error_kind = 'sem'
        elif stat_kind == 'median':
            error_kind = 'sem'

        Plot = plot_sm.PlotClass()

        if 'log' in self.DistanceBin.scaling:
            Plot.set_axis(self.DistanceBin.scaling, 'linear', self.DistanceBin.log_limits,
                          axis_y_limits)
            x_values = self.DistanceBin.log_mids
        else:
            Plot.set_axis(self.DistanceBin.scaling, 'linear', self.DistanceBin.limits,
                          axis_y_limits, tick_location=[0.5, 1, 0, 0])
            x_values = self.DistanceBin.mids

        Plot.set_axis_label(
            ut.plot.get_label_distance(
                'central.distance/Rcen', self.DistanceBin.dimension_number, '200m'),
            '%s M_{(sub)halo}(d)/M_{max}' % stat_kind)
        Plot.make_window()
        # Plot.set_label(0.35, 0.4, line_length=1.5, move_value=0.09)
        Plot.set_point(color_number=self.GHMassBin.vary.number)
        Plot.make_label('z=%.2f' % self.redshift)
        Plot.make_label('host halo', self.GHMassBin.fix.limits)
        Plot.draw('c', [0, 0], [-2, 2], ct='def', lt='.')
        for vmi in range(self.GHMassBin.vary.number):
            # mass_ratios = (self.MassDistanceHalo.stat[stat_kind][vmi] /
            #                self.MassRef.stat[stat_kind][vmi]
            # errs = self.MassDistanceHalo.stat[error_kind][vmi] / self.MassRef.stat[stat_kind][vmi]
            mass_ratios = self.MassRatDistanceHalo.stat[stat_kind][vmi]
            errs = self.MassRatDistanceHalo.stat[error_kind][vmi]
            stds = self.MassRatDistanceHalo.stat['std'][vmi]
            if self.GHMassBin.vary.number == 1:
                draw_if = np.array(self.MassDistanceHalo.stat['number'][vmi]) >= bin_number_min
                Plot.fill_err(x_values, mass_ratios, stds, draw_if=draw_if, ct=vmi - 0.2)
            draw_if = np.array(self.MassDistanceHalo.stat['number'][vmi]) >= bin_number_min
            Plot.fill_err(x_values, mass_ratios, errs, draw_if=draw_if, ct=vmi - 0.1)

        for vmi in range(self.GHMassBin.vary.number):
            # mass_ratios = (self.MassDistanceHalo.stat[stat_kind][vmi] /
            #                self.MassRef.stat[stat_kind][vmi]
            mass_ratios = self.MassRatDistanceHalo.stat[stat_kind][vmi]
            draw_if = np.array(self.MassDistanceHalo.stat['number'][vmi]) >= bin_number_min
            Plot.draw('c', x_values, mass_ratios, draw_if=draw_if, ct=vmi, lt='-')
            if self.GHMassBin.vary.number == 1:
                point_kind = ''
                ct = 'def'
            else:
                point_kind = 'self'
                ct = None
            Plot.make_label(self.GHMassBin.vary.mass_kind, self.GHMassBin.vary.get_bin_limits(vmi),
                            point_kind=point_kind, ct=ct)
        if fit:
            params = [[0, -1, 1], [0.5, 0.01, 1], [0.5, 0.01, 1], [0.1, 0.01, 1]]
            xs_fit = np.arange(x_values.min(), x_values.max(), 0.01)
            for vmi in range(self.GHMassBin.vary.number):
                # mass_ratios = (self.MassDistanceHalo.stat[stat_kind][vmi] /
                #                self.MassRef.stat[stat_kind][vmi])
                mass_ratios = self.MassRatDistanceHalo.stat[stat_kind][vmi]
                Fit = ut.math.fit(Function.erf_AtoB, params, x_values, mass_ratios)
                print(Fit.params)
                ys = Function.erf_AtoB(xs_fit, Fit.params)
                Plot.draw('c', xs_fit, ys, ct=vmi, lt='.')
        Plot.make_window(erase=False)

MassDistanceHaloNeig = MassDistanceHaloNeigClass()


#===================================================================================================
# satellite fraction
#===================================================================================================
def plot_fraction_satellite_qu_v_mass(
    subs, zi=1, mass_kind='star.mass', mass_limits=[9, 12], mass_width=0.2, hm_limits=[1, Inf],
    disrupt_mf=0):
    '''
    Plot fraction of (quiescent) galaxies that are satellites v mass kind.

    Parameters
    ----------
    subhalo catalog across snapshots: list
    snapshot index: int
    mass kind: string
    mass limits: list
    mass bin width: float
    halo mass bin limits: list
    disruption threshhold: float
    '''
    Say = ut.io.SayClass(plot_satellite_fraction_v_mass)
    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    gal_is_sat_fracs = np.zeros(MassBin.number)
    gal_is_sat_fracs_err = np.zeros((MassBin.number, 2))
    qu_is_sat_fracs = np.zeros(MassBin.number)
    qu_is_sat_fracs_err = np.zeros((MassBin.number, 2))
    sub = subs[zi]
    for mi in range(MassBin.number):
        sis = ut.array.get_indices(sub[mass_kind], MassBin.get_bin_limits(mi))
        if disrupt_mf:
            sis = ut.array.get_indices(sub['mass.frac.min'], [disrupt_mf, Inf], sis)
        sis_cen = ut.catalog.get_indices_ilk(sub, 'central', sis)
        sis_sat = ut.catalog.get_indices_ilk(sub, 'satellite', sis)
        sis_sat = ut.array.get_indices(sub['halo.mass'], hm_limits, sis_sat)
        # include centrals that are ejected satellites
        sis_ej = get_indices_ejected(subs, zi, zi + 20, ilk='central', mass_grow_max='sat.first',
                                     sis=sis_cen)
        sis_sat = np.concatenate((sis_sat, sis_ej))
        sis_qu = ut.catalog.get_indices_sfr(sub, 'lo', 'ssfr', sis)
        sis_sat_qu = ut.catalog.get_indices_ilk(sub, 'satellite', sis_qu)

        gal_is_sat_fracs[mi], gal_is_sat_fracs_err[mi] = Fraction.get_fraction(
            sis_sat.size, sis_sat.size + sis_cen.size, 'beta')
        qu_is_sat_fracs[mi], qu_is_sat_fracs_err[mi] = Fraction.get_fraction(
            sis_sat_qu.size, sis_qu.size, 'beta')
        Say.say('m %5.3f | sat.number %d | sat.frac %.3f | qu.number %d' %
                (MassBin.mids[mi], sis_sat.size, gal_is_sat_fracs[mi], sis_qu.size))

    qu_is_sat_fracs[[0, 1, 2]] = [0.98, 0.84, 0.72]
    gal_is_sat_fracs_err[-1] = 0

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', mass_limits, [0, 1.0])
    Plot.set_axis_label(mass_kind, 'Fraction')
    Plot.make_window()
    # Plot.set_label(0.7)
    Plot.set_point(color_number=2)
    Plot.draw('c', MassBin.mids, gal_is_sat_fracs, gal_is_sat_fracs_err, ct='def')
    Plot.make_label('fraction\,of\,all\,are\,satellites', position_x=0.15, small=2)
    #Plot.draw('c', MassBin.mids, qu_is_sat_fracs, qu_is_sat_fracs_err, qu_is_sat_fracs, ct='red',
    #          lt='__')
    #Plot.make_label('fraction\,of\,quiescent\,are\,satellites', small=2)


def plot_satellite_fraction_v_mass(
    cats, tis=[], mass_kind='mass.peak', mass_limits=[], mass_width=0.2, hm_limits=[1, Inf],
    disrupt_mfs=0):
    '''
    Plot satellite fraction v mass kind.

    Import subhalo catalog[s], snapshot index[s], mass kind and range and bin width, disrupt thresh.
    '''
    # labels = ['fof6d', 'fof6d+ejected']
    # labels = ['subhalo', 'mock', 'galaxy']
    Say = ut.io.SayClass(plot_satellite_fraction_v_mass)
    if mass_kind in cats:
        cats = [cats]
    tis, disrupt_mfs = ut.array.arrayize((tis, disrupt_mfs), repeat_number=len(cats))
    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    sat_fracs, sat_fracs_err = np.zeros((2, len(cats), MassBin.number))
    for cat_i in range(len(cats)):
        cats = cats[cat_i]
        cat = cats[tis[cat_i]]
        for mi in range(MassBin.number):
            cis = ut.array.get_indices(cat[mass_kind], MassBin.get_bin_limits(mi))
            if disrupt_mfs[cat_i]:
                cis = ut.array.get_indices(cat['mass.frac.min'], [disrupt_mfs[cat_i], Inf], cis)
            cis_cen = ut.catalog.get_indices_ilk(cat, 'central', cis)
            cis_sat = ut.catalog.get_indices_ilk(cat, 'satellite', cis)
            cis_sat = ut.array.get_indices(cat['halo.mass'], hm_limits, cis_sat)
            sat_number = cis_sat.size
            # include centrals that are ejected satellites
            sat_number += get_indices_ejected(cats, tis[cat_i], tis[cat_i] + 20, ilk='central',
                                              mass_grow_max='sat.first', sis=cis_cen).size
            sat_fracs[cat_i, mi], sat_fracs_err[cat_i, mi] = Fraction.get_fraction(
                sat_number, cis_sat.size + cis_cen.size, 'normal')
            Say.say('m %5.3f | sat.frac %.3f | sat.number %d' %
                    (MassBin.mids[mi], sat_fracs[cat_i, mi], sat_number))

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('linear', 'linear', mass_limits, [0, 0.4])
    Plot.set_axis_label(mass_kind, 'Satellite fraction')
    Plot.make_window()
    # Plot.set_label(0.7)
    Plot.set_point(color_number=len(cats))
    for cat_i in range(len(cats)):
        x_offset = 0.05 * cat_i
        Plot.draw('c', MassBin.mids + x_offset, sat_fracs[cat_i], sat_fracs_err[cat_i],
                  sat_fracs[cat_i], ct=cat_i)
        # Plot.make_label(labels[cat_i])


def plot_satellite_frac_v_mass_v_redshift(
    subs, mass_kind='star.mass', mass_limits=[9.5, 11.5], mass_width=0.5, disrupt_mf=0.007,
    axis_x_kind='redshift', source='li-drory-marchesini'):
    '''
    Plot satellite fraction v mass / z in bins of z / mass.

    Import subhalo catalog, mass kind & range & bin width, disrupt mass fraction,
    x-axis kind (mass, redshift), SHAM source.
    '''
    redshifts = np.array([0.05, 0.3, 0.5, 0.7, 0.9])
    scatter = 0.15  # for SHAM

    Say = ut.io.SayClass(plot_satellite_frac_v_mass_v_redshift)
    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    tis = ut.binning.get_bin_indices(redshifts, subs.snapshot['redshift'], round_kind='near')
    # redshifts = np.array(subs.snapshot['redshift'][tis])
    sat_fracs = np.zeros((redshifts.size, MassBin.number))
    sat_fracs_unc = np.zeros((redshifts.size, MassBin.number))
    for zii, zi in enumerate(tis):
        Say.say('z.i %d' % tis[zii])
        if mass_kind in ['star.mass', 'mag.r']:
            sham.assign_to_catalog(subs[zi], mass_kind, scatter, disrupt_mf, source)
        for mi in range(MassBin.number):
            sis = ut.array.get_indices(subs[tis[zii]][mass_kind], MassBin.get_bin_limits(mi))
            sis_sat = ut.catalog.get_indices_ilk(subs[tis[zii]], 'satellite', sis)
            # impose decrease with mass at high mass
            # if gm_kind == 'mass.peak' and mass > 12.0 and sf > sat_fracs[zi][-1]:
            #    sf = sat_fracs[zi][-1]
            sat_fracs[zii, mi], sat_fracs_unc[zii, mi] = Fraction.get_fraction(
                sis_sat.size, sis.size, 'normal')
            Say.say('m %5.2f | sat-frac %.3f number %d' %
                    (MassBin.mids[mi], sat_fracs[zii][mi], sis_sat.size))
    # add points for m_star - m_max relation
    '''
    m_peak_refs = np.array([11.5, 12.0, 12.5, 13.0])
    if gm_kind in ['star.mass', 'mag.r'):
        masses_star_ref = np.zeros((redshifts.size, vals.size))
        sat_fracs_ref = np.zeros((redshifts.size, vals.size))
        for zii in range(tis.size):
            satfrac_m_spl = interpolate.splrep(vals, sat_fracs[zii])
            for mi in range(m_peak_refs.size):
                masses_star_ref[zii][mi] = gen.convert_mass(
                    subs, tis[zii], m_peak_refs[mi], 'mass.peak', 'star.mass')
                sat_fracs_ref[zii][mi] = interpolate.splev(m_star_refs[zii][mi], satfrac_m_spl)'
    '''
    # plot v mass
    if axis_x_kind == 'mass':
        # fit
        params = [[2.0, 0, 6], [-0.160, -0.161, -0.159]]
        Fits = []
        for zii in range(tis.size):
            # not try to fit where sat_frac ~ 0
            sis = ut.array.get_indices(sat_fracs[zii], [0.02, Inf])
            Fits.append(ut.math.fit(Function.line, params, MassBin.mids[sis],
                                    sat_fracs[zii][sis]))
            print(Fits[zii].params)
        xs_fit = np.array([5, 15])

        # plot ----------
        Plot = plot_sm.PlotClass()
        Plot.set_axis('log', 'linear', mass_limits, [0, 0.4])
        Plot.set_axis_label(mass_kind, 'Satellite fraction')
        Plot.make_window()
        Plot.set_label(0.7)
        for zii in range(tis.size):
            Plot.draw('c', MassBin.mids, sat_fracs[zii], lt=zii, ct='def')
            Plot.make_label('z= % .1f' % redshifts[zii])
            Plot.draw('c', xs_fit, Function.line(xs_fit, Fits[zii].params), ct='green',
                      lt='.')
            # if gm_kind == 'star.mass':
            #    Plot.draw('pp', m_star_refs[zi], sat_frac_refs[zi], ct='default')
    # plot v redshift
    elif axis_x_kind == 'redshift':
        sat_fracs = sat_fracs.transpose()
        sat_fracs_unc = sat_fracs_unc.transpose()
        # fit
        params = [[0.5, 0., 1.], [-0.160, -2., -0.]]
        Fits = []
        for mi in range(MassBin.number):
            # not try to fit where sat_frac ~ 0
            sis = ut.array.get_indices(sat_fracs[mi], [0.02, Inf])
            Fits.append(ut.math.fit(Function.line, params, redshifts[sis],
                                    sat_fracs[mi][sis]))
            print(Fits[mi].params)
        xs_fit = np.arange(0, 1.1, 0.01)

        # plot ----------
        Plot = plot_sm.PlotClass()
        # log - lin
        '''
        redshifts = log10(1 + redshifts)
        Plot.set_axis('log', 'linear', [0, log10(2)], [0, 0.35], tick_loc=[-0.1, 0.5, 0, 0])
        Plot.set_axis_label('1 + z', 'Satellite fraction')
        Plot.make_window()
        Plot.set_label(pos_y=0.3)
        for mi in range(MassBin.number):
            Plot.draw('pp', redshifts, sat_fracs[mi], sat_fracs_unc[mi], ct=mi, lt='-')
            Plot.make_label(mass_kind, MassBin.get_bin_limits(mi), small=2)
            Plot.draw('c', log10(1 + xs_fit), gen.Func.line(xs_fit, Fits[mi].params), lt='.')
        '''
        # lin - lin
        Plot.set_axis('linear', 'linear', [0, 1], [0, 0.34])
        Plot.set_axis_label('redshift', 'satellite fraction')
        Plot.make_window()
        Plot.set_label(position_y=0.28, move_value=0.065)
        for mi in range(MassBin.number):
            Plot.draw('pp', redshifts, sat_fracs[mi], sat_fracs_unc[mi], ct=mi, lt='-')
            Plot.make_label(mass_kind, MassBin.get_bin_limits(mi), small=2)
            Plot.draw('c', xs_fit, Function.line(xs_fit, Fits[mi].params), lt='.')


#===================================================================================================
# HOD
#===================================================================================================
def get_hod_halos(subs, hals=None, ti_now=1, gm_kind='mass.peak', gm_limits=[], hm_kind='mass.fof',
                  hm_limits=[11, 15.5], disrupt_mf=0, mass_ratio_min=0.333):
    '''
    Get HOD halo dictionary, real counts, no log.

    Import subhalo & halo catalog, snapshot index, galaxy mass kind & range, halo mass kind & range,
    disrupt mass fraction, subhalo merge / gain mass ratio.
    '''
    def get_indices_mgain(
        subs, ti_now, ti_max, mgain_kind='mass.peak', mass_gain_min=0.25, sis=None):
        '''
        Import subhalo catalog, snapshot, snapshot to measure recent mass gain,
        min mass gain rate (if < 1 use specific mass gain, if > 1 use absolute mass gain),
        mass kind to measure gain.
        '''
        ti_now = ti_now
        sis_haspar = ut.array.get_indices(subs[ti_now]['parent.index'], [1, Inf], sis)
        parpar_is = subs[ti_now]['parent.index'][sis_haspar]
        parpar_is, iis = ut.array.get_indices(
            subs[ti_now + 1]['mass.peak'], [1, Inf], parpar_is, get_masks=True)
        sis_haspar = sis_haspar[iis]
        par_sis_haspar = np.array(sis_haspar)

        for ti_now in range(ti_now + 1, ti_max + 1):
            par_sis_haspar, iis = ut.array.get_indices(
                subs[ti_now], 'parent.index', [1, Inf], parpar_is, get_masks=True)
            sis_haspar = sis_haspar[iis]
            parpar_is = subs[ti_now]['parent.index'][par_sis_haspar]
            parpar_is, siis = ut.array.get_indices(
                subs[ti_now + 1]['mass.peak'], [1, Inf], parpar_is, get_masks=True)
            par_sis_haspar = par_sis_haspar[siis]
            sis_haspar = sis_haspar[siis]

        if mass_gain_min <= 10:
            mass_gains = 10 ** (subs[ti_now][mgain_kind][par_sis_haspar] -
                                subs[ti_now + 1][mgain_kind][parpar_is]) - 1
            mass_gains /= subs.info['time.width'][ti_max]  # rate of mass change per Gyr
        else:
            mass_gains = 10 ** (subs[ti_now][mgain_kind][par_sis_haspar] -
                                subs[ti_now + 1][mgain_kind][parpar_is])
            mass_gains /= subs.info['time.width'][ti_max] * 1e9  # {M_sun/yr}

        return sis_haspar[mass_gains > mass_gain_min]

    ti_gain = ti_now + 1

    Say = ut.io.SayClass(get_hod_halos)
    halo = {}
    mass_ratio_kind = gm_kind + '.ratio'
    sub = subs[ti_now]
    hal = hals[ti_now]
    sis = ut.array.get_arange(sub['ilk'])
    if hm_kind == 'mass.200c':
        sis_hm = ut.array.get_indices(sub['halo.index'], [1, Inf])
        sis_hm = ut.array.get_indices(hal[hm_kind][sub['halo.index'][sis_hm]], hm_limits)
    else:
        sis_hm = ut.array.get_indices(sub['halo.mass'], hm_limits)
    sis_halo = ut.catalog.get_indices_ilk(sub, 'central', sis_hm)
    sis_cen = ut.array.get_indices(sub[gm_kind], gm_limits, sis_halo)
    halo['halo.mass'] = sub['halo.mass'][sis_halo]
    # centrals in mass range
    cen_numbers = np.histogram(sis_cen, sis)[0]
    halo['central'] = cen_numbers[sis_halo]
    # satellite counts in each halo bin
    sis_sat = ut.catalog.get_indices_subhalo(
        sub, gm_kind, gm_limits, ilk='satellite', disrupt_mf=disrupt_mf, sis=sis_hm)
    cen_is = sub['central.index'][sis_sat]
    sat_numbers = np.histogram(cen_is, sis)[0]
    halo['satellite'] = sat_numbers[sis_halo]
    halo['all'] = halo['central'] + halo['satellite']
    # cen merge halos
    if len(sub[mass_ratio_kind]):
        sis_mrg = ut.array.get_indices(sub[mass_ratio_kind], [mass_ratio_min, Inf], sis_cen)
        mrg_numbers = np.histogram(sis_mrg, sis)[0]
        halo['central.mrg'] = mrg_numbers[sis_halo]
        # sat merge halos
        sis_mrg = ut.array.get_indices(sub[mass_ratio_kind], [mass_ratio_min, Inf], sis_sat)
        cen_is_mrg = sub['central.index'][sis_mrg]
        mrg_numbers = np.histogram(cen_is_mrg, sis)[0]
        halo['sat.mrg'] = mrg_numbers[sis_halo]
        halo['all.mrg'] = halo['central.mrg'] + halo['sat.mrg']
    else:
        Say.say('! %s not assigned at ti_now = %d, not assigning m.rat to hod' %
                (mass_ratio_kind, ti_now))
    # mass gain halos
    if not min([len(subs[zi][gm_kind]) for zi in range(ti_now, ti_gain + 1)]):
        Say.say('! no %s for ti_now = [%d, %d], not assign m.gain to hod' %
                (gm_kind, ti_now, ti_gain))
    else:
        sis_mgain = get_indices_mgain(subs, ti_now, ti_gain, gm_kind, mass_ratio_min, sis_cen)
        mgain_numbers = np.histogram(sis_mgain, sis)[0]
        halo['central.mgain'] = mgain_numbers[sis_halo]
        sis_mgain = get_indices_mgain(subs, ti_now, ti_gain, gm_kind, mass_ratio_min, sis_sat)
        cen_is_mgain = sub['central.index'][sis_mgain]
        mgain_numbers = np.histogram(cen_is_mgain, sis)[0]
        halo['sat.mgain'] = mgain_numbers[sis_halo]
        halo['all.mgain'] = halo['central.mgain'] + halo['sat.mgain']
    if hals is not None:
        halo_is = sub['halo.index'][sis_halo]
        halo['c.200c'] = hals[ti_now]['c.200c'][halo_is]
    return halo


def get_hod(
    subs, ti, gm_kind='mass.peak', gm_limits=[], hm_limits=[11, 15.5], hm_width=0.25, disrupt_mf=0,
    mass_ratio_min=0.2):
    '''
    Get HOD class, with log(count).

    Import subhalo catalog, snapshot index, galaxy mass kind & range, halo mass range & bin width,
    disrupt mass fraction, subhalo merge / gain mass ratio.
    '''
    Say = ut.io.SayClass(get_hod)
    Say.say('t_i = %d' % ti)
    if gm_kind in ['star.mass', 'mag.r']:
        Say.say('mass.peak min %.1f' % ut.catalog.convert_mass(subs, ti, gm_kind, gm_limits[0],
                                                               'mass.peak'))
    HMBin = ut.binning.BinClass(hm_limits, hm_width)
    hod = {
        'halo.mass': HMBin.mids,
        'central': np.zeros(HMBin.number, float32),
        'satellite': np.zeros(HMBin.number, float32),
        'all': np.zeros(HMBin.number, float32),
        'sat.mom': np.zeros(HMBin.number, float32),
        'central.mrg': np.zeros(HMBin.number, float32),
        'sat.mrg': np.zeros(HMBin.number, float32),
        'all.mrg': np.zeros(HMBin.number, float32),
        'central.m.gain': np.zeros(HMBin.number, float32),
        'sat.m.gain': np.zeros(HMBin.number, float32),
        'all.m.gain': np.zeros(HMBin.number, float32)
    }
    hodaux = {}
    # make uncertainties & aux dictionary
    for k in list(hod.keys()):
        if k not in ['halo.mass']:
            hodaux[k] = []
            hod[k + '.err'] = np.zeros(HMBin.number, float32)
    # find halos & each's cen/sat count
    halo = get_hod_halos(subs, None, ti, gm_kind, gm_limits, hm_limits, disrupt_mf, mass_ratio_min)
    mbin_is = HMBin.get_bin_indices(halo['halo.mass'])
    his = ut.array.get_arange(halo['halo.mass'])
    for mi in range(HMBin.number):
        his_mbin = his[mbin_is == mi]
        # first assign all counts for halos in mbin
        for k in hodaux:
            if k in halo:
                hodaux[k] = halo[k][his_mbin]
        hodaux['sat.mom'] = halo['satellite'][his_mbin] * (halo['satellite'][his_mbin] - 1)
        for k in hodaux:
            if hodaux[k].size:
                # assign average & uncertainty
                obj_number = hodaux[k].mean()
                hod[k][mi] = log10(obj_number + 1e-10)
                hod[k + '.err'][mi] = stats.sem(hodaux[k])
                # make uncertainty at least poisson (only 1 halo or halos have same sat number)
                if (hod[k + '.err'][mi] == 0 or np.isnan(hod[k + '.err'][mi])) and obj_number:
                        hod[k + '.err'][mi] = (obj_number / hodaux[k].size) ** 0.5
            else:
                hod[k][mi] = -10
                hod[k + '.err'][mi] = 0
        Say.say('m %.3f | halo number %6d | <sat number> %6.2f +/- %.3f' %
                (hod['halo.mass'][mi], hodaux['central'].size, 10 ** hod['satellite'][mi],
                 hod['sat.err'][mi]))
    return hod


def fit_hod_sat(m, p):
    '''
    Import mass, fit parameter array.
    '''
    # return (m / p[0])**p[1] * exp(-p[2] / m)
    # return (m - p[0]) * p[1] - log10(e) * (10**(p[2] - m))
    # return (m / p[0])**p[1] * exp(-(p[2] / m)**p[3])
    # return (m - p[0]) * p[1] - log10(e) * ( 10**((p[2] - m) * p[3]) )
    return (m - p[0]) * p[1] + log10(0.5 * (1 + special.erf((m - log10(2) - p[2]) / p[3])))


def fit_hod_cen(m, p):
    '''
    Import mass, fit parameter array.
    '''
    return log10(0.5 * (1 + special.erf((m - p[0]) / p[1]) + 1e-10))


def get_fit_hod(hod, hod_kind, mass_min, log_number_min=-2.5):
    '''
    Fit & return as Fit.params[i].

    Import HOD dictionary, HOD kind (sat, cen, sat.mrg, etc), halo mass minimum,
    minimum occupation to use for fit.
    '''
    y = hod[hod_kind][hod[hod_kind] > log_number_min]
    x = hod['halo.mass'][hod[hod_kind] > log_number_min]
    y_err = hod[hod_kind + '.err'][hod[hod_kind] > log_number_min].clip(1e-4)
    y_err = y - log10(10 ** y - y_err)
    if 'satellite' in hod_kind:
        '''
        mfit_sat = mass_min + 1.201
        mfit_cen = mass_min + 0.601
        params = [[mfit_sat, 7., 16.], [0.8, 0.2, 3.], [mfit_cen, 7., 16.], [1.01, 0.01, 4.]]
        '''
        mfit_sat = mass_min + 1.2
        mfit_cen = mass_min
        params = [[mfit_sat, 7., 16.], [0.9, 0.2, 3.], [mfit_cen, 7., 16.], [0.25, 0.01, 4.]]
        func_fit = fit_hod_sat
    elif 'central' in hod_kind:
        params = [[mass_min, 7., 16.], [0.1, 0.001, 2.]]
        func_fit = fit_hod_cen
    return ut.math.fit(func_fit, params, x, y, y_err)


def fit_hod_cen_m_rat(m, p):
    '''
    Import mass, fit parameter array.
    '''
    return log10(p[2] * (1 + special.erf((m - p[0]) / p[1]) + 1e-10))


def get_fit_hod_mass_ratio(hod, hod_kind, mass_min, log_n_min=-2.5):
    '''
    Fit & return as Fit.params[i].

    Import HOD dictionary, HOD kind (sat, cen, sat.mrg, etc), halo mass minimum,
    occupation minimum to use for fit.
    '''
    y = hod[hod_kind][hod[hod_kind] > log_n_min]
    x = hod['halo.mass'][hod[hod_kind] > log_n_min]
    y_err = hod[hod_kind + '.err'][hod[hod_kind] > log_n_min].clip(1e-4)
    y_err = y - log10(10 ** y - y_err)
    if 'satellite' in hod_kind:
        '''
        mfit_sat = mass_min + 1.201
        mfit_cen = mass_min + 0.601
        params = [[mfit_sat, 7., 16.], [0.8, 0.2, 3.], [mfit_cen, 7., 16.], [1.01, 0.01, 4.]]
        '''
        mfit_sat = mass_min + 1.2
        mfit_cen = mass_min
        params = [[mfit_sat, 7., 16.], [0.9, 0.2, 3.], [mfit_cen, 7., 16.], [0.25, 0.01, 4.]]
        func_fit = fit_hod_sat
    elif 'central' in hod_kind:
        params = [[mass_min, 7., 16.], [0.1, 0.001, 2.], [0.5, 0., 0.5]]
        func_fit = fit_hod_cen_m_rat
    return ut.math.fit(func_fit, params, x, y, y_err)


def plot_hods(subs, tis, gm_kind, gm_mins, gm_maxs, disrupt_mfs, hm_limits, hm_width=0.25,
              mass_ratio_kind='mrg', mass_ratio_min=0.25):
    '''
    Plot HOD for each mass range for each snapshot/catalog.

    Import subhalo catalog[s], snapshot[s], subhalo mass kind & range[s], disrupt mass fraction[s],
    halo mass range & bin width.
    '''
    Say = ut.io.SayClass(plot_hods)
    if gm_kind in subs:
        subs = [subs]
    tis = ut.array.arrayize(tis)
    if len(subs) > tis.size:
        tis = np.r_[len(subs) * tis]
    gm_mins, gm_maxs = ut.array.arrayize((gm_mins, gm_maxs))
    disrupt_mfs = ut.array.arrayize(disrupt_mfs, repeat_number=tis.size)
    sat_kind = 'sat-' + mass_ratio_kind
    cen_kind = 'cen-' + mass_ratio_kind
    hods = {}
    for zii in range(len(tis)):
        iss = zii
        if len(subs) == 1:
            iss = 0
        hods_z = {}
        for mi in range(len(gm_mins)):
            hod = get_hod(subs[iss], tis[zii], gm_kind, [gm_mins[mi], gm_maxs[mi]],
                          hm_limits, hm_width, disrupt_mfs[zii], mass_ratio_min)
            ut.array.append_dictionary(hods_z, hod)
        ut.array.append_dictionary(hods, hods_z)

    # plot ----------
    log_nmin = -3.5
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'log', hm_limits, [log_nmin + 0.5, np.max(hods['satellite']) + 0.2])
    Plot.set_axis_label('halo.mass', 'N_{galaxy}')
    Plot.make_window()
    Plot.set_label(move_width=0.06)
    # Plot.make_label('z=%.1f' % (subs[0]['redshift'][tis[0]]))
    # Plot.draw('c', [5, 20], [0, 0],    lt='.')
    Plot.label_(gm_kind, [gm_mins[mi], gm_maxs[mi]])
    for zii in range(len(tis)):
        Plot.make_label('z=%.1f' % subs[0]['redshift'][tis[zii]], 'c', ct=zii, lt='-')
        for mi in range(gm_mins.size):
            # Plot.make_label(gm_kind, [gm_mins[mi], gm_maxs[mi]])
            Plot.draw('c', hods['halo.mass'][zii][mi], hods[cen_kind][zii][mi],
                      draw_if=(hods[cen_kind][zii][mi] > log_nmin), ct=zii, lt='--')
            Plot.draw('c', hods['halo.mass'][zii][mi], hods[sat_kind][zii][mi],
                      hods[sat_kind + '.err'][zii][mi],
                      draw_if=(hods[sat_kind][zii][mi] > log_nmin), ct=zii, lt='-')
            # print 'sat number =', hods[sat_kind][zii][mi]
            # print 'cen number =', hods[sat_kind][zii][mi]
            # fit
            xfit = np.arange(hm_limits[0] - 1, hm_limits[1] + 1, 0.1)
            hod_fit = {}
            for k in hods:
                hod_fit[k] = hods[k][zii][mi]
            # sat
            Fit = get_fit_hod(hod_fit, sat_kind, gm_mins[mi], log_nmin)
            yfit = fit_hod_sat(xfit, Fit.params)
            Say.say('sat: mass_min = %.3f | %.3f %.3f %.3f %.3f' %
                    (gm_mins[mi], Fit.params[2], Fit.params[3], Fit.params[0], Fit.params[1]))
            Plot.draw('c', xfit, yfit, ct=zii, lt='.')
            # cen
            Fit = get_fit_hod(hod_fit, cen_kind, gm_mins[mi], log_nmin)
            yfit = fit_hod_cen(xfit, Fit.params)
            Say.say('cen: mass_min = %.3f | %.3f %.3f' %
                    (gm_mins[mi], Fit.params[0], Fit.params[1]))
            Plot.draw('c', xfit, yfit, draw_if=(yfit > log_nmin), ct=zii, lt='.')


def get_hod_ave(
    subs, ti_min, ti_max, gm_kind, gm_limits, hm_limits, hm_width, disrupt_mf, mass_ratio_min,
    time_width_mrg):
    '''
    Get HOD, averaged across snapshots.

    Import subhalo catalog, snapshot range, galaxy mass kind and range,
    halo mass range and bin width, disrupt mass fraction, minimum mass ratio,
    timescale for counting mergers.
    '''
    hod = {}
    snapshot_number = ti_max - ti_min + 1
    tis = list(range(ti_min, ti_max + 1))
    for zi in tis:
        hod_z = get_hod(
            subs, zi, gm_kind, gm_limits, hm_limits, hm_width, disrupt_mf, mass_ratio_min)
        if not hod:
            for k in hod_z:
                hod[k] = np.zeros(hod_z['halo.mass'].size)  # initialize if first loop
        for k in hod:
            if 'err' in k:
                hod[k] += hod_z[k] / snapshot_number  # average the uncertainty
            else:
                hod[k] += 10 ** hod_z[k] / snapshot_number

    for k in hod:
        if 'mrg' in k:
            # scale merge counts by timescale
            hod[k] = hod[k] * time_width_mrg / np.mean(subs.info['time.width'][tis])
        if 'err' not in k:
            hod[k] = log10(hod[k])

    return hod


def plot_hod_mass_ratio(
    subs, ti_min, ti_max, gm_kind='mass.peak', gm_limits=[12, Inf],
    hm_limits=[12, 15], hm_width=0.25, disrupt_mf=0, mass_ratio_kind='mrg', mass_ratio_min=0.25):
    '''
    Plot HOD for galaxies satisfying mass ratio.

    Import subhalo catalog, snapshot range, galaxy mass kind & range, halo mass range & bin width,
    disrupt mass fraction, mass ratio kind (mrg, m.gain), minimum mass ratio.
    '''
    time_width_mrg = 0.5  # Gyr
    log_numden_min = -4  # min log(numden) to consider for fit & plot

    Say = ut.io.SayClass(plot_hod_mass_ratio)
    hod = get_hod_ave(
        subs, ti_min, ti_max, gm_kind, gm_limits, hm_limits, hm_width, disrupt_mf, mass_ratio_min,
        time_width_mrg)

    # plot ----------
    lab_pos_x = 0.04
    Plot = plot_sm.PlotClass(panel_number_y=2)
    Plot.set_axis('log', 'log', hm_limits, [max(hod['all-mrg'].min() - 0.3, log_numden_min),
                  hod['nall'].max() + 0.6])
    Plot.set_axis_label('', 'N_{galaxy}')
    Plot.panel_grid = (1, -5, 1, 3, 1, 5)
    Plot.make_window(axis_x_label_kind=0)
    Plot.set_label(0.04, 0.9, move_value=0.16)
    Plot.make_label('z=%.1f' % subs.snapshot['redshift'][ti_min], move_direction='x')
    Plot.make_label(gm_kind, gm_limits, move_direction='y')
    if mass_ratio_kind == 'mrg':
        Plot.make_label('R>%.2f' % mass_ratio_min, position_x=lab_pos_x, move_direction='x')
        Plot.make_label('\Delta t=%.1f\,Gyr' % time_width_mrg, move_direction='y')
    elif mass_ratio_kind == 'mgain':
        if mass_ratio_min <= 10:
            words = 'R>%.1f\,Gyr^{-1}' % mass_ratio_min
        else:
            words = 'R>%.0f\,M_\odot\,yr^{-1}' % mass_ratio_min
        Plot.make_label(words, position_x=lab_pos_x, move_direction='y')
    Plot.draw('c', hod['halo.mass'], hod['all'])
    Plot.draw('c', hod['halo.mass'], hod['all' + mass_ratio_kind], lt='--')
    Plot.draw('c', hod['halo.mass'], hod['central'], ct='red', lt='-')
    Plot.draw('c', hod['halo.mass'], hod['cen-' + mass_ratio_kind],
              draw_if=(hod['central' + mass_ratio_kind] > log_numden_min), ct='red', lt='--')
    Plot.draw('c', hod['halo.mass'], hod['satellite'], ct='blue', lt='-')
    Plot.draw('c', hod['halo.mass'], hod['sat-' + mass_ratio_kind], ct='blue', lt='--')
    # fit
    xfit = np.arange(hm_limits[0] - 1, hm_limits[1] + 1, 0.1)
    hod_fit = {}
    for k in hod:
        hod_fit[k] = hod[k]
    # cen
    Fit = get_fit_hod_mass_ratio(
        hod_fit, 'central' + mass_ratio_kind, hm_limits[0] + 0.5, log_numden_min)
    yfit = fit_hod_cen_m_rat(xfit, Fit.params)
    Say.say('cen: m.min = %.3f | %.3f %.3f %.3f' %
            (gm_limits[0], Fit.params[0], Fit.params[1], Fit.params[2]))
    Plot.draw('c', xfit, yfit, draw_if=(yfit > log_numden_min), ct=2, lt='.')
    # sat
    Fit = get_fit_hod_mass_ratio(
        hod_fit, 'sat-' + mass_ratio_kind, hm_limits[0] + 0.5, log_numden_min)
    yfit = fit_hod_sat(xfit, Fit.params)
    Say.say('sat: m.min = %.3f | %.3f %.3f %.3f %.3f' %
            (gm_limits[0], Fit.params[2], Fit.params[3], Fit.params[0], Fit.params[1]))
    Plot.draw('c', xfit, yfit, ct=1, lt='.')
    # plot ratio
    Plot.reset()
    Plot.panel_grid = [1, -5, 1, 1, 1, 2]
    Plot.set_axis(hm_limits, [-3, 0])
    Plot.set_axis_label('halo.mass', 'Fraction')
    Plot.make_window(erase=None)
    # Plot.draw('c', hod['halo.mass'], hod['all-' + mass_ratio_kind] - hod['all'],
    #           plotif=(hod['all-' + mass_ratio_kind] > log_numden_min))
    Plot.draw('c', hod['halo.mass'], hod['cen-' + mass_ratio_kind] - hod['central'],
              draw_if=(hod['cen-' + mass_ratio_kind] > log_numden_min))
    Plot.draw('c', hod['halo.mass'], hod['sat-' + mass_ratio_kind] - hod['satellite'],
              draw_if=(hod['sat-' + mass_ratio_kind] > log_numden_min))


#===================================================================================================
# mass function
#===================================================================================================
# satellite mass function ----------
def fit_mass_function_sat(x, p):
    '''
    Import log10(mass_ratios), parameters.
    '''
    return log10(p[0]) - p[1] * x + log10(np.e) * (-p[2] * (10 ** x) ** p[3])


def plot_mass_function_sat(
    subs, tis, gm_kind='mass.peak', gm_limits=[], hm_limits=[], hm_width=0.5,
    mass_scaling='mass.ratio', log_mass_ratio_limits=[-3, 0], gm_width=0.2, disrupt_mf=0):
    '''
    Plot satellite mass function v satellite mass or mass ratio.

    Import subhalo catalog[s], snapshot index[s], galaxy mass kind & range,
    halo mass range & bin width, mass scaling (mass, m.rat), mass ratio range,
    disrupt mass fraction, mass (ratio) bin width.
    '''
    def get_mass_function_sat(
        sub, gm_kind='mass.peak', gm_limits=[], hm_limits=[], mass_scaling='mass.ratio',
        log_mass_ratio_limits=[-3, 0], gm_width=0.2, disrupt_mf=0):
        '''
        Get mass values, satellite mass function, & uncertainty.

        Import catalog of subhalo at snapshot, subhalo mass kind & range, halo mass range,
        disrupt mass fraction, mass scaling (mass, m.rat), mass ratio range, m/mass_ratio bin width.
        '''
        # find halos & satellites in ranges
        sis = ut.array.get_indices(sub['halo.mass'], hm_limits)
        halo_number = ut.catalog.get_indices_ilk(sub, 'central').size
        print('halo.m %s | halo.number %6d' % (hm_limits, halo_number))
        sis = ut.catalog.get_indices_subhalo(
            sub, gm_kind, gm_limits, ilk='satellite', disrupt_mf=disrupt_mf, sis=sis)
        # count satellites by mass
        if mass_scaling == 'mass':
            MassBin = ut.binning.BinClass(gm_limits, gm_width)
            masses = sub[gm_kind][sis]
            sat_numbers = np.histogram(masses, MassBin.number, gm_limits, False)[0]
        # count satellites by mass ratio
        elif mass_scaling == 'mass.ratio':
            MassBin = ut.binning.BinClass(log_mass_ratio_limits, gm_width)
            masses = sub[gm_kind][sis] - sub['halo.mass'][sis]
            sat_numbers = np.histogram(masses, MassBin.number, log_mass_ratio_limits, False)[0]
        dndms = Fraction.get_fraction(sat_numbers, halo_number) / gm_width  # * log10(e)
        dndms_err = dndms / halo_number ** 0.5
        print(sat_numbers)
        return MassBin, dndms, dndms_err

    if gm_kind in subs:
        subs = [subs]
    tis = ut.array.arrayize(tis, repeat_number=len(subs))
    HMBin = ut.binning.BinClass(hm_limits, hm_width)
    dn_dlogms = [[] for _ in range(len(subs))]
    dn_dlogms_err = [[] for _ in range(len(subs))]
    for sub_i in range(len(subs)):
        for hmi in range(HMBin.number):
            MassBin, dn_dlogms_m, dn_dlogms_err_m = get_mass_function_sat(
                subs[sub_i][tis[sub_i]], gm_kind, gm_limits, HMBin.get_bin_limits(hmi),
                mass_scaling, log_mass_ratio_limits, gm_width, disrupt_mf)
            for dni in range(dn_dlogms_m.size - 2):
                # if dn_dlogms_m[dni] < dn_dlogms_m[dni+1] < dn_dlogms_m[dni+2]:
                #    dn_dlogms_m[dni] = 0
                if 0:  # dn_dlogms_m[dni] < dn_dlogms_m[dni+1]:
                    dn_dlogms_m[dni] = 0
                    dn_dlogms_err_m[dni] = 1
            dn_dlogms[sub_i].append(dn_dlogms_m)
            dn_dlogms_err[sub_i].append(dn_dlogms_err_m)

    # literature fits
    # angulo = -2.1 + -0.9 * vals - log10(e) * (10**vals / 0.16)**2
    # tinker = log10(0.13) - 0.7 * vals + log10(e) * (-9.9 * (10**vals)**2.5)
    # giocoli = log10(0.18) - 0.8 * vals + log10(e) * (-12.27 * (10**vals)**3)

    '''
    yfits = []
    for sub_i in range(len(subs)):
        xfit = np.concatenate(mbin_number * [vals])
        yfit = np.concatenate(dn_dlogms[sub_i])
        # estimate uncertainty in log(yfit)
        yfit_err = yfit ** 0.5     #yfit - np.concatenate(dndms_err[sub_i])
        iys = gen.get_arange(yfit)
        iys = iys[yfit > 0]
        xfit = xfit[iys]
        yfit = log10(yfit[iys])
        # kluge convert to log uncertainty
        yfit_err = log10(yfit_err[iys]) - yfit
        params = [[0.2, 0, 5], [0.9, 0.6, 1.4], [5., 0.8, 15], [3., 1, 10]]
        fit = gen.fit(fit_sat_massfunc, params, xfit, yfit, yfit_err)
        print 'massfunction fit: %.3f %.3f %.2f %.2f' % (
            fit.params[0], fit.params[1], fit.params[2], fit.params[3])
        xfit = np.arange(vals.min() - 0.5, vals.max() + 0.5, 0.1)
        yfits.append(fit_sat_massfunc(xfit, fit.params))
        '''

    # plot ----------
    Plot = plot_sm.PlotClass()
    if mass_scaling == 'mass.ratio':
        Plot.set_axis('log', 'log', log_mass_ratio_limits, log10(dn_dlogms), tick_label_kind='log')
        Plot.set_axis_label('M_{sat}/M_{halo}', 'dN_{sat}/dlog(M_{sat}/M_{halo})')
        Plot.make_window()
        Plot.make_label('halo.mass', position_y=0.4)
        for sub_i in range(len(subs)):
            for hmi in range(HMBin.number):
                Plot.draw('c', MassBin.mids, log10(dn_dlogms[sub_i][hmi]),
                          draw_if=dn_dlogms[sub_i][hmi], ct=hmi, lt=sub_i)
                if sub_i == 0:
                    Plot.make_label('halo.mass', HMBin.get_bin_limits(hmi))
            # Plot.draw('c', xfit, yfits[sub_i], ct='def', lt='.')
    elif mass_scaling == 'mass':
        Plot.set_axis('log', 'log', gm_limits, log10(dn_dlogms))
        Plot.set_axis_label('M_{sat} [M_\odot]', 'dN_{sat}/dlogM_{sat}')
        Plot.make_window()
        Plot.make_label('halo.mass')
        for sub_i in range(len(subs)):
            for hmi in range(HMBin.number):
                Plot.draw('c', MassBin.mids, log10(dn_dlogms[sub_i][hmi]),
                          draw_if=dn_dlogms[sub_i][hmi], ct=hmi, lt=sub_i)
                if sub_i == 0:
                    Plot.make_label('halo.mass', HMBin.get_bin_limits(hmi))


# mass function - general ----------
def plot_mass_function(
    cats, tis, mass_kind='mass.peak', mass_limits=[], mass_width=0.2, ilk='all', disrupt_mf=0,
    func_kind='density.dif', plot_ratio=False, get_fit=False, axis_y_limits=None):
    '''
    Plot mass functions [& ratios].

    Parameters
    ----------
    [sub]halo catalo[s]: list
    snapshot index[s]: list
    mass kind: string
    mass limits: list
    mass bin width: float
    ilk: string
    disrupt mass fraction: float
    mass function kind to plot:
        options: number.dif, density.dif, number.cum, density.cum
    whether to plot ratio:
    '''
    if mass_kind in cats[0]:
        cats = [cats]
    tis = ut.array.arrayize(tis)
    mass_funcs = [[] for _ in range(len(cats))]
    mass_func_errs = [[] for _ in range(len(cats))]
    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    for cat_i in range(len(cats)):
        cat = cats[cat_i][tis[cat_i]]
        cis = ut.catalog.get_indices_subhalo(
            cat, mass_kind, mass_limits, ilk=ilk, disrupt_mf=disrupt_mf)
        masses = cats[cat_i][tis[cat_i]][mass_kind][cis]
        obj_numbers = np.histogram(masses, MassBin.number, mass_limits, False)[0]
        obj_number_errs = np.sqrt(obj_numbers)
        print(obj_numbers)
        if 'dif' in func_kind:
            mass_func = log10(obj_numbers / mass_width + 1e-10)
            mass_func_err = obj_number_errs / mass_width
        elif 'cum' in func_kind:
            mass_func = log10(obj_numbers[::-1].cumsum()[::-1])
            mass_func_err = np.sqrt(obj_numbers[::-1].cumsum()[::-1])
        if 'density' in func_kind:
            mass_func -= 3 * log10(cat.info['box.length'])
            mass_func_err /= log10(cat.info['box.length']) ** 3
        mass_funcs[cat_i] = mass_func
        mass_func_errs[cat_i] = mass_func_err
    mass_funcs = np.array(mass_funcs)
    mass_func_errs = np.array(mass_func_errs)
    print(mass_func_errs)

    # plot ----------
    if plot_ratio:
        panel_number_y = 2
        axis_labkind_x = 0
    else:
        panel_number_y = 1
        axis_labkind_x = 1
    Plot = plot_sm.PlotClass(panel_number_y=panel_number_y)
    if not axis_y_limits:
        axis_y_limits = np.concatenate(mass_funcs)
    if plot_ratio:
        Plot.panel.grid = [1, -5, 1, 2, 1, 5]
    if mass_kind == 'mass.peak':
        mass_kind_name = 'M_{sub}'
    elif mass_kind == 'star.mass':
        mass_kind_name = 'M_{star}'
    elif mass_kind == 'm':
        mass_kind_name = 'M'
    elif mass_kind == 'mass.vir':
        mass_kind_name = 'M_{200m}'
    elif mass_kind == 'vel.circ.max':
        mass_kind_name = 'V'
    if func_kind == 'dif':
        Plot.axis.lab_y = 'dn/dlog(%s)' % mass_kind_name
    elif func_kind == 'cum':
        Plot.axis.lab_y = 'n(>%s)' % mass_kind_name
    if 'density' in func_kind:
        Plot.axis.lab_y += ' [kpc^{-3}]'
    if plot_ratio:
        Plot.axis.lab_x = ''
    else:
        if mass_kind == 'mass.peak':
            Plot.axis.lab_x = 'M_{(sub)halo} [M_\odot]'
        else:
            Plot.axis.lab_x = '%s [M_\odot]' % mass_kind_name

    Plot.set_axis('log', 'log', mass_limits, axis_y_limits)
    Plot.make_window(axis_labkind_x)
    Plot.set_label(position_y=0.7)

    # plot mass function
    for cat_i in range(len(cats)):
        Plot.draw('c', MassBin.mids, mass_funcs[cat_i], mass_func_errs[cat_i], ct='blue')

    # get get_fit
    if get_fit:
        params = [[-0.1, -20, 20], [-1.0, -5, 0]]
        xs_fit = np.arange(MassBin.limits[0], MassBin.limits[1], mass_width * 0.1)
        for cat_i in range(len(cats)):
            Fit = ut.math.fit(Function.line, params, MassBin.mids, mass_funcs[cat_i],
                              log10(mass_func_errs[cat_i]))
            print(Fit.params)
            ys = Function.line(xs_fit, Fit.params)
            Plot.draw('c', xs_fit, ys, ct='red', lt='.')

    # plot ratio
    if plot_ratio:
        Plot.reset()
        Plot.pan_grid = [1, -5, 1, 1, 1, 1]
        Plot.tic_loc = [-1, 10, 0.1, 0.2]
        Plot.tic_labkind = [0, 0, 0, 0]
        Plot.set_axis('log', 'linear', mass_limits, [0.6, 1.8], tick_label_kind='log')
        Plot.set_axis_label(mass_kind, 'Ratio')
        Plot.make_window(erase=None)
        Plot.draw('c', [0, 20], [1, 1])
        for cat_i in range(1, len(cats)):
            ratios = 10 ** (mass_funcs[cat_i] - mass_funcs[0])
            Plot.draw('c', MassBin.mids, ratios, ct=cat_i)
            for mi in range(MassBin.number):
                print('# %.2f: %.3f' % (MassBin.mids[mi], ratios[mi]))


#===================================================================================================
# SHAM
#===================================================================================================
def plot_mass_star_v_mass(
    subs, tis=1, mass_limits=[], disrupt_mf=0, scatter=0.007, mass_width=0.25,
    sources='li-drory-marchesini', x_kind='sub'):
    '''
    Plot M_star v M_[max or halo] relation.

    Import subhalo catalog, snapshot[s], mass range (m.max or m.halo), mass scatter [dex],
    disrupt mass fraction, mass bin width, SHAM source, x-axis mass kind (subs, halo).
    '''
    tis = ut.array.arrayize(tis)
    sources = ut.array.arrayize(sources, repeat_number=tis.size)
    # source_names = ['Li & White', 'Cole et al']
    volume = subs.info['box.length'] ** 3
    MassBin = ut.binning.BinClass(mass_limits, mass_width, include_max=True)
    masses_star = np.zeros((tis.size, MassBin.number), float32)

    # central galaxy stellar mass
    if x_kind == 'sub':
        if scatter:
            masses_star_scat = np.zeros((tis.size, MassBin.number))
        for zii, zi in enumerate(tis):
            SMF = sham.SMFClass(sources[zii], max(subs.snapshot['redshift'][zi], 0.1), scatter,
                                subs.Cosmology['hubble'])
            sis = ut.array.get_indices(subs[zi]['mass.frac.min'], [disrupt_mf, Inf])
            for mi in range(MassBin.number):
                sis_m = ut.array.get_indices(subs[zi]['mass.peak'], MassBin.get_bin_limits(mi), sis)
                masses_star[zii][mi] = SMF.m((sis_m.size + 1) / volume)
                if scatter:
                    masses_star_scat[zii, mi] = SMF.mass_scat((sis_m.size + 1) / volume)
        if scatter:
            masses_star_scat_ratio = 10 ** (masses_star + scatter - MassBin.mins)
        mass_kind = 'mass.peak'
    # full host halo stellar mass
    elif x_kind == 'halo':
        for zii, zi in enumerate(tis):
            sham.assign_to_catalog(subs[zi], 'star.mass', scatter, disrupt_mf, sources[zii])
            sis = ut.array.get_indices(subs[zi]['star.mass'], [1, Inf])
            for mi in range(MassBin.number):
                sis_all = ut.array.get_indices(
                    subs[zi]['halo.mass'], MassBin.get_bin_limits(mi), sis)
                sis_cen = ut.catalog.get_indices_ilk(subs[zi], 'central', sis_all)
                masses_star[zii, mi] = log10((10 ** subs[zi]['star.mass'][sis_all]).sum() /
                                             sis_cen.size)
        mass_kind = 'halo.mass'

    mass_star_ratios = 10 ** (masses_star - MassBin.mins)

    # plot ----------
    Plot = plot_sm.PlotClass(panel_number_y=-2)
    Plot.set_axis('log', 'log', mass_limits, ut.array.get_limits_expanded(masses_star, 0.2))
    Plot.set_axis_label('', 'M_{star}')
    Plot.make_panel()
    # Plot.make_label(mov_val=0.08)
    for zii in range(tis.size):
        Plot.draw('c', MassBin.mins, masses_star[zii], ct=zii)
        if len(tis) > 1:
            Plot.make_label('z=%.1f' % subs.snapshot['redshift'][tis[zii]])
        # Plot.make_label(source_names[zii])
        if x_kind == 'sub' and scatter:
            Plot.draw('c', MassBin.mins, masses_star[zii] + scatter, lt='.')
            Plot.draw('c', MassBin.mins, masses_star[zii] - scatter)
            Plot.make_label('\sigma_{logM_{star}}=%.2f' % scatter)
    # plot ratio
    Plot.set_axis('log', 'linear', mass_limits, [0, (mass_star_ratios.max() * 1.1)])
    Plot.set_axis_label(mass_kind, 'M_{star}/M_{200c}')
    Plot.make_panel()
    for zii in range(tis.size):
        Plot.draw('c', MassBin.mins, mass_star_ratios[zii], ct=zii)
        if scatter:
            Plot.draw('c', MassBin.mins, masses_star_scat_ratio[zii], ct=zii, lt='-.')


# scatter in halo mass at fixed stellar mass ----------
def plot_ave_mhalo_scatter(sub, mass_kind='star.mass'):
    '''
    .
    '''
    disrupt_mf = 0.01
    source = 'blanton'
    dscatter = 0.05
    scatters = np.arange(0, 0.4 + dscatter, 0.05)

    vol = sub.info['box.length'] ** 3
    dlog_numden = 0.5
    log_numdens_hi = np.arange(-4, -1.5, dlog_numden)
    log_numdens_lo = log_numdens_hi - dlog_numden
    lo_numbers = 10 ** log_numdens_lo * vol
    high_numbers = 10 ** log_numdens_hi * vol
    halo_ms_ave = np.zeros((len(log_numdens_lo), len(scatters)))
    halo_ms_cut = np.zeros(len(log_numdens_lo))
    for scat_i in range(len(scatters)):
        sham.assign_to_catalog(sub, mass_kind, scatters[scat_i], disrupt_mf, source)
        sis_cen = ut.catalog.get_indices_ilk(sub, 'central')
        sis_mag = np.argsort(sub[mass_kind][sis_cen]).astype(sis_cen.dtype)[::-1]
        halo_ms = sub['halo.mass'][sis_cen[sis_mag]]
        sis_hm = np.argsort(sub['halo.mass'][sis_cen]).astype(sis_cen.dtype)[::-1]
        halo_ms_test = sub['halo.mass'][sis_cen[sis_hm]]
        for num_i in range(len(high_numbers)):
            halo_ms_ave[num_i, scat_i] = np.mean(halo_ms[0:high_numbers[num_i]])
            halo_ms_cut[num_i] = halo_ms_test[high_numbers[num_i]]
            print('%.2f %.1f %.1f %.2f %.2f %.2f %.2f' % (
                scatters[scat_i], log_numdens_lo[num_i], log_numdens_hi[num_i],
                sub[mass_kind][sis_cen[sis_mag[lo_numbers[num_i]]]],
                sub[mass_kind][sis_cen[sis_mag[high_numbers[num_i]]]],
                halo_ms_test[lo_numbers[num_i]], halo_ms_test[high_numbers[num_i]])
            )
    for num_i in range(len(halo_ms_ave)):
        halo_ms_ave[num_i] /= halo_ms_ave[num_i][0]

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('linear', 'linear', scatters, halo_ms_ave)
    Plot.set_axis_label('\sigma [dex]', '\\bar{M}(\sigma)/\\bar{M}(\sigma=0)')
    Plot.make_window()
    for mi in range(halo_ms_ave.size):
        Plot.draw('c', scatters, halo_ms_ave[mi], ct=mi)
        Plot.make_label('log(n)<%.1f  log(M)>%.2f' % (log_numdens_hi[mi], halo_ms_cut[mi]))


def plot_cen_not_massive_v_mass(
    sub, gm_kind='star.mass', hm_limits=[12.333, 15], hm_width=0.333,
    scats=[0.1, 0.15, 0.2], disrupt_mf=0):
    '''
    Plot fraction of satellites that are more massive than their central v halo mass.

    Import subhalo catalog, snapshot index, galaxy mass kind to compare cen & sat,
    halo mass limit and bin width, log-normal scatter in mass galaxy m, disrupt mass fraction.
    '''
    random_number = 5

    scats = np.array(scats)
    HMBin = ut.binning.BinClass(hm_limits, hm_width)
    sis = ut.catalog.get_indices_subhalo(sub, 'mass.peak', [11, Inf], hm_limits, 'satellite')
    sat_massive_fracs, sat_massivefracs_err = np.zeros((2, scats.size, HMBin.number))
    for scat_i in range(scats.size):
        sat_massive_numbers = np.zeros(HMBin.number, int32)
        halo_numbers = np.zeros(HMBin.number, int32)
        for rand_i in range(random_number):
            sham.assign_to_catalog(sub, gm_kind, scats[scat_i], disrupt_mf)
            for hmi in range(HMBin.number):
                sis_sat = ut.array.get_indices(sub['halo.mass'], HMBin.get_bin_limits(hmi), sis)
                cen_is = sub['central.index'][sis_sat]
                if rand_i == 0:
                    halo_numbers[hmi] = random_number * np.unique(cen_is).size
                sis_sat_massive = sis_sat[sub[gm_kind][sis_sat] > sub[gm_kind][cen_is]]
                sat_massive_numbers[hmi] += np.unique(sub['halo.index'][sis_sat_massive]).size
        for hmi in range(HMBin.number):
            f, f_err = Fraction.get_fraction(sat_massive_numbers[hmi], halo_numbers[hmi], 'normal')
            sat_massive_fracs[scat_i, hmi], sat_massivefracs_err[scat_i, hmi] = f, f_err

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', hm_limits, [0, 0.6])
    Plot.set_axis('halo.mass', 'Satellite massive fraction')
    Plot.make_window()
    for scat_i in range(scats.size):
        Plot.draw('c', HMBin.mids, sat_massive_fracs[scat_i], sat_massivefracs_err[scat_i],
                  ct=scat_i)
        Plot.make_label('\sigma_{logM_{star}}=%.2f' % scats[scat_i])


class MassGrowClass:
    '''
    Compute log ratio of M(z_now) from M(z) as a function of M(z) and z.
    '''
    def __init__(
        self, subs, ti_now=1, ti_max=23, mass_kind='mass.peak', mass_limits=[10, 15],
        mass_width=0.1, mass_direction='forward', ilk='central'):
        '''
        Compile median log ratio of M(z_now) for [sub]halos in bins of mass & z back to ti_max.

        Import [sub]halo catalog, snapshot range, mass kind & range & bin width, ilk [if subhalo].
        '''
        if mass_direction == 'forward':
            mass_limits_hist = mass_limits
            mass_limits_now = [mass_limits_hist[0] - 1, Inf]
        elif mass_direction == 'backward':
            mass_limits_hist = [1, Inf]
            mass_limits_now = mass_limits
        self.ti_now = ti_now
        self.ti_max = ti_max
        self.MassBin = ut.binning.BinClass(mass_limits, mass_width)
        self.mass_ratio = {
            'value': np.zeros((ti_max + 1, self.MassBin.number)),
            'percent.16': np.zeros((ti_max + 1, self.MassBin.number)),
            'percent.84': np.zeros((ti_max + 1, self.MassBin.number))
        }
        cat = subs[ti_now]
        cis = ut.catalog.get_indices_subhalo(cat, mass_kind, mass_limits_now, ilk=ilk)
        # select those with parent
        cis_now = ut.array.get_indices(cat['parent.index'], [1, Inf], cis)
        masses_now = cat[mass_kind][cis_now]
        if mass_direction == 'backward':
            mbin_indices_now = self.MassBin.get_bin_indices(masses_now)
        ciis_now = ut.array.get_arange(cis_now)  # indices to point back to ti_now
        cis = cat['parent.index'][cis_now]  # indices of parents
        for zi in range(ti_now + 1, ti_max + 1):
            cat = subs[zi]
            sis_zi, iis = ut.array.get_indices(
                cat[mass_kind], mass_limits_hist, cis, get_masks=True)
            siis_now_zi = ciis_now[iis]
            masses = cat[mass_kind][sis_zi]
            if mass_direction == 'backward':
                mbin_indices_now_zi = mbin_indices_now[siis_now_zi]
            elif mass_direction == 'forward':
                mbin_indices = self.MassBin.get_bin_indices(masses)
            log_mass_ratios = masses_now[siis_now_zi] - masses
            for mi in range(self.MassBin.number):
                if mass_direction == 'forward':
                    ciis_m = ut.array.get_indices(mbin_indices, mi)
                elif mass_direction == 'backward':
                    ciis_m = ut.array.get_indices(mbin_indices_now_zi, mi)
                if ciis_m.size > 0:
                    self.mass_ratio['value'][zi, mi] = np.median(log_mass_ratios[ciis_m])
                    self.mass_ratio['percent.16'][zi, mi] = \
                        np.percentile(log_mass_ratios[ciis_m], 16)
                    self.mass_ratio['percent.84'][zi, mi] = \
                        np.percentile(log_mass_ratios[ciis_m], 84)
            # keep *all* subhalos with a parent (if central, might be sat at previous snapshot)
            cis_has_par, iis = ut.array.get_indices(
                cat['parent.index'], [1, Inf], cis, get_masks=True)
            ciis_now = ciis_now[iis]
            cis = cat['parent.index'][cis_has_par]  # shift to earlier snapshot

    def value(self, ti, masses, stat='value'):
        '''
        Get median log (M(ti.now) / M(ti)) = logM(ti.now) - logM(ti).

        Import snapshot, masses, statistic.
        '''
        if ti <= self.ti_now or ti > self.ti_max:
            raise ValueError('t_i = %d needs to be in [%d, %d]' %
                             (ti, self.ti_now + 1, self.ti_max))
        masses = ut.array.arrayize(masses, bit_number=32)
        mbin_indices = self.MassBin.get_bin_indices(masses)
        mbin_indices = mbin_indices.clip(0, self.MassBin.number - 1)
        return self.mass_ratio[stat][ti, mbin_indices]


def plot_mass_grow_v_mass(
    cats, ti_now=1, ti_max=5, mass_kind='mass.peak', mass_limits=[11, 15], mass_width=0.2,
    mass_direction='forward', ilk='central'):
    '''
    .
    '''
    stat = 'value'

    MassGrow = MassGrowClass(
        cats, ti_now, ti_max, mass_kind, mass_limits, mass_width, mass_direction, ilk)
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', MassGrow.MassBin.limits, [0, 0.3])
    Plot.set_axis(mass_kind, 'log[M(ti_now)/M(zi)]')
    Plot.make_window()
    Plot.set_lines(color_number=(ti_max - ti_now))
    for zi in range(ti_now + 1, ti_max + 1):
        Plot.draw('c', MassGrow.MassBin.mids, MassGrow.mass_ratio[stat][zi], ct=zi - ti_now - 1)


def plot_mass_grow_v_redshift(
    cats, ti_now=1, ti_max=5, mass_kind='mass.peak', mass_limits=[11.9, 12.3], mass_width=0.4,
    mass_direction='backward', ilk='central', time_kind='redshift'):
    '''
    Plot mass kind growth v redshift.

    Import [sub]halo catalog, current & max snapshot, mass kind & range & bin width,
    which direction to examine mass growth, ilk.
    '''
    ts = cats.snapshot[time_kind][np.arange(ti_max + 1)]
    if time_kind == 'time':
        ts = ts.max() - ts
    #log_ts = log10(1 + ts)
    MassGrow = MassGrowClass(
        cats, ti_now, ti_max, mass_kind, mass_limits, mass_width, mass_direction, ilk)
    Plot = plot_sm.PlotClass()
    """
    Plot.set_axis('log', 'log', [0, log_redshifts.max()], [-2, 0])
    Plot.set_axis_label('1+z', 'M(z)/M(z=0)')
    Plot.make_window()
    Plot.set_point(color_number=MassGrow.MassBin.number)
    for mi in range(MassGrow.MassBin.number):
        Plot.fill(log_redshifts, [-MassGrow.mass_ratio['percent.16'][:, mi],
        -MassGrow.mass_ratio['percent.84'][:, mi]],
        ct='blue.lite2')
        Plot.draw('c', log_redshifts, -MassGrow.mass_ratio['value'][:, mi], ct=mi)
    Plot.make_window(erase=False)
    """
    Plot.set_axis('linear', 'linear', [0, ts.max()], [0, 1])
    Plot.set_axis_label(time_kind, 'M(z)/M(z=0)')
    Plot.make_window()
    Plot.set_point(color_number=MassGrow.MassBin.number)
    for mi in range(MassGrow.MassBin.number):
        Plot.fill(ts, [10 ** (-MassGrow.mass_ratio['percent.16'][:, mi]),
                       10 ** (-MassGrow.mass_ratio['percent.84'][:, mi])],
                  ct='blue.lite2')
        Plot.draw('c', ts, 10 ** -MassGrow.mass_ratio['value'][:, mi], ct=mi)
    Plot.make_window(erase=False)


#===================================================================================================
# formation history
#===================================================================================================
def plot_formation_v_mass(
    subs, ti_now, ti_max, mass_kind='mass.peak', mass_limits=[12, 15], ilk='central',
    form_time_kind='redshift', form_prop_kind='mass.peak', thresh=0.5, mass_ratio_min=0.25):
    '''
    .
    '''
    mass_width = 0.5

    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    sis = ut.catalog.get_indices_ilk(subs[ti_now], ilk)
    Formation = FormationClass()
    Formation.assign_to_self(
        subs, ti_now, ti_max, mass_kind, mass_limits, form_time_kind, form_prop_kind, thresh, sis)
    sis = ut.array.get_indices(subs[ti_now][mass_kind], mass_limits, sis)
    # all subhalos
    stat_all = MassBin.get_statistics_of_array(
        subs[ti_now][mass_kind][sis], subs[ti_now][form_time_kind + 'form'][sis])
    # subhalos without massive satellite
    sat_is = subs[ti_now]['satellite.index'][sis]
    sis_nosat = sis[10 ** (subs[ti_now][mass_kind][sat_is] - subs[ti_now][mass_kind][sis]) <
                    mass_ratio_min]
    stat_nosat = MassBin.get_statistics_of_array(
        subs[ti_now][mass_kind][sis_nosat], subs[ti_now]['form.' + form_time_kind][sis_nosat])
    # subhalos with massive satellite
    sat_is = subs[ti_now]['satellite.index'][sis]
    sis_sat = sis[10 ** (subs[ti_now][mass_kind][sat_is] - subs[ti_now][mass_kind][sis]) >
                  mass_ratio_min]
    stat_sat = MassBin.get_statistics_of_array(
        subs[ti_now][mass_kind][sis_nosat], subs[ti_now]['form.' + form_time_kind][sis_sat])
    # subhalos that experienced merger
    mrg_tis = get_merge_hist(
        't.index', subs, ti_now, ti_max, mass_kind, mass_ratio_min, mass_limits, sis_nosat)
    mrg_sis = sis[mrg_tis >= ti_now]
    stat_mrg = MassBin.get_statistics_of_array(
        subs[ti_now][mass_kind][mrg_sis], subs[ti_now]['form.' + form_time_kind][mrg_sis])

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', mass_limits, [0, 1.2])
    Plot.set_axis_label('halo.mass', 'Formation redshift')
    Plot.draw('c', stat_all['x'], stat_all['median'])
    Plot.make_label('all')
    Plot.draw('c', stat_nosat['x'], stat_nosat['median'], ct='green')
    Plot.make_label('sat<%.2f' % mass_ratio_min)
    Plot.draw('c', stat_sat['x'], stat_sat['median'], ct='blue')
    Plot.make_label('sat>%.2f' % mass_ratio_min)
    Plot.draw('c', stat_mrg['x'], stat_mrg['median'], ct='red')
    Plot.make_label('sat<%.2f+merge' % mass_ratio_min)


def plot_number_sat_mrg(
    subs, ti_now, ti_max, mass_kind='star.mass', mass_limits=[12, 15], mass_width=0.2,
    mass_ratio_min=0.25):
    '''
    Need to SHAM & assign mass ratios first.
    '''
    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    sis = ut.catalog.get_indices_subhalo(subs[ti_now], mass_kind, mass_limits, ilk='central')
    mrg_tis = get_merge_hist(
        't.index', subs, ti_now, ti_max, mass_kind, mass_ratio_min, [8, Inf], sis)
    # assign_mass_ratio(subs, ti_now, ti_max, gm_kind, mass_limits, disrupt_mf, scatter=0)
    sat_frac_all, sat_frac_all_err = np.zeros(MassBin.number), np.zeros(MassBin.number)
    sat_frac_mrg, sat_frac_mrg_err = np.zeros(MassBin.number), np.zeros(MassBin.number)
    for mi in range(MassBin.number):
        sis_m, iis = ut.array.get_indices(
            subs[ti_now][mass_kind], MassBin.get_bin_limits(mi), sis, get_masks=True)
        mrg_tis_m = mrg_tis[iis]
        # all centrals
        sat_sis = subs[ti_now]['satellite.index'][sis_m]
        sis_sat = sis_m[10 ** (subs[ti_now][mass_kind][sat_sis] -
                               subs[ti_now][mass_kind][sis_m]) > mass_ratio_min]
        sat_frac_all[mi], sat_frac_all_err[mi] = Fraction.get_fraction(
            sis_sat.size, sis_m.size, 'normal')
        # merged centrals
        sis_mrg = sis_m[mrg_tis_m >= ti_now]
        sat_is_mrg = subs[ti_now]['satellite.index'][sis_mrg]
        sis_sat_mrg = sis_mrg[10 ** (subs[ti_now][mass_kind][sat_is_mrg] -
                                     subs[ti_now][mass_kind][sis_mrg]) > mass_ratio_min]
        sat_frac_mrg[mi], sat_frac_mrg_err[mi] = Fraction.get_fraction(
            sis_sat_mrg.size, sis_mrg.size, 'normal')

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', mass_limits, [0, sat_frac_all.max() * 1.1])
    Plot.set_axis_label(mass_kind, 'Fraction with satellite')
    Plot.make_label('R > %.2f' % mass_ratio_min)
    Plot.draw('c', MassBin.mids, sat_frac_all)
    Plot.make_label('all')
    Plot.draw('c', MassBin.mids, sat_frac_mrg, sat_frac_mrg_err, ct='red')
    Plot.make_label('merged')


def plot_property_v_density(
    subs, ti_now=1, ti_max=16, mass_kind='star.mass', mass_limits=[9.7, 10.1],
    form_time_kind='redshift', form_prop_kind='mass.peak', form_prop_thresh=0.9,
    y_kind='frac', prop_frac_ave=0.9):
    '''
    Plot fraction of central subhalos that have formation property above / below given value as a
    function of neighboring subhalo density.

    Import subhalo catalog, snapshot range, mass kind & range, ...
    '''
    distance_max = 10
    density_limits = [-1.4, 1.4]
    density_width = 0.2

    sis = ut.catalog.get_indices_subhalo(subs[ti_now], mass_kind, mass_limits, ilk='central')
    # remove ejected satellites
    '''
    sis = np.concatenate((gen.get_indices(subs[ti_now]['ilk.dif.zi'], [-1, 0.1], sis),
                          gen.get_indices(subs[ti_now]['ilk.dif.zi'], [ti_max + 1, Inf], sis)))
    halo_is = subs[ti_now]['halo.index'][sis]
    '''
    neig_numbers = subs[ti_now]['number.neig'][sis]
    DenBin = ut.binning.BinClass(density_limits, density_width, include_max=True)
    neig_bin_numbers = 10 ** DenBin.mins * subs[ti_now]['number.ave']

    # neighbor history properties
    Formation = FormationClass()
    Formation.assign_to_self(
        subs, ti_now, ti_max, mass_kind, mass_limits,
        form_time_kind, form_prop_kind, form_prop_thresh, sis)
    form_times = subs[ti_now]['form.' + form_time_kind][sis]
    form_time_thresh = np.percentile(form_times, (1 - prop_frac_ave) * 100)

    # mergers
    '''
    mass_ratio_kind = 'mass.peak'
    mass_ratio_min = 0.25
    form_time_thresh = 0
    form_times = get_merge_hist('t.index', subs, ti_now, ti_max, mass_ratio_kind, mass_ratio_min,
    [1, Inf], sis)
    '''
    # count them
    vals, vals_err = np.zeros((2, neig_bin_numbers.size - 1))
    for neigi in range(neig_bin_numbers.size - 1):
        niis = ut.array.get_indices(
            neig_numbers, [neig_bin_numbers[neigi], neig_bin_numbers[neigi + 1] + 0.1])
        early_number = niis[form_times[niis] > form_time_thresh].size
        if y_kind == 'frac':
            vals[neigi], vals_err[neigi] = Fraction.get_fraction(early_number, niis.size, 'normal')
        elif y_kind == 'average':
            vals[neigi] = np.mean(form_times[niis])
            vals_err[neigi] = stats.sem(form_times[niis])
        else:
            raise ValueError('not recognize y_kind = %s' % y_kind)

    # plot ----------
    Plot = plot_sm.PlotClass()
    if y_kind == 'frac':
        axis_min_y = 0
        Plot.axis.lab_y = 'Fraction (%s_{form}>%.2f)' % (form_time_kind, form_time_thresh)
    elif y_kind == 'average':
        axis_min_y = '\\bar{%s}' % form_time_kind
        Plot.axis.lab_y = 'ave time since form [Gyr]'
    Plot.set_axis('log', 'linear', density_limits, vals, vals_err, axis_min_y)
    Plot.set_axis_label('\\rho_{gal}/\\bar{\\rho}_{gal} [%d kpc]' % distance_max, '')
    Plot.make_window()
    Plot.make_label(mass_kind, mass_limits)
    Plot.make_label('form=%.2f\,%s(z=%.2f)' %
                    (form_prop_thresh, ut.plot.get_label_mass(form_prop_kind),
                     subs.snapshot['redshift'][ti_now]))
    if y_kind == 'frac':
        Plot.make_label('f_{ave}=%.2f' % prop_frac_ave)
    Plot.draw('pp', DenBin.mids, vals, vals_err, draw_if=vals)


#===================================================================================================
# merge
#===================================================================================================
# rate / count ----------
def get_merge_kind(subs, zi, si):
    '''
    Get subhalo merge kind: 0 = cen-cen, 1 = cen-sat, 2 = sat-cen, 3 = sat-sat.
    '''
    par_zi, par1_si = zi + 1, subs[zi]['parent.index'][si]
    if 0 < par_zi < len(subs) and par1_si > 0:
        par2_si = subs[par_zi]['parent.n.index'][par1_si]
        if par2_si > 0:
            if subs[par_zi]['ilk'][par1_si] >= 1:
                if subs[par_zi]['ilk'][par2_si] >= 1:
                    return 0
                else:
                    return 1
            else:
                if subs[par_zi]['ilk'][par2_si] >= 1:
                    return 2
                else:
                    return 3
    return -1


def assign_mass_ratio(
    cats, ti_min, ti_max, mass_kind='star.mass', mass_limits=[9, Inf], disrupt_mf=0.007,
    scatter=0.15, source='li-marchesini'):
    '''
    Assign mass kind ratio of highest mass kind parents.
    Assign disrupted satellite as merger with central if sat just crossed threshold.

    Import catalog of [sub]halo, snapshot range (need to have one snapshot beyond ti_max),
    mass kind & range (apply at each snapshot), disrupt mass fraction, mass scatter, SHAM source.
    '''
    mass_ratio_kind = mass_kind + '.ratio'

    Say = ut.io.SayClass(assign_mass_ratio)
    if cats['catalog.kind'] == 'subhalo':
        mass_ratio_kind_raw = 'mass.peak.rat.raw'
    elif cats['catalog.kind'] == 'halo':
        mass_ratio_kind_raw = 'mass.ratio'
    tis = list(range(ti_min, ti_max + 2))
    if mass_kind in ['star.mass', 'mag.r']:
        sham.assign_to_catalog(cats, mass_kind, scatter, disrupt_mf, source, snapshot_indices=tis)
        Say.say('assigned %s' % mass_kind)
    for zi in tis[:-1]:
        par_zi = zi + 1
        cat = cats[zi]
        cat[mass_ratio_kind] = np.zeros(cat['child.index'].size, float32) - 1
        cis = ut.array.get_indices(cat[mass_kind], mass_limits)
        cis = ut.array.get_indices(cat[mass_ratio_kind_raw], [1e-5, Inf], cis)
        par1_cis = cat['parent.index'][cis]
        par1_ms = cats[par_zi][mass_kind][par1_cis]
        par2_cis = cats[par_zi]['parent.n.index'][par1_cis]
        par2_ms = cats[par_zi][mass_kind][par2_cis]
        if scatter == 0:
            # if no scatter, use 1st & 2nd highest m_peak parents
            cat[mass_ratio_kind][cis] = 10 ** -abs(par2_ms - par1_ms)
            # fix any that have par1_m <= 0
            cat[mass_ratio_kind][cis[par1_ms <= 0]] = 0
        else:
            # if scatter, assume highest m_peak parent is in top 2, loop over other parents
            # typically, only tiny fraction (< 1%) of mergers are out of order
            for cii, ci in enumerate(cis):
                par1_m = par1_ms[cii]
                par2_m = par2_ms[cii]
                if par1_m > 0:
                    if par2_m > par1_m:
                        par2_m, par1_m = par1_m, par2_m
                    # start loop
                    par2_ci = cats[par_zi]['parent.n.index'][par2_cis[cii]]
                    while par2_ci > 0:
                        if cats[par_zi][mass_kind][par2_ci] > par2_m:
                            par2_m = cats[par_zi][mass_kind][par2_ci]
                        par2_ci = cats[par_zi]['parent.n.index'][par2_ci]
                    cat[mass_ratio_kind][ci] = 10 ** -abs(par2_m - par1_m)
        # deal with disrupted satellites
        # if have disrupt_mf > 0 for SHAM, galaxies about to merge often have star_m = 0
        dis_number = 0
        if disrupt_mf:
            # select satellites with a central
            # ignore disrupted centrals, assume if got ejected then not a merger
            cis = ut.catalog.get_indices_subhalo(cat, 'mass.peak', [1, Inf], ilk=[-1, 0.1])
            cis = ut.array.get_indices(cat['mass.frac.min'], [-Inf, disrupt_mf], cis)
            par_cis = cat['parent.index'][cis]
            # just merged if parent was above m_frac threshold - assume not fluctuate up & down
            par_cis_survive, iis = ut.array.get_indices(
                cats[par_zi]['mass.frac.min'], [disrupt_mf, Inf], par_cis, get_masks=True)
            cis = cis[iis]
            cen_cis = cat['central.index'][cis]
            cen_cis_m, iis = ut.array.get_indices(
                cat[mass_kind], mass_limits, cen_cis, get_masks=True)
            cis = cis[iis]
            cen_cis_haspar, iis = ut.array.get_indices(
                cat['parent.index'], [1, Inf], cen_cis_m, get_masks=True)
            par_cis_survive = par_cis_survive[iis]
            cenpar_cis = cat['parent.index'][cen_cis_haspar]
            mass_ratios = 10 ** -abs(cats[par_zi][mass_kind][par_cis_survive] -
                                     cats[par_zi][mass_kind][cenpar_cis])
            # fix any that have cenpar_m <= 0
            mass_ratios[cenpar_cis <= 0] = 0
            cis = ut.array.get_indices(cat[mass_ratio_kind][cen_cis_haspar], [-Inf, mass_ratios])
            cat[mass_ratio_kind][cen_cis_haspar[cis]] = mass_ratios[cis]
            dis_number = cis.size
        if cat[mass_ratio_kind].max() >= 1:
            raise ValueError('max %s = %.2f, but should not occur' %
                             (mass_ratio_kind, cat[mass_ratio_kind].max()))
        Say.say('t_i %2d | %5d merge %5d disrupt' % (zi, par1_cis.size, dis_number))


def get_merge_hist(
    props, cats, ti_now=1, ti_max=22, mass_kind='star.mass', mass_ratio_min=0.2,
    mass_limits=[9, Inf], cis=None):
    '''
    If zi or i in props, get zi/i of last merger > mass_ratio_min while in mass range (-1 if never);
    if number in props,
    neig total number of mergers > mass_ratio_min since ti_max while in mass range;
    if number.tot in props, neig number of mergers > mass_ratio_min at each snapshot.

    Import merge properties to get (z.i, i, number, number.tot), [sub]halo catalog, snapshot range
    (need one snapshot prior to highest for recent merger), mass kind & ratio & range
    (apply at all snapshots).
    '''
    props = props.split()
    mass_ratio_kind = mass_kind + '.ratio'
    if cis is None:
        cis = ut.array.get_arange(cats[ti_now][mass_kind])
    par_cis = np.array(cis)
    par_zi = ti_now
    mrg = {}
    if 't.index' in props:
        mrg['z.index'] = ut.array.get_array_null(cis.size) - 1
    if 'index' in props:
        mrg['index'] = ut.array.get_array_null(cis.size) - 1
    if 'number' in props:
        mrg['number'] = np.zeros(cis.size, dtype=int32)
    if 'number.tot' in props:
        mrg['number.tot'] = np.zeros(ti_max + 1)
    cis_now = ut.array.get_arange(cis)
    while par_zi <= ti_max:
        # in mass range
        par_cis_m, iis = ut.array.get_indices(
            cats[par_zi][mass_kind], mass_limits, par_cis, get_masks=True)
        cis_now_m = cis_now[iis]
        # has parent
        par_cis_haspar, iis = ut.array.get_indices(
            cats[par_zi]['parent.index'], [1, Inf], par_cis_m, get_masks=True)
        cis_now_haspar = cis_now_m[iis]
        # above mass ratio
        par_cis_mrat, iis = ut.array.get_indices(
            cats[par_zi][mass_ratio_kind], [mass_ratio_min, Inf], par_cis_haspar, get_masks=True)
        cis_now_mrat = cis_now_haspar[iis]
        if 'number.tot' in props:
            mrg['number.tot'][par_zi] = par_cis_mrat.size
        if 'number' in props:
            mrg['number'][cis_now_mrat] += 1
        # assign those that say not already have merger
        if 'z.index' in props:
            ciis_mrg_new = ut.array.get_indices(mrg['t.index'][cis_now_mrat], [-Inf, -0.1])
            mrg['t.index'][cis_now_mrat[ciis_mrg_new]] = np.zeros(ciis_mrg_new.size, int32) + par_zi
        if 'index' in props:
            ciis_mrg_new = ut.array.get_indices(mrg['index'][cis_now_mrat], [-Inf, 0.1])
            mrg['index'][cis_now_mrat[ciis_mrg_new]] = par_cis_mrat[ciis_mrg_new]
        # shift to earlier snapshot
        par_cis = cats[par_zi]['parent.index'][par_cis_haspar]
        cis_now = cis_now_haspar
        par_zi += 1
    if len(props) == 1:
        return mrg[props[0]]
    else:
        mrg_return = []
        for prop in props:
            mrg_return.append(mrg[prop])
        return mrg_return


def plot_merge_frac_v_mass(
    subs, gal=None, ti_now=1, ti_max=22, gm_kind='star.mass', gm_limits=[9.6, 11.8], gm_width=0.2,
    gm_limits_history=[9, Inf], mass_ratio_min=0.2, ilk='central',
    hm_limits=[1, Inf], hm_width=None, vary_kind='galaxy', redshift_limits=[]):
    '''
    Plot merger fraction v mass considering all mergers back to zi.max.
    *** First, run:
      assign_mass_ratio(subs, 1, 22, 'star.mass', [9, Inf], 0.007, 0.15, 'li-marchesini')

    Import subhalo & galaxy catalog, snapshot range, max snapshot to look back,
    galaxy mass kind & range & bin width, mass range when looking back, merger mass ratio,
    ilk (cen, sat, all).
    '''
    Say = ut.io.SayClass(plot_merge_frac_v_mass)
    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    mass_ratio_name = gm_kind + '.ratio'
    if not len(subs[ti_now][mass_ratio_name]) and not len(subs[ti_max][mass_ratio_name]):
        raise ValueError('%s not assigned at all snapshots' % mass_ratio_name)
    sis = ut.catalog.get_indices_subhalo(subs[ti_now], gm_kind, gm_limits, hm_limits, ilk)
    masses = subs[ti_now][GHMassBin.vary.mass_kind][sis]
    mrg_tis = get_merge_hist(
        't.index', subs, ti_now, ti_max, gm_kind, mass_ratio_min, gm_limits_history, sis)
    mrg_fracs = np.zeros(GHMassBin.vary.number)
    mrg_fracs_err = np.zeros(GHMassBin.vary.number)
    for vmi in range(GHMassBin.vary.number):
        sis_m = ut.array.get_indices(masses, GHMassBin.vary.get_bin_limits(vmi))
        mrg_tis_m = mrg_tis[sis_m]
        mrg_number = mrg_tis_m[mrg_tis_m >= 0].size
        mrg_fracs[vmi], mrg_fracs_err[vmi] = Fraction.get_fraction(
            mrg_number, sis_m.size, 'normal')
        Say.say('%s %5.2f | gal %6d  merge %6d (%.3f)' %
                (gm_kind, GHMassBin.vary.mids[vmi], sis_m.size, mrg_number, mrg_fracs[vmi]))

    # central quiescent fraction from galaxy group catalog
    if gal is not None:
        gis = ut.catalog.get_indices_galaxy(
            gal, gm_kind, gm_limits, hm_limits, ilk, redshift_limits=redshift_limits)
        qu_fracs = np.zeros(GHMassBin.vary.number)
        qu_fracs_err = np.zeros(GHMassBin.vary.number)
        for vmi in range(GHMassBin.vary.number):
            sis_m = ut.array.get_indices(
                gal[GHMassBin.vary.mass_kind], GHMassBin.vary.get_bin_limits(vmi), gis)
            gis_qu = ut.catalog.get_indices_sfr(gal, 'lo', 'ssfr', sis_m)
            qu_fracs[vmi], qu_fracs_err[vmi] = Fraction.get_fraction(
                gis_qu.size, sis_m.size, 'normal')

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', GHMassBin.limits, [0, 1])
    Plot.set_axis_label(GHMassBin.vary.mass_kind, 'Fraction')
    Plot.make_window()
    if GHMassBin.vary.kind == 'halo' or (GHMassBin.vary.kind == 'galaxy' and ilk == 'satellite'):
        Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.limits)
    Plot.make_label('R>%.2f' % mass_ratio_min)
    Plot.draw('c', GHMassBin.mids, mrg_fracs, mrg_fracs_err, draw_if=(mrg_fracs > 0), ct='blue')
    Plot.make_label('merged since z=%.1f' % subs.snapshot['redshift'][ti_max + 1])
    if gal is not None:
        Plot.draw('c', GHMassBin.mids, qu_fracs, qu_fracs_err, draw_if=(qu_fracs_err < 0.2),
                  ct='red')
        Plot.make_label('quiescent')


def plot_merge_history_v_mass(
    subs, ti_now=1, ti_max=22, gm_kind='star.mass', gm_limits=[10.5, 10.9], gm_width=0.4,
    gm_limits_history=[9, Inf], mass_ratio_min=0.2, ilk='central',
    hm_limits=[1, Inf], hm_width=None, vary_kind='galaxy'):
    '''
    Plot merger fraction v mass considering all mergers back to u.max.
    *** First run:
      assign_mass_ratio(subs, 1, 22, 'star.mass', [9, Inf], 0.007, 0.15, 'li-drory-marchesini')

    Import subhalo catalog, snapshot range, max snapshot to look back,
    galaxy mass kind & range & bin width, mass range when tracking back, merger mass ratio,
    ilk (cen, sat, all), halo mass range & bin width, which mass to vary.
    '''
    mrg_number_limits = [0, 5]

    mrg_bin_numbers = np.arange(mrg_number_limits[0], mrg_number_limits[-1], 1)
    GHMassBin = ut.binning.GalaxyHaloMassBinClass(
        gm_kind, gm_limits, gm_width, 'halo.mass', hm_limits, hm_width, vary_kind)
    mass_ratio_kind = gm_kind + '.ratio'
    if not len(subs[ti_now][mass_ratio_kind]) and not len(subs[ti_max][mass_ratio_kind]):
        raise ValueError('%s not assigned at all snapshots' % mass_ratio_kind)
    sis = ut.catalog.get_indices_subhalo(subs[ti_now], gm_kind, gm_limits, hm_limits, ilk)
    masses = subs[ti_now][GHMassBin.vary.mass_kind][sis]
    mrg_numbers = get_merge_hist(
        'number', subs, ti_now, ti_max, gm_kind, mass_ratio_min, gm_limits_history, sis)
    MrgNumber = ut.statistic.StatisticClass()
    for mi in range(GHMassBin.vary.number):
        sis_m = ut.array.get_indices(masses, GHMassBin.vary.get_bin_limits(mi))
        mrg_numbers_m = mrg_numbers[sis_m]
        MrgNumber.append_dictionary(mrg_numbers_m, mrg_number_limits, mrg_bin_numbers.size)

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('linear', 'linear', [0.5, mrg_number_limits[-1] - 0.5], [0, 0.45])
    Plot.set_axis_label('Number of mergers', 'Fraction')
    Plot.make_window()
    Plot.set_label(0.4)
    if GHMassBin.vary.kind == 'halo' or (GHMassBin.vary.kind == 'galaxy' and ilk == 'satellite'):
        Plot.make_label(GHMassBin.fix.mass_kind, GHMassBin.fix.limits)
    Plot.make_label('R>%.2f' % mass_ratio_min)
    Plot.make_label('z_{max}=%.1f' % subs.snapshot['redshift'][ti_max + 1])
    for mi in range(GHMassBin.number):
        Plot.draw('h', MrgNumber.distr['bin.mid'][mi] - 0.5, MrgNumber.distr['probability'][mi],
                  MrgNumber.distr['prob.err'][mi], ct='blue')
        Plot.make_label(GHMassBin.vary.mass_kind, GHMassBin.vary.limits)


def plot_merge_frac_v_density(
    subs, ti_min, ti_max, mass_kind='mass.peak', mass_limits=[], mass_ratio_min=0.25,
    density_limits=[-1, 1.25], disrupt_mf=0, distance_max=10):
    '''
    Plot merge fraction v density.
    *** First, run: assign_mass_ratio(subs, ti_now, ti_max, gm_kind, gm_limits, disrupt_mf, scatter)

    Import subhalo catalog, snapshot range, mass kind & range & merger ratio threshold,
    log density range, disrupt mass fraction, max neighbor distance.
    '''
    dlog_den = 0.25

    t_width_mrg = 5 * subs.info['time.width'][ti_min]
    mass_ratio_kind = mass_kind + '.ratio'
    DensityBin = ut.binning.BinClass(density_limits, dlog_den)
    if mass_kind != 'mass.peak':
        print('# m.peak.min %.1f' %
              ut.catalog.convert_mass(subs[ti_min], mass_kind, mass_limits[0], 'mass.peak'))
    den_bin_numbers = np.zeros(DensityBin.number, int32)
    mrg_bin_numbers = np.zeros(DensityBin.number, float32)
    for ti in range(ti_min, ti_max + 1):
        print('# ti = %d' % ti)
        sis = ut.catalog.get_indices_subhalo(
            subs[ti], mass_kind, mass_limits, disrupt_mf=disrupt_mf)
        for _ in range(DensityBin.number):
            log_dens = subs[ti]['num-den.ave'] * np.array([10 ** DensityBin.mins[id],
                                                          10 ** (DensityBin.mins[id] + dlog_den)])
            sis_n = ut.array.get_indices(subs[ti]['neig.number'], log_dens, sis)
            if sis_n.size:
                den_bin_numbers[id] += sis_n.size
                sis_m = ut.array.get_indices(
                    subs[ti][mass_ratio_kind], [mass_ratio_min, Inf], sis_n)
                # scale merge counts to t_width_mrg
                mrg_bin_numbers[id] += sis_m.size * t_width_mrg / subs.info['time.width'][ti]
    mrg_fracs, mrg_fracs_err = Fraction.get_fraction(mrg_bin_numbers, den_bin_numbers, 'normal')

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'linear', density_limits, [0, 0.4])
    Plot.set_axis_label('\\rho_{gal}/\\bar{\\rho}_{gal} [%.0f kpc]' % distance_max,
                        'Merge fraction')
    Plot.make_window()
    Plot.make_label('z=%.1f' % subs.snapshot['redshift'][ti_min])
    Plot.make_label('R=%.2f  \Delta t=%.2f\,Gyr' % (mass_ratio_min, t_width_mrg))
    Plot.make_label(mass_kind, mass_limits)
    Plot.draw('pp', DensityBin.mids, mrg_fracs, mrg_fracs_err, draw_if=mrg_fracs, ct='blue')


def plot_merge_redshift_distr(
    subs, ti_min=1, ti_max=22, mass_kind='star.mass', mass_limits=[10.5, 10.9],
    mass_limits_history=[9, Inf], mass_ratio_min=0.2, plot_kind='last'):
    '''
    Import subhalo catalog, snapshot current & max to look back, mass kind & current range &
    range for history, minimum mass ratio, plot kind (last, all).
    Plot redshift distribution of mergers.
    ** First, run:
      assign_mass_ratio(subs, 1, 25, 'star.mass', [9, Inf], 0.007, 0.15, 'li-marchesini')
    '''
    Say = ut.io.SayClass(plot_merge_redshift_distr)
    x_bin_number = ti_max - ti_min + 1
    mass_ratio_kind = mass_kind + '.ratio'
    if not len(subs[ti_min][mass_ratio_kind]) and not len(subs[ti_max][mass_ratio_kind]):
        raise ValueError('%s not assigned at all snapshots' % mass_ratio_kind)
    sis = ut.array.get_indices(subs[ti_min][mass_kind], mass_limits)
    mrg_tis, mrg_numtots = get_merge_hist(
        't.i number_tot', subs, ti_min, ti_max, mass_kind, mass_ratio_min, mass_limits_history, sis)
    if plot_kind == 'all':
        mrg_tis = np.array([], int32)
        for zi in range(mrg_numtots.size):
            mrg_tis = np.append(mrg_tis, np.r_[[zi] * mrg_numtots[zi]].astype(int32))
    tis_mrg_nz = mrg_tis[mrg_tis >= 0]
    mrg_frac = tis_mrg_nz.size / sis.size
    Say.say('%d of %d (%.2f) had merger' % (tis_mrg_nz.size, sis.size, mrg_frac))
    Z = ut.statistic.StatisticClass(tis_mrg_nz, [ti_min - 0.5, ti_max + 0.5],
                                    bin_number=x_bin_number)
    # convert snapshot vals to redshift vals
    Z.distr['bin.mid'] = subs.snapshot['redshift'][Z.distr['bin.mid'].astype(int)]

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('linear', 'linear', [0, subs.snapshot['redshift'][ti_max + 1]],
                  [0, Z.distr['probability'].max() * 1.05])
    Plot.set_axis_label('Redshift', 'dP(merge)/dz')
    Plot.make_window()
    Plot.make_label(mass_kind, mass_limits, position_x=0.45)
    Plot.make_label('R>%.2f' % mass_ratio_min)
    Plot.make_label('f_{merge,tot}=%.2f' % mrg_frac)
    Plot.draw('c', Z.distr['bin.mid'], Z.distr['probability'], ct='blue')


def get_merge_stat(
    cat, mass_kind='mass.peak', mass_limits=[], mass_ratio_min=0.25, disrupt_mf=0, prop_kind='',
    prop_limits=[]):
    '''
    Get dictionary of merger statistics.

    Import catalog of [sub]halo at snapshot, mass kind & range, merger mass ratio,
    disruption mass fraction, second property.
    '''
    mrg = {'frac': 0, 'frac.err': 0, 'rate': 0, 'rate.err': 0}
    cis = ut.catalog.get_indices_subhalo(cat, mass_kind, mass_limits, disrupt_mf=disrupt_mf)
    if prop_kind:
        cis = ut.array.get_indices(cat[prop_kind], prop_limits, cis)
    mass_ratio_kind = mass_kind + '.ratio'
    mass_ratios = cat[mass_ratio_kind][cis]
    mrg['frac'], mrg['frac.err'] = Fraction.get_fraction(
        mass_ratios[mass_ratios > mass_ratio_min].size, cis.size, 'normal')
    mrg['rate'] = mrg['frac'] / cat.snapshot['time.width']
    mrg['rate.err'] = mrg['frac.err'] / cat.snapshot['time.width']
    return mrg


def plot_merge_rate(
    cats, ti_min, ti_max, mass_kind='mass.peak', mass_limits=[], mass_width=1,
    mass_ratio_min=0.25, disrupt_mf=0, prop_kind='', prop_range=[]):
    '''
    Plot merger rate.

    Import [sub]halo catalog, snapshot index range, mass kind & range & bin width,
    mass ratio minimum, disruption mass fraction, second property & range.
    '''
    mrg = {'frac': [], 'frac.err': [], 'rate': [], 'rate.err': []}
    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    tis = list(range(ti_min, ti_max + 1))
    # for k in mrg:
    #    mrg[k] = [[] for _ in range(vals.size)]
    for mi in range(MassBin.number):
        for zi in tis:
            mrg_z = get_merge_stat(
                cats[zi], mass_kind, MassBin.get_bin_limits(mi), mass_ratio_min, disrupt_mf,
                prop_kind, prop_range)
            ut.array.append_dictionary(mrg, mrg_z)

    ut.array.arrayize_dictionary(mrg)
    mrg['log rate'] = log10(mrg['rate'] + 1e-5)
    mrg['redshift'] = cats.snapshot['redshift'][tis]
    mrg['log redshift'] = log10(cats.snapshot['redshift'][tis] + 1)

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'log', [0, mrg['log redshift'].max() + 0.02], log10(mrg['rate']),
                  tick_locs=[-1, -1, -1, 0], extrafrac=0.05)
    Plot.set_axis_label('1+z', 'n_{merge}/n_{obj}/dt')
    Plot.make_window()
    Plot.make_label('R > %.2f' % mass_ratio_min, position_y=0.6)
    for mi in range(MassBin.number):
        Plot.set_lines(ct=mi)
        Plot.draw('c', mrg['log redshift'], mrg['log rate'][mi], draw_if=mrg['rate'][mi])
        Plot.make_label(mass_kind, MassBin.get_bin_limits(mi))
