'''
Analyze orbits of infalling halos or satellite subhalos.

Masses in {log M_sun}, positions and distances in [kpc comoving].

@author: Andrew Wetzel
'''


# system ----

import copy
import numpy as np
from numpy import log10
from scipy import integrate, stats
# local ----
import utilities as ut
from visualize import plot_sm


#===================================================================================================
# plot utility
#===================================================================================================
orbit_axis_limits = {
    'velocity.tan': (0, 2), 'velocity.rad': (0, 2), 'velocity.tot': (0, 2),
    'distance.peri': (0, 1), 'distance.apo': (1, 3),
    'circularity': (0, 1), 'eccentricity': (0, 3), 'energy': (0, 2), 'angle': (0, 90)
}

orbit_bin_number = {
    'velocity.tan': 24, 'velocity.rad': 24, 'velocity.tot': 36,
    'distance.peri': 12, 'distance.apo': 24,
    'circularity': 12, 'eccentricity': 60, 'energy': 24, 'angle': 16
}


#===================================================================================================
# orbit at virial infall
#===================================================================================================
orbit_dict = {
    'eccentricity': [],
    'circularity': [],
    'velocity.rad': [],
    'velocity.tan': [],
    'velocity.tot': [],
    'distance.peri': [],
    'distance.apo': [],
    'time.shell': [],
    'time.peri': []
    #'radius.vir': [],
    #'momentum.angular': []
    #'period': [],
    #'energy': [],
    #'angle': [],
}


def get_orbit(cat, ci_1, ci_2, select_kind='shell'):
    '''
    Get orbital parameters of object 2 (smaller) object onto 1 (bigger).

    Parameters
    ----------
    catalog of [sub]halos at snapshot: dict
    [sub]halo indices: int
    selection kind: string
        options: shell, all
    '''
    radius_width = 0.25  # fraction of R_halo to find satellites (for shell)
    virial_kind = 'fof.100m'

    if virial_kind == 'fof.100m':
        mass_kind = 'mass.fof'
    elif virial_kind == '200c':
        mass_kind = 'mass.200c'
    elif virial_kind == '200m':
        mass_kind = 'mass.200m'

    Orbit = ut.orbit.OrbitClass(cat.Cosmology)
    HaloProperty = ut.halo_property.HaloPropertyClass(cat.Cosmology)

    # virial radii [kpc physical]
    if mass_kind in ['mass.200c', 'mass.200m']:
        rad_vir_1 = HaloProperty.radius_virial_catalog(virial_kind, cat, ci_1)
        rad_vir_2 = HaloProperty.radius_virial_catalog(virial_kind, cat, ci_2)
    else:
        rad_vir_1 = HaloProperty.radius_virial_catalog(virial_kind, cat, ci_1)
        rad_vir_2 = HaloProperty.radius_virial_catalog(virial_kind, cat, ci_2)
    # radius at which to compute infall velocity
    dist_inf = rad_vir_1

    # get orbital parameters
    orb = ut.orbit.get_orbit_dictionary_catalog(cat, ci_1, ci_2, mass_kind, mass_kind)
    mass_1 = 10 ** cat[mass_kind][ci_1]
    mass_2 = 10 ** cat[mass_kind][ci_2]
    mass_tot = mass_1 + mass_2

    if select_kind == 'shell':
        dist_min = rad_vir_1 + rad_vir_2
        dist_max = rad_vir_1 * (1 + radius_width) + rad_vir_2
        # these are not within search radius, so do not use
        if dist_min > orb['distance'] or orb['distance'] > dist_max:
            return 0

    # host halo virial circular velocity to scale to
    vel_vir = (Orbit.grav_sim_units * mass_1 / rad_vir_1) ** 0.5

    # compute values at virial radius
    if orb['distance.peri'] > dist_inf or 0 < orb['distance.apo'] < dist_inf:
        vel_tan_inf = vel_rad_inf = vel_tot = -1
    else:
        vel_rad_inf = (2 * orb['energy.tot'] + 2 * Orbit.grav_sim_units * mass_tot / dist_inf -
                       (orb['momentum.angular'] / dist_inf) ** 2) ** 0.5
        vel_tan_inf = orb['distance'] * orb['velocity.tan'] / dist_inf
        vel_tot = (vel_rad_inf ** 2 + vel_tan_inf ** 2) ** 0.5
    if np.isnan(vel_rad_inf) or np.isnan(vel_tan_inf):
        vel_rad_inf = vel_tan_inf = vel_tot = -1

    if select_kind == 'shell':
        if orb['distance.peri'] > dist_min:
            time_shell = -3
        elif orb['distance.apo'] < dist_max:
            time_shell = -2
        else:
            time_shell = Orbit.time_integrate(dist_max, dist_min, orb['energy.tot'],
                                              orb['momentum.angular'], mass_tot)
        if np.isnan(time_shell):
            time_shell = -1
            print('! bad time.shell integration')
    else:
        time_shell = -1

    # correct pericenter for NFW profile
    if 0 < orb['distance.apo'] < rad_vir_1:
        vir_concen = cat['c.200c'][ci_1]
        # use NFW distance.peri
        orb['distance.peri'] = Orbit.distance_extremum(
            'peri', rad_vir_1, orb['energy.tot'], orb['momentum.angular'], mass_tot, rad_vir_1,
            vir_concen)
        time_peri = Orbit.time_integrate(rad_vir_1, orb['distance.peri'], orb['energy.tot'],
                                         orb['momentum.angular'], mass_tot, rad_vir_1, vir_concen)
    else:
        time_peri = -1

    # orbital parameters scale to host halo virial values
    orb_vir = {}
    orb_vir['time.shell'] = time_shell
    orb_vir['time.peri'] = time_peri
    orb_vir['eccentricity'] = orb['eccentricity']
    orb_vir['velocity.rad'] = vel_rad_inf / vel_vir
    orb_vir['velocity.tan'] = vel_tan_inf / vel_vir
    orb_vir['velocity.tot'] = vel_tot / vel_vir
    orb_vir['distance.peri'] = orb['distance.peri'] / rad_vir_1
    orb_vir['distance.apo'] = orb['distance.apo'] / rad_vir_1
    if orb_vir['eccentricity'] < 1:
        orb_vir['circularity'] = (1 - orb_vir['eccentricity'] ** 2) ** 0.5
    else:
        orb_vir['circularity'] = -1
    '''
    if vel_rad_inf > 0 and vel_tan_inf > 0:
        orb_vir['angle'] = np.arccos(vel_rad_inf / (vel_rad_inf**2 + vel_tan_inf**2) ** 0.5) *
        ut.const.deg_per_rad
    else:
        orb_vir['angle'] = -1
    orb_vir['energy.tot'] = orb['energy.tot'] / (-0.5 * Orbit.grav_sim_units * mass_tot / rad_vir_1)
    orb_vir['period'] = (4*pi**2 / (Orbit.grav_sim_units * mass_tot) * a_semi**3) ** 0.5
    '''
    return orb_vir


def get_infall_orbits(hals, ti, mass_kind='mass.fof', mass1_limits=[], mass2_limits=[],
                      select_kind='shell'):
    '''
    Get dict of orbital properties of infalling satellite halos.

    Parameters
    ----------
    catalog of halos across snapshots: list
    snapshot index: int
    halo mass kind: string
    host halo mass limits: list
    satellite halo mass limits: list
    radius selection kind: string
        options: shell, all
    '''
    ti_max = min(len(hals[ti][mass_kind]) - 1, ti + 6)

    orb = copy.deepcopy(orbit_dict)
    his = ut.array.get_indices(hals[ti][mass_kind], mass1_limits)
    for hi in his:
        chi_zi, chi_i = ti - 1, hals[ti]['child.index'][hi]
        if chi_i > 0 and hals[chi_zi]['parent.index'][chi_i] == hi:
            if hals[chi_zi]['mass.fof.ratio'][chi_i] > 0:
                parn_i = hals[ti]['parent.n.index'][hi]
                while parn_i > 0 and hals[ti][mass_kind][parn_i] > mass2_limits[0]:
                    if hals[ti][mass_kind][parn_i] < mass2_limits[1]:
                        if not ut.catalog.is_orphan(hals, ti, ti_max, parn_i):
                            orb_i = get_orbit(hals[ti], hi, parn_i, select_kind)
                            if orb_i:
                                for k in orb_i:
                                    orb[k].append(orb_i[k])
                    parn_i = hals[ti]['parent.n.index'][parn_i]
    for k in orb:
        orb[k] = np.array(orb[k])
    return orb


def get_infall_orbits_stats(
    hal, ti_limits, mass_kind='mass.fof', mass_1_limits=[], mass_2_limits=[]):
    '''
    Return dictionary of median/ave orbital parameters.

    Parameters
    ----------
    hal : list : catalog of [sub]halos across snapshots
    ti_limits : list : min and max limits of snapshot indices
    mass_kind : string : host halo mass kind
    mass_1_limits : list : min and max limits of host halo mass
    mass_2_limits : list : min and max limits of satellite halo mass
    '''
    select_kind = 'shell'  # radial selection method (shell, all)

    if select_kind == 'shell':
        oversample_fac = 20  # oversample fraction for shell method
    else:
        oversample_fac = 1
    orb_tot = copy.deepcopy(orbit_dict)
    obj_number_tot = 0

    nonzero_number = copy.deepcopy(orbit_dict)
    for k in orb_tot:
        nonzero_number[k] = 0

    for zi in range(min(ti_limits), max(ti_limits) + 1):
        orbtemp = get_infall_orbits(hal, zi, mass_kind, mass_1_limits, mass_2_limits, select_kind)
        obj_number = orbtemp['eccentricity'].size
        obj_number_tot += obj_number
        print('i %d | z %.2f | t_width %.2f' %
              (zi, hal.snapshot['redshift'][zi], hal.snapshot['time.width'][zi]))

        for k in orbtemp:
            nonzero_number[k] += len(orbtemp[k][orbtemp[k] > 0])

        if select_kind == 'shell':
            # correct for crossing time fractions
            orb = copy.deepcopy(orbit_dict)
            time_max = orbtemp['time.shell'].max()
            bad_number = 0
            peribad_number = 0
            apobad_number = 0
            for timei in range(orbtemp['time.shell'].size):
                if orbtemp['time.shell'][timei] < 0:
                    if orbtemp['time.shell'][timei] == -3:
                        peribad_number += 1
                    elif orbtemp['time.shell'][timei] == -2:
                        apobad_number += 1
                    orbtemp['time.shell'][timei] = time_max
                    bad_number += 1
            time_min = orbtemp['time.shell'].min()
            print('time.shell = [%.2f, %.2f], ratio = %.1f |' %
                  (time_min, time_max, time_max / time_min), end='')
            print('have no time.shell = %d (peri = %d, apo = %d) of %d (%.2f)' %
                  (bad_number, peribad_number, apobad_number, len(orbtemp['tshell']),
                   bad_number / len(orbtemp['tshell'])))
            # scale number by snapshot time width (rough agreement with 'all' sample)
            time_weights = hal.snapshot['time.wid'][zi - 1] / orbtemp['time.shell']
            for k in orbtemp:
                for obji in range(obj_number):
                    orb[k].extend(
                        [orbtemp[k][obji]] * int(round(time_weights[obji] * oversample_fac)))
                orb[k] = np.array(orb[k])
        else:
            orb = orbtemp
        for k in orb_tot:
            orb_tot[k] = np.append(orb_tot[k], orb[k])

    temp = copy.deepcopy(orbit_dict)
    for k in orb_tot:
        if 'time.shell' not in k:
            tnz = orb_tot[k].compress(orb_tot[k] > 0)
            # find median
            temp[k + '.med'] = np.array([np.median(tnz), np.percentile(tnz, 25),
                                         np.percentile(tnz, 75)])
            # find average
            temp[k + '.ave'] = np.array([np.mean(tnz), stats.sem(tnz), np.std(tnz, ddof=1)])
            # fix sem estimates since oversampled
            if select_kind == 'shell':
                temp[k + '.ave'][1] = (temp[k + '.ave'][1] *
                                       (oversample_fac ** 0.5 * (len(tnz) / (len(tnz) - 1)) ** 0.5))

    for k in temp:
        if k not in orb_tot:
            orb_tot[k] = temp[k]

    frac_std = orb_tot['circ.ave'][2] / orb_tot['circ.ave'][0]
    frac_sem = orb_tot['circ.ave'][1] / orb_tot['circ.ave'][0]
    # for non-normal distributions, ave -> median
    for k in ['eccentricity', 'rperi', 'rapo']:
        orb_tot[k + '.ave'] = np.array(
            [orb_tot[k + '.med'][0],
             2 ** 0.5 * frac_sem * orb_tot[k + '.med'][0],
             2 ** 0.5 * frac_std * orb_tot[k + '.med'][0]])

    print('ecccentricity %.3f (%.3f, %.3f) | not %d    est %d' %
          (orb_tot['ecccentricity.ave'][0], orb_tot['ecccentricity.ave'][2],
           orb_tot['ecccentricity.ave'][1],
           obj_number_tot, len(orb_tot['eccentricity']) / oversample_fac))
    print('circularity %.3f (%.3f, %.3f) | (%.2f)' %
          (orb_tot['circ.ave'][0], orb_tot['circularity.ave'][2], orb_tot['circularity.ave'][1],
           nonzero_number['circularity'] / obj_number_tot))
    print('velocity.tot %.3f (%.3f, %.3f) | (%.2f)' %
          (orb_tot['velocity.tan.ave'][0], orb_tot['velocity.tan.ave'][2],
           orb_tot['velocity.tan.ave'][1], nonzero_number['velocity.tot'] / obj_number_tot))
    print('velocity.rad' '%.3f (%.3f, %.3f) | (%.2f)' %
          (orb_tot['velocity.rad.ave'][0], orb_tot['velocity.rad.ave'][2],
           orb_tot['velocity.rad.ave'][1], nonzero_number['velocity.rad'] / obj_number_tot))
    print('velocity.tot %.3f (%.3f, %.3f) | (%.2f)' %
          (orb_tot['velocity.tot.ave'][0], orb_tot['velocity.tot.ave'][2],
           orb_tot['velocity.tot.ave'][1], nonzero_number['velocity.tot'] / obj_number_tot))
    print('distance.peri %.3f (%.3f, %.3f) | (%.2f)' %
          (orb_tot['distance.peri.ave'][0], orb_tot['distance.peri.ave'][2],
           orb_tot['distance.peri.ave'][1], nonzero_number['rperi'] / obj_number_tot))
    print('distance.apo %.3f (%.3f, %.3f) | (%.2f)' %
          (orb_tot['distance.apo.ave'][0], orb_tot['distance.apo.ave'][2],
           orb_tot['distance.apo.ave'][1], nonzero_number['distance.apo'] / obj_number_tot))
    print('time.peri %.3f (%.3f, %.3f) | (%.2f)' %
          (orb_tot['time.peri.ave'][0], orb_tot['time.peri.ave'][2], orb_tot['time.peri.ave'][1],
           nonzero_number['time.peri'] / obj_number_tot))
    #print('angle %.2f (%.2f, %.3f) | (%.2f)' %
    #       (orb_tot['angle.ave'][0], orb_tot['ang.ave'][2], orb_tot['angle.ave'][1],
    #nonzero_number['angle']/obj_number_tot)
    orb_tot['oversample.fac'] = oversample_fac
    return orb_tot


def get_df2d(vr, vt, source='benson', i=0):
    '''
    Get 2-d velocity distribution function from fits.
    '''
    if source == 'benson':
        # i for z = 0, 1
        a1 = [3.90, 6.38]
        a2 = [2.49, 2.30]
        a3 = [10.2, 18.8]
        a4 = [0.684, 0.506]
        a5 = [0.354, -0.0934]
        a6 = [1.08, 1.05]
        a7 = [0.510, 0.267]
        a8 = [0.206, -0.154]
        a9 = [0.315, 0.157]
    elif source == 'wang':
        # i for dr
        a1 = [2.6595, 3.2861]
        a2 = [2.12, 2.62]
        a3 = [2.90, 4.48]
        a4 = [-0.333, -0.525]
        a5 = [-0.490, 0.238]
        a6 = [1.17, 1.20]
        a7 = [0.155, 0.140]
        a8 = [-0.564, -0.731]
        a9 = [0.314, 0.294]
    b1 = a3[i] * np.exp(-a4[i] * (vt - a5[i]) ** 2)
    b2 = a6[i] * np.exp(-a7[i] * (vt - a8[i]) ** 2)
    return a1[i] * vt * np.exp(-a2[i] * (vt - a9[i]) ** 2 - b1 * (vr - b2) ** 2)


#===================================================================================================
# utilities
#===================================================================================================
def df_vel(source, i, vrmin, vrmax, vtmin, vtmax, dv):
    '''
    Make 1-d or 2-d velocity distribution function from fit.
    '''
    return integrate.dblquad(get_df2d, vtmin, vtmax, lambda _: vrmin, lambda _: vrmax,
                             (source, i))[0] / dv


def fit_circ(x, p):
    return p[0] * x ** 1.05 * (1 - x) ** p[1]


def get_circ_normalization(p):
    return 1 / integrate.quad(fit_circ, 0, 1, (p))[0]


def fit_rperi(x, p):
    return p[0] * np.exp(-(x / p[1]) ** 0.85)


def get_rperi_normalization(p):
    return 1 / integrate.quad(fit_rperi, 0, 1, (p))[0]


def fit_orbit_params(x, p):
    return p[0] * (1 + p[1] * (10 ** x) ** p[2])


def plot_orbit_fit_params(hals, ti_min, ti_max, mass_kind='mass.fof', m_1_limits=[], m_2_limits=[]):
    '''
    Plot fits to circularity & distance.peri v halo mass.

    Import halo catalog, snapshot range, host halo mass kind & range, satellite halo mass range.
    '''
    mass_width = 0.5
    props = ['circularity', 'distance.peri']
    prop_params = 2
    axis_x_limit_dict = {'circularity': (0, 1), 'distance.peri': (0, 1)}
    bin_number_dict = {'circularity': 12, 'distance.peri': 12}

    mass_bins = ut.array.get_arange_safe(m_1_limits, mass_width)
    ps = []
    orbs = []
    Stat = ut.statistic.StatisticClass()
    fitparams = np.zeros((prop_params * len(props), 2, mass_bins.size))
    for mi in range(mass_bins.size):
        ps.append({})
        orbs.append(get_infall_orbits_stats(
            hals, ti_min, ti_max, mass_kind, [mass_bins[mi], mass_bins[mi] + mass_width],
            m_2_limits))

        # make probability distribution from histogram & correct for oversample
        for x in axis_x_limit_dict:
            ps[mi][x] = Stat.get_distribution_dict(
                orbs[mi][x], axis_x_limit_dict[x], bin_number=bin_number_dict[x])

        for k in ps[mi]:
            if '.err' in k:
                ps[mi][k] = ps[mi][k] * (2.5 * orbs['oversample.fac']) ** 0.5

        # fit distribution
        k = 'circularity'
        # make sure no bin has uncertainty = 0 for fit
        ps[mi][k]['y.err'] = ps[mi][k]['y.err'].clip(ps[mi][k]['y.err'].mean())
        params = [[4., 0, 100], [1.5, 0, 4]]
        fit = ut.math.fit(fit_circ, params, ps[mi][k]['x'], ps[mi][k]['y'], ps[mi][k]['yer'])
        fitparams[0][0][mi], fitparams[0][1][mi] = fit.params[0], fit.perror[0]
        fitparams[1][0][mi], fitparams[1][1][mi] = fit.params[1], fit.perror[1]
        k = 'distance.peri'
        # make sure no bin has uncertainty = 0 for fit
        ps[mi][k]['y.err'] = ps[mi][k]['y.err'].clip(ps[mi][k]['y.err'].mean())
        params = [[2., 0, 10], [0.2, 0, 1]]
        fit = ut.math.fit(fit_rperi, params, ps[mi][k]['x'], ps[mi][k]['y'], ps[mi][k]['y.err'])
        fitparams[2][0][mi], fitparams[2][1][mi] = fit.params[0], fit.perror[0]
        fitparams[3][0][mi], fitparams[3][1][mi] = fit.params[1], fit.perror[1]
    for fiti in range(1, 4):
        fitparams[fiti][1] = fitparams[fiti][1].clip(0.04 * fitparams[fiti][0].mean())

    mass_bins += 0.5 * mass_width
    MassChar = ut.cosmic_structure.MassNonlinearClass()
    mass_char = MassChar.value(hals.snapshot['redshift'][ti_min])
    mass_char_fracs = mass_bins - mass_char
    print('halo.m:    ')
    for m in mass_bins:
        print('%6.2f ' % m, end='')
    print()
    print('mass.char.frac: ', end='')
    for m in mass_char_fracs:
        print('%6.2f ' % m, end='')
    print()
    for pi in range(len(fitparams)):
        print('param%d:         ' % pi, end='')
        for param in fitparams[pi][0]:
            print('%6.2f ' % param, end='')
        print()
    fits = plot_orbit_fit_params2(mass_char_fracs, fitparams)

    return mass_char_fracs, fitparams, fits


def plot_orbit_fit_params2(masses, fitparams, fits=0):
    '''
    .
    '''
    # plotting
    axis_lab_ys = ['c1', 'c2', 'p1', 'p2']
    axis_lab_xs = ['', '', '', 'halo.mass']
    panel_number_x = 1
    panel_number_y = -4
    x_fit = np.arange(masses.min() - 0.4, masses.max() + 0.4, 0.2)
    axis_x_limits = x_fit
    if np.isscalar(fits):
        fits = np.zeros((len(fitparams), 3))
        params = [[5.5, 0, 20], [0.5, -100, 100], [1.1, -15, 15]]
        for pi in range(len(fitparams)):
            fit = ut.math.fit(fit_orbit_params, params, masses, fitparams[pi][0], fitparams[pi][1])
            fits[pi] = fit.params

    Plot = plot_sm.PlotClass()
    for pi in range(len(fitparams)):
        y_fit = fit_orbit_params(x_fit, fits[pi])
        y_limits = [y_fit.min() * 0.75, y_fit.max() * 1.25]
        Plot.make_panel(axis_lab_xs[pi], axis_lab_ys[pi], axis_x_limits, y_limits, 'linear', 'linear',
                        panel_number_x, panel_number_y, pi, 3, 1.1)
        Plot.draw('cp', masses, fitparams[pi][0], fitparams[pi][1], 'linear')
        Plot.draw('c', x_fit, y_fit)

    return fits


def plot_orbit_fit_params_all(m_char_fracs, fit_params):
    '''
    .
    '''
    panel_number = 4
    axis_x_limits = [-1.4, 5.4]
    x_fit = ut.array.get_arange_safe(axis_x_limits, 0.1)
    fits = np.zeros((panel_number, 3))
    y_fits = np.zeros((panel_number, x_fit.size))
    params = [[5.5, 0, 20], [0.5, -100, 100], [1.1, -15, 15]]
    masses_all = []
    binlim = copy.deepcopy(m_char_fracs)
    for mi in range(len(m_char_fracs)):
        # binlim[mi] -= 4 * log10(1 + mi )
        masses_all = np.concatenate((masses_all, binlim[mi][:]))
    fitparams_all = []
    for panel_i in range(panel_number):
        fitparams_all.append([[], []])
        for mi in range(len(m_char_fracs)):
            fitparams_all[panel_i][0] = np.concatenate((fitparams_all[panel_i][0],
                                                       fit_params[mi][panel_i][0]))
            fitparams_all[panel_i][1] = np.concatenate((fitparams_all[panel_i][1],
                                                       fit_params[mi][panel_i][1]))
        fit = ut.math.fit(fit_orbit_params, params, masses_all, fitparams_all[panel_i][0],
                          fitparams_all[panel_i][1])
        fits[panel_i] = fit.params
        y_fits[panel_i] = fit_orbit_params(x_fit, fits[panel_i])

    # re-normalize c0 & p0 to fit
    for panel_i in [0, 2]:
        for mi in range(x_fit.size):
            if panel_i == 0:
                y_fits[panel_i][mi] = (
                    y_fits[panel_i][mi] * get_circ_normalization([y_fits[panel_i][mi],
                                                                  y_fits[panel_i + 1][mi]]))
            # elif panel_i == 2:
            #    y_fits[panel_i][mi] *= get_rperi_normalization([y_fits[panel_i][mi],
            # y_fits[panel_i+1][mi]])
        fit = ut.math.fit(fit_orbit_params, params, x_fit, y_fits[panel_i],
                          np.r_[0.03 * y_fits[panel_i].mean() * y_fits[panel_i].size])
        fits[panel_i] = fit.params
        y_fits[panel_i] = fit_orbit_params(x_fit, fits[panel_i])

    # plotting
    Plot = plot_sm.PlotClass()
    axis_lab_ys = ['c0', 'c1', 'p0', 'p1']
    axis_lab_xs = ['', '', '', 'M/M_*']
    panel_number_x = 1
    panel_number_y = -4
    for panel_i in range(panel_number):
        axis_y_limits = [fitparams_all[panel_i][0].min() * 0.8,
                         fitparams_all[panel_i][0].max() * 1.2]
        Plot.make_panel(
            axis_lab_xs[panel_i], axis_lab_ys[panel_i], axis_x_limits, axis_y_limits, 'linear', 'linear',
            panel_number_x, panel_number_y, panel_i, 3, 1.1)
        for mi in range(len(m_char_fracs)):
            Plot.draw('cp', binlim[mi], fit_params[mi][panel_i][0], fit_params[mi][panel_i][1],
                      'linear')
        Plot('c', x_fit, y_fits[panel_i])

    return fits


def plot_orbit_distr(
    halss, ti_mins, ti_maxs, mass_kind, m_1_limitss, m_2_limitss, props='', mass_scaling=''):
    '''
    Plot orbital distributions.

    Import halo catalog[s], snpahost range[s], halo mass kind & host range[s] & satellite range[s],
    properties to plot, mass scalling ('', m.char).
    '''
    if mass_kind in halss:
        halss = [halss]
    ti_mins, ti_maxs, m_1_limitss, m_2_limitss = ut.array.arrayize(
        [ti_mins, ti_maxs, m_1_limitss, m_2_limitss], repeat_number=len(halss))

    axis_lab_y = {}
    for k in orbit_axis_label:
        axis_lab_y[k] = 'df/d%s' % orbit_axis_label[k]

    circ_params_mass = np.array([[3.38, 0.567, 0.152], [0.242, 2.36, 0.108]])
    rperi_params_mass = np.array([[3.14, 0.152, 0.410], [0.450, -0.395, 0.109]])

    #MChar = eps.MassNonlinearClass()
    #masses_char = Mchar.value(halss[0]['redshift'][ti_mins])
    masses_char = (12.42 - 1.56 * halss[0]['redshift'][ti_mins] + 0.038 *
                   halss[0]['redshift'][ti_mins] ** 2)

    if mass_scaling == 'mass.char':
        mass_width = m_1_limitss[1] - m_1_limitss[0]
        mass_char_frac = m_1_limitss[0] + 0.5 * mass_width - masses_char[0]
        print('mass.char.frac %.1f | m_width %.2f' % (10 ** mass_char_frac, mass_width))
        m_1_limits = [masses_char + mass_char_frac - 0.5 * mass_width,
                      masses_char + mass_char_frac + 0.5 * mass_width]
        print('mass ranges: ', end='')
        for halo_i in range(len(halss)):
            print('(%.1f, %.1f)' % (m_1_limitss[0], m_1_limitss[1]), end='')
        print()
    mass_char_fs = m_1_limitss + 0.5 * (m_1_limitss[1] - m_1_limitss[0]) - masses_char
    mass_char_fs_rperi = mass_char_fs - 4 * log10(1 + halss[0]['redshift'][ti_mins])

    ps = []
    orbs = []
    fits = []
    fits_uni = []
    Stat = ut.statistic.StatisticClass()
    for halo_i in range(len(halss)):
        ps.append({})
        fits.append({})
        fits_uni.append({})
        orbs.append(get_infall_orbits_stats(
            halss[halo_i], ti_mins[halo_i], ti_maxs[halo_i], mass_kind, m_1_limits,
            m_2_limitss[[halo_i]]))

        # make probability distribution from histogram & correct for oversample
        for kx in orbit_axis_limits:
            ps[halo_i][kx] = Stat.get_distribution_dict(
                orbs[halo_i][kx], orbit_axis_limits[kx], bin_number=orbit_bin_number[kx])
        for k in ps[halo_i]:
            if '.err' in k:
                ps[halo_i][k] = ps[halo_i][k] * (2.5 * orbs['oversample.fac']) ** 0.5

        # fit distribution
        for k in props:
            if k == 'circularity' or k == 'distance.peri':
                # make sure no bin has uncertainty = 0 for fit
                ps[halo_i][k]['y.err'] = ps[halo_i][k]['y.err'].clip(ps[halo_i][k]['y.err'].mean())
                xfit = np.linspace(
                    orbit_axis_limits[k][0], orbit_axis_limits[k][1], orbit_bin_number[k] * 10)

                if k == 'circularity':
                    params = [[4., 0, 100], [1.5, 0, 4]]
                    fit = ut.math.fit(fit_circ, params, ps[halo_i][k]['x'], ps[halo_i][k]['y'],
                                      ps[halo_i][k]['y.err'])
                    yfit = fit_circ(xfit, fit.params)
                    circ_params = [fit_orbit_params(mass_char_fs[halo_i], circ_params_mass[0]),
                                   fit_orbit_params(mass_char_fs[halo_i], circ_params_mass[1])]
                    normalization = get_circ_normalization(circ_params)
                    circ_params[0] = circ_params[0] * normalization
                    yfit_uni = fit_circ(xfit, circ_params)
                elif k == 'distance.peri':
                    params = [[2., 0, 10], [0.2, 0, 1]]
                    fit = ut.math.fit(fit_rperi, params, ps[halo_i][k]['x'], ps[halo_i][k]['y'],
                                      ps[halo_i][k]['y.err'])
                    yfit = fit_rperi(xfit, fit.params)
                    rperi_params = [
                        fit_orbit_params(mass_char_fs_rperi[halo_i], rperi_params_mass[0]),
                        fit_orbit_params(mass_char_fs_rperi[halo_i], rperi_params_mass[1])]
                    yfit_uni = fit_rperi(xfit, rperi_params)

                # print '%6s fit: ' % k,
                # for pp in fit.params:
                #    print '%.2f ' % pp,
                # print ''
                fits[halo_i][k] = {'x': xfit, 'y': yfit}
                fits_uni[halo_i][k] = {'x': xfit, 'y': yfit_uni}

    # plot ----------
    label_number = 1  # panel on which to put label (0 is top)
    panel_number_x = 1
    panel_number_y = len(props)
    if panel_number_y == 1:
        lw = 5
        ex = 1.5
    elif panel_number_y == 2:
        lw = 5
        ex = 1.5
    else:
        lw = 3
        ex = 1.01
    Plot = plot_sm.PlotClass()
    for propi in range(len(props)):
        k = props[propi]
        y_max = 0
        for halo_i in range(len(halss)):
            y_max = max(y_max, (ps[halo_i][k]['y'] + ps[halo_i][k]['yer']).max())
        axis_y_limits = [0, y_max * 1.08]
        Plot.make_panel(orbit_axis_label[k], axis_lab_y[k], orbit_axis_limits[k], axis_y_limits,
                        'linear', 'linear', panel_number_x, panel_number_y, propi, lw, ex)
        if propi == label_number:
            Plot.make_label('z=%.1f' % halss[0]['redshift'][ti_mins[0]])
        for halo_i in range(len(halss)):
            Plot.draw('c', ps[halo_i][k]['x'], ps[halo_i][k]['y'], ps[halo_i][k]['yer'])
            # plot average/median
            Plot.draw('c', [orbs[halo_i][k + '.ave'][0], orbs[halo_i][k + '.ave'][0]],
                      [0, y_max / 5], ct='blue')
            # for r.peri, plot value not using NFW
            Plot.draw('c', [0.215, 0.215], [0, y_max / 5], ct='blue', lt='--')
            # plot fit to this curve
            # sm.ctype('cyan'); sm.ltype(1)
            # if k == 'circularity' or k == 'rperi':
            #    sp.plot('c', fits[halo_i][k]['x'], fits[halo_i][k]['y'])
            # plot universal fit
            if k == 'circularity' or k == 'rperi':
                Plot.draw('c', fits_uni[halo_i][k]['x'], fits_uni[halo_i][k]['y'],
                          ct='red', lt='--')

        # literature velocity fits
        '''
        if k == 'vt' or k == 'vr':
            dv = 0.025
            velmin_fit, velmax_fit = 0, 2
            vels_fit = np.arange(velmin_fit, velmax_fit, dv)
            dfbenson = {'vt': [], 'vr': []}
            dfwang = {'vt': [], 'vr': []}
            for v in vels_fit:
                iz = 0
                dfbenson['vr'].append(df_vel('benson', iz, v, v+dv, velmin_fit, velmax_fit,dv))
                dfbenson['vt'].append(df_vel('benson', iz, velmin_fit, velmax_fit, v, v+dv,dv))
                jr = 1
                dfwang['vr'].append(df_vel('wang', jr, v, v+dv, velmin_fit, velmax_fit,dv))
                dfwang['vt'].append(df_vel('wang', jr, velmin_fit, velmax_fit, v, v+dv,dv))
            vels_fit += 0.5*dv

            ltype = 2; sm.ltype(ltype); sm.ctype('magenta')
            sp.plot('c', vs_fit, dfbenson[k])
            if propi == label_number:
                sp.label(xl, yl, 'B05', ltype); yl-=dyl
            ltype = 4; sm.ltype(ltype); sm.ctype('cyan')
            sp.plot('c', vs_fit, dfwang[k])
            if propi == label_number:
                sp.label(xl, yl, 'W05', ltype); yl-=dyl
        '''


def plot_orbit_correlate(hals, ti_min, ti_max, mass_kind, m_1_limits, m_2_limits=[], props=''):
    '''
    Import halo catalog, snapshot range, halo mass kind & host range & satellite range,
    properties to plot against each other, mass scalling ('', m.char).
    '''
    if not m_2_limits:
        m_2_limits = m_1_limits
    axis_y_label = {}
    for k in orbit_axis_label:
        axis_y_label[k] = 'df/d%s' % orbit_axis_label[k]

    orb = get_infall_orbits_stats(hals, ti_min, ti_max, mass_kind, m_1_limits, m_2_limits)

    # make probability distribution from histogram & correct for oversample
    p = {}
    Stat = ut.statistic.StatisticClass()
    for kx in orbit_axis_limits:
        p[k] = Stat.get_distribution_dict(orb[kx], orbit_axis_limits[kx], orbit_bin_number[kx])
    for k in p:
        if 'er' in k:
            p[k] = p[k] * (2.5 * orb['foversample']) ** 0.5

    # plot ----------
    # y_limits = [-2.5, 0]
    # x_limits = [-1, 0]
    y_limits = [0, 1]
    x_limits = [0, 1]
    log_o0 = np.real(log10(orb[props[0]]))
    log_o1 = np.real(log10(orb[props[1]]))
    Plot = plot_sm.PlotClass()
    Plot.set_axis(
        'linear', 'linear', x_limits, y_limits, orbit_axis_label[props[0]], orbit_axis_label[props[1]])
    Plot.make_window()
    Plot.draw('p', orb[props[0]], orb[props[1]])
    a, b = np.polyfit(log_o0, log_o1, 1)
    print(a, b)
    xfit = ut.array.get_arange_safe(x_limits, 0.05, True)
    # yfit = b + a*xfit
    yfit = 10 ** b * xfit ** a
    Plot.draw('c', xfit, yfit, ct='red', lt='-')


def plot_orbit_v_mass(
    hals, ti_min, ti_max, vary_mass_kind, mass_kind, m_1_limits, m_2_limits, props):
    '''
    Plot orbits v m1 or m2 masses.

    Import halo catalog, snapshot range, which mass to vary (host, sat),
    halo mass kind & host range & satellite range, properties to plot.
    '''
    mass_width = 0.5

    if m_2_limits[1] > m_1_limits[0]:
        print('! satellite mass %.2f > host mass %.2f' % (m_2_limits[1], m_1_limits[0]))
    if vary_mass_kind == 'host':
        print('# varying host mass')
        masses = np.arange(m_1_limits[0], m_1_limits[1], mass_width)
    elif vary_mass_kind == 'satellite':
        masses = np.arange(m_2_limits[0], m_2_limits[1], mass_width)
        print('# varying satellite mass')
    else:
        raise ValueError('not recognize vary.m.kind = %s' % vary_mass_kind)

    orbmass = copy.deepcopy(orbit_dict)
    temp = copy.deepcopy(orbit_dict)
    for k in temp:
        orbmass[k + '.med'] = [np.zeros(masses.size), np.zeros(masses.size), np.zeros(masses.size)]
        orbmass[k + '.ave'] = [np.zeros(masses.size), np.zeros(masses.size), np.zeros(masses.size)]

    for mi in range(masses.size):
        if vary_mass_kind == 'host':
            log_m_1_limits = [masses[mi], masses[mi] + mass_width]
            log_m_2_limits = m_2_limits
        elif vary_mass_kind == 'satellite':
            log_m_1_limits = m_1_limits
            log_m_2_limits = [masses[mi], masses[mi] + mass_width]
        orb = get_infall_orbits_stats(
            hals, ti_min, ti_max, mass_kind, log_m_1_limits, log_m_2_limits)
        for k in temp:
            if 'time.shell' not in k:
                for propi in range(3):
                    orbmass[k + '.med'][propi][mi] = orb[k + '.med'][propi]
                    orbmass[k + '.ave'][propi][mi] = orb[k + '.ave'][propi]

    # plot
    xs_plot = masses + 0.5 * mass_width
    if vary_mass_kind == 'host':
        x_limits = m_1_limits
        axis_lab_x = 'M_{host} [M_\odot]'
    elif vary_mass_kind == 'satellite':
        x_limits = m_2_limits
        axis_lab_x = 'M_{sat} [M_\odot]'
    space_x = 'log'
    space_y = 'linear'

    nxpan = 1
    nypan = len(props)
    if nypan <= 2:
        lw, ex = 5, 1.6
    else:
        lw, ex = 3, 1.01

    Plot = plot_sm.PlotClass(nxpan, -nypan)
    for propi in range(len(props)):
        k = props[propi] + '.ave'
        if '.med' in k:
            y_limits = [0.95 * np.min(orbmass[k][1]), 1.05 * np.max(orbmass[k][2])]
        elif '.ave' in k:
            y_limits = [0, 1]

        axis_lab_x = ''
        if propi == len(props) - 1:
            axis_lab_x = axis_lab_x
        Plot.set_axis(
            space_x, space_y, x_limits, y_limits, axis_lab_x, orbit_axis_label[props[propi]])
        Plot.make_panel(lw, ex)
        Plot('c', xs_plot, orbmass[k][0], lt='-')
        Plot('c', xs_plot, orbmass[k][0] + orbmass[k][1], lt=1)
        Plot('c', xs_plot, orbmass[k][0] - orbmass[k][1], lt=1)


def plot_orbit_evol(hals, ti_min, ti_max, m_1_limits, m_2_limits, props, mass_scaling=''):
    '''
    Plot orbital parameters v redshift.

    Import halo catalog, snapshot range, host halo mass range, satellite halo mass range,
    properties to plot, mass scalling ('', mass.char).
    '''
    ti_step = 2  # number of snapshots over which to average at a time

    if mass_scaling == 'mass.char':
        mass_width = m_1_limits[1] - m_1_limits[0]
        MassChar = ut.cosmic_structure.MassNonlinearClass()
        mass_char = MassChar.value(hals.snapshot['redshift'][ti_min])
        mass_char_frac = m_1_limits[0] + 0.5 * mass_width - mass_char
        print('mass.char.frac %.1f | m_width %.2f' % (10 ** mass_char_frac, mass_width))

    zis = np.arange(ti_min, ti_max + 1, ti_step)
    redshifts = np.zeros(zis.size)

    orbevol = copy.deepcopy(orbit_dict)
    temp = copy.deepcopy(orbit_dict)
    for t in temp:
        orbevol[t + '.med'] = [np.zeros(zis.size), np.zeros(zis.size), np.zeros(zis.size)]
        orbevol[t + '.ave'] = [np.zeros(zis.size), np.zeros(zis.size), np.zeros(zis.size)]

    for zii, zi in enumerate(zis):
        if mass_scaling == 'mass_char':
            mass_char = MassChar.value(hals.snapshot['redshift'][zi])
            mass = mass_char_frac + mass_char
            m_1_limits = [mass - 0.5 * mass_width, mass + 0.5 * mass_width]
            print('\n  mass_char %.2f | mass %.2f (%.2f, %.2f)' %
                  (mass_char, mass, m_1_limits[0], m_1_limits[1]))
        orb = get_infall_orbits_stats(hals, zi, zi + (ti_step - 1), m_1_limits, m_2_limits)
        for t in temp:
            if 'time.shell' not in t:
                for i in range(3):
                    orbevol[t + '.med'][i][zii] = orb[t + '.med'][i]
                    orbevol[t + '.ave'][i][zii] = orb[t + '.ave'][i]
        redshifts[zii] = 0.5 * (hals.snapshot['redshift'][zi - 1] +
                                hals.snapshot['redshift'][zi])
    log_redshifts = log10(1 + redshifts)

    # plot ----------
    xs_plot = log_redshifts
    x_limits = [log10(1 + hals.snapshot['redshift'][ti_max]), 0]
    space_x = 'log'
    space_y = 'linear'
    axis_lab_x = '1+z'

    nxpan = 1
    nypan = len(props)
    if nypan <= 2:
        lw = 5
        ex = 1.5
    else:
        lw = 3
        ex = 1.01
    Plot = plot_sm.PlotClass()
    for i in range(len(props)):
        t = props[i] + '.ave'
        if 'median' in t:
            y_limits = [0.95 * np.min(orbevol[t][1]), 1.05 * np.max(orbevol[t][2])]
        elif 'average' in t:
            y_limits = [Plot.get_limits(orbevol[t][0], orbevol[t][1], 'linear', 0.05)]
            if 'circularity' in t and mass_scaling == 'mass.char':
                y_limits = [0.38, 0.56]
            elif 'velocity.tot' in t:
                y_limits = [1, 1.21]
        axis_label_x = ''
        if i == len(props) - 1:
            axis_label_x = axis_lab_x
        Plot.make_panel(
            axis_label_x, orbit_axis_label[props[i]], x_limits, y_limits, space_x, space_y,
            nxpan, -nypan, i, lw, ex)
        Plot.draw('c', xs_plot, orbevol[t][0])
        Plot.draw('c', xs_plot, orbevol[t][0] + orbevol[t][1])
        Plot.draw('c', xs_plot, orbevol[t][0] - orbevol[t][1])


def plot_virial_radius_evolution(hals, ti_min, ti_max, mass_limits, mass_scaling=''):
    '''
    Plot R_halo v redshift.

    Import halo catalog, snapshot range, mass range, mass scalling (none, mass_char).
    '''
    mass_kind = 'mass.200c'
    if mass_scaling == 'mass.char':
        mass_width = mass_limits[1] - mass_limits[0]
        MassChar = ut.cosmic_structure.MassNonlinearClass()
        mass_char = MassChar.value(hals.snapshot['redshift'][ti_min])
        mass_char_frac = mass_limits[0] + 0.5 * mass_width - mass_char

    HaloProperty = ut.halo_property.HaloPropertyClass(hals.Cosmology)
    snapshots = list(range(ti_min, ti_max + 1))
    snapshot_number = len(snapshots)
    redshifts = np.zeros(snapshot_number)
    vir_radiuss = np.zeros(snapshot_number)
    vir_concens = np.zeros(snapshot_number)
    for zi in range(snapshot_number):
        redshifts[zi] = hals.snapshot['redshift'][snapshots[zi]]
        if mass_scaling == 'mass.char':
            mass_char = MassChar.value(hals.snapshot['redshift'][snapshots[zi]])
            m = mass_char_frac + mass_char
            mass_min = m - 0.5 * mass_width
            mass_max = m + 0.5 * mass_width
        mass_ave = 10 ** ((mass_min + mass_max) / 2)
        vir_concens[zi] = hals[zi]['c.200c'][
            ut.array.get_indices(hals[snapshots[zi]][mass_kind], [mass_min, mass_max])].mean()
        print('t_i %.1f | concen ave %.1f' % (redshifts[zi], vir_concens[zi]))
        # virial radius [kpc physical]
        if mass_kind == 'mass.200c':
            vir_radiuss[zi] = HaloProperty.get_radius_virial('200c', mass_ave, redshift=redshifts[zi])
        else:
            vir_radiuss[zi] = HaloProperty.get_radius_virial('fof', mass_ave, vir_concens[zi],
                                                         redshift=redshifts[zi])
    log_redshifts = log10(1 + redshifts)

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.window('log', 'linear', [log_redshifts.max(), 0], vir_radiuss, 4, 1.4)
    Plot.draw('c', log_redshifts, vir_radiuss)


#===================================================================================================
# satellite subhalo orbit
#===================================================================================================
def get_halo_sat_orbits(
    subs, hals, ti, mass_kind='mass.fof', mass1_limits=[], mass2_limits=[], select_kind='shell'):
    '''
    For infalling satellite halos, get dictionary of orbital properties.

    Import subhalo & halo catalog, snapshot index, halo mass kind & host range & satellite range,
    radius selection kind (shell, all).
    '''
    from . import treepm_analysis
    ti_max = min(len(hals) - 1, ti + 6)

    orb = copy.deepcopy(orbit_dict)
    his = ut.array.get_indices(hals[ti][mass_kind], mass1_limits)
    merge_number = orbit_number = sat_number = noperi_number = 0
    for hi in his:
        chi_zi, chi_i = ti - 1, hals[ti]['child.index'][hi]
        if chi_i > 0 and hals[ti]['parent.index'][chi_i] == hi:
            if hals[chi_zi]['mass.fof.ratio'][chi_i] > 0:
                next_i = hals[ti]['parent.n.index'][hi]
                while next_i > 0 and hals[ti][mass_kind][next_i] > mass2_limits[0]:
                    if hals[ti][mass_kind][next_i] < mass2_limits[1]:
                        if not ut.catalog.is_orphan(hals, ti, ti_max, next_i):
                            merge_number += 1
                            orb_i = get_orbit(hals[ti], hi, next_i, select_kind)
                            if orb_i:
                                orbit_number += 1
                                cen_i = hals[ti]['central.index'][next_i]
                                sat_i = subs[ti]['child.index'][cen_i]
                                if sat_i > 0:
                                    sat_number += 1
                                    dist_peri_sat, _end_kind = treepm_analysis.get_satellite_end(
                                        subs, hals, ti, cen_i)
                                    if dist_peri_sat < 0:
                                        noperi_number += 1
                                    orb_i['distance.peri.sat'] = dist_peri_sat / orb_i['radius.vir']
                                    for k in orb_i:
                                        orb[k].append(orb_i[k])
                    next_i = hals[ti]['parent.n.index'][next_i]
    print(merge_number, orbit_number, sat_number, noperi_number)

    for k in orb:
        orb[k] = np.array(orb[k])

    return orb


def plot_halo_sat_orbits(subs, hals, ti, m_1_limits=[], m_2_limits=[], select_kind='shell'):
    '''
    .
    '''
    orb = get_halo_sat_orbits(subs, hals, ti, m_1_limits, m_2_limits, select_kind)
    Plot = plot_sm.PlotClass()
    Plot.set_axis('linear', 'linear', [0, 1], [0, 1])
    Plot.draw('p', orb['distance.peri'], orb['distance.peri.sat'])
