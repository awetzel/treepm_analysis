'''
Create virtual catalog of satellite subhalos based on halo-halo merger trees,
using dynamical friction lifetime.

Also measure satellite lifetimes directly from catalog of subhalos.

@author: Andrew Wetzel
'''


# system ----

import numpy as np
from numpy import log10, Inf
# local ----
import utilities as ut
from visualize import plot_sm
from . import treepm_analysis


#===================================================================================================
# dynamical friction lifetimes at infall of surviving satellites in simulation
#===================================================================================================
def get_dynfric_time_infall(
    subs, ti_now, ti_max=None, sis=None, infall_kind='sat.first', dynfric_fac=0.1):
    '''
    Get dynamical friction lifetimes of surviving satellites based on their mass prior to infall.

    Import subhalo catalog, snapshot index and max to follow back, indices of satellites,
    infall kind, dynamical friction lifetime pre-factor.
    '''
    log_mass_ratio_default = log10(5)  # value if inf.first.zi beyond t_i_max
    Say = ut.io.SayClass(get_dynfric_time_infall)
    if ti_max is None:
        ti_max = len(subs) - 1
        Say.say('use ti_max = %d' % ti_max)
    if sis is None:
        sis = ut.array.get_arange(subs[ti_now][infall_kind + '.ti'])
    times_dynfric = np.zeros(sis.size, np.float32) - 1
    # get those with defined infall time
    sis_inf, siis_inf = ut.array.get_indices(
        subs[ti_now][infall_kind + '.ti'], [ti_now, Inf], sis, get_masks=True)
    Say.say('number %d | %d (%.2f) have defined %s.ti' %
            (sis.size, sis_inf.size, sis_inf.size / sis.size, infall_kind))
    log_mass_ratios = np.zeros(sis_inf.size, np.float32)
    times_hubble = subs.snapshot['time.hubble'][subs[ti_now][infall_kind + '.ti'][sis_inf]]

    if ti_max:
        sis_ti_max, iis = ut.array.get_indices(
            subs[ti_now][infall_kind + '.ti'], [ti_now, ti_max + 0.1], sis_inf, get_masks=True)
        siis_ti_max = siis_inf[iis]
        log_mass_ratios[np.setdiff1d(siis_inf, siis_ti_max)] = log_mass_ratio_default
    else:
        sis_ti_max = sis_inf
        siis_ti_max = siis_inf

    inf_tis = subs[ti_now][infall_kind + '.ti'][sis_ti_max],
    inf_sis = subs[ti_now][infall_kind + '.index'][sis_ti_max]
    for sii in range(sis_ti_max.size):
        inf_ti, inf_si = inf_tis[sii], inf_sis[sii]
        chi_i = subs[inf_ti]['child.index'][inf_si]
        if chi_i <= 0:
            Say.say('! somehow, child index = %d for t_i, i = %d, %d' % (chi_i, inf_ti, inf_si))
            continue
        cen_i = subs[inf_ti - 1]['central.index'][chi_i]
        if cen_i > 0:
            par_i = subs[inf_ti - 1]['parent.index'][cen_i]
            mass_sat = subs[inf_ti]['halo.mass'][inf_si]
            if mass_sat <= 0:
                mass_sat = subs[inf_ti]['mass.peak'][inf_si]
            if par_i > 0 and subs[inf_ti]['halo.mass'][par_i] > mass_sat:
                # use central's parent
                log_mass_ratios[siis_ti_max[sii]] = subs[inf_ti]['halo.mass'][par_i] - mass_sat
            else:
                # central has no parent, must use central, even though at different snapshot
                log_mass_ratios[siis_ti_max[sii]] = subs[inf_ti - 1]['halo.mass'][cen_i] - mass_sat
    Say.say('M_halo ratio < 1 or not defined for %d (%.2f)' %
            (np.sum(log_mass_ratios <= 0), np.sum(log_mass_ratios <= 0) / sis_ti_max.size))
    log_mass_ratios[log_mass_ratios == 0] = log_mass_ratio_default
    times_dynfric[siis_inf] = (dynfric_fac * times_hubble * 10 ** log_mass_ratios /
                               np.log(1 + 10 ** log_mass_ratios))

    return times_dynfric


#===================================================================================================
# virtual subhalo catalog from halo - halo mergers
#===================================================================================================
def make_dynfric_sub_dict(hals, subs, ti, ti_max, hm_limits=[1, Inf]):
    '''
    Make sat & cen subhalo catalog from halo mergers, keep everything, assign dynamical
    infall that can use later to remove subs.

    Import catalog of halo [& subhalo (if want to use their m.max)], snapshot index,
    earliest snapshot index to start making subhalos, halo mass range.
    '''
    sub_dyn = {
        'mass.peak': [0],
        'sat.last.ti': [0],
        'mass.ratio': [0],
        'satellite.index': [0],
        'ilk': [0],
        'halo.index': [0],
        'halo.mass': [0],
        'mass.frac.min': [],
        'central.index': [0],
        'hsatid': []
    }
    sat_number = 0
    ti_min = ti
    # cen_halo_mfrac = 0.93
    for ti in range(ti_max + 1):
        if ti >= ti_min:
            sub_dyn['hsatid'].append(np.r_[[-1] * hals[ti]['mass.fof'].size])
        else:
            sub_dyn['hsatid'].append([])

    for ti in range(ti_max, ti_min, -1):
        his = ut.array.get_indices(hals[ti]['mass.fof'], hm_limits)
        for hi in his:
            chi_ti, chi_si = ti - 1, hals[ti]['child.index'][hi]
            if chi_si > 0:
                if hals[chi_ti]['parent.index'][chi_si] != hi:
                    if not ut.catalog.is_orphan(hals, ti, min(ti_max, ti + 4), hi, subs):
                        sat_number += 1
                        # sub_dyn['mass.peak'].append(cen_halo_mfrac * hals[ti]['mass.fof'][hi])
                        sub_dyn['mass.peak'].append(
                            subs[ti]['mass.peak'][hals[ti]['central.index'][hi]])
                        sub_dyn['sat.last.ti'].append(ti)
                        sub_dyn['mass.ratio'].append(
                            10 ** (hals[ti]['mass.fof'][hals[chi_ti]['parent.index'][chi_si]] -
                                   hals[ti]['mass.fof'][hi]))
                        sub_dyn['ilk'].append(0)
                        sub_dyn['satellite.index'].append(-1)
                        sub_dyn['halo.index'].append(-1)
                        sub_dyn['halo.mass'].append(-1)
                        sub_dyn['central.index'].append(-1)
                        if sub_dyn['hsatid'][chi_ti][chi_si] == -1:
                            sub_dyn['hsatid'][chi_ti][chi_si] = sat_number
                        else:
                            esat = sub_dyn['hsatid'][chi_ti][chi_si]
                            while sub_dyn['satellite.index'][esat] > 0:
                                esat = sub_dyn['satellite.index'][esat]
                            sub_dyn['satellite.index'][esat] = sat_number
                            sub_dyn['satellite.index'][sat_number] = sub_dyn['hsatid'][ti][hi]
                # passively evolve halo
                else:
                    sub_dyn['hsatid'][chi_ti][chi_si] = sub_dyn['hsatid'][ti][hi]

    # make central subhalos at final snapshot
    cen_number = len(sub_dyn['mass.peak'])
    his = ut.array.get_indices(hals[ti]['mass.fof'], hm_limits)
    for hi in his:
        # sub_dyn['mass.peak'].append(log10(cen_halo_mrat) + hals[ti_min]['mass.fof'][hi])
        sub_dyn['mass.peak'].append(subs[ti_min]['mass.peak'][hals[ti_min]['central.index'][hi]])
        sub_dyn['sat.last.ti'].append(-1)
        sub_dyn['mass.ratio'].append(-1)
        sub_dyn['ilk'].append(1)
        sub_dyn['halo.index'].append(hi)
        sub_dyn['halo.mass'].append(hals[ti_min]['mass.fof'][hi])
        esat = sub_dyn['hsatid'][ti_min][hi]
        sub_dyn['central.index'].append(cen_number)
        sub_dyn['satellite.index'].append(esat)
        while esat > 0:
            sub_dyn['halo.index'][esat] = hi
            sub_dyn['halo.mass'][esat] = hals[ti_min]['mass.fof'][hi]
            sub_dyn['central.index'][esat] = cen_number
            esat = sub_dyn['satellite.index'][esat]
        cen_number += 1
    sub_dyn['hsatid'] = sub_dyn['hsatid'][ti_min]
    ut.array.arrayize_dictionary(sub_dyn)
    # FIX THIS ut.append_snapshots(sub_dyn, ti_max + 1)
    sub_dyn['mass.frac.min'][ti_min] = np.zeros(sub_dyn['mass.peak'][ti_min].size, np.float32) + 1
    sub_dyn.info = hals.info
    sub_dyn.snapshot = hals.snapshot
    sub_dyn.Cosmology = hals.Cosmology
    return sub_dyn


def assign_dynfric_mass_frac(subs_dyn, ti, dynfric_fac=0.1):
    '''
    Assign mass fraction (1, 0) corresponding to whether satellite survives at snapshot.

    Import dynamical friction subhalo catalog, snapshot, dynamical friction factor.
    '''
    # assign centrals to have mass fracion = 1
    sis = ut.catalog.get_indices_ilk(subs_dyn[ti], 'central')
    subs_dyn['mass.frac.min'][ti][sis] = 1
    # assign satellites to have mass fraction = 0 if disrupted
    sis = ut.array.get_indices(subs_dyn['ilk'][ti], 0)
    inf_ts_hubble = subs_dyn.snapshot['time.hubble'][subs_dyn['sat.last.ti'][ti][sis]]
    inf_ts = subs_dyn.snapshot['time'][subs_dyn['sat.last.ti'][ti][sis]]
    tnow = subs_dyn.snapshot['time'][ti]
    tdyns = dynfric_fac * inf_ts_hubble * (subs_dyn['mass.ratio'][ti][sis] /
                                           np.log(1 + subs_dyn['mass.ratio'][ti][sis]))
    subs_dyn['mass.frac.min'][ti][sis] = 1 * (tnow - inf_ts < tdyns)


def print_sat_dynfric(sub, si, mass_limits=[1, Inf]):
    '''
    Print surviving sats in mass range in halo (can include self).

    Import catalog of dynamical friction subhalo at snapshot, subhalo index, mass range.
    '''
    si_sat = si
    while si_sat > 0:
        if mass_limits[0] < sub['mass.peak'][si_sat] < mass_limits[1]:
            if sub['mass.frac.min'][si_sat] > 0:
                print('mass.peak: %.3e inf.last.ti: %d' %
                      (sub['mass.peak'][si_sat], sub['sat.last.ti'][si_sat]))
        si_sat = sub['sat.si'][si_sat]


#===================================================================================================
# satellite lifetime from subhalos
#===================================================================================================
def get_subhalo_infall_dict(
    subs, hals, ti, gm_limits=[1, Inf], hm_kind='mass.fof', hm_limits=[1, Inf]):
    '''
    Find infalling centrals & get dictionary of their survival times.

    Import subhalo & halo catalog, snapshot index, galaxy mass range, halo mass range.
    '''
    sub_inf = {'time.sim': [], 'time.dyn': [], 'mass.peak': [], 'halo.mass': [], 'mass.ratio': []}
    time_inf = subs.snapshot['time'][ti]
    hubble_t_inf = subs.snapshot['time.hubble'][ti]
    cen_number = cen_inf_number = dif_halo_par_number = mrg_fast_number = ejected_number = 0
    his = ut.array.get_indices(hals[ti][hm_kind], hm_limits)
    for si in his:
        halo_chi_ti, halo_chi_si = ti - 1, hals[ti]['child.index'][si]
        if halo_chi_si <= 0 or hals[halo_chi_ti]['parent.index'][halo_chi_si] != si:
            continue

        if hals[halo_chi_ti]['mass.fof.ratio'][halo_chi_si] > 0:
            halo_i = hals[ti]['parent.n.index'][si]
            while halo_i > 0:
                si = hals[ti]['central.index'][halo_i]
                if si > 0 and gm_limits[0] < subs[ti]['mass.peak'][si] < gm_limits[1]:
                    cen_number += 1
                    chi_zi, chi_i = ti - 1, subs[ti]['child.index'][si]
                    if (chi_i > 0 and
                            not ut.catalog.is_in_same_halo(subs, hals, chi_zi, chi_i, ti, si) and
                            subs[chi_zi]['ilk'][chi_i] > -2):
                        cen_inf_number += 1
                        child_halo_i = subs[chi_zi]['halo.index'][chi_i]
                        if child_halo_i != halo_chi_si:
                            dif_halo_par_number += 1
                        else:
                            # merges right away, not seen as satellite
                            if subs[chi_zi]['ilk'][chi_i] >= 1:
                                end_ti = ti
                                mrg_fast_number += 1
                            else:
                                end_ti, _end_kind = treepm_analysis.get_satellite_end(
                                    subs, hals, ti, si)

                            if end_ti == -2:
                                ejected_number += 1
                            else:
                                if -1 <= end_ti <= 0:
                                    time_end = 1e10
                                else:
                                    time_end = subs.snapshot['time'][end_ti]
                                mass_ratio = 10 ** (hals[ti]['mass.fof'][si] -
                                                    subs[ti]['halo.mass'][si])
                                sub_inf['time.sim'].append(time_end - time_inf + 0.01)
                                sub_inf['time.dyn'].append(0.1 * hubble_t_inf *
                                                           mass_ratio / np.log(1 + mass_ratio))
                                sub_inf['mass.peak'].append(subs[ti]['mass.peak'][si])
                                sub_inf['halo.mass'].append(subs[ti]['halo.mass'][si])
                                sub_inf['mass.ratio'].append(mass_ratio)
                halo_i = hals[ti]['parent.n.index'][halo_i]
    ut.array.arrayize_dictionary(sub_inf)
    print(('t_i %2d | diff halo-par %d | inf %d | inf good %d | merge %d | ' +
           'merge-fast %d | eject %d') %
          (ti, dif_halo_par_number, cen_number, cen_inf_number, sub_inf['t.sim'].size,
           mrg_fast_number, ejected_number))

    return sub_inf


def plot_time_infall(subss, halss, ti_min, ti_max, gm_kind, gm_limits=[1, Inf], hm_limits=[1, Inf],
                     time_scaling='fix'):
    '''
    Plot fraction of satellites remain v time since infall.

    Import subhalo catalog[s], halo[s], snapshot range, galaxy mass kind & range, halo mass range,
    infall time scaling (fixed, tdyn).
    '''
    if gm_kind in subss:
        subss = [subss]
    if 'mass.fof' in halss:
        halss = [halss]
    time_limits = 0, 2
    if time_scaling == 'fix':
        time_width = 0.25
        time_limits[1] = subss[0]['time'][0] - subss[0]['time'][ti_max] + 2 * time_width
    elif time_scaling == 'tdyn':
        time_width = 0.025

    time_bins = ut.array.get_arange_safe(time_limits, time_width, True)
    fremains = []
    for sub_i in range(len(subss)):
        sinf = {}
        for ti in range(ti_min, ti_max + 1):
            temp = get_subhalo_infall_dict(subss[sub_i], halss[sub_i], ti, gm_limits, hm_limits)
            if not sinf:
                sub_inf = temp
            else:
                for k in sub_inf:
                    sub_inf[k] = np.concatenate((sub_inf[k], temp[k]))
        if time_scaling == 'fix':
            dest_numbers, time_bins = np.histogram(sinf['tsim'], time_bins)
        elif time_scaling == 'tdyn':
            dest_numbers, time_bins = np.histogram(sinf['tsim'] / sinf['tdyn'], time_bins)
        fremains.append(1 - dest_numbers.cumsum() / sinf['tsim'].size)
    time_bins = time_bins[:-1] + 0.5 * time_width

    # plot ----------
    Plot = plot_sm.PlotClass()
    if time_scaling == 'fix':
        Plot.axis.lab_x = 'time since infall [Gyr]'
    elif time_scaling == 'tdyn':
        Plot.axis.lab_x = 't_{inf}/t_{dyn}'
    Plot.set_axis('linear', 'linear', time_limits, [0, 1])
    Plot.set_axis_label(None, 'Fraction remain')
    Plot.make_window()
    for sub_i in range(len(subss)):
        Plot.draw('c', time_bins, fremains[sub_i], ct=sub_i)
        if sub_i == 0:
            label = 'SUBFIND'
        else:
            label = 'FoF6d'
        Plot.make_label(label)


def plot_dynfric_time_ratio_v_mass(subs, hals, ti, mass_limits=[1, Inf], select_mass_kind='mass'):
    '''
    .
    '''
    if select_mass_kind == 'mass':
        mass_width = 1 / 3
        mass_ratios = [0, 0.4, 0.7, 0.95, 10]
        print('max t_dyn %.1f Gyr | sim t_width %.1f Gyr' %
              (0.1 * subs.snapshot['time.hubble'][ti] * 30 / np.log(1 + 30),
               subs.snapshot['time'][0] - subs.snapshot['time'][ti]))
        MassBin = ut.binning.BinClass(mass_limits, mass_width)
        sub_inf = get_subhalo_infall_dict(subs, hals, ti, mass_limits)
        mass_bins = MassBin.get_bin_indices(sub_inf['mass.peak'])

        time_ratio_mrat = []
        for mri in range(len(mass_ratios) - 1):
            time_ratio_med = [[], [], []]
            for mi in range(MassBin.mum):
                time_ratios = []
                for ti in range(len(sub_inf['trat'])):
                    if mass_bins[ti] == mi:
                        # if mass_ratios[mri] < sdyn['mass.ratio'][ti] < mass_ratios[mri+1]:
                        if 1 < sub_inf['mass.ratio'][ti] < 10:
                            if mass_ratios[mri] < sub_inf['ecc'][ti] < mass_ratios[mri + 1]:
                                time_ratios.append(sub_inf['time.ratio'][ti])
                med = np.median(time_ratios)
                if med == 0 or np.isnan(med):
                    med = 100
                time_ratio_med[1].append(med)
                #time_ratio_med[0].append(np.percentile(time_ratios, 25))
                #time_ratio_med[2].append(np.percentilee(time_ratios, 75))
            time_ratio_mrat.append(time_ratio_med)
    if select_mass_kind == 'mass.ratio':
        mass_ratio_limits = [0, 1]
        mass_ratio_width = 1 / 3
        eccs = [0, 0.66, 0.87, 0.97, 1.03]
        # eccs = [0, 0.9, 10]
        # eccs = [0, 10]
        MassBin = ut.binning.BinClass(mass_ratio_limits, mass_ratio_width)
        sub_inf = get_subhalo_infall_dict(subs, hals, ti, mass_limits)
        mass_ratio_bins = MassBin.get_bin_indices(sub_inf['mass.ratio'])

        time_ratio_ecc = []
        for ei in range(len(eccs) - 1):
            time_ratio_med = [[], [], []]
            for mri in range(len(mass_ratios)):
                time_ratios = []
                for ti in range(len(sub_inf['time.ratio'])):
                    if mass_ratio_bins[ti] == mri:
                        if eccs[ei] < sub_inf['ecc'][ti] < eccs[ei + 1]:
                            time_ratios.append(sub_inf['time.ratio'][ti])
                med = np.median(time_ratios)
                if med == 0 or np.isnan(med):
                    med = 100
                time_ratio_med[1].append(med)
                # time_ratio_med[0].append(np.percentile(time_ratios, 25))
                # time_ratio_med[2].append(np.percentile(time_ratios, 75))
            time_ratio_ecc.append(time_ratio_med)

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('linear', 'linear', mass_limits, [0, 5])
    Plot.set_axis_label('M_{peak} [M_\odot]', 't_{sim}/t_{dyn}')
    Plot.make_window()
    Plot.make_label('z_{inf}=%.1f' % subs.snapshot['redshift'][ti])
    Plot.draw('p', sub_inf['mass.peak'], log10(sub_inf['trat']))
    Plot.draw('c', MassBin.mids, log10(time_ratio_med[1]), ct='red')
    for mi in range(len(mass_ratios) - 1):
        Plot.make_label('[%d, %d]' % (mass_ratios[mri], mass_ratios[mri + 1]), ct=mi)
        Plot.draw('c', MassBin.mids, time_ratio_mrat[mri][1])


# HOD ----------
def plot_hods_dynfric(subss, tis, skinds, threshs, gm_kind='mass.peak', gm_limits=[12, Inf],
                      hm_limits=[12, 15], hm_width=0.25):
    '''
    Plot HOD for each threshold.
    *** Use ilk = dynfric with thresh = Inf for "no merge" catalog.

    Import subhalo catalog[s], snapshot[s], catalog kinds (sub, dynfric),
    disrupttion mass fraction[s] for each catalog, galaxy mass kind & range,
    halo mass range & bin size.
    '''
    if gm_kind in subss:
        subss = [subss]
    tis = ut.array.arrayize(tis, repeat_number=len(subss))
    hods = {}
    for subi in range(len(subss)):
        hods_sub = {}
        for thresh in threshs[subi]:
            if skinds[subi] == 'dynfric':
                assign_dynfric_mass_frac(subss[subi], tis[subi], thresh)
            hod = treepm_analysis.get_hod(
                subss[subi], tis[subi], gm_kind, gm_limits, hm_limits, hm_width, thresh)
            ut.array.append_dictionary(hods_sub, hod)
        ut.array.append_dictionary(hods, hods_sub)

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.set_axis('log', 'log', hm_limits, hods['sat.number'])
    Plot.set_axis_label('halo.mass', 'N_{galaxy}')
    Plot.make_window()
    Plot.make_label('z=%.1f    log(M_{max})>%.1f' % (subss[0]['redshift'][tis[0]], gm_limits[0]))
    Plot.draw('c', [0, 20], [0, 0], lt='.')

    for subi in range(len(subss)):
        Plot.set_line(ct=subi + 1)
        for thresh in threshs[subi]:
            Plot.draw('c', hods['halo.mass'][subi], hods['sat.number'][subi],
                      hods['sat.number.err'][subi], lt=subi)
            Fit = treepm_analysis.get_fit_hod(hods[subi], 'sat.number', hm_limits[0])
            # xfit = np.arange(gm_limits + 1, 0.1)
            # yfit = (Fit.params[1] * (xfit-Fit.params[0]) -
            #                log10(e) * 10**((Fit.params[2] - xfit) * Fit.params[3]))
            # Plot.draw('c', xfit, yfit)
            if hods['skind'][subi] == 'sub':
                if hods['threshs'][subi] == 0:
                    label = 'no f_{max}        \\alpha=%.2f' % Fit.params[1]
                else:
                    label = 'f_{max}>%.3f \\alpha=%.2f' % (hods['thresh'][subi], Fit.params[1])
            elif hods['skind'][subi] == 'dynfric':
                label = 'f_{dyn}=%.1f \\alpha=%.2f' % (hods['thresh'][subi], Fit.params[1])
            Plot.make_label(label)
            print('%%.2f %.3f %.3f %.3f %.3f' %
                  (threshs[subi], Fit.params[0], Fit.params[1], Fit.params[2], Fit.params[3]))
