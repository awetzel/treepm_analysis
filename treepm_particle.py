'''
Read catalog of particles from Martin White's TreePM simulations.
Track particles between snapshots.

Masses in {log M_sun}, distances in [kpc comoving].

@author: Andrew Wetzel
'''


# system ----

import os
import numpy as np
from numpy import log10, int32, float32
# local ----
import utilities as ut
#from visualize import plot_sm


DIR_TREEPM = os.environ['HOME'] + '/work/research/simulation/treepm/data/'
dir_sim_name = 'lcdm50/'
dir_sim = DIR_TREEPM + dir_sim_name
subfind_type = '-old'
dir_snapshot = dir_sim + 'snapshots_fof6d/'

dir_phase_space = dir_snapshot + 'particle_phase_space/'
dir_halo_id = dir_snapshot + 'particle_halo_id/'
dir_potential = dir_snapshot + 'particle_potential/'
dir_subhalo_id = dir_snapshot + 'particle_subhalo_id/'

dir_halo_fofp = dir_snapshot + 'halo_fofp/'
dir_halo_child = dir_snapshot + 'halo_child/'
dir_subhalo_fofp = dir_snapshot + 'subhalo_fofp/'
dir_subhalo_child = dir_snapshot + 'subhalo_child/'

# cosmology parameters
Cosmology = ut.cosmology.CosmologyClass(
    omega_lambda=0.75, omega_matter=0.25, omega_baryon=0.0311, hubble=0.72, sigma_8=0.8, n_s=0.97)

sim = {
    'box.length': 50000 / Cosmology['hubble'],  # [kpc comoving]
    'particle.number': 512,  # per dimension
    'particle.mass': 0
}

sim['particle.mass'] = Cosmology.particle_mass(sim['box.length'], sim['particle.number'])


#===================================================================================================
# read
#===================================================================================================
def read_times():
    snap = {
        'scalefactor': [],
        'redshift': [],
        'snap': [],
        'snap.width': [],
        'snap.hubble': []
    }

    filename = dir_sim + 'snapshots.txt'
    infile = open(filename, 'r')
    for line in infile:
        if line.isspace() or line[0] == '#':
            continue
        else:
            ls = line.split()
            snap['scalefactor'].append(float(ls[1]))
            snap['redshift'].append(float(ls[2]))
            snap['snap'].append(float(ls[3]))
            snap['snap.width'].append(float(ls[4]))
            snap['snap.hubble'].append(float(ls[5]))
    snap['scalefactor'] = np.array(snap['scalefactor'], float32)
    snap['redshift'] = np.array(snap['redshift'], float32)
    snap['snap'] = np.array(snap['snap'], float32)
    snap['snap.width'] = np.array(snap['snap.width'], float32)
    snap['snap.hubble'] = np.array(['snap.hubble'], float32)
    print('loaded ' + filename)
    infile.close()

    return snap


def read_particles(scale_factor=1.0000, subfind_kind=''):
    '''
    Load all particle data, order everything by particle id.

    Import scale-factor, subfind_kind ('', .old, .new).
    '''
    pa = {}
    # load particle positions & ordering of the particle ids
    file_name = dir_phase_space + 'tpmsph_%.4f.bin' % scale_factor
    if not os.path.exists(file_name):
        raise ValueError('file = %s not found' % file_name)
    ff = open(file_name, 'r')
    hdr = np.fromfile(ff, int32, 7)
    N = hdr[2]
    print('# read %d particles from %s' % (N, file_name))
    pa['r'] = np.fromfile(ff, float32, 3 * N) * sim['box.length']
    # p['r'].shape = (np, 3)
    del pa['r']
    pa['v'] = np.fromfile(ff, float32, 3 * N)
    # p['v'].shape = (np, 3)
    del pa['v']
    pa['pid'] = np.fromfile(ff, int32, N)
    ff.close()
    print('    loaded ' + file_name)
    pis = np.argsort(pa['pid']).astype(pa['pid'].dtype)
    del pa['pid']
    # load particles' halo id
    '''
    file_name = dir_halo_id + 'tpmsph_%.4f.grp' % scale_factor
    pa['hid'] = np.fromfile(file_name, int32, np)
    print('loaded ' + file_name)
    '''
    # load particles' subhalo id
    file_name = dir_subhalo_id + 'tpmsph_%.4f.grp%s' % (scale_factor, subfind_kind)
    pa['sid'] = np.fromfile(file_name, int32, np)
    print('    loaded ' + file_name)
    # load particles' potential
    file_name = dir_potential + 'tpmsph_%.4f.pot%s' % (scale_factor, subfind_kind)
    pa['pot'] = np.fromfile(file_name, float32, np)
    print('    loaded ' + file_name)

    # sort in increasing order by pid
    for k in pa:
        if len(pa[k].shape) == 2:
            pa[k] = pa[k][pis, :]
        elif len(pa[k].shape) == 1:
            pa[k] = pa[k][pis]

    return pa


def read_fofp(kind, scale_factor=1.0000, subfind_kind=''):
    '''
    Read FoFP file & get [sub]halo mass & position.

    Import kind ([sub]halo), scale-factor, subfind_kind ('', .old, .new).
    '''
    g = {}
    if kind == 'halo':
        filename = dir_halo_fofp + 'tpmsph_%.4f.fofp' % scale_factor
    elif kind == 'subhalo':
        filename = dir_subhalo_fofp + 'tpmsph_%.4f.fofp%s' % (scale_factor, subfind_kind)
    else:
        raise ValueError('not recognize kind = %s' % kind)
    if not os.path.exists(filename):
        raise ValueError('file %s not found' % filename)
    ff = open(filename, 'r')
    grp_number = np.fromfile(ff, int32, 1)
    print('# read %d halos from %s' % (grp_number, filename))
    g['m'] = np.fromfile(ff, float32, grp_number)
    g['vel.disp'] = np.fromfile(ff, float32, grp_number)
    del(g['vel.disp'])
    g['vel.circ'] = np.fromfile(ff, float32, grp_number)
    del(g['vel.circ'])
    g['ppid'] = np.fromfile(ff, int32, grp_number)
    g['vp'] = np.fromfile(ff, float32, grp_number)
    del(g['vp'])
    g['r'] = np.fromfile(ff, float32, 3 * grp_number) * sim['box.length']
    g['r'].shape = (grp_number, 3)
    g['v'] = np.fromfile(ff, float32, 3 * grp_number)
    g['v'].shape = (grp_number, 3)
    ff.close()
    print('loaded ' + filename)

    return g


def read_child(kind, scale_factor=1.0000, subfind_kind=''):
    '''
    Read child file, return as array.

    Import kind ([sub]halo), scale-factor, subfind_kind ('', .old, .new);
    '''
    if kind == 'halo':
        filename = dir_halo_child + 'tpmsph_%.4f.child' % scale_factor
    elif kind == 'subhalo':
        filename = dir_subhalo_child + 'tpmsph_%.4f.child%s' % (scale_factor, subfind_kind)
    else:
        raise ValueError('not recognize kind' % kind)
    if not os.path.exists(filename):
        raise ValueError('file = %s not found' % filename)
    ff = open(filename, 'r')
    grp_number = np.fromfile(ff, int32, 2)
    cn = np.fromfile(ff, int32, grp_number[0])
    ff.close()
    print('loaded ' + filename)

    return cn


#===================================================================================================
# paraview
#===================================================================================================
def print_paraview_cosmo(gh, ti):
    '''
    Print data for Paraview.

    Import object class.
    '''
    frand = 10
    xs = np.transpose(gh['r'])
    vs = np.transpose(gh['v'])
    allprop = np.array([xs[0], vs[0], xs[1], vs[1], xs[2], vs[2], gh['m'], gh['hid']], float32)
    allprop = np.transpose(allprop)
    keep = np.arange(0, len(xs[0]), frand)
    filename = 'paraview/' + dir_sim_name + 'particle%d_%d.cosmo' % (frand, ti)
    outfile = open(filename, 'wb')
    np.array(allprop[keep], float32).tofile(outfile)
    outfile.close()
    del(xs, vs, allprop)


def convert_paraview_cosmo(ti):
    '''
    .
    '''
    snap = read_times()
    pa = read_particles(ti['scalefactor'][ti])
    units = Cosmology['hubble'] * ut.const.km_per_Mpc / ut.const.sec_per_Gyr
    # check!
    pa['v'] = (pa['v'] * sim['box.length'] / snap['time.hubble'][ti] * snap['scalefactor'][ti] /
               units)
    hal = read_fofp('halo', snap['scalefactor'][ti])
    hal['v'] = (hal['v'] * sim['box.length'] / snap['time.hubble'][ti] * snap['scalefactor'][ti] /
                units)
    sub = read_fofp('subhalo', snap['scalefactor'][ti])
    sub['v'] = (sub['v'] * sim['box.length'] / snap['time.hubble'][ti] * snap['scalefactor'][ti] /
                units)
    pa['m'] = hal['m'][pa['hid']]
    minmass = 10 * sim['particle.mass']
    pa['m'][pa['m'] < minmass] = minmass
    print_paraview_cosmo(pa, ti)
    del(ti, pa, hal, sub)


def print_paraview_den(densities, ti, power):
    '''
    Print data for Paraview.

    Import object class.
    '''
    filename = 'paraview/' + dir_sim_name + 'density_halo0_%d_d%.1f.raw' % (ti, power)
    outfile = open(filename, 'wb')
    # array(densities.reshape(-1)**power, float32).tofile(outfile)
    d1d = densities.reshape(-1)
    d1d[d1d < 1] = 1
    np.array(log10(d1d), float32).tofile(outfile)
    outfile.close()


def convert_paraview_den(ti, power):
    snap = read_times()
    pa = read_particles(snap['scalefactor'][ti])
    bin_number = 64  # per dimension
    rad_bins = np.linspace(0, 0.5 * sim['box.length'], bin_number + 1)
    bins3d = np.array([rad_bins, rad_bins, rad_bins])
    densities, bins3d = np.histogramdd(pa['r'], bins3d, normed=False)
    print_paraview_den(densities, ti, power)


def convert_paraview_den_halo(ti, power, dx):
    snap = read_times()
    pa = read_particles(snap['scalefactor'][ti])
    hal = read_fofp('halo', snap['scalefactor'][ti])

    nth_massive_halo = 0
    bin_number = 200  # per dimension

    his_sort = np.argsort(hal['m'])[::-1]
    rad_halo = hal['r'][his_sort[nth_massive_halo]]
    rad_center = np.array([sim['box.length'] / 2, sim['box.length'] / 2, sim['box.length'] / 2])
    shift = rad_center - rad_halo
    part_positions = pa['r'].transpose()
    for pi in range(len(part_positions)):
        part_positions[pi] = ut.coordinate.get_positions_periodic(
            part_positions[pi] + shift[pi], sim['box.length'])
    part_positions = part_positions.transpose()

    rad_bins = np.linspace(rad_center[0] - dx, rad_center[0] + dx, bin_number + 1)
    bins3d = np.array([rad_bins, rad_bins, rad_bins])
    densities, bins3d = np.histogramdd(part_positions, bins3d, normed=False)
    print_paraview_den(densities, ti, power)


#===================================================================================================
# test
#===================================================================================================
# fly-bys in FOF6D ----------
def test_fof6d_tracking(ppar, pmid, pchild, chi_is, print_number=20):
    '''
    Import parent snapshot indices & subhalo indices, child subhalo index, particles.
    '''
    def get_stuff(p, sids, pis, part_ids, pots, pis_sort):
        for id_i in range(len(sids)):
            part_ids.append(pis[p['sid'] == sids[id_i]])
            pots.append(p['pot'][part_ids[id_i]])
            pis_sort.append(np.argsort(pots[id_i]))
            print('%.2e' % pots[id_i][pis_sort[id_i]][0])

    pis = ut.array.get_arange(ppar['sid'])
    partids_par = []
    partids_mid = []
    partids_child = []
    pots_par = []
    pots_mid = []
    pots_child = []
    par_is = []
    mid_is = []
    child_is = []

    get_stuff(ppar, par_is, pis, partids_par, pots_par, par_is)
    get_stuff(pmid, mid_is, pis, partids_mid, pots_mid, mid_is)
    get_stuff(pchild, chi_is, pis, partids_child, pots_child, child_is)

    for pi in range(len(par_is)):
        for mi in range(len(mid_is)):
            shares = np.intersect1d(partids_mid[mi], partids_par[pi][par_is[pi]][:print_number])
            if len(shares) > 0:
                print('%2d %.2e %.2e' % (shares.size, ppar['pot'][shares].mean(),
                                         ppar['pot'][shares].min()))
            else:
                print(0)
        for ci in range(len(chi_is)):
            shares = np.intersect1d(partids_child[ci], partids_par[pi][par_is[pi]][:print_number])
            if len(shares) > 0:
                print('%2d %.2e %.2e' % (len(shares), ppar['pot'][shares].mean(),
                                         ppar['pot'][shares].min()))
            else:
                print(0)


# SUBFIND tweak ----------
def get_subhalos(g_new, g_old, mass_frac=0.5):
    '''
    Print subhalos whose mass differs by mass_frac.

    Import mass fraction difference between old & new subfind.
    '''
    for si in range(len(g_new['m'])):
        if g_new['m'][si] < mass_frac * g_old['m'][si]:
            print(si, g_new['m'][si], g_old['m'][si])


def plot_subfind_compare(hal, snew, sold, pnew, pold, si, side=0.8):
    '''
    Plot halo, central subhalo, and central's nearest neighbor for old and new
    versions of subfind.

    Import halo & subhalo catalogs, subhalo snapshot & id, box size for plotting.
    '''
    sample_frac = 1.0
    sii = si
    # if want to read automatically
    '''
    aout = 1.0000
    hal = read_fofp('halo', aout, '')
    snew = read_fofp('subhalo', aout, '-new')
    sold = read_fofp('subhalo', aout, '-old')
    pnew = read_particles(aout, '-new')
    pold = read_particles(aout, '-old')
    '''
    Plot = plot_sm.PlotClass()
    Plot.sm.window(1, 1, 1, 1)
    Plot.sm.erase()
    Plot.sm.ctype('default')
    Plot.sm.ltype(0)
    Plot.sm.lweight(3)
    Plot.sm.expand(1.3)
    Plot.sm.location(3500, 31000, 3500, int(3500 + (31000 - 3500) * 0.5))
    Plot.sm.get_limit((-side / 2, side / 2), (-side / 2, side / 2))
    Plot.sm.window(-2, 1, 1, 1)
    Plot.sm.box()
    Plot.sm.axislabx('x [h^{-1}kpc]')
    Plot.sm.axislaby('y [h^{-1}kpc]')
    hidold = pold['hid'][sold['ppid'][si]]
    jj = np.nonzero((pold['hid'] == hidold) & (np.random.rand(pold['hid'].size) < sample_frac))[0]
    plot_dots(pold['r'][jj, :], hal['r'][hidold], side, ctype='red')

    jj = np.nonzero((pold['sid'] == si) & (np.random.rand(pold['sid'].size) < sample_frac))[0]
    plot_dots(pold['r'][jj, :], hal['r'][hidold], side, ctype='cyan')
    distance_min = 1e10
    for si in range(len(sold['m'])):
        if si != sii:
            distance = ut.coordinate.get_distances(
                'scalar', sold['r'][si], sold['r'][sii], sim['box.length'])

            if distance < distance_min:
                distance_min = distance
                near_i = si

    jj = np.nonzero((pold['sid'] == near_i) & (np.random.rand(pold['sid'].size) < 1))[0]
    plot_dots(pold['r'][jj, :], hal['r'][hidold], side, ctype='green')
    print('# old: halo        (%6d) M = %.3e, pos = (%6.3f, %6.3f, %6.3f)' %
          (hidold, hal['m'][hidold], hal['r'][hidold][0], hal['r'][hidold][1], hal['r'][hidold][2]))
    print('    old: subhalo (%6d) M = %.3e, pos = (%6.3f, %6.3f, %6.3f)' %
          (sii, sold['m'][sii], sold['r'][sii][0], sold['r'][sii][1], sold['r'][sii][2]))
    print('    old: subneig (%6d) M = %.3e, pos = (%6.3f, %6.3f, %6.3f)' %
          (near_i, sold['m'][near_i], sold['r'][near_i][0], sold['r'][near_i][1],
           sold['r'][near_i][2]))

    Plot.sm.window(-2, 1, 2, 1)
    Plot.sm.box(1, 0)
    Plot.sm.axislabx('x [h^{-1}kpc]')

    hidnew = pnew['hid'][snew['ppid'][sii]]

    jj = np.nonzero((pnew['hid'] == hidnew) & (np.random.rand(pnew['hid'].size) < sample_frac))[0]
    plot_dots(pnew['r'][jj, :], hal['r'][hidnew], side, ctype='red')

    jj = np.nonzero((pnew['sid'] == sii) & (np.random.rand(pnew['sid'].size) < sample_frac))[0]
    plot_dots(pnew['r'][jj, :], hal['r'][hidnew], side, ctype='cyan')

    distance_min = 1e10
    for si in range(len(snew['m'])):
        if si != sii:
            distance = ut.coordinate.get_distances(
                'scalar', snew['r'][si], snew['r'][sii], sim['box.length'])
            if distance < distance_min:
                distance_min = distance
                near_i = si

    jj = np.nonzero((pnew['sid'] == near_i) & (np.random.rand(pold['sid'].size) < sample_frac))[0]
    plot_dots(pnew['r'][jj, :], hal['r'][hidnew], side, ctype='green')

    print('# new: halo        (%6d) M = %.3e, pos = (%6.3f, %6.3f, %6.3f)' %
          (hidnew, hal['m'][hidnew], hal['r'][hidnew][0], hal['r'][hidnew][1], hal['r'][hidnew][2]))
    print('    new: subhalo (%6d) M = %.3e, pos = (%6.3f, %6.3f, %6.3f)' %
          (sii, snew['m'][sii], snew['r'][sii][0], snew['r'][sii][1], snew['r'][sii][2]))
    print('    new: subneig (%6d) M = %.3e, pos = (%6.3f, %6.3f, %6.3f)' %
          (near_i, sold['m'][near_i], sold['r'][near_i][0], sold['r'][near_i][1],
           sold['r'][near_i][2]))


#===================================================================================================
# plot
#===================================================================================================
def sminit(ppr, nout):
    '''
    Set up SM window.
    '''
    aspect = float(nout / ppr)  # aspect ratio: width/height, an integer.
    aspect /= ppr
    Plot = plot_sm.PlotClass()
    Plot.sm.erase()
    Plot.sm.window(1, 1, 1, 1)
    Plot.sm.location(3500, 31000, 3500, int(3500 + (31000 - 3500) * aspect))
    Plot.sm.lweight(3)
    Plot.sm.expand(1.3)
    Plot.sm.ctype('default')
    Plot.sm.ltype(0)
    Plot.sm.get_limit([0, 1], [0, 1])


def plot_dots(r, ctr, side, ctype='default', iaxis=[0, 1, 2]):
    '''
    Plot dots in a cube, centered on 'ctr' and of side length 'side'.
    Current plotting window should have limit = [0, 1].
    Viewing orientation is set by iaxis, with default down the z-axis.
    '''
    x = r[:, iaxis[0]]
    y = r[:, iaxis[1]]
    z = r[:, iaxis[2]]
    j = np.nonzero((x > ctr[iaxis[0]] - side / 2.0) & (x < ctr[iaxis[0]] + side / 2.0) &
                   (y > ctr[iaxis[1]] - side / 2.0) & (y < ctr[iaxis[1]] + side / 2.0) &
                   (z > ctr[iaxis[2]] - side / 2.0) & (z < ctr[iaxis[2]] + side / 2.0))[0]
    Plot = plot_sm.PlotClass()
    Plot.sm.ctype(ctype)
    Plot.sm.ptype([11])
    # Plot.sm.points((x[j]-ctr[iaxis[0]])/side+0.5, (y[j]-ctr[iaxis[1]])/side+0.5)
    Plot.sm.points(x[j] - ctr[iaxis[0]], y[j] - ctr[iaxis[1]])
    Plot.sm.ctype('default')


# functions ----------
def get_main_trunk(snapshot_number, pick_number):
    '''
    Get last number of snapshot times, group numbers, & centers of the main trunk of the
    pick_number'th most massive halo in the final snapshot time.
    If snapshot_number is larger than the total number of snapshots, it is reduced.

    Import number of snapshots to look back, pick_number'th most massive halo at snapshot.
    '''
    subfind_type = ''
    file_name = dir_snapshot + 'aout.txt'
    aout = np.fromfile(file_name, sep=' ')
    if aout.size < snapshot_number:
        print('! found only %d snapshot times' % aout.size)
        snapshot_number = aout.size
    aout = aout[-snapshot_number:]
    # select pick_number'th most massive halo from the final snapshot
    aa = aout[-1]
    hal = read_fofp('halo', aa, subfind_type)
    jj = np.argsort(hal['m'])[::-1][pick_number]
    print('# aout = %.4f' % aa)
    print('    selecting %dth most massive halo (id=%d)' % (pick_number + 1, jj))
    print('    M = %.3e at r = (%.3f, %.3f, %.3f)' %
          (hal['m'][jj], hal['r'][jj, 0], hal['r'][jj, 1], hal['r'][jj, 2]))
    # figure out its parents, storing their centers etc in lists
    alist = []
    idlist = []
    rlist = []
    mlist = []
    alist.append(aa)
    idlist.append(jj)
    rlist.append(hal['r'][jj, :])
    mlist.append(hal['m'][jj])
    icur = jj
    for aa in aout[::-1][1:]:
        hal = read_fofp('halo', aa, subfind_type)
        hal.cn = read_child('halo', aa, subfind_type)
        jj = np.argmax(hal.cn == icur)
        alist.append(aa)
        idlist.append(jj)
        rlist.append(hal['r'][jj, :])
        mlist.append(hal['m'][jj])
        icur = jj
    return alist, idlist, rlist, mlist


def print_tree(npick=0):
    '''
    Print the 2nd most massive progentor on the main trunk of the npick'th
    most massive halo at the last snapshot time.
    Useful for figuring out which halos to track.

    Import npick'th most massive halo at final snapshot.
    '''
    subfind_type = ''
    alist, idlist, _rlist, mlist = get_main_trunk(50, npick)
    for ai in range(1, len(alist)):
        hal = read_fofp('halo', alist[ai], subfind_type)
        hal.cn = read_child('halo', alist[ai], subfind_type)
        jj = np.argsort(hal['m'] * (hal.cn == idlist[ai - 1]))[::-1]
        itar = jj[1]
        print('aout %.4f: mmpid %6d (M=%.3e), nmmpid %6d (M=%8.2e, %4.1f%%)' %
              (alist[ai], idlist[ai], mlist[ai], itar, hal['m'][itar],
               100 * hal['m'][itar] / mlist[ai]))


def plot_track(subfind_type='-old'):
    '''
    Which halo is plotted is hard-wired in the 'maintrunk' call,
    and which snapshot time to select the merger from is hard-wired as iout.
    '''
    ppr = 3  # number of panels per row
    nout = 9  # number of snapshots
    side = 0.04  # side the width of each panel [units?]
    nthmostmassive = 11
    mostboundfrac = 0.1
    sampfrac = 0.50  # fraction of particles in background halo to plot
    if nout % ppr > 0:
        raise ValueError('require nout to be a multiple of ppr')
    alist, idlist, rlist, mlist = get_main_trunk(nout, nthmostmassive)
    # plot a fraction of these particles, id_i.e. the background
    sminit(ppr, nout)
    Plot = plot_sm.PlotClass()
    for id_i in range(len(idlist)):
        Plot.sm.window(-ppr, -nout / ppr, ppr - id_i % ppr, 1 + id_i / ppr)
        Plot.sm.box(0, 0, 0, 0)
        tid = idlist[id_i]
        rtr = rlist[id_i]
        p = read_particles(alist[id_i], subfind_type)
        jj = np.nonzero((p['hid'] == tid) & (np.random.rand(p['hid'].size) < sampfrac))[0]
        print('aout %.4f, tid = %6d, M = %.3e, rtr = (%.3f, %.3f, %.3f)' %
              (alist[id_i], idlist[id_i], mlist[id_i], rtr[0], rtr[1], rtr[2]))
        plot_dots(p['r'][jj, :], rtr, side, ctype='red')
        Plot.sm.relocate(0.05, 0.075)
        Plot.sm.putlabel(6, 'z=%.2f' % (1 / alist[id_i] - 1.0))
    '''
    Find all halos which fall into major branch halo in the iout'th
    time step, pick the 2nd largest, record which particles belong to
    this group, & the lowest potential ones.
    '''
    iout = nout - 1
    hal = read_fofp('halo', alist[iout], subfind_type)
    hal.cn = read_child('halo', alist[iout], subfind_type)
    jj = np.argsort(hal['m'] * (hal.cn == idlist[iout - 1]))[::-1]
    itar = jj[1]
    print('# at aout = %.4f main trunk halo (id=%d) has M = %.3e' %
          (alist[iout], idlist[iout], mlist[iout]))
    print('    infalling halo (id=%d) has M = %.3e' % (itar, hal['m'][itar]))
    pa = read_particles(alist[iout], subfind_type)
    # find largest subhalo in this halo
    rbins = np.arange(p['sid'].max() + 1)
    y, x = np.histogram(p['sid'][np.nonzero(p['hid'] == itar)[0]], rbins=rbins)
    x = x[:-1]
    star = np.argmax(y)
    pidlst = np.nonzero(p['sid'] == star)[0]
    print('    most massive subhalo (id=%d) in infall halo has %d part, M=%.3e' %
          (star, pidlst.size, pidlst.size * sim['particle.mass']))
    # find most bound particles
    pa['pot'] = pa['pot'] * (p['sid'] == star)
    potlst = np.argsort(p['pot'])
    print('    selecting %d most bound particles' % (pidlst.size * mostboundfrac))
    potlst = potlst[:pidlst.size * mostboundfrac]
    # sub-sample the infalling halo
    pidlst = pidlst[np.nonzero(np.random.rand(pidlst.size) < 2 * sampfrac)[0]]
    # overplot the subhalo particles on each panel
    for id_i in range(len(idlist)):
        Plot.sm.window(-ppr, -nout / ppr, ppr - id_i % ppr, 1 + id_i / ppr)
        Plot.sm.box(0, 0, 0, 0)
        rtr = rlist[id_i]
        p = read_particles(alist[id_i], subfind_type)
        plot_dots(p['r'][pidlst, :], rtr, side, ctype='cyan')
    # overplot most bound particles on each panel
    for id_i in range(len(idlist)):
        Plot.sm.window(-ppr, -nout / ppr, ppr - id_i % ppr, 1 + id_i / ppr)
        Plot.sm.box(0, 0, 0, 0)
        rtr = rlist[id_i]
        p = read_particles(alist[id_i], subfind_type)
        plot_dots(p['r'][potlst, :], rtr, side, ctype='magenta')
    # give the final (subhalo) mass
    p = read_particles(alist[0], subfind_type)
    rbins = np.arange(p['sid'].max() + 1)
    y, x = np.histogram(p['sid'][potlst], rbins=rbins)
    isub = np.argmax(y)
    print('# final subhalo (id=%d) has %d particles, M = %.3e' %
          (isub, y[isub], y[isub] * sim['particle.mass']))
    # plot in blue the final subhalo particles
    Plot.sm.window(-ppr, -nout / ppr, ppr, 1)
    rtr = rlist[0]
    p = read_particles(alist[0], subfind_type)
    plot_dots(p['r'][np.nonzero(p['sid'] == isub)[0], :], rtr, side, ctype='blue')


#===================================================================================================
# plot halo particles
#===================================================================================================
def plot_halo_particles(subs, zi, cen_i, x1name, x2name):
    '''
    Plot particles of halo & its subhalos.

    Import subhalo catalog, snapshot, subs id of central, dimensions to plot.
    '''
    #r_max = 0.4    # plotting window radius
    subsample = 1  # subsample fraction
    pa = {
        'sid': [],
        'x': [],
        'y': [],
        'z': [],
        'r': [],
        'rbin': [],
    }
    x1 = []
    x2 = []
    satx1 = []
    satx2 = []
    # read particles
    filename = 'particle/particles/particles_%d_%d.txt' % (zi, cen_i)
    infile = open(filename, 'r')
    for line in infile:
        if line.isspace() or line[0] == '#':
            continue
        else:
            ls = line.split()
            pa['subid'].append(int(ls[0]))
            pa['x'].append(float(ls[1]))
            pa['y'].append(float(ls[2]))
            pa['z'].append(float(ls[3]))
    infile.close()
    for k in pa:
        pa[k] = np.array(pa[k])

    for e in range(pa['sid'].size):
        if not e % subsample:
            if pa['sid'][e] == cen_i:
                x1.append(pa[x1name][e] - subs[zi][x1name][cen_i])
                x2.append(pa[x2name][e] - subs[zi][x2name][cen_i])
            elif subs[zi]['central.index'][pa['sid'][e]] == cen_i:
                satx1.append(pa[x1name][e] - subs[zi][x1name][cen_i])
                satx2.append(pa[x2name][e] - subs[zi][x2name][cen_i])

    x1 = np.array(x1)
    x2 = np.array(x2)
    satx1 = np.array(satx1)
    satx2 = np.array(satx2)

    # plot ----------
    Plot = plot_sm.PlotClass()
    Plot.sm.window(1, 1, 1, 1)
    Plot.sm.ticksize(0, 0, 0, 0)
