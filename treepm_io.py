'''
Read catalogs of subhalo or halos from Martin White's TreePM simulations.
Read files: subtree.dat and halotree.dat.

Masses in {log M_sun}, positions and distances in [kpc comoving].

@author: Andrew Wetzel

lcdm_250
i scale-factor redshift time[Gyr] time_wid[Gyr]
 0    1.0000    0.0000    13.8099 0.6771
 1    0.9522    0.0502    13.1328 0.6604
 2    0.9068    0.1028    12.4724 0.6453
 3    0.8635    0.1581    11.8271 0.6291
 4    0.8222    0.2162    11.1980 0.6087
 5    0.7830    0.2771    10.5893 0.5905
 6    0.7456    0.3412     9.9988 0.5700
 7    0.7100    0.4085     9.4289 0.5505
 8    0.6760    0.4793     8.8783 0.5259
 9    0.6438    0.5533     8.3525 0.5060
10    0.6130    0.6313     7.8464 0.4830
11    0.5837    0.7132     7.3635 0.4587
12    0.5559    0.7989     6.9048 0.4382
13    0.5293    0.8893     6.4665 0.4152
14    0.5040    0.9841     6.0513 0.3916
15    0.4800    1.0833     5.6597 0.3724
16    0.4570    1.1882     5.2873 0.3496
17    0.4352    1.2978     4.9378 0.3298
18    0.4144    1.4131     4.6080 0.3099
19    0.3946    1.5342     4.2980 0.2901
20    0.3758    1.6610     4.0079 0.2735
21    0.3578    1.7949     3.7343 0.2541
22    0.3408    1.9343     3.4802 0.2394
23    0.3245    2.0817     3.2408 0.2235
24    0.3090    2.2362     3.0172 0.2094
25    0.2942    2.3990     2.8078 0.1942
26    0.2802    2.5689     2.6136 0.1821
27    0.2668    2.7481     2.4315 0.1704
28    0.2540    2.9370     2.2611 0.1576
29    0.2419    3.1339     2.1035 0.1466
30    0.2304    3.3403     1.9569 0.1371
31    0.2194    3.5579     1.8198 0.1280
32    0.2089    3.7870     1.6918 0.1192
33    0.1989    4.0277     1.5726 0.1106
34    0.1894    4.2798     1.4620 0.0000
'''


# system ----

import os
import copy
from numpy import log10, Inf
import numpy as np
# local ----
import utilities as ut


# directories
RESEARCH_DIRECTORY = os.environ['HOME'] + '/work/research/'


#===================================================================================================
# read
#===================================================================================================
class ReadClass(ut.io.SayClass):
    '''
    Read catalog of [sub]halos from Martin White's TreePM simulations.
    Catalog at each snapshot is a dictionary.
    Catalog across snapshots is a list of dicts.
    '''
    def __init__(self, sigma_8=0.8):
        '''
        Parameters
        ----------
        sigma_8 : float : sigma_8 cosmological parameter
        '''
        self.treepm_directory = RESEARCH_DIRECTORY + 'simulation/treepm/data/'
        self.dimension_number = 3
        self.particle_number = {
            # connect simulation box length {kpc/h comoving} to number of particles per dimension
            50000: 256,  # 512
            64000: 800,
            100000: 800,
            125000: 1024,
            200000: 1500,
            250000: 2048,
            720000: 1500
        }
        self.sigma_8 = sigma_8

    def read_catalog(
        self, catalog_kind='subhalo', box_length=250000, snapshot_indices=1, cat_in=None):
        '''
        Read catalog[s] of [sub]halos across snapshots given by input snapshot indices.

        Parameters
        ----------
        catalog_kind : string : which catalog[s] to read: subhalo, halo, both
        box_length : int : simulation box length {kpc/h comoving}
        snapshot_indices : int, float, or list : indices of snapshots to read
        cat_in : list : catalog of [sub]halos to append snapshots to
        '''
        if catalog_kind == 'both':
            subs = self.read('subhalo', box_length, snapshot_indices)
            hals = self.read('halo', box_length, snapshot_indices)
            return subs, hals
        elif catalog_kind == 'subhalo':
            cats = ut.array.ListClass()
            cat = ut.array.DictClass()
            cat['position'] = []  # position (3D) of most bound particle {Mpc/h -> kpc comoving}
            cat['velocity'] = []  # velocity (3D) {Mpc/h / Gyr comoving -> km / sec physical}
            cat['mass.bound'] = []  # mass of subhalo [M_sun]
            #cat['vel.circ.max'] = []    # maximum of circular velocity [km / s physical]
            cat['mass.peak'] = []  # maximum mass in history [M_sun]
            #cat['vel.circ.peak'] = []    # max of max of circular velocity [km / s physical]
            cat['ilk'] = []
            # 1 = central, 2 = virtual central
            # 0 = satellite, -1 = virtual satellite
            # -2 = virtual satellite with no central
            # -3 = virtual satellite with no host halo
            cat['parent.index'] = []  # index of parent, at previous snapshot, with highest M_peak
            #cat['parent.next.index'] = []    # index of next parent to same child, at same snapshot
            cat['child.index'] = []  # index of child, at next snapshot
            cat['mass.frac.min'] = []  # minimum of (M_bound / M_peak) experienced
            #cat['mass.peak.ratio.raw'] = []    # M_peak ratio of two highest M_peak parents (< 1)
            cat['central.index'] = []  # index of central subhalo in same halo (can be self)
            cat['central.distance'] = []  # distance from central {Mpc/h -> kpc comoving}
            #cat['satellite.index'] = []    # index of next highest M_peak satellite in same halo
            cat['halo.index'] = []  # index of host halo
            cat['halo.mass'] = []  # FoF mass of host halo [M_sun]
            cat['sat.last.ti'] = []  # last snapshot that was a satellite
            #cat['sat.last.index'] = []    # subhalo index of above
            cat['ilk.dif.ti'] = []  # last snapshot when satellite/central was central/satellite
            cat['ilk.dif.index'] = []  # subhalo index of above
            cat['sat.first.ti'] = []  # first snapshot that was a satellite
            cat['sat.first.index'] = []  # subhalo index of above
            # derived ----------
            #cat['star.mass'] = []    # stellar mass [M_sun]
            #cat['mag.r'] = []    # magnitude in r-band
            #cat['mass.peak.ratio'] = []    # same as mass.peak.ratio.raw, but ignores disrupted
            #cat['star.mass.ratio'] = []    # M_star ratio of two highest M_star parents
            #cat['ssfr'] = []
            #cat['dn4k'] = []
            #cat['g-r'] = []
        elif catalog_kind == 'halo':
            cats = ut.array.ListClass()
            cat = ut.array.DictClass()
            cat['position'] = []  # 3D position of most bound particle {Mpc/h -> kpc comoving}
            #cat['velocity'] = []    # 3D velocity {Mpc/h/Gyr comoving -> km / sec physical}
            cat['mass.fof'] = []  # FoF mass [M_sun]
            cat['vel.circ.max'] = []  # maximum circ vel = sqrt(G * M(r) / r) [km / s physical]
            #cat['vel.disp'] = []    # velocity dispersion [km / s] (DM, 1D, no hubble flow)
            cat['mass.200c'] = []  # M_200c from unweighted fit of NFW M(< r) [M_sun]
            cat['concen.200c'] = []  # concentration (200c) from unweighted fit of NFW M(< r)
            #cat['concen.fof'] = []    # concentration derive from r_{2/3 mass} / r_{1/3 mass}
            cat['parent.index'] = []  # index of parent, at previous snapshot, with max mass
            #cat['parent.next.index'] = []    # index of next parent to same child, at same snapshot
            #cat['child.index'] = []    # index of child, at next snapshot
            #cat['mass.fof.ratio'] = []    # FoF mass ratio of two highest mass parents (< 1)
            #cat['central.index'] = []     # index of central subhalo
        else:
            raise ValueError('catalog kind = %s not valid' % catalog_kind)

        self.directory_sim = self.treepm_directory + 'lcdm%d/' % (box_length / 1000)
        self.directory_tree = self.directory_sim + 'tree/'
        snapshot_indices = ut.array.arrayize(snapshot_indices)

        # read auxilliary data
        cat.Cosmology = self.read_cosmology()
        cat.info = {
            'catalog.kind': catalog_kind,
            'catalog.source': 'L%d' % box_length,
            'box.length/h': float(box_length),
            'box.length': float(box_length) / cat.Cosmology['hubble'],
            'particle.number': self.particle_number[box_length],
            'particle.mass': cat.Cosmology.particle_mass(box_length / cat.Cosmology['hubble'],
                                                         self.particle_number[box_length])
        }

        if catalog_kind == 'subhalo':
            cat.info['mass.kind'] = 'mass.peak'
        elif catalog_kind == 'halo':
            cat.info['mass.kind'] = 'mass.fof'

        if cat_in is not None:
            cats = cat_in
        else:
            cats.Cosmology = cat.Cosmology
            cats.info = cat.info
            cats.snapshot = self.read_snapshot_times()

        # sanity check on snapshot range
        if snapshot_indices.max() >= cats.snapshot['redshift'].size:
            snapshot_indices = snapshot_indices[snapshot_indices < cats.snapshot['redshift'].size]
        for ti_all in range(snapshot_indices.max() + 1):
            if ti_all >= len(cats):
                cats.append({})
        for ti in snapshot_indices:
            cats[ti] = copy.copy(cat)
            cats[ti].snapshots = cats.snapshot
            cats[ti].snapshot = {}
            for k in cats.snapshot.dtype.names:
                cats[ti].snapshot[k] = cats.snapshot[k][ti]
            cats[ti].snapshot['index'] = ti
            self.read_catalog_snapshot(cats[ti], ti, cats.snapshot['redshift'][ti])

        return cats

    def read_catalog_snapshot(self, cat, snapshot_index, redshift):
        '''
        Read catalog at single snapshot, assign to catalog dictionary.

        Parameters
        ----------
        cat : dict : catalog of [sub]halos at snapshot
        snapshot_index : int : snapshot index
        redshift : float : redshift of snapshot
        '''
        if cat.info['catalog.kind'] == 'subhalo':
            file_name_base = 'subhalo_tree_%s.dat' % str(snapshot_index).zfill(2)
            props = [
                'position', 'velocity', 'mass.bound', 'vel.circ.max', 'mass.peak', 'vel.circ.peak',
                'ilk', 'parent.index', 'parent.next.index', 'child.index', 'mass.frac.min',
                'mass.peak.ratio.raw', 'sat.last.ti', 'sat.last.index',
                'ilk.dif.ti', 'ilk.dif.index',
                'central.index', 'central.distance', 'satellite.index',
                'halo.index', 'halo.mass',
            ]
        elif cat.info['catalog.kind'] == 'halo':
            file_name_base = 'halo_tree_%s.dat' % str(snapshot_index).zfill(2)
            props = [
                'position', 'velocity', 'mass.fof',
                'vel.circ.max', 'vel.disp',
                'mass.200c', 'concen.200c', 'concen.fof',
                'parent.index', 'parent.next.index',
                'child.index', 'mass.fof.ratio', 'central.index',
            ]
            if cat.info['box.length/h'] == 720:
                props.remove('central.index')

        file_name = self.directory_tree + file_name_base
        file_in = open(file_name, 'r')
        obj_number = int(np.fromfile(file_in, np.int32, 1))
        for prop in props:
            if len(prop) > 2 and (prop[-2:] == '.index' or prop[-3:] == '.ti' or prop == 'ilk'):
                dtype = np.int32
            else:
                dtype = np.float32
            self.read_property(file_in, cat, prop, dtype, obj_number, redshift)
            if prop == 'concen.200c' and 'concen.200c' in cat:
                cat['concen.200c'][1:] = cat['concen.200c'][1:].clip(1.5, 40)

        file_in.close()
        self.say('read %8d %s from %s' % (obj_number, cat.info['catalog.kind'], file_name_base))

    def read_property(self, file_in, cat, property_name, dtype, obj_number, redshift):
        '''
        Read property from file, assign to [sub]halo catalog.

        Parameters
        ----------
        file_in : file : input file (already opened)
        cat : dict : catalog of [sub]halos at snapshot
        property_name : string : property name
        dtype : data type of property
        obj_number : int : number of objects to read
        redshift : float : redshift of snapshot
        '''
        if property_name in ['position', 'velocity']:
            dimension_number = self.dimension_number
        else:
            dimension_number = 1

        temp = np.fromfile(file_in, dtype, obj_number * dimension_number)
        if property_name in cat:
            if dimension_number > 1:
                cat[property_name] = temp.reshape(obj_number, dimension_number)
            else:
                cat[property_name] = temp
            if property_name in ['position', 'central.distance']:
                cis = np.where(cat[property_name] > 0)[0]
                # {Mpc/h -> kpc}
                cat[property_name][cis] *= ut.const.kilo_per_mega / cat.Cosmology['hubble']
            elif property_name == 'velocity':
                # {Mpc/h / Gyr comoving -> km / s physical}
                cat[property_name] *= (
                    ut.const.km_per_Mpc / cat.Cosmology['hubble'] / ut.const.sec_per_Gyr /
                    (1 + redshift))

    def read_snapshot_times(self):
        '''
        Read time properties of snapshot, assign to dictionary, return.
        '''
        file_name_base = 'snapshots.txt'

        file_name = self.directory_sim + file_name_base
        snaptime = np.genfromtxt(
            file_name, comments='#', usecols=[1, 2, 3, 4, 5],
            dtype=[
                ('scalefactor', np.float32),
                ('redshift', np.float32),
                ('time', np.float32),
                ('time.width', np.float32),
                ('time.hubble', np.float32)  # Hubble time = 1 / H(t) [Gyr]
            ]
        )
        self.say('read ' + file_name)

        return snaptime

    def read_cosmology(self):
        '''
        Read cosmological parameters, save as class, return.
        '''
        file_name_base = 'cosmology.txt'

        file_name = self.directory_sim + file_name_base
        file_in = open(file_name, 'r')

        Cosmology = ut.cosmology.CosmologyClass(sigma_8=self.sigma_8)

        for line in file_in:
            cin = np.array(line.split(), np.float32)
            if len(cin) == 7:
                Cosmology['omega_matter'] = cin[0]
                Cosmology['omega_lambda'] = cin[1]
                Cosmology['w'] = cin[2]
                omega_baryon_0_h2 = cin[3]
                Cosmology['hubble'] = cin[4]
                Cosmology['n_s'] = cin[5]
                Cosmology['omega_baryon'] = omega_baryon_0_h2 / Cosmology['hubble'] ** 2
                break
            else:
                raise ValueError('%s not formatted correctly' % file_name_base)

        file_in.close()

        if (Cosmology['omega_matter'] < 0 or Cosmology['omega_matter'] > 0.5 or
                Cosmology['omega_lambda'] < 0.5 or Cosmology['omega_lambda'] > 1):
            self.say('! read strange cosmology in %s' % file_name_base)

        return Cosmology

    def pickle_first_infall(
        self, io_direction='read', subs=None, snapshot_indices=np.arange(34), mass_peak_min=10.5):
        '''
        Read/write first infall times and subhalo indices at all snapshots in input range.

        Parameters
        ----------
        direction : string : pickle direction: read, write
        subs : list : catalog of subhalos across snapshots
        snapshot_indices : int or list : indices of snapshots at which to write/assign
        mass_peak_min : float : M_peak minnimum to write/assign
        '''
        infall_name = 'sat.first'
        file_name_short = 'subhalo_sat.first_mass.peak.%.1f' % (mass_peak_min)

        file_name_base = (self.treepm_directory +
                          'lcdm%d/tree/' % (subs.info['box.length/h'] / 1000) + file_name_short)

        if io_direction == 'write':
            snapshot_indices = np.arange(max(snapshot_indices) + 1)
            siss = []
            inf_tiss = []
            inf_siss = []
            for ti in snapshot_indices:
                sub = subs[ti]
                sis = ut.array.get_indices(sub[infall_name + '.ti'], [ti, Inf])
                siss.append(sis)
                inf_tiss.append(sub[infall_name + '.ti'][sis])
                inf_siss.append(sub[infall_name + '.index'][sis])
            ut.io.file_pickle(
                file_name_base, [snapshot_indices, siss, inf_tiss, inf_siss])
        elif io_direction == 'read':
            snapshot_indices = ut.array.arrayize(snapshot_indices)
            _tis_in, siss, inf_tiss, inf_siss = ut.io.file_pickle(file_name_base)
            for ti in snapshot_indices:
                sub = subs[ti]
                sub[infall_name + '.ti'] = np.zeros(sub['parent.index'].size, np.int32) - 1
                sub[infall_name + '.ti'][siss[ti]] = inf_tiss[ti]
                sub[infall_name + '.index'] = ut.array.get_array_null(sub['parent.index'].size)
                sub[infall_name + '.index'][siss[ti]] = inf_siss[ti]
        else:
            raise ValueError('not recognize i/o direction = %s' % io_direction)

Read = ReadClass()


#===================================================================================================
# read/write
#===================================================================================================
class SubhaloPropertyClass(ut.io.SayClass):
    '''
    Read/write additional properties for subhalo catalog, mostly for satellite histories.
    '''
    def __init__(self):
        self.directory_prop = (RESEARCH_DIRECTORY +
                               'project/finished/galaxy_group_catalog/subhalo_data/')

    def pickle_property(
        self, io_direction='read', subs=None, ti_now=1, ti_max=15, property_name='central.distance.int',
        tis_assign=[]):
        '''
        Read/write: central.distance.int [kpc comoving], M_star (> 9.7 for galaxy mock),
        tidal.velocity, tidal.energy

        Parameters
        ----------
        direction : string : pickle direction: read, write
        subs : list : catalog of subhalos across snapshots
        ti_now : int : current index of snapshot to select subhalos
        ti_max : int : maximum index of snapshot to follow back
        property_name : string : name of property to pickle
        '''
        if not tis_assign:
            tis_assign = list(range(ti_now, ti_max + 1))

        if property_name == 'central.distance.int':
            prop_limits = [1e-10, Inf]
            mass_kind = 'star.mass'
            mass_limits = [9.7, Inf]
            file_name_short = ('%s_ti.%d-%d_%s.%.1f' %
                               (property_name, ti_now, ti_max, mass_kind, mass_limits[0]))
        elif property_name == 'star.mass':
            prop_limits = [8.5, Inf]
            mass_kind = 'star.mass'
            scatter = 0.15
            file_name_short = '%s_ti.%d-%d_%s.%.1f_scat.%.2f' % (
                property_name, ti_now, ti_max, mass_kind, prop_limits[0], scatter)
        elif property_name == 'neig.5.distance' or property_name[:3] == 'tid':
            if property_name[:3] == 'tid':
                prop_limits = [1e-10, Inf]
                neig_distance_max = 0.29
            elif property_name == 'neig.5.distance':
                prop_limits = [1e-10, 19.99]
                neig_distance_max = 3.57
            mass_kind = 'star.mass'
            mass_min = 9.7
            neig_mass_min = 8.5
            neig_number_max = 200
            file_name_short = '%s_ti%d-%d_%s%.1f_neig_%s%.1f_num%2d_dist%.1f' % (
                property_name, ti_now, ti_max, mass_kind, mass_min, mass_kind, neig_mass_min,
                neig_number_max, neig_distance_max)
        else:
            raise ValueError('not recognize property_name = %s' % property_name)

        file_name_base = self.directory_prop + file_name_short
        tis = np.arange(ti_now, ti_max + 1)

        if io_direction == 'write':
            propss = []
            siss = []
            for ti in tis:
                sis = ut.array.get_indices(subs[ti][property_name], prop_limits)
                propss.append(subs[ti][property_name][sis])
                siss.append(sis)
                self.say('t_i = %2d | %7d above property_name min' % (ti, sis.size))
            ut.io.file_pickle(file_name_base, [tis, siss, propss])
        elif io_direction == 'read':
            tis, siss, propss = ut.io.file_pickle(file_name_base)
            snapshot_number = len(subs)
            for ti in range(snapshot_number):
                subs[ti][property_name] = []
            for tii, ti in enumerate(tis):
                if ti < snapshot_number:
                    subs[ti][property_name] = np.zeros(subs[ti]['mass.peak'].size, np.float32)
                    subs[ti][property_name][1:] -= 1
                    subs[ti][property_name][siss[tii]] = propss[tii]
                    self.say('snapshot = %d | read %d objects' % (ti, siss[tii].size))
                else:
                    self.say('subhalo goes back to snap = %d, only assign %s to there' %
                             (ti - 1, property_name))
                    break
        else:
            raise ValueError('not recognize i/o direction = %s' % io_direction)

    # satellite history properties ----------
    def pickle_first_infall_znow(
        self, io_direction='read', sub=None, mass_kind='mass.peak', mass_limits=[10.8, Inf],
        disrupt_mf=0.007):
        '''
        Parameters
        ----------
        direction : string : pickle direction: read, write
        sub : dict : catalog of subhalos at snapshot
        mass_kind : string : subhalo mass kind to select by
        mass_limits : list : min and max of mass_kind to select
        disrupt_mf : float : mass fraction (wrt M_peak) for disrupting (removing) subhalos

        Have star.mass = [9.7, Inf] & mass.peak = [10.8, Inf] to ti.max = 34.
        '''
        infall_name = 'sat.first'
        file_name_short = '{}_ti.{}_{}.{:.1f}_mf.{:.3f}'.format(
            infall_name, sub.snapshot['index'], mass_kind, mass_limits[0], disrupt_mf)

        file_name_base = self.directory_prop + file_name_short
        if io_direction == 'write':
            sis = ut.array.get_indices(sub[infall_name + '.ti'], [sub.snapshot['index'], Inf])
            ut.io.file_pickle(
                file_name_base,
                [sis, sub[infall_name + '.ti'][sis], sub[infall_name + '.index'][sis]])
        elif io_direction == 'read':
            sis, inf_tis, inf_sis = ut.io.file_pickle(file_name_base)
            sub[infall_name + '.ti'] = np.zeros(sub[list(sub.keys())[0]].size, np.int32) - 1
            sub[infall_name + '.ti'][sis] = inf_tis
            sub[infall_name + '.index'] = ut.array.get_array_null(sub[list(sub.keys())[0]].size)
            sub[infall_name + '.index'][sis] = inf_sis
        else:
            raise ValueError('not recognize i/o direction = %s' % io_direction)

    def pickle_property_extrema_satellite(
        self, io_direction='read', sub=None, ti_max=15,
        mass_kind='star.mass', mass_limits=[9.7, Inf], neig_mass_limits=[8.5, Inf]):
        '''
        Read/write extrema history data for satellites.

        Parameters
        ----------
        direction : string : pickle direction: read, write
        sub : dict : catalog of subhalos at snapshot
        ti_max : int : max snapshot index to follow back
        mass_kind : string : mass kind for selecting subhalos
        mass_limits : list : min and max limits for mass_kind
        neig_mass_limits : list : min and max limits for mass_kind to select neighbor subhalos
        '''
        file_name_short = 'sat_property_extrema_ti.%d-%d_%s.%.1f_neig.%s.%.1f' % (
            sub.snapshot['index'], ti_max, mass_kind, mass_limits[0], mass_kind,
            neig_mass_limits[0])

        file_name_base = self.directory_prop + file_name_short

        if io_direction == 'write':
            ut.io.file_pickle(file_name_base, sub.sathistory)
        elif io_direction == 'read':
            sub.sathistory = {}
            sub.sathistory = ut.io.file_pickle(file_name_base)
        else:
            raise ValueError('not recognize i/o direction = %s' % io_direction)

    def pickle_history_satellite(self, prop_names='some', subs=None, ti_now=1, ti_max=15):
        '''
        Read and assign satellite properties going back to ti_max.

        Parameters
        ----------
        prop_names : string : properties to assign history of: some, all
        subs : list : catalog of subhalos across snapshots
        ti_now : int : current snapshot index to select subhalos
        ti_max : int : maximum snapshot index to follow back
        '''
        if prop_names in ['some', 'all']:
            # self.pickle_first_infall_znow('read', subs[ti_now], 'star.mass', [9.7, Inf])
            self.pickle_first_infall_znow('read', subs[ti_now], 'mass.peak', [10.8, Inf])
            self.pickle_property_extrema_satellite(
                'read', subs[ti_now], ti_max, 'star.mass', [9.7, Inf], [8.5, Inf])
            if prop_names == 'all':
                self.pickle_property('read', subs, ti_now, ti_max, 'central.distance')
                self.pickle_property('read', subs, ti_now, ti_max, 'neig.5.distance')
                self.pickle_property('read', subs, ti_now, ti_max, 'tidal.energy.m.bound')
                self.pickle_property('read', subs, ti_now, ti_max, 'star.mass')
        else:
            raise ValueError('not recognize prop_names = %s' % prop_names)

    def pickle_central_ejected_znow(
        self, io_direction='read', eje=None, snapshot_index=1, mass_kind='mass.peak',
        mass_limits=[10.8, Inf], disrupt_mf=0.007):
        '''
        Parameters
        ----------
        direction : string : pickle direction: read, write
        eje : dict : catalog of ejected satellites at snapshot
        snapshot_index : int : index of snapshot
        mass_kind : string : mass kind for selecting subhalos
        mass_limits : list : min and max limits for mass_kind
        disrupt_mf : float : mass fraction (wrt M_peak) for disrupting (removing) subhalos

        Have mass_peak = [10.8, Inf] to t_i_max = 34.
        '''
        file_name_short = ('central.ejected_ti%d_%s%.1f_mf%.3f' %
                           (snapshot_index, mass_kind, mass_limits[0], disrupt_mf))
        file_name_base = self.directory_prop + file_name_short

        if io_direction == 'write':
            ut.io.file_pickle(file_name_base, eje)
        elif io_direction == 'read':
            eje = ut.io.file_pickle(file_name_base)
            return eje
        else:
            raise ValueError('not recognize i/o direction = %s' % io_direction)

    # nearest halo properties ----------
    def pickle_nearest_halo(
        self, io_direction='read', sub=None, gm_kind='mass.peak', gm_limits=[10.8, Inf],
        disrupt_mf=0.007, neig_hm_limits=[12, Inf], neig_number_max=100, neig_distance_max=10):
        '''
        Pickle properties of nearest (minimum distance/R_200m,neig) halo.

        Parameters
        ----------
        direction : string : pickle direction: read, write
        sub : dict : catalog of subhalos at snapshot
        gm_kind : string : mass kind for galaxy/subhalo
        gm_limits : list : min and max limits for gm_kind
        disrupt_mf : float : mass fraction (wrt M_peak) for disrupting (removing) subhalos
        neig_hm_limits : list : min and max limits on halo mass to select neighbor halos
        neig_number_max : int : maximum number of neighbors per subhalo
        neig_distance_max : float : maximum distance of neighbor
        '''
        prop_base_name = 'nearest'

        file_name_base = 'nearest_%s%s_mf%.3f_neig_hm%s_num%d_dist%d' % (
            gm_kind, gm_limits[0], disrupt_mf, neig_hm_limits[0], neig_number_max,
            neig_distance_max)
        file_name = self.directory_prop + file_name_base

        if io_direction == 'write':
            sis = ut.array.get_indices(sub[prop_base_name + '.central.index'], [0, Inf])
            self.say('write %d nearest halos' % sis.size)
            propss = []
            prop_names = []
            for k in list(sub.keys()):
                if prop_base_name in k:
                    prop_names.append(k)
                    propss.append(sub[k][sis])
            ut.io.file_pickle(file_name, [sis, prop_names, propss])
        elif io_direction == 'read':
            sis, prop_names, propss = ut.io.file_pickle(file_name)
            for pi in range(len(prop_names)):
                sub[prop_names[pi]] = np.zeros(sub[gm_kind].size, propss[pi].dtype) - 1
                sub[prop_names[pi]][sis] = propss[pi]
            self.say('read %d nearest halos' % sis.size)
        else:
            raise ValueError('not recognize i/o direction = %s' % io_direction)

SubhaloProperty = SubhaloPropertyClass()


#===================================================================================================
# write
#===================================================================================================
def write_subhalo_mock(
    sub, mass_kind='star.mass', mass_limits=[8.5, Inf], scatter=0.15, disrupt_mf=0.007):
    '''
    Write subhalo catalog with neighbor counts for Jeremy.

    Parameters
    ----------
    sub : list : catalog of subhalos at snapshot
    mass_kind : string : mass kind for selecting subhalos
    mass_limits : list : min and max limits for mass_kind
    scatter : float : log-normal scatter in M_galaxy - M_halo
    disrupt_mf : float : mass fraction (wrt M_peak) for disrupting (removing) subhalos
    '''
    from . import sham

    directory_name = RESEARCH_DIRECTORY + 'sdss/group_catalog/tinker/subhalo/'

    sham.assign_to_catalog(sub, mass_kind, scatter, disrupt_mf)
    masses = sub[mass_kind] + 2 * log10(sub.Cosmology['hubble'])
    sis = ut.array.get_indices(masses, mass_limits)
    poss = sub['position'][sis]
    vels = sub['velocity'][sis]

    file_name_base = 'subhalo_L%.0f_ti.%d_%s.%.1f_scat.%s_mf.%s.txt' % (
        sub.info['box.length/h'], sub.snapshot['index'], mass_kind, mass_limits[0],
        str(scatter).split('.')[1], str(disrupt_mf).split('.')[1])
    file_name = directory_name + file_name_base
    file_out = open(file_name, 'w')
    file_out.write('# box length = %.3f kpc\n' % sub.info['box.length'])
    file_out.write('# snapshot = %d\n' % sub.snapshot['index'])
    file_out.write('# redshift = %.3f\n' % sub.snapshot['redshift'])
    file_out.write('# %s = [%.2f, %.2f]\n' % (mass_kind, mass_limits[0], masses.max()))
    file_out.write('# scatter in log M_star - log M_peak = %.2f dex\n' % scatter)
    file_out.write('# position(x,y,z)[kpc comoving] velocity(x,y,z)[km / s physical] ' +
                   'logM_star{M_sun / h^2} logM_peak[M_sun] id_subhalo\n')
    for sii, si in enumerate(sis):
        file_out.write('%.3f %.4f %.4f %.2f %.2f %.2f %.3f %.3f %d' %
                       (poss[sii, 0], poss[sii, 1], poss[sii, 2],
                        vels[sii, 0], vels[sii, 1], vels[sii, 2],
                        masses[si], sub['mass.peak'][si], si))
    file_out.close()


def write_subhalo_mock_alis(
    sub, hal, mass_kind='mass.peak', mass_limits=[11, Inf], disrupt_mf=0.007):
    '''
    Write subhalo catalog for Alis.

    Parameters
    ----------
    sub : list : catalog of subhalos at snapshot
    hal : list : catalog of halos at snapshot
    mass_kind : string : mass kind for selecting subhalos
    mass_limits : list : min and max limits for mass_kind
    disrupt_mf : float : mass fraction (wrt M_peak) for disrupting (removing) subhalos
    '''
    from . import sham

    file_name_base = 'subhalo_%s%.1f.txt' % (mass_kind, mass_limits[0])
    file_name = RESEARCH_DIRECTORY + 'sdss/group_catalog/tinker/galaxy-mock_share/' + file_name_base
    file_out = open(file_name, 'w')
    sis = ut.catalog.get_indices_subhalo(
        sub, mass_kind, mass_limits, [1, Inf], disrupt_mf=disrupt_mf)
    scats = [0, 0.15, 0.2]
    masses_star = []
    for scatter in scats:
        sham.assign_to_catalog(sub, 'star.mass', scatter, disrupt_mf)
        masses_star.append(sub['star.mass'][sis])
    file_out.write('# masses in log{M/M_sun}, using h = %.2f\n' % sub.Cosmology['hubble'])
    file_out.write('# positions in [kpc comoving], velocities in [km / s physical]\n')
    file_out.write('# number of subhalos = %d\n' % sis.size)
    file_out.write('# log(M_peak / M_sun) = [%.2f, %.2f]' %
                   (mass_limits[0], sub[mass_kind][sis].max()))
    file_out.write('# subhalo-id central-id halo-id M_peak am-sat M_halo C_200c ' +
                   'position(x, y, z) velocity(x, y, z) M_star(0) M_star(0.15) M_star(0.2)\n')
    for sii, si in enumerate(sis):
        if sub['ilk'][si] <= 0:
            am_sat = 1
        else:
            am_sat = 0
        hi = sub['halo.index'][si]
        file_out.write(
            '%d %d %d %.3f %d %.3f %.2f %.3f %.3f %.3f %.5f %.5f %.5f %.3f %.3f %.3f\n' %
            (si, sub['central.index'][si], sub['halo.index'][si], sub[mass_kind][si], am_sat,
             sub['halo.mass'][si] + ut.catalog.MASS_RATIO_200M_LL168, hal['c.200c'][hi],
             sub['position'][si, 0], sub['position'][si, 1], sub['position'][si, 2],
             sub['velocity'][si, 0], sub['velocity'][si, 1], sub['velocity'][si, 2],
             masses_star[0][sii], masses_star[1][sii], masses_star[2][sii])
        )
    file_out.close()


def write_subhalo_mass_grow_central(
    subs, ti_now=1, ti_max=22, mass_kind='mass.peak', mass_limits=[11, Inf], disrupt_mf=0.007):
    '''
    Write subhalo mass growth information for Jeremy/Alis.

    Parameters
    ----------
    subs : list : catalog of subhalos across snapshots
    ti_now : int : index of snapshot for selecting subhalos
    ti_max : int : maximum snapshot index to follow back
    mass_kind : string : mass kind for selecting subhalos
    mass_limits : list : min and max limits for mass_kind
    disrupt_mf : float : mass fraction (wrt M_peak) for disrupting (removing) subhalos
    '''
    from . import treepm_analysis

    form_time_kind = 'time'
    form_prop_kind = 'mass.peak'

    sis = ut.catalog.get_indices_subhalo(
        subs[ti_now], mass_kind, mass_limits, ilk='central', disrupt_mf=disrupt_mf)
    form_times_50 = treepm_analysis.Formation.get_times(
        subs, ti_now, ti_max, sis, form_time_kind, form_prop_kind, 0.5)
    form_times_85 = treepm_analysis.Formation.get_times(
        subs, ti_now, ti_max, sis, form_time_kind, form_prop_kind, 0.85)
    tis = np.array([3, 5, 8, 10, 12, 14, 16, 18, 20, 22])
    file_name_base = 'subhalo_central_%s.grow_L%.0f_ti.%d_%s.%.1f.txt' % (
        mass_kind, subs.info['box.length'], ti_now, mass_kind, mass_limits[0])
    file_out = open(file_name_base, 'w')
    file_out.write('# box length = %.1f kpc\n' % subs.info['box.length'])
    file_out.write('# snapshot = %d\n' % ti_now)
    file_out.write('# redshift = %.2f\n' % subs.snapshot['redshift'][ti_now])
    file_out.write('# age = %.3f Gyr\n' % subs.snapshot['time'][ti_now])
    file_out.write('# %s range = [%.2f, %.2f]\n' %
                   (mass_kind, mass_limits[0], subs[ti_now][mass_kind][sis].max()))
    file_out.write(
        '# id_subhalo t(M=0.5) t(M=0.85) M(z=%.2f) M(z=%.2f) M(z=%.2f) M(z=%.2f) ' +
        'M(z=%.2f) M(z=%.2f) M(z=%.2f) M(z=%.2f) M(z=%.2f) M(z=%.2f) M(z=%.2f)\n' %
        (subs.snapshot['redshift'][ti_now], subs.snapshot['redshift'][tis[0]],
         subs.snapshot['redshift'][tis[1]], subs.snapshot['redshift'][tis[2]],
         subs.snapshot['redshift'][tis[3]], subs.snapshot['redshift'][tis[4]],
         subs.snapshot['redshift'][tis[5]], subs.snapshot['redshift'][tis[6]],
         subs.snapshot['redshift'][tis[7]], subs.snapshot['redshift'][tis[8]],
         subs.snapshot['redshift'][tis[9]])
    )
    for sii, si in enumerate(sis):
        masses = np.zeros(tis.size, np.float32)
        par_ti, par_si = ti_now + 1, subs[ti_now]['parent.index'][si]
        while 0 < par_ti <= tis.max() and par_si > 0:
            for tii in range(tis.size):
                if par_ti == tis[tii]:
                    masses[tii] = subs[par_ti]['mass.peak'][par_si]
            par_ti, par_si = par_ti + 1, subs[par_ti]['parent.index'][par_si]
        file_out.write(
            '%d %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f' %
            (si, form_times_50[sii], form_times_85[sii], subs[ti_now]['mass.peak'][si],
             masses[0], masses[1], masses[2], masses[3], masses[4], masses[5], masses[6], masses[7],
             masses[8], masses[9])
        )
    file_out.close()


def write_position_binary(cat, indices, file_name_base, property_name=None):
    '''
    Write positions, in units of box length, to binary file.

    Parameters
    ----------
    cat : dict : catalog of [sub]halos at snapshot
    indices : array : indices of [sub]halos
    file_name_base : string : base name of file (without extension)
    property_name : string : additional property to write
    '''
    file_name_base += '.dat'
    file_out = open(file_name_base, 'wb')

    np.array(len(indices), np.int32).tofile(file_out)
    np.array(cat['position'][indices][:, 0] / cat.info['box.length'], np.float32).tofile(file_out)
    np.array(cat['position'][indices][:, 1] / cat.info['box.length'], np.float32).tofile(file_out)
    np.array(cat['position'][indices][:, 2] / cat.info['box.length'], np.float32).tofile(file_out)
    if property_name:
        np.array(cat[property_name][indices], np.float32).tofile(file_out)

    file_out.close()
    print('# printed %d objects in %s' % (indices.size, file_name_base))
