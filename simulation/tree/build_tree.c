#include	<stdlib.h>
#include	<stdio.h>
#include	<math.h>
#include	<string.h>
#include	<time.h>

/*
Read in raw tpmsph files from Martin White's TreePM simulation.
Track halos & subhalos across time.

Read in:
	tpmsph_*.fofp		(halo)
	tpmsph_*.child		(halo)
	tpmsph_*.fofp		(subhalo)
	tpmsph_*.child		(subhalo)
	tpmsph_*.par		(subhalo)
	tpmsph_*.cen		(subhalo)

Complier flags:
	SUBFIND - use for SUBFIND mass & history
	USE_INFALL_MASS - use for mass only at infall, not maximum throughout central history
	WRITE_SUBHALO_TREE - print tree for subhalos in treepm/ directory
	WRITE_HALO_TREE - write tree files for halos in treepm/ directory
	PRINT_SNAPSHOTS - print simulation snapshot information
	NO_SUBHALO - build halo tree without subhalos (excludes pointer to central subhalo index)
*/

#define DIMEN_NUM               3       /* number of spatial dimensions */
#define	Halo_Mmin        		0		/* minimum halo mass (ignore those below this mass) */
#define Snapshot_Index_Min      1       /* final snapshot for child assignment, >1 to not use all */
#define	Snapshot_Num_Max        64      /* maximum number of snapshots */
#define	Subhalo_Virtual_Factor  1.4		/* extra memory for virtual subs */

#define	DirTreePM       "/Users/awetzel/work/research/treepm/data"  /* simulation data  */
#ifdef SUBFIND
#define DirOutput       "outputs_subfind"	/* base folder, input files */
#define	DirTree         "tree_subfind"		/* output tree folder */
#else
#define DirOutput       "outputs_fof6d"		/* base folder, input files */
#define	DirTree         "tree_fof6d"		/* output tree folder */
#endif
#define DirSubProp      "subhalo_fofp" 		/* subhalo properties folder */
#define	DirSubChild		"subhalo_child"		/* subhalo child index folder */
#define	DirHaloProp		"halo_fofp"	 		/* halo properties folder */
#define DirHaloChild	"halo_child"		/* halo child index folder */
#define	DirHaloCen		"halo_central"		/* halo central subhalo index folder */
#define	DirSubHost		"subhalo_host"		/* subhalo halo index folder */
#define	FileCosmo       "cosmology.txt"		/* simulation cosmology */
#define	FileAOut        "aout.txt"	 		/* output a's */

float THubble = 9.7778  // 1 / H_0 {Gyr / h}
float Nspl = 29         // number of spline points for concentration

struct SubGrp {
    float pos[DIMEN_NUM];	// position of most bound particle, in box units
    float vel[DIMEN_NUM];	// velocity in units of box expansion
    float m;				// self-bound current mass
    float vc;				// peak circ velocity [km / s] v ^ 2 = G M(< r) / r
    //float	vs;			// velocity dispersion [km / s] (DM, 1D, no hubble flow)
    //float	vp;			// potential well velocity [km / s] Phi = (1/2) M vp ^ 2
    float mmax;			// mass at infall/maximum
    float vcmax;			// vc at infall/maximum
    float mfracmin;		// minimum mnow/mmax (1 for cen)
    int uinf;				// snapshot before fell into current halo
    int einf;				// subhalo id before fell into current halo
    int udif;				// snapshot when sat/central last was central/sat
    int edif;				// subhalo id when sat/central last was central/sat
    int upar;				// snapshot time of first progenitor
    int epar;				// head of parent list (> 0 if there is a parent)
    int uparn;			// next progenitor output time
    int eparn;			// next progenitor index
    int uchi;				// child snapshot
    int echi;				// child index
    float mrat;			// ratio of mass of 2 parents
    //int	mergetype;		// type of merger (cen-cen,cen-sat,sat-cen,sat-sat)
    int ilk;				// 1 = central, 2 = virtual central
                            // 0 = satellite, -1 = virtual satellite,
                            // -2 = virtual satellite without central, -3 = without halo
    int ecen;				// id of central subhalo in halo (can be self)
    float distcen;		// distance from central {Mpc/h comoving}
    //int esat;			// index of next satellite subhalo in same halo
    int ehalo;			// index of halo (points to below structure)
    float mhalo;			// mass of halo
}*S[Snapshot_Num_Max];

struct FoFGrp {
    float pos[DIMEN_NUM];	// position of most bound particle, in box units
    float vel[DIMEN_NUM];	// velocity in units of box expansion
    float m;				// FoF mass
    float vc;				// peak circular velocity [km / s] v ^ 2 = G M(< r) / r
    float vs;				// velocity dispersion [km / s] (DM, 1D ave, no hubble flow)
    float m200;			// M_200c {M_sun / h}, from unweighted chi ^ 2 fit of NFW M(< r)
    float c200;			// concentration (200c) from unweighted chi ^ 2 fit of NFW M(< r)
    float cfof;			// c = r_{2/3} / r_{1/3}
    int epar;				// head of progenitor list (> 0 if there is a parent)
    int eparn;
    int echi;				// index of child at next snapshot
    float mrat;			// ratio of mass of 2 parents
    int ecen;				// index of central subhalo
}*G[Snapshot_Num_Max];

struct struct_compare {
  int id;
  int o;
  float sortprop;
}*C;

char Simulation_Directory[128];			// directory for input simulation files
float Box_Length;
float Particle_Mass;
int nout;					// number of outputs
int *subhalo_num;					// array of number of subhalos in each timestep
int *halo_num;				// array of number of halos in each timestep
int *halo_mmin_num;
int *hid[Snapshot_Num_Max];
float *aexps;					// array of all output a's
float *redshifts;					// redshift at given output
float *times;					// age at given output [Gyr]
float *time_wids;				// time since last output at given output [Gyr]
float *hubble_times;				// Hubble time at given output [Gyr]
float Omega_M0, Omega_L0, Omega_B0, hubble;		// cosmological parameters


/* UTILITY ****************************************************************************************/
void breakpoint() {
    int temp, i;
    for (i = 0; i < 10; i++)
        temp = 0;
    temp += 1;
}

int fcompare(const void *a, const void *b)
/* Sort floats in decreasing order. */
{
  int tmp = 0;
  float Pa, Pb;
  Pa = *(float *) a;
  Pb = *(float *) b;
  if (Pa > Pb)
    tmp = -1;
  if (Pa == Pb)
    tmp = 0;
  if (Pa < Pb)
    tmp = 1;
  return tmp;
}

int structcomparedecr(const void *a, const void *b)
/* Sort structure in decreasing order. */
{
  int tmp = 0;
  struct struct_compare Pa = *(struct struct_compare *) a;
  struct struct_compare Pb = *(struct struct_compare *) b;
  if (Pa.sortprop > Pb.sortprop)
    tmp = -1;
  if (Pa.sortprop == Pb.sortprop)
    tmp = 0;
  if (Pa.sortprop < Pb.sortprop)
    tmp = 1;
  return tmp;
}

int structcompareincr(const void *a, const void *b)
/* Sort structure in increasing order. */
{
  int tmp = 0;
  struct struct_compare Pa = *(struct struct_compare *) a;
  struct struct_compare Pb = *(struct struct_compare *) b;
  if (Pa.sortprop < Pb.sortprop)
    tmp = -1;
  if (Pa.sortprop == Pb.sortprop)
    tmp = 0;
  if (Pa.sortprop > Pb.sortprop)
    tmp = 1;
  return tmp;
}

int icompare(const void *a, const void *b)
/* Sort ints in increasing order. */
{
  int tmp = 0, Pa, Pb;
  Pa = *(int *) a;
  Pb = *(int *) b;
  if (Pa < Pb)
    tmp = -1;
  if (Pa == Pb)
    tmp = 0;
  if (Pa > Pb)
    tmp = 1;
  return tmp;
}

void *mymalloc(size_t size)
/* Allocate memory and check if it worked. */
{
  void *tmp;
  tmp = malloc(size);
  if (tmp == NULL) {
    printf("! mymalloc call failed\n");
    printf("  Attempting to allocate %lu bytes\n", (long) size);
    fflush(NULL);
    exit(1);
  }
  return tmp;
}

void *mycalloc(size_t n, size_t size)
/* Allocate memory, initialize to 0, & check if it worked. */
{
  void *tmp;
  tmp = calloc(n, size);
  if (tmp == NULL) {
    printf("! mycalloc call failed\n");
    printf("  Attempting to allocate %lu bytes\n", (long) size);
    fflush(NULL);
    exit(1);
  }
  return tmp;
}

void spline(float x[], float y[], int n, float yp1, float ypn, float y2[])
/* Import independent variable, dependent variable, # of spline points,
2 numbers that determine initial conditions, array to change.
Fit spline to x-y, using
y_{i+1} = y_i + (y_i-y_{i-1}) / (x_i-x_{i-1}) * (x_{i+1}-x_i) + C*(x_{i+1}-x_i)^2
Assign coefficent of 2nd derivative to y2[]. */
{
  int i, k;
  float p, qn, sig, un, *u;

  u = malloc(n * sizeof(float));
  if (u == NULL) {
    perror("malloc");
    exit(1);
  }
  if (x[0] >= x[n - 1]) {
    printf("! x must be monotonic increasing in spline\n");
    printf("  passed x[0] = %e, x[%d] = %e\n", x[0], n - 1, x[n - 1]);
    exit(1);
  }

  /* normalization of spline array */
  if (yp1 > 0.99e30)
    y2[0] = u[0] = 0.0;
  else {
    y2[0] = -0.5;
    u[0] = (3.0 / (x[1] - x[0])) * ((y[1] - y[0]) / (x[1] - x[0]) - yp1);
  }
  for (i = 1; i < n - 1; i++) {
    sig = (x[i] - x[i - 1]) / (x[i + 1] - x[i - 1]);
    p = sig * y2[i - 1] + 2.;
    y2[i] = (sig - 1.) / p;
    u[i] = (y[i + 1] - y[i]) / (x[i + 1] - x[i])
        - (y[i] - y[i - 1]) / (x[i] - x[i - 1]);
    //printf("%.10f %.10f %.10f %.10f %.10f\n",x[i],sig,p,y2[i],u[i]);
    u[i] = (6. * u[i] / (x[i + 1] - x[i - 1]) - sig * u[i - 1]) / p;
  }

  /* more normalization */
  if (ypn > 0.99e30)
    qn = un = 0.;
  else {
    qn = 0.5;
    un = (3.0 / (x[n - 1] - x[n - 2]))
        * (ypn - (y[n - 1] - y[n - 2]) / (x[n - 1] - x[n - 2]));
  }
  y2[n - 1] = (un - qn * u[n - 2]) / (qn * y2[n - 2] + 1.);

  for (k = n - 2; k >= 0; k--)
    y2[k] = y2[k] * y2[k + 1] + u[k];
  free(u);
}

void splint(float xa[], float ya[], float y2a[], int n, float x, float *y)
/* Import spline array points x, y, array of coefficents of 2nd derivative
from spline(), number of spline points, x-value to evaluate spline function,
y-value to change. Use the cubic spline generated with spline() to interpolate
values, assign to y. */
{
  int klo, khi, k;
  float h, b, a;

  if (x <= xa[0] || x >= xa[n - 1]) {
    printf("! x = %e out of range (%e,%e) in splint", x, xa[0], xa[n - 1]);
    exit(1);
  }

  klo = 0;
  khi = n - 1;

  while (khi - klo > 1) {
    k = (khi + klo) >> 1;
    if (xa[k] > x)
      khi = k;
    else
      klo = k;
  }

  h = xa[khi] - xa[klo];
  if (h == 0.0) {
    printf("! Bad xa input to routine splint\n");
    exit(1);
  }
  a = (xa[khi] - x) / h;
  b = (x - xa[klo]) / h;
  *y =
      a * ya[klo] + b * ya[khi]
          + ((a * a * a - a) * y2a[klo] + (b * b * b - b) * y2a[khi]) * (h * h)
              / 6.;
}

float periodic(float x)
/* Wrap x periodically into range [0,1). */
{
  float tmp;
  tmp = x;
  while (tmp >= 1 || tmp < 0) {
    if (tmp >= 1)
      tmp -= 1.0;
    if (tmp < 0)
      tmp += 1.0;
  }
  return tmp;
}

float diff_periodic(float x)
/* Wrap x periodically into range [-0.5, 0.5). */
{
  float tmp;
  tmp = x;
  while (tmp >= 0.5 || tmp < -0.5) {
    if (tmp >= 0.5)
      tmp -= 1.0;
    if (tmp < -0.5)
      tmp += 1.0;
  }
  return tmp;
}

float get_age(float aout)
/* Import scale factor. Get age of the universe [Gyr] at scale factor. */
{
  int Nint = 500;						// # of integration bins
  float aa, a2;
  float hal, tt;
  int k, wt;

  hal = (aout - 0.0) / Nint;
  aa = aout;
  a2 = aa * aa;
  tt = 1.0 / sqrt(Omega_L0 * a2 + (1. - Omega_M0 - Omega_L0) + Omega_M0 / aa);
  wt = 4;
  for (k = 1; k < Nint; k++) {
    aa = 0.0 + k * hh;
    a2 = aa * aa;
    tt += wt / sqrt(Omega_L0 * a2 + (1. - Omega_M0 - Omega_L0) + Omega_M0 / aa);
    wt = 8 / wt;
  }
  tt *= hal / 3.;
  return THubble * tt / hubble;
}


/* INPUT ******************************************************************************************/
void read_cosmology_parameters()
/* Read the cosmological parameters from file. */
{
  int ngot;
  float ombh2;
  char buf[BUFSIZ];
  char fname[128];
  FILE *fp;

  sprintf(fname, "%s/%s", Simulation_Directory, FileCosmo);
  if ((fp = fopen(fname, "r")) == NULL) {
    printf("! cannot open %s\n", fname);
    exit(1);
  }
  fgets(buf, BUFSIZ, fp);
  ngot = sscanf(buf, "%f %f %*f %f %f %*f %*e", &Omega_M0, &Omega_L0, &ombh2, &hubble);
  if (ngot != 4) {
    printf("! Error parsing cosmological parameters in %s\n", fname);
    printf("  Read %d items, expecting 4\n", ngot);
    printf("  Got: %s\n", buf);
    exit(1);
  }
  if (Omega_M0 <= 0 || Omega_M0 > 1 || Omega_L0 < 0 || Omega_L0 > 1
      || fabs(hubble - 0.7) > 0.3) {
    printf("! strange cosmology read from %s\n", fname);
    printf("  Omega_M0 = %f, Omega_L0 = %f, h = %f\n", Omega_M0, Omega_L0, hubble);
    exit(1);
  }
  Omega_B0 = ombh2 / hubble / hubble;
  fclose(fp);
}

void read_expansion_scale_factors()
/* Read simulation snapshot expansion scale factors from file, assign to array,
 * assign number of outputs.
 */
{
  int ngot, o;
  float temp;
  char buf[BUFSIZ];
  char fname[128];
  FILE *fp;

  sprintf(fname, "%s/%s/%s", Simulation_Directory, DirOutput, FileAOut);
  if ((fp = fopen(fname, "r")) == NULL) {
    printf("! cannot open %s\n", fname);
    exit(1);
  }
  /* skip past headers in input file */
  buf[0] = '#';
  while (buf[0] == '#' && feof(fp) == 0)
    fgets(buf, BUFSIZ, fp);

  nout = 0;
  while (feof(fp) == 0) {
    ngot = sscanf(buf, "%f", &temp);
    if (ngot != 1) {
      printf("! Error reading line %d of %s:\n", nout, FileAOut);
      printf("  %s\n", buf);
      exit(1);
    }
    if (temp >= 0 && temp <= 1) {
      aexps[nout] = temp;
      nout++;
    }
    if (nout >= Snapshot_Num_Max) {
      printf("! Snapshot_Num_Max = %d exceeded\n", Snapshot_Num_Max);
      exit(1);
    }
    fgets(buf, BUFSIZ, fp);
  }
  fclose(fp);

  /* Sort aexps in descending order */
  qsort(aexps, nout, sizeof(float), fcompare);

  /* Assign simulation times */
  for (o = 0; o < nout; o++) {
    redshifts[o] = (1. - aexps[o]) / aexps[o];
    times[o] = get_age(aexps[o]);
    hubble_times[o] = 1. / hubble * THubble
        / sqrt(
            Omega_M0 / aexps[o] / aexps[o] / aexps[o]
                + (1.0 - Omega_M0 - Omega_L0) / aexps[o] / aexps[o] + Omega_L0);
  }
  for (o = o - 2; o >= 0; --o)
    time_wids[o] = times[o] - times[o + 1];
}

void read_halo_prop()
/* Read in host FoF properties for each output. Assign to global structure. */
{
  int o, e;									// index over # of outputs, subhalo id
  int ngot;									// temp for readin check
  int nobj;									// # of subhalos read in at a given output
  float *fbuf;
  struct FoFGrp *glist;
  char fname[BUFSIZ];
  FILE *fp;

  float crat, concen;
  float concen_crat_spline[Nspl];
  /* pre-compiled values for converting r(M=2/3)/r(M=1/3) to concentration */
  float concens[Nspl] = { 0.5, 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11.,
      12., 13., 14., 15., 16., 17., 18., 19., 20., 21., 22., 23., 24., 25., 30.,
      100., 300. };
  float crats[Nspl] = { 1.513, 1.594, 1.723, 1.827, 1.915, 1.992, 2.061, 2.124,
      2.182, 2.235, 2.286, 2.333, 2.377, 2.420, 2.461, 2.500, 2.537, 2.573,
      2.607, 2.641, 2.672, 2.704, 2.734, 2.763, 2.792, 2.820, 2.949, 4.044,
      5.516 };

  spline(crats, concens, Nspl, 9.e31, 9.e31, concen_crat_spline);

  for (o = 0; o < nout; o++) {
    sprintf(fname, "%s/%s/%s/tpmsph_%.4f.fofp", Simulation_Directory, DirOutput, DirHaloProp,
        aexps[o]);
    if ((fp = fopen(fname, "r")) == NULL) {
      printf("! Error opening %s\n", fname);
      exit(1);
    }

    /* read number of objects */
    ngot = fread(&nobj, sizeof(int), 1, fp);
    if (ngot != 1) {
      printf("! ngot = %d in %s\n", ngot, fname);
      exit(1);
    }

    halo_num[o] = nobj;
    glist = G[o] = mycalloc(nobj, sizeof(struct FoFGrp));
    halo_mmin_num[o] = 1;
    hid[o] = mycalloc(nobj, sizeof(int));
    fbuf = mycalloc(nobj, sizeof(float));

    /* read FoF mass */
    ngot = fread(fbuf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    for (e = 1; e < nobj; e++)
      if (fbuf[e] > Halo_Mmin) {
        glist[halo_mmin_num[o]].m = fbuf[e];
        hid[o][e] = halo_mmin_num[o];
        halo_mmin_num[o] += 1;
      }

    if (Halo_Mmin <= 0)
      if (halo_mmin_num[o] != halo_num[o])
        printf("! halo_mmin_num[%d]=%d, halo_num[%d]=%d\n", o, halo_mmin_num[o], o, halo_num[o]);

    /* read velocity dispersion */
    ngot = fread(fbuf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      if (hid[o][e] > 0)
        glist[hid[o][e]].vs = fbuf[e];

    /* read circular velocity */
    ngot = fread(fbuf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      if (hid[o][e] > 0)
        glist[hid[o][e]].vc = fbuf[e];

    /* skip past minid (most bound particle id) */
    if (fseek(fp, nobj * sizeof(int), SEEK_CUR) != 0) {
      perror("fseek");
      exit(1);
    }

    /* read potential energy */
    ngot = fread(fbuf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    //for (e=0; e < nobj; e++) if (hid[o][e] > 0) glist[hid[o][e]].vp = fbuf[e];

    free(fbuf);
    fbuf = mycalloc(3 * nobj, sizeof(float));

    /* read position */
    ngot = fread(fbuf, sizeof(float), 3 * nobj, fp);
    if (ngot != 3 * nobj) {
      printf("! Error reading positions from %s\n", fname);
      printf("  Got %d items, expecting 3*%d = %d\n", ngot, nobj, 3 * nobj);
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      if (hid[o][e] > 0)
        for (ngot = 0; ngot < 3; ngot++)
          glist[hid[o][e]].pos[ngot] = fbuf[3 * e + ngot];

    free(fbuf);
    fbuf = mycalloc(nobj, sizeof(float));

    /* read virial mass */
    ngot = fread(fbuf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      if (hid[o][e] > 0)
        glist[hid[o][e]].m200 = fbuf[e];

    /* read concentration */
    ngot = fread(fbuf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      if (hid[o][e] > 0)
        glist[hid[o][e]].c200 = fbuf[e];

    free(fbuf);
    fbuf = mycalloc(3 * nobj, sizeof(float));

    /* read velocity */
    ngot = fread(fbuf, sizeof(float), 3 * nobj, fp);
    if (ngot != 3 * nobj) {
      printf("! Error reading velocities from %s\n", fname);
      printf("  Got %d items, expecting 3*%d = %d\n", ngot, nobj, 3 * nobj);
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      if (hid[o][e] > 0)
        for (ngot = 0; ngot < 3; ngot++)
          glist[hid[o][e]].vel[ngot] = fbuf[3 * e + ngot];

    free(fbuf);

    /* read r(M=2/3)/r(M=1/3) ratio, convert to FoF concentration */
    fbuf = mycalloc(nobj, sizeof(float));
    ngot = fread(fbuf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      if (hid[o][e] > 0) {
        crat = fbuf[e];
        if (crat < 1.52)
          crat = 1.52;
        else if (crat > 5.5)
          crat = 5.5;
        splint(crats, concens, concen_crat_spline, Nspl, crat, &concen);
        G[o][e].cfof = concen;
      }
    free(fbuf);
    fclose(fp);
  }
}

void read_halo_child()
/* Read in halo child ids for each output.
 Assign child ids, parent ids in descending order by FoF mass. */
{
  int o, e, i;					// index over # of outputs, halo id
  int ngot;							// temp for readin check
  int nobj;							// temp for number of halos read in at each output
  int next, nold;
  int nchildren;
  int *echild;
  char fname[BUFSIZ];
  FILE *fp;

  for (o = 0; o < nout; o++)
    for (e = 1; e < halo_num[o]; e++)
      G[o][e].echi = G[o][e].epar = G[o][e].eparn = -1;

  for (o = 1; o < nout; o++) {
    sprintf(fname, "%s/%s/%s/tpmsph_%.4f.child", Simulation_Directory, DirOutput,
        DirHaloChild, aexps[o]);
    if ((fp = fopen(fname, "r")) == NULL) {
      printf("! Error opening %s\n", fname);
      exit(1);
    }

    ngot = fread(&nobj, sizeof(int), 1, fp);
    if (ngot != 1) {
      printf("! ngot = %d in %s\n", ngot, fname);
      exit(1);
    }
    if (nobj != halo_num[o]) {
      printf("! nsub mismatch in file %s\n", fname);
      printf("  read ns = %d, expecting halo_num[%d] = %d\n", nobj, o, halo_num[o]);
      exit(1);
    }

    echild = mycalloc(nobj, sizeof(int));
    ngot = fread(&nchildren, sizeof(int), 1, fp);
    ngot = fread(echild, sizeof(int), nobj, fp);
    if (ngot != nobj) {
      printf("! ngot = %d != nobj = %d at o = %d in read_halo_child()\n", ngot,
          nobj, o);
      exit(1);
    }

    for (e = 0; e < nobj; e++) {
      /* assign halo child */
      if (hid[o][e] > 0) {
        i = hid[o][e];
        if (echild[e] > 0)
          if (hid[o - 1][echild[e]] > 0)
            G[o][i].echi = hid[o - 1][echild[e]];
        /* assign halo parents, sorted by decreasing mass */
        if (G[o][i].echi > 0) {
          if (G[o - 1][G[o][i].echi].epar <= 0) {
            G[o - 1][G[o][i].echi].epar = i;
          } else {
            next = G[o - 1][G[o][i].echi].epar;
            if (G[o][i].m > G[o][next].m) {
              G[o - 1][G[o][i].echi].epar = i;
              G[o][i].eparn = next;
            } else if (G[o][next].eparn <= 0) {
              G[o][next].eparn = i;
            } else {
              nold = next;
              next = G[o][next].eparn;
              while (1) {
                if (G[o][i].m > G[o][next].m) {
                  G[o][nold].eparn = i;
                  G[o][i].eparn = next;
                  break;
                } else if (G[o][next].eparn <= 0) {
                  G[o][next].eparn = i;
                  break;
                } else {
                  nold = next;
                  next = G[o][next].eparn;
                }
              }
            }
          }
        }
      }
    }
    fclose(fp);
    free(echild);
  }
}

void read_subhalo_prop()
/* Read subhalo properties at all outputs. Assign # of subhalos nsub. */
{
  int o, e, ngot, nobj, nobjextra;
  int *pid;
  float *buf;
  struct SubGrp *Sin;
  char fname[BUFSIZ];
  FILE *fp;

  for (o = 0; o < nout; o++) {
    sprintf(fname, "%s/%s/%s/tpmsph_%.4f.fofp", Simulation_Directory, DirOutput, DirSubProp,
        aexps[o]);
    if ((fp = fopen(fname, "r")) == NULL) {
      printf("! cannot open %s\n", fname);
      exit(1);
    }

    ngot = fread(&nobj, sizeof(int), 1, fp);
    if (ngot != 1) {
      perror("fread");
      exit(1);
    }

    nobjextra = Subhalo_Virtual_Factor * nobj;
    Sin = mycalloc(nobjextra, sizeof(struct SubGrp));

    buf = mycalloc(nobj, sizeof(float));

    /* read bound mass */
    ngot = fread(buf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      Sin[e].m = buf[e];

    /* read velocity dispersion */
    ngot = fread(buf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    //for (e=0; e < nobj; e++) Sin[e].vs = buf[e];

    /* read circular velocity */
    ngot = fread(buf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      Sin[e].vc = buf[e];

    free(buf);
    pid = mycalloc(nobj, sizeof(int));

    /* read min particle id */
    ngot = fread(pid, sizeof(int), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    //for (e=0; e < nobj; e++) Sin[e].minid = pid[e];

    free(pid);
    buf = mycalloc(nobj, sizeof(float));

    /* read potential energy velocity */
    ngot = fread(buf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    //for (e=0; e < nobj; e++) Sin[e].vp = buf[e];

    free(buf);
    buf = mycalloc(3 * nobj, sizeof(float));

    /* read positions */
    ngot = fread(buf, sizeof(float), DIMEN_NUM * nobj, fp);
    if (ngot != DIMEN_NUM * nobj) {
      printf("! Error reading positions from %s\n", fname);
      printf("  Got %d items, expecting 3*%d = %d\n", ngot, nobj, 3 * nobj);
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      for (ngot = 0; ngot < DIMEN_NUM; ngot++)
        Sin[e].pos[ngot] = buf[DIMEN_NUM * e + ngot];

    free(buf);
    buf = mycalloc(nobj, sizeof(float));

    /* read virial mass */
    ngot = fread(buf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    //for (e=0; e < nobj; e++) Sin[e].m200 = buf[e];

    /* read virial concentration */
    ngot = fread(buf, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    //for (e=0; e < nobj; e++) Sin[e].c200 = buf[e];

    free(buf);
    buf = mycalloc(DIMEN_NUM * nobj, sizeof(float));

    /* read velocities */
    ngot = fread(buf, sizeof(float), DIMEN_NUM * nobj, fp);
    if (ngot != DIMEN_NUM * nobj) {
      printf("! Error reading velocities from %s\n", fname);
      printf("  Got %d items, expecting 3*%d = %d\n", ngot, nobj, 3 * nobj);
      exit(1);
    }
    for (e = 0; e < nobj; e++)
      for (ngot = 0; ngot < DIMEN_NUM; ngot++)
        Sin[e].vel[ngot] = buf[DIMEN_NUM * e + ngot];

    free(buf);
    fclose(fp);

    S[o] = Sin;
    subhalo_num[o] = nobj;
  }
}

void read_subhalo_child()
/* Read & assign children of subhalos. Use to assign subhalo parents. */
{
  int o, e, otemp;			// index over # of outputs, subhalo id
  int ngot;							// temp for readin check
  int nobj;							// temp for number of subhalos read in at each output
  int c, t;
  int *Ltail, *Ttail;
  long indx, totgrp, *ncum;
  int *echilds;
  float *achilds;
  struct SubGrp *St;
  char fname[BUFSIZ];
  FILE *fp;

  for (o = 0; o < nout; o++)
    for (e = 1; e < subhalo_num[o]; e++)
      S[o][e].epar = S[o][e].upar = S[o][e].eparn = S[o][e].uparn = -1;

  /* Calculate cumulative group counts */
  ncum = mycalloc(nout, sizeof(long));
  for (totgrp = subhalo_num[0], ncum[0] = 0, t = 1; t < nout; t++) {
    ncum[t] = ncum[t - 1] + subhalo_num[t - 1];
    totgrp += subhalo_num[t];
  }

  /* Set up auxiliary arrays for linked list construction */
  Ltail = mycalloc(totgrp, sizeof(int));
  Ttail = mycalloc(totgrp, sizeof(int));

  for (o = Snapshot_Index_Min; o < nout; o++) {
    /* Load child id's & outputs */
    sprintf(fname, "%s/%s/%s/tpmsph_%.4f.child", Simulation_Directory, DirOutput, DirSubChild,
        aexps[o]);
    if ((fp = fopen(fname, "r")) == NULL) {
      printf("! Error opening %s\n", fname);
      exit(1);
    }

    ngot = fread(&nobj, sizeof(int), 1, fp);
    if (ngot != 1) {
      printf("! ngot = %d in %s\n", ngot, fname);
      exit(1);
    }
    if (nobj != subhalo_num[o]) {
      printf("! Nsub mismatch in %s\n", fname);
      printf("  Read ns = %d, expecting subhalo_num[%d] = %d\n", nobj, o, subhalo_num[o]);
      exit(1);
    }

    echilds = mycalloc(nobj, sizeof(int));
    achilds = mycalloc(nobj, sizeof(float));

    ngot = fread(echilds, sizeof(int), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    ngot = fread(achilds, sizeof(float), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }

    for (e = 1; e < nobj; e++) {
      S[o][e].echi = echilds[e];
      if (S[o][e].echi < 0)
        S[o][e].uchi = -1;
      else {
        for (otemp = 0; otemp < nout; otemp++) {
          if (aexps[otemp] == achilds[e]) {
            S[o][e].uchi = otemp;
            break;
          }
        }
      }
    }
    fclose(fp);

    /* assign parents */
    for (e = 1; e < subhalo_num[o]; e++) {
      if ((c = echilds[e]) > 0) {
        /* find the time this child refers to */
        t = o - 1;
        while (t > 0 && fabs(aexps[t] - achilds[e]) > 0.01 * aexps[t])
          t--;
        if (fabs(aexps[t] - achilds[e]) > 0.01 * aexps[t]) {
          printf("! Unable to find aout = %.4f for parent (%d,%d)\n",
              achilds[e], o, e);
          exit(1);
        } else
          St = S[t];
        if (c >= subhalo_num[t] || c < 0) {
          printf("! c = %d at %d out of range [0,%d)\n", c, t, subhalo_num[t]);
          exit(1);
        }
        indx = ncum[t] + c;
        if (St[c].epar < 0) {
          /* child has no previously assigned parents */
          St[c].epar = e;
          St[c].upar = o;
          Ltail[indx] = e;
          Ttail[indx] = o;
        } else {
          St = S[Ttail[indx]];
          St[Ltail[indx]].eparn = e;
          St[Ltail[indx]].uparn = o;
          Ltail[indx] = e;
          Ttail[indx] = o;
        }
      }
    }
    free(echilds);
    free(achilds);
  }

  /* initialize subhalos at last output(s) */
  for (o = 0; o < Snapshot_Index_Min; o++)
    for (e = 1; e < subhalo_num[o]; e++)
      S[o][e].echi = S[o][e].och.uchi -1;

  free(Ttail);
  free(Ltail);
  free(ncum);
}

void read_subhalos_halo_id()
/* Read in subhalos' halo ids. Assign halo id & FoF mass to its subhalos. */
{
  int o, e;							// index over # of outputs, subhalo id
  int ngot;							// temp for readin check
  int nobj;							// # of subhalos read in at a given output
  int *ehalos;					// temp array to store halo ids
  char fname[BUFSIZ];
  FILE *fp;

  for (o = 0; o < nout; o++) {
    sprintf(fname, "%s/%s/%s/tpmsph_%.4f.par", Simulation_Directory, DirOutput, DirSubHost,
        aexps[o]);
    if ((fp = fopen(fname, "r")) == NULL) {
      printf("! Error opening %s\n", fname);
      exit(1);
    }

    ngot = fread(&nobj, sizeof(int), 1, fp);
    if (ngot != 1) {
      printf("! ngot = %d in %s\n", ngot, fname);
      exit(1);
    }
    if (nobj != subhalo_num[o]) {
      printf("! Nsub mismatch in %s\n", fname);
      printf("  Read ns = %d, expecting subhalo_num[%d] = %d\n", nobj, o, subhalo_num[o]);
      exit(1);
    }

    ehalos = mycalloc(nobj, sizeof(int));
    ngot = fread(ehalos, sizeof(int), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }

    for (e = 1; e < subhalo_num[o]; e++) {
      if (ehalos[e] >= halo_num[o] || ehalos[e] < -1) {
        printf("! Halo id out of bounds in %s\n", fname);
        printf("  Read in %d but halo_num[%d] = %d\n", ehalos[e], o, halo_num[o]);
      }
      S[o][e].ehalo = ehalos[e];
      if (S[o][e].ehalo > 0)
        S[o][e].mhalo = G[o][ehalos[e]].m;
    }
    free(ehalos);
    fclose(fp);
  }
}

void read_halos_central_id()
/* Read in halos' central subhalo ids. Assign central subhalo id to halo. */
{
  int o, e;							// index over # of outputs, halo ids
  int ngot;							// temp for readin check
  int nobj;							// temp for # of subhalos read in at each output
  int *ecens;
  char fname[BUFSIZ];
  FILE *fp;

  for (o = 0; o < nout; o++) {
    sprintf(fname, "%s/%s/%s/tpmsph_%.4f.cen", Simulation_Directory, DirOutput, DirHaloCen,
        aexps[o]);
    if ((fp = fopen(fname, "r")) == NULL) {
      printf("! Error opening %s\n", fname);
      exit(1);
    }

    /* read number of objects */
    ngot = fread(&nobj, sizeof(int), 1, fp);
    if (ngot != 1) {
      printf("! ngot = %d in %s\n", ngot, fname);
      exit(1);
    }
    if (nobj != halo_num[o]) {
      printf("! Nhalo mismatch in %s\n", fname);
      printf("  Read ns = %d expecting halo_num[%d] = %d\n", nobj, o, halo_num[o]);
      exit(1);
    }

    ecens = mycalloc(nobj, sizeof(int));

    ngot = fread(ecens, sizeof(int), nobj, fp);
    if (ngot != nobj) {
      perror("fread");
      exit(1);
    }
    for (e = 1; e < nobj; e++)
      G[o][e].ecen = ecens[e];

    fclose(fp);
    free(ecens);
  }
}

/** PROCESSING ****************************************************************/
int get_subhalo_ilk(int o, int e)
/* Get subhalo ilk:
 * 1 = normal central
 * 2 = virtual central
 * 0 = normal satellite
 * -1 = virtual satellite (with halo & central)
 * -2 = virtual satellite (with halo, no central)
 * -3 = virtual satellite (no halo, no central)
 *
 * Import snapshot index & subhalo index.
 */
{
  /* deal with virtual subhalos, depending on if orphan */
  if (S[o][e].ilk < 0) {
    if (S[o][e].ehalo <= 0)
      return -3;
    else if (G[o][S[o][e].ehalo].ecen <= 0)
      return -2;
    else if (G[o][S[o][e].ehalo].ecen == e)
      return 2;
    else
      return -1;
    /* deal with normal subhalos */
  } else if (S[o][e].ehalo <= 0)
    printf("! (%d,%d) is normal subhalo without halo\n", o, e);
  else if (G[o][S[o][e].ehalo].ecen <= 0)
    return 0;
  else if (G[o][S[o][e].ehalo].ecen == e)
    return 1;
  else
    return 0;

  return 0;
}


float get_distance_to_central(int zi, int si)
/* Get (comoving) distance between satellite & its central.
 *
 * Import snapshot index & subhalo index.
 */
{
  int dimi;					// index over number of spatial dimensions
  int cen_i;
  float dist_cen = 0.;		// distance between satellite & central
  float dist_temp;

  cen_i = S[zi][si].ecen;
  if (cen_i > 0) {
    for (dimi = 0; dimi < 3; dimi++) {
      dist_temp = diff_periodic(S[o][e].pos[idim] - S[o][ecen].pos[idim]);
      dist_cen += dist_temp * dist_temp;
    }
    dist_cen = sqrt(dist_cen);
  } else
    dist_cen = -1; 			// if virtual subhalo, might not have a central

  return dist_cen;
}


void get_main_parent(int kind, int o, int e, int *parent)
 * Import kind ('s','h'), output, subhalo id, & 2-D parent arrays (to change).
 * Point to highest Mmax parent output & id; if no parent, point to -1.
 * Require Mmax assignment, NOT assume ordered parents.
 */
{
  int op, ep, optemp;
  float mmax = -1;

  parent[0] = parent[1] = -1;
  if (kind == 's') {
    if (S[o][e].epar <= 0)
      return;
    ep = S[o][e].epar;
    op = S[o][e].upar;
    while (ep > 0) {
      if (S[op][ep].mmax > mmax) {
        parent[0] = op;
        parent[1] = ep;
        mmax = S[op][ep].mmax;
      }
      optemp = S[op][ep].uparn;
      ep = S[op][ep].eparn;
      op = optemp;
    }
  } else if (kind == 'h') {
    if (G[o][e].epar <= 0)
      return;
    ep = G[o][e].epar;
    op = o + 1;
    while (ep > 0) {
      if (G[op][ep].m > mmax) {
        parent[0] = op;
        parent[1] = ep;
        mmax = G[op][ep].m;
      }
      ep = G[op][ep].eparn;
    }
  }
}

float get_parents(int kind, int o, int e, int *par1, int *par2)
 * Import kind ('s','h'), output, id, & two 2-D parent arrays to change.
 * Get two parents with largest M[inf], return their M[inf] ratio.
 * If none, return -1. Point to parents' outputs & ids in 2-D array (highest
 * M[inf] parent first). Require Mmax assignment, NOT assume ordered parents.
 */
{
  int op, ep, optemp;
  float mtemp = 0;
  float mrat = -1;
  float m1 = -1;      // temp m of highest m parent
  float m2 = -1;      // temp m of 2nd highest m parent

  par1[0] = par1[1] = -1;
  par2[0] = par2[1] = -1;

  if (kind == 's') {
    if (S[o][e].epar <= 0)
      return -1;
    ep = S[o][e].epar;
    op = S[o][e].upar;
    while (ep > 0) {
      mtemp = S[op][ep].mmax;
      if (mtemp > m1) {
        m2 = m1;
        m1 = mtemp;
        par2[0] = par1[0];
        par1[0] = op;
        par2[1] = par1[1];
        par1[1] = ep;
      } else if (mtemp > m2) {
        m2 = mtemp;
        par2[0] = op;
        par2[1] = ep;
      }
      optemp = S[op][ep].uparn;
      ep = S[op][ep].eparn;
      op = optemp;
    }
  } else if (kind == 'h') {
    if (G[o][e].epar <= 0)
      return -1;
    ep = G[o][e].epar;
    op = o + 1;
    while (ep > 0) {
      mtemp = G[op][ep].m;
      if (mtemp > m1) {
        m2 = m1;
        m1 = mtemp;
        par2[0] = par1[0];
        par1[0] = op;
        par2[1] = par1[1];
        par1[1] = ep;
      } else if (mtemp > m2) {
        m2 = mtemp;
        par2[0] = op;
        par2[1] = ep;
      }
      ep = G[op][ep].eparn;
    }
  }
  if (m1 > 0 && m2 > 0)
    mrat = m2 / m1;

  return mrat;
}

int get_same_halo_parent(int o, int e)
/* Import output, subhalo id. Find if subhalo's main parent is in its halo's
parent: if yes return 1, if no return 0. */
{
  int ehalo, ehalzpar, op;
  int samehalo = 0;
  int *parent;
  parent = mycalloc(2, sizeof(int));

  get_main_parent('s', o, e, parent);
  if (parent[0] >= 0 && parent[1] > 0) {
    ehalzpar = S[parent[0]][parent[1]].ehalo;
    ehalo = S[o][e].ehalo;
    if (ehalzpar > 0 && ehalo > 0) {
      for (op = o; op < parent[0]; op++) {
        ehalo = G[op][ehalo].epar;
        if (ehalo == -1)
          break;
      }
      if (ehalzpar == ehalo)
        samehalo = 1;
    }
  }
  free(parent);
  return samehalo;
}

int get_same_halo(int uchild, int echild, int upar, int epar)
/* Import child output, child subhalo id, parent output, parent subhalo id.
Find if child's parent is in child's halo's parent: if yes return 1, if no return 0. */
{
  int ehalo, ehalzpar, o;
  int samehalo = 0;

  ehalzpar = S[upar][epar].ehalo;
  ehalo = S[uchild][echild].ehalo;
  if (ehalzpar > 0 && ehalo > 0) {
    for (o = uchild; o < upar; o++) {
      ehalo = G[o][ehalo].epar;
      if (ehalo == -1)
        break;
    }
    if (ehalzpar == ehalo)
      samehalo = 1;
  }
  return samehalo;
}

void get_uinf(int o, int e, int *parent, int *partemp)
/* Import output, index, parent array to change, temp parent array.
Point parent array to output & id when subhalo's main parent was not in same
halo (current halo's parent).
If never fell in, point to -1's.
NOT require subhalo was central before falling in,
require Mmax assignment, NOT assume parents sorted by Mmax. */
{
  int op, ep;

  parent[0] = parent[1] = -1;
  op = o;
  ep = e;
  if (S[op][ep].mmax <= 0)
    return;
  if (S[op][ep].ilk >= 1)
    return;
  while (op < nout && ep > 0) {
    get_main_parent('s', op, ep, partemp);
    if (get_same_halo_parent(op, ep) == 0) {
      /* check that parent exists */
      if (partemp[0] > 0 && partemp[1] > 0) {
        parent[0] = partemp[0];
        parent[1] = partemp[1];
        break;
      }
    }
    op = partemp[0];
    ep = partemp[1];
  }
}

void get_when_ilk(int ilk, int o, int e, int *parent, int *partemp)
/* Import ilk (sat=0, cen=1), output, subhalo id, 2-D parent array to change,
temp parent array. Point parent array to output & id when last was ilk (can be self).
If never, return -1. Require ilk assignment, NOT assume parents sorted by Mmax. */
{
  int op, ep;

  parent[0] = parent[1] = -1;
  op = o;
  ep = e;
  while (op < nout && ep > 0) {
    if (ilk == 1) {
      if (S[op][ep].ilk >= 1) {
        parent[0] = op;
        parent[1] = ep;
        break;
      }
    } else if (ilk == 0) {
      if (S[op][ep].ilk <= 0) {
        parent[0] = op;
        parent[1] = ep;
        break;
      }
    }
    get_main_parent('s', op, ep, partemp);
    op = partemp[0];
    ep = partemp[1];
  }
}

float get_mfrac_min(int o, int e, int *partemp)
/* Import output, id, temp parent array. Walk back history to find minimum
Mnow/Mmax. If am central, return current Mnow/Mmax.
Require ilk, Mmax assignment, NOT assume parents sorted by Mmax. */
{
  float mfracmin = 1.;
  float mfrac;
  int op, ep;

  op = o;
  ep = e;
  if (S[op][ep].mmax <= 0)
    return 0;
  /* useful to have central mfrac for ejected satellites */
  if (S[op][ep].ilk >= 1)
#ifdef SUBFIND
    return S[o][e].m / S[o][e].mmax;
#else
    return S[o][e].mhalo / S[o][e].mmax;
#endif
  while (op < nout && ep > 0) {
    if (S[op][ep].ilk >= 1)
      break;
    mfrac = S[op][ep].m / S[op][ep].mmax;
    if (mfrac < mfracmin)
      mfracmin = mfrac;
    get_main_parent('s', op, ep, partemp);
    op = partemp[0];
    ep = partemp[1];
  }
  return mfracmin;
}


/* ASSIGNMENTS ************************************************************************************/
void assign_halo_formation_prop()
/* Assign halo formation/merger properties. */
{
  int o, e;													// index over output / id
  int *par1, *par2;
  par1 = mycalloc(2, sizeof(int));
  par2 = mycalloc(2, sizeof(int));

  for (o = 0; o < nout; o++) {
    for (e = 1; e < halo_mmin_num[o]; e++) {
      G[o][e].mrat = get_parents('h', o, e, par1, par2);
    }
  }
  free(par1);
  free(par2);
}

void fix_subhalo_tracking_switch()
/* Fix tracking bug (during switch) where a satellite & its 'child' central at
next output are both assigned as parents to satellite in following output. */
{
  int o, e, op, ep, opnext, epnext;
  int ocen, ecen, osat, esat;
  int nfixed = 0;

  for (o = 0; o < nout - 2; o++) {
    for (e = 1; e < subhalo_num[o]; e++) {
      if (get_subhalo_ilk(o, e) <= 0 && S[o][e].epar > 0) {
        ocen = ecen = osat = esat = -1;
        op = S[o][e].upar;
        ep = S[o][e].epar;
        while (op < nout && ep > 0) {
          opnext = S[op][ep].uparn;
          epnext = S[op][ep].eparn;
          if (get_subhalo_ilk(op, ep) >= 1 && op == (o + 1) && S[op][ep].upar < 0) {
            ocen = op;
            ecen = ep;
          } else if (get_subhalo_ilk(op, ep) <= 0 && op > (o + 1)) {
            osat = op;
            esat = ep;
          }
          ep = epnext;
          op = opnext;
        }
        /* if find buggy parents, re-do parent & child assignments */
        if (ecen > 0 && esat > 0) {
          nfixed += 1;
          //printf("! found tracking bug: [%d][%d]\n",o,n);
          /* re-do parent & child assignments of parents */
          S[osat][esat].uchi = ocen;
          S[osat][esat].echi = ecen;
          S[ocen][ecen].upar = osat;
          S[ocen][ecen].epar = esat;

          /* re-do parent linked list */
          op = S[o][e].upar;
          ep = S[o][e].epar;
          if (op == osat && ep == esat) {
            S[o][e].upar = S[osat][esat].uparn;
            S[o][e].epar = S[osat][esat].eparn;
          } else {
            opnext = S[op][ep].uparn;
            epnext = S[op][ep].eparn;
            while (opnext < nout && epnext > 0) {
              if (opnext == osat && epnext == esat) {
                S[op][ep].uparn = S[osat][esat].uparn;
                S[op][ep].eparn = S[osat][esat].eparn;
                break;
              }
              op = opnext;
              ep = epnext;
              opnext = S[op][ep].uparn;
              epnext = S[op][ep].eparn;
            }
          }

          /* satellite parent is isolated (central is only descendant) */
          S[osat][esat].uparn = -1;
          S[osat][esat].eparn = -1;
        }
      }
    }
  }
  printf("  subs fixed in switches: %d\n", nfixed);
}

void fix_subhalo_tracking_fof6d()
/* Fix FoF6D tracking bug where subhalo's most bound particles get split into
one or more Mmax=0 satellites, and its child skips an output. If gets split
into two or more Mmax = 0 satellites, randomly pick one to use for fix. */
{
  int o, e, op, ep, optemp;
  int ocen, ecen, osat, esat;
  int nfixed = 0;

  for (o = 0; o < nout - 2; o++) {
    for (e = 1; e < subhalo_num[o]; e++) {
      if (S[o][e].epar > 0) {
        ocen = ecen = osat = esat = -1;
        op = S[o][e].upar;
        ep = S[o][e].epar;
        while (op < nout && ep > 0) {
          /* find if has Mmax = 0 satellite parent */
          if (get_subhalo_ilk(op, ep) <= 0 && op == (o + 1) && S[op][ep].epar <= 0) {
            osat = op;
            esat = ep;
            /* find if has central parent which skips to output */
          } else if (get_subhalo_ilk(op, ep) >= 1 && op > (o + 1)) {
            ocen = op;
            ecen = ep;
          }
          optemp = S[op][ep].uparn;
          ep = S[op][ep].eparn;
          op = optemp;
        }
        /* if find buggy parents, re-do parent & child assignments */
        if (ecen > 0 && esat > 0) {
          S[ocen][ecen].uchi = osat;
          S[ocen][ecen].echi = esat;
          nfixed += 1;
        }
      }
    }
  }
  printf("  subs fixed in fof6d: %d\n", nfixed);
}

void fill_in_skipped_subhalos()
/* Find subhalos whose child has skipped output. Create new satellite
subhalos to fill in tree, reassign parent linked list. */
{
  int idim, o, e, oc, ec;
  int onew, enew;
  int uchildpar, echildpar;
  int opprevious, epprevious, opnext, epnext;
  int nfilled = 0;
  float dx, dtfrac;

  for (o = nout - 1; o > 1; --o) {
    for (e = 1; e < subhalo_num[o]; e++) {
      oc = S[o][e].uchi;
      ec = S[o][e].echi;
      if (oc >= 0 && oc < o - 1) {
        /* make new subhalo */
        onew = o - 1;
        enew = subhalo_num[onew];
        subhalo_num[onew]++;
        nfilled += 1;
        /* re-assign parent/child pointers */
        S[o][e].uchi = onew;
        S[o][e].echi = enew;
        S[onew][enew].upar = o;
        S[onew][enew].epar = e;
        S[onew][enew].uchi = oc;
        S[onew][enew].echi = ec;
        S[onew][enew].uparn = S[o][e].uparn;
        S[onew][enew].eparn = S[o][e].eparn;
        S[o][e].uparn = -1;
        S[o][e].eparn = -1;
        uchildpar = S[oc][ec].upar;
        echildpar = S[oc][ec].epar;
        /* child's 'first' parent is new subhalo's parent, insert new subhalo */
        if (uchildpar == o && echildpar == e) {
          S[oc][ec].upar = onew;
          S[oc][ec].epar = enew;
          /* new subhalo's parent is in middle of linked list */
        } else {
          opnext = S[uchildpar][echildpar].uparn;
          epnext = S[uchildpar][echildpar].eparn;
          opprevious = uchildpar;
          epprevious = echildpar;
          while (opnext < nout && epnext > 0) {
            if (opnext == o && epnext == e) {
              S[opprevious][epprevious].uparn = onew;
              S[opprevious][epprevious].eparn = enew;
              break;
            }
            opprevious = opnext;
            epprevious = epnext;
            opnext = S[opprevious][epprevious].uparn;
            epnext = S[opprevious][epprevious].eparn;
          }
        }
        /* assign new subhalo properties */
        dtfrac = (times[onew] - times[o]) / (times[oc] - times[o]);
        for (idim = 0; idim < DIMEN_NUM; idim++) {
          dx = diff_periodic(S[oc][ec].pos[idim] - S[o][e].pos[idim]);
          S[onew][enew].pos[idim] = periodic(S[o][e].pos[idim] + dx * dtfrac);
          dx = S[oc][ec].vel[idim] - S[o][e].vel[idim];
          S[onew][enew].vel[idim] = S[o][e].vel[idim] + dx * dtfrac;
        }
        S[onew][enew].m = S[o][e].m;
        S[onew][enew].vc = S[o][e].vc;
        //S[onew][enew].vs = S[o][e].vs;
        S[onew][enew].ilk = -1;
        /* make sure parent's halo index exists (parent might be virtual too) */
        if (S[o][e].ehalo > 0) {
          S[onew][enew].ehalo = G[o][S[o][e].ehalo].echi;
          /* make sure parent's halo's child exists */
          if (S[onew][enew].ehalo > 0) {
            S[onew][enew].mhalo = G[onew][S[onew][enew].ehalo].m;
          } else {
            S[onew][enew].mhalo = -1;
            S[onew][enew].ilk = -3;
          }
        } else {
          S[onew][enew].ehalo = -1;
          S[onew][enew].mhalo = -1;
          S[onew][enew].ilk = -3;
        }
      }
    }
  }
  printf("  subs filled-in: %d\n", nfilled);
}

void assign_subhalo_central()
/* Assign *history-dependent* subhalo properties: ilk, ecen, distcen. */
{
  int o, e, oc, ec;								// index over output / id
  int ehalo;
  int nswapped = 0;
#ifdef SUBFIND
  int ecenold;
  float mtemp;
#endif

  /* work forward in time */
  for (o = nout - 1; o >= 0; --o) {
    for (e = 1; e < subhalo_num[o]; e++) {

      /* assign current subhalo's central properties */
      S[o][e].ilk = get_subhalo_ilk(o, e);
      if (S[o][e].ehalo <= 0) {
        S[o][e].ecen = -1;
        S[o][e].distcen = -1;
      } else {
        S[o][e].ecen = G[o][S[o][e].ehalo].ecen;
        S[o][e].distcen = get_distance_to_central(o, e);
      }

      /* adjust child centrals if need be, so fixed for next output loop:
       assign child as central if I am central in same halo */
      if (S[o][e].ilk >= 1) {
        /* subhalo is assigned as central */
        oc = S[o][e].uchi;
        ec = S[o][e].echi;
        if (oc >= 0 && ec > 0) {
          if (get_same_halo(oc, ec, o, e) == 1) {
            /* child of central is in same halo, assign it as central */
            if (get_subhalo_ilk(oc, ec) == 0 || get_subhalo_ilk(oc, ec) == -1) {
              /* make sure halo & central exist */
              ehalo = S[oc][ec].ehalo;
              G[oc][ehalo].ecen = ec;
#ifdef SUBFIND
              /* switch masses in SUBFIND */
              ecenold = G[oc][ehalo].ecen;
              mtemp = S[oc][ec].m;
              S[oc][ec].m = S[oc][ecenold].m;
              S[oc][ecenold].m = mtemp;
#endif
              nswapped += 1;
            }
          }
        }
      }

    }
  }
  printf("  centrals swapped: %d\n", nswapped);
}

void assign_subhalo_maximum_m()
/* Assign subhalo Mmax & Vcmax.
Require ilk assignment.
Assume once central, always central unless go into different halo. */
{
  int o, e, oc, ec, ehalo;
  float mpar, vcpar;

  /* assign Mmax properties, work forward in time */
  for (o = nout - 1; o >= 0; --o) {
    for (e = 1; e < subhalo_num[o]; e++) {
      oc = S[o][e].oc;
      ec = S[o][e].echi;
      if (S[o][e].ilk >= 1) {
        /* subhalo is central */
        if (S[o][e].mmax == 0) {
          /* new central (no parent), assign Mmax as current [sub]halo mass */
#ifdef SUBFIND
          S[o][e].mmax = S[o][e].m;
          S[o][e].vcmax = S[o][e].vc;
#else
          ehalo = S[o][e].ehalo;
          S[o][e].mmax = G[o][ehalo].m;
          S[o][e].vcmax = G[o][ehalo].vc;
#endif
        }
        if (oc >= 0 && ec > 0) {
          if (S[oc][ec].ilk >= 1) {
            /* child is central - assign Mmax as current [sub]halo mass */
#ifdef SUBFIND
            /* SUBFIND maximum mass/vcirc */
            S[oc][ec].mmax = S[oc][ec].m;
            S[oc][ec].vcmax = S[oc][ec].vc;
#else
            /* FoF6D maximum mass/vcirc */
            ehalo = S[oc][ec].ehalo;
            S[oc][ec].mmax = G[oc][ehalo].m;
            S[oc][ec].vcmax = G[oc][ehalo].vc;
#endif
#ifndef USE_INFALL_MASS
            /* use max history Mmax/vcmax */
            mpar = S[o][e].mmax;
            vcpar = S[o][e].vcmax;
            if (mpar > S[oc][ec].mmax)
              S[oc][ec].mmax = mpar;
            if (vcpar > S[oc][ec].vcmax)
              S[oc][ec].vcmax = vcpar;
#endif
          } else {
            /* child is a satellite - add to its Mmax */
            S[oc][ec].mmax += S[o][e].mmax;
            if (S[o][e].vcmax > S[oc][ec].vcmax)
              S[oc][ec].vcmax = S[o][e].vcmax;
          }
        }
      } else {
        /* subhalo is satellite */
        if (oc >= 0 && ec > 0) {
          if (S[oc][ec].ilk >= 1) {
            /* child is central - could be normal merger or ejected satellite.
             Assign Mmax only if sat parent is more massive (ejected),
             can be overwritten later for normal merger.
             Thus, ejected satellite with retain old Mmax for one output. */
            if (S[o][e].mmax > S[oc][ec].mmax)
              S[oc][ec].mmax = S[o][e].mmax;
            if (S[o][e].vcmax > S[oc][ec].vcmax)
              S[oc][ec].vcmax = S[o][e].vcmax;
          } else {
            /* child is a satellite - add to its Mmax */
            S[oc][ec].mmax += S[o][e].mmax;
            if (S[o][e].vcmax > S[oc][ec].vcmax)
              S[oc][ec].vcmax = S[o][e].vcmax;
          }
        }
      }
    }
  }

}

void assign_subhalo_satellite_list()
/* Assign satellite linked list, sorted in decreasing order by Mmax.
Require Mmax, ilk assignment. */
{
  int o, e;												// index over output / id
  int eold, enext;

  for (o = 0; o < nout; o++) {
    for (e = 1; e < subhalo_num[o]; e++)
      S[o][e].esat = -1;

    /* assign fellow sibling satellite linked list (sorted by Mmax) */
    for (e = 1; e < subhalo_num[o]; e++) {
      if (S[o][e].ilk <= 0) {
        eold = S[o][e].ecen;
        if (eold > 0) {
          enext = S[o][eold].esat;
          if (enext < 0)
            S[o][eold].esat = e;
          else {
            while (1) {
              if (S[o][e].mmax > S[o][enext].mmax) {
                S[o][eold].esat = e;
                S[o][e].esat = enext;
                break;
              } else if (S[o][enext].esat < 0) {
                S[o][enext].esat = e;
                break;
              } else {
                eold = enext;
                enext = S[o][eold].esat;
                if (enext <= 0)
                  printf("! (%d,%d) bad esat\n", o, e);
              }
            }
          }
        }
      }
    }
  }

}

void sort_subhalo_parents()
/* Sort subhalo parents by Mmax. Require Mmax assignment. */
{
  int NParMax = 3000; // max # of parents
  int o, e, op, ep;   // index over output / id
  int npar, i;
  struct SubGrp *grp;
  struct struct_compare *CC;
  CC = mycalloc(NParMax, sizeof(struct struct_compare));

  for (o = 0; o < nout - 2; o++) {
    for (e = 1; e < subhalo_num[o]; e++) {
      op = S[o][e].upar;
      ep = S[o][e].epar;
      if (op < nout && ep > 0) {
        if (S[op][ep].uparn < nout && S[op][ep].eparn > 0) {
          npar = 0;
          while (op < nout && ep > 0) {
            grp = S[op];
            CC[npar].id = ep;
            CC[npar].o = op;
            CC[npar].sortprop = S[op][ep].mmax;
            npar++;
            if (npar >= NParMax) {
              printf("! Too many parents in %d,%d for sort_parents()\n", o, e);
              exit(1);
            }
            op = grp[ep].uparn;
            ep = grp[ep].eparn;
          }

          qsort(CC, npar, sizeof(struct struct_compare), structcomparedecr);
          S[o][e].epar = CC[0].id;
          S[o][e].upar = CC[0].o;
          for (i = 1; i < npar; i++) {
            S[CC[i - 1].o][CC[i - 1].id].eparn = CC[i].id;
            S[CC[i - 1].o][CC[i - 1].id].uparn = CC[i].o;
          }
          S[CC[i - 1].o][CC[i - 1].id].eparn = -1;
          S[CC[i - 1].o][CC[i - 1].id].uparn = -1;
        }
      }

    }
  }
  free(CC);
}

void assign_subhalo_history_prop()
/* Assign subhalo history properties going backward in time.
Require Mmax, ilk assignment, does not assume parents sorted by Mmax. */
{
  int o, e;
  int *parent, *partemp;
  parent = mycalloc(2, sizeof(int));
  partemp = mycalloc(2, sizeof(int));

  for (o = 0; o < nout; o++) {
    for (e = 1; e < subhalo_num[o]; e++) {
      /* if central, find when last was sat, if sat find when last was central */
      if (S[o][e].ilk >= 1)
        get_when_ilk(0, o, e, parent, partemp);
      else
        get_when_ilk(1, o, e, parent, partemp);
      S[o][e].udif = parent[0];
      S[o][e].edif = parent[1];
      get_uinf(o, e, parent, partemp);
      S[o][e].uinf = parent[0];
      S[o][e].einf = parent[1];
      S[o][e].mfracmin = get_mfrac_min(o, e, partemp);
      S[o][e].mrat = get_parents('s', o, e, parent, partemp);
      //S[o][e].mergetype = (o,n);
    }
  }
  free(parent);
  free(partemp);
}

void print_snapshots() {
  int o;

  printf("# lcdm%.0f\n", Box_Length);
  printf("# outputs: %d\n", nout);
  printf("#  a\tz\tt[Gyr]\tdt[Gyr]\ttH[Gyr]\n");
  for (o = 0; o < nout; o++)
    printf("%2d\t%6.4f\t%6.4f\t%7.4f\t%6.4f\t%7.4f\n", o, aexps[o], redshifts[o],
        times[o], time_wids[o], hubble_times[o]);
}

/* OUTPUT *****************************************************************************************/
void write_subhalo_tree()
/* Print subhalo tree to binary files, one per output. */
{
  int o, e, idim, ngot;
  int *ibuf;
  float *fbuf, *fbufndim;
  char fname[128];
  FILE *fp;

  for (o = 0; o < nout; o++) {
    sprintf(fname, "%s/%s/subtree_%d.dat", Simulation_Directory, DirTree, o);
    if ((fp = fopen(fname, "w")) == NULL) {
      printf("! Error opening %s\n", fname);
      exit(1);
    }
    printf("# printing %s\n", fname);

    fbuf = mymalloc(subhalo_num[o] * sizeof(float));
    fbufndim = mymalloc(subhalo_num[o] * DIMEN_NUM * sizeof(float));
    ibuf = mymalloc(subhalo_num[o] * sizeof(int));

    ngot = fwrite(&subhalo_num[o], sizeof(int), 1, fp);
    if (ngot != 1) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++) {
      for (idim = 0; idim < DIMEN_NUM; idim++)
        fbufndim[e + idim] = S[o][e].pos[idim] * Box_Length;
      ngot = fwrite(fbufndim, sizeof(float), DIMEN_NUM * subhalo_num[o], fp);
      if (ngot != DIMEN_NUM * subhalo_num[o]) {
        perror("fwrite");
        exit(1);
      }
    }

    for (e = 0; e < subhalo_num[o]; e++) {
      for (idim = 0; idim < DIMEN_NUM; idim++)
        fbufndim[e + idim] = S[o][e].vel[idim] * Box_Length / hubble_times[o];
      ngot = fwrite(fbufndim, sizeof(float), DIMEN_NUM * subhalo_num[o], fp);
      if (ngot != DIMEN_NUM * subhalo_num[o]) {
        perror("fwrite");
        exit(1);
      }
    }

    for (e = 0; e < subhalo_num[o]; e++) {
      if (S[o][e].m <= 0)
        fbuf[e] = 0;
      else
        fbuf[e] = log10(S[o][e].m / hubble);
    }
    ngot = fwrite(fbuf, sizeof(float), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++) {
      if (S[o][e].vc > 0)
        fbuf[e] = log10(S[o][e].vc);
      else
        fbuf[e] = 0;
    }
    ngot = fwrite(fbuf, sizeof(float), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++) {
      if (S[o][e].mmax <= 0)
        fbuf[e] = 0;
      else
        fbuf[e] = log10(S[o][e].mmax / hubble_0);
    }
    ngot = fwrite(fbuf, sizeof(float), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++) {
      if (S[o][e].vcmax > 0)
        fbuf[e] = log10(S[o][e].vcmax);
      else
        fbuf[e] = 0;
    }
    ngot = fwrite(fbuf, sizeof(float), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++)
      ibuf[e] = S[o][e].ilk;
    ngot = fwrite(ibuf, sizeof(int), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++)
      ibuf[e] = S[o][e].epar;
    ngot = fwrite(ibuf, sizeof(int), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    /*for (e=0; e < subhalo_num[o]; e++) ibuf[e] = S[o][e].upar;
     ngot = fwrite(ibuf,sizeof(int),subhalo_num[o],fp);
     if (ngot != subhalo_num[o]) {perror("fwrite"); exit(1);}*/

    for (e = 0; e < subhalo_num[o]; e++)
      ibuf[e] = S[o][e].eparn;
    ngot = fwrite(ibuf, sizeof(int), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    /*for (e=0; e < subhalo_num[o]; e++) ibuf[e] = S[o][e].uparn;
     ngot = fwrite(ibuf,sizeof(int),subhalo_num[o],fp);
     if (ngot != subhalo_num[o]) {perror("fwrite"); exit(1);}*/

    for (e = 0; e < subhalo_num[o]; e++)
      ibuf[e] = S[o][e].echi;
    ngot = fwrite(ibuf, sizeof(int), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    /*for (e=0; e < subhalo_num[o]; e++) ibuf[e] = S[o][e].echi;
     ngot = fwrite(ibuf,sizeof(int),subhalo_num[o],fp);
     if (ngot != subhalo_num[o]) {perror("fwrite"); exit(1);}*/

    for (e = 0; e < subhalo_num[o]; e++)
      fbuf[e] = S[o][e].mfracmin;
    ngot = fwrite(fbuf, sizeof(float), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++)
      fbuf[e] = S[o][e].mrat;
    ngot = fwrite(fbuf, sizeof(float), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++)
      ibuf[e] = S[o][e].uinf;
    ngot = fwrite(ibuf, sizeof(int), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++)
      ibuf[e] = S[o][e].einf;
    ngot = fwrite(ibuf, sizeof(int), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++)
      ibuf[e] = S[o][e].udif;
    ngot = fwrite(ibuf, sizeof(int), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++)
      ibuf[e] = S[o][e].edif;
    ngot = fwrite(ibuf, sizeof(int), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++)
      ibuf[e] = S[o][e].ecen;
    ngot = fwrite(ibuf, sizeof(int), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++)
      fbuf[e] = S[o][e].distcen * Box_Length;
    ngot = fwrite(fbuf, sizeof(float), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    /*for (e=0; e < subhalo_num[o]; e++) ibuf[e] = S[o][e].esat;
     ngot = fwrite(ibuf,sizeof(int),subhalo_num[o],fp);
     if (ngot != subhalo_num[o]) {perror("fwrite"); exit(1);}*/

    for (e = 0; e < subhalo_num[o]; e++)
      ibuf[e] = S[o][e].ehalo;
    ngot = fwrite(ibuf, sizeof(int), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < subhalo_num[o]; e++) {
      if (S[o][e].mhalo <= 0)
        fbuf[e] = 0;
      else
        fbuf[e] = log10(S[o][e].mhalo / hubble);
    }
    ngot = fwrite(fbuf, sizeof(float), subhalo_num[o], fp);
    if (ngot != subhalo_num[o]) {
      perror("fwrite");
      exit(1);
    }

    fclose(fp);
    free(ibuf);
    free(fbuf);
    free(fbufndim);
    free(S[o]);
  }
}

void print_halo_tree()
/* Print halo tree to binary files, one per output. */
{
  int o, e, idim, ngot;
  int *ibuf;
  float *fbuf, *fbufndim;
  char fname[128];
  FILE *fp;

  for (o = 0; o < nout; o++) {
    sprintf(fname, "%s/%s/halotree_%d.dat", Simulation_Directory, DirTree, o);
    if ((fp = fopen(fname, "w")) == NULL) {
      printf("! Error opening: %s\n", fname);
      exit(1);
    }
    printf("# printing %s\n", fname);

    fbuf = mymalloc(halo_mmin_num[o] * sizeof(float));
    ibuf = mymalloc(halo_mmin_num[o] * sizeof(int));

    ngot = fwrite(&halo_mmin_num[o], sizeof(int), 1, fp);
    if (ngot != 1) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < halo_mmin_num[o]; e++) {
      for (idim = 0; idim < DIMEN_NUM; idim++)
        fbufndim[e + idim] = G[o][e].pos[idim] * Box_Length;
      ngot = fwrite(fbuf, sizeof(float), DIMEN_NUM * halo_mmin_num[o], fp);
      if (ngot != DIMEN_NUM * halo_mmin_num[o]) {
        perror("fwrite");
        exit(1);
      }
    }

    for (e = 0; e < halo_mmin_num[o]; e++) {
      for (idim = 0; idim < DIMEN_NUM; idim++)
        fbufndim[e + idim] = G[o][e].vel[idim] * Box_Length / hubble_times[o];
      ngot = fwrite(fbuf, sizeof(float), DIMEN_NUM * halo_mmin_num[o], fp);
      if (ngot != DIMEN_NUM * halo_mmin_num[o]) {
        perror("fwrite");
        exit(1);
      }
    }

    for (e = 0; e < halo_mmin_num[o]; e++) {
      if (G[o][e].m == 0)
        fbuf[e] = 0;
      else
        fbuf[e] = log10(G[o][e].m / hubble);
    }
    ngot = fwrite(fbuf, sizeof(float), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < halo_mmin_num[o]; e++)
      fbuf[e] = G[o][e].vc;
    ngot = fwrite(fbuf, sizeof(float), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < halo_mmin_num[o]; e++)
      fbuf[e] = G[o][e].vs;
    ngot = fwrite(fbuf, sizeof(float), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < halo_mmin_num[o]; e++) {
      if (G[o][e].m200 == 0)
        fbuf[e] = 0;
      else
        fbuf[e] = log10(G[o][e].m200 / hubble);
    }
    ngot = fwrite(fbuf, sizeof(float), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < halo_mmin_num[o]; e++)
      fbuf[e] = G[o][e].c200;
    ngot = fwrite(fbuf, sizeof(float), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < halo_mmin_num[o]; e++)
      fbuf[e] = G[o][e].cfof;
    ngot = fwrite(fbuf, sizeof(float), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < halo_mmin_num[o]; e++)
      ibuf[e] = G[o][e].epar;
    ngot = fwrite(ibuf, sizeof(int), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < halo_mmin_num[o]; e++)
      ibuf[e] = G[o][e].eparn;
    ngot = fwrite(ibuf, sizeof(int), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < halo_mmin_num[o]; e++)
      ibuf[e] = G[o][e].echi;
    ngot = fwrite(ibuf, sizeof(int), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }

    for (e = 0; e < halo_mmin_num[o]; e++)
      fbuf[e] = G[o][e].mrat;
    ngot = fwrite(fbuf, sizeof(float), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }

#ifndef NO_SUBHALO
    for (e = 0; e < halo_mmin_num[o]; e++)
      ibuf[e] = G[o][e].ecen;
    ngot = fwrite(ibuf, sizeof(int), halo_mmin_num[o], fp);
    if (ngot != halo_mmin_num[o]) {
      perror("fwrite");
      exit(1);
    }
#endif

    fclose(fp);
    free(G[o]);
  }
}

/* MAIN *******************************************************************************************/
int main(int argc, char **argv) {
  if (argc != 2) {
    printf("! Usage: a.out <Box_Length>\n");
    exit(1);
  }
  Box_Length = atof(argv[1]);

  if (Box_Length == 50)
    Particle_Mass = 6.938e7;		// 512^3 	(5.5509e8 for 256^3)
  else if (Box_Length == 64)
    Particle_Mass = 3.5526e7;
  else if (Box_Length == 100)
    Particle_Mass = 1.3552e8;
  else if (Box_Length == 125)
    Particle_Mass = 1.2622e8;
  else if (Box_Length == 200)
    Particle_Mass = 1.6447e8;
  else if (Box_Length == 250)
    Particle_Mass = 1.3833e8;
  else if (Box_Length == 500)
    Particle_Mass = 9.6933e9;
  else if (Box_Length == 720)
    Particle_Mass = 7.6737e9;
  else {
    printf("! Box_Length not valid\n");
    exit(1);
  }
  sprintf(Simulation_Directory, "%s/lcdm%.0f", DirTreePM, Box_Length);

  /* Load tree into memory */
  subhalo_num = mycalloc(Snapshot_Num_Max, sizeof(int));
  halo_num = mycalloc(Snapshot_Num_Max, sizeof(int));
  halo_mmin_num = mycalloc(Snapshot_Num_Max, sizeof(int));
  aexps = mycalloc(Snapshot_Num_Max, sizeof(float));
  redshifts = mycalloc(Snapshot_Num_Max, sizeof(float));
  times = mycalloc(Snapshot_Num_Max, sizeof(float));
  time_wids = mycalloc(Snapshot_Num_Max, sizeof(float));
  hubble_times = mycalloc(Snapshot_Num_Max, sizeof(float));

  /** Read in tree files **/
  /* Load cosmological parameters */
  read_cosmology_parameters();

  /* Load output scale factors & sort in descending order */
  puts("# load cosmology & output scale factors");
  read_expansion_scale_factors();

  /* Load FoF halo (instantaneous) properties */
  puts("# load halo properties");
  read_halo_prop();

  /* Load halo outputs & IDs, assign halo parent linked list */
  puts("# load halo children");
  read_halo_child();

#ifndef NO_SUBHALO
  /* Load subhalo (instantaneous) properties */
  puts("# load subhalo properties");
  read_subhalo_prop();

  /* Load subhalo child outputs & id's, assign subhalo parent linked list */
  puts("# load subhalo children");
  read_subhalo_child();

  /* Load halo IDs for subhalos & assign to subhalos */
  puts("# load subhhalo halo id");
  read_subhalos_halo_id();

  /* Load central subhalos of halos & assign to halos */
  puts("# load halo central id");
  read_halos_central_id();
#endif

  /** assign halo/subhalo auxilliary properties **/
  /* assign derived halo history properties */
  puts("# halo: assign formation");
  assign_halo_formation_prop();

#ifndef	NO_SUBHALO
  /* fix tracking bug during switches */
  puts("# subhalo: fix tracking bug - switches");
  fix_subhalo_tracking_switch();

#ifndef SUBFIND
  /* fix FoF6D tracking bug */
  puts("# subhalo: fix tracking bug - fof6d");
  fix_subhalo_tracking_fof6d();
#endif

  /* fill in missing/skipped subhalos */
  puts("# subhalo: fill in skipped");
  fill_in_skipped_subhalos();

  /* assign subhalo central properties & maximum m */
  puts("# subhalo: central properties");
  assign_subhalo_central();
  puts("# subhalo: m_max & v_circ_peak");
  assign_subhalo_maximum_m();

  /* assign satellite linked list */
  puts("# subhalo: satellite linked list");
  //assign_subhalo_satellite_list();

  /* sort subhalo parents by m_max */
  puts("# subhalo: sort parents by m_max");
  sort_subhalo_parents();

  /* assign subhalo history properties */
  puts("# subhalo: assign histories");
  assign_subhalo_history_prop();
#endif

  breakpoint();

#ifdef PRINT_SNAPSHOTS
  print_snapshots();
#endif

#ifdef WRITE_HALO_TREE
  print_halo_tree();
#endif

#ifdef WRITE_SUBHALO_TREE
  write_subhalo_tree();
#endif

  return 0;
}
