#include	<stdlib.h>
#include	<stdio.h>
#include	<math.h>
#include	<string.h>
#include	<time.h>

/* 
Reads in subhalo child files.  Fixes tracking bugs.  Writes new child files.

Reads in:
tpmsph_*.child	(subhalo)
tpmsph_*.par		(subhalo)
tpmsph_*.cen		(halo)

Complier flags:
	SUBFIND - use for SUBFIND tree (not do extra FoF6D correction)
*/

#define	MaxOut					64										/* maximum # of outputs */
#define	DirTreePM				"../../data"					/* base folder, all simulations */
#define DirOutput				"outputs_fof6d"				/* base folder, input files */
#define	DirSubChild			"subhalo_child"				/* subhalos' child folder */
#define	DirSubChildFix	"subhalo_child"				/* subhalos' fixed child folder */
#define	DirHaloCen			"halo_central"				/* halos' central sub id folder */
#define	DirSubHost			"subhalo_host"				/* FoF indicies folder */
#define	FileAOut				"aout.txt"						/* output a's */

struct SubGrp {
	int		op;								// output time of first progenitor
	int		ep;								// head of progenitor list (> 0 if there is a parent)
	int		ol;								// next progenitor output time
	int		el;								// next progenitor ID
	int		oc;								// child output
	int		ec;								// child ID
	int		ehalo;
} *S[MaxOut];

struct FoFGrp {
	int	ecen;								// id of central subhalo
} *G[MaxOut];

char	DirSim[128];				// directory for input simulation files
float	LBox;
int		nout;								// # of outputs
int		*nsubs;							// array of # of subhalos in each timestep
float	*aouts;							// array of all output a's


/** UTILITY *******************************************************************/
void *mycalloc(size_t n,size_t size)
/* Allocate memory, initialize to 0, & check if it worked. */
{
	void *tmp;
  tmp = calloc(n,size);
  if (tmp==NULL) {
    printf("! mycalloc call failed\n");
    printf("  Attempting to allocate %lu bytes\n",(long)size);
    fflush(NULL);
  }
  return tmp;
}


int	fcompare(const void *a,const void *b)
/* Sort floats in decreasing order. */
{
	int	tmp=0;
	float	Pa,Pb;
	Pa = *(float *)a;
	Pb = *(float *)b;
	if (Pa >  Pb) tmp = -1;
	if (Pa == Pb) tmp =  0;
	if (Pa <  Pb) tmp =  1;
	return tmp;
}


/** INPUT *********************************************************************/
void read_aout()
/* Read output a's from file, assign to aout array, assign # of outputs nout. */
{
	int		ngot;
	float	temp;
	char	buf[BUFSIZ];
	char	fname[128];
	FILE	*fp;

	sprintf(fname,"%s/%s/%s",DirSim,DirOutput,FileAOut);
	if ( (fp=fopen(fname,"r"))==NULL) {
    printf("! Error opening %s\n",fname); exit(1);
  }
	/* skip past headers in input file */
	buf[0]='#';
	while ( buf[0] == '#' && feof(fp)==0 ) fgets(buf,BUFSIZ,fp);

  nout = 0;
  while ( feof(fp)==0 ) {
    ngot = sscanf(buf,"%f",&temp);
    if (ngot != 1) {
      printf("! Error reading line %d of %s:\n",nout,FileAOut);
      printf("  %s\n",buf); exit(1);
    }
 		if (temp >= 0 && temp <= 1) {
			aouts[nout] = temp;
    	nout++;
		}
    if (nout >= MaxOut) {
      printf("! MaxOut = %d exceeded\n",MaxOut); exit(1);
    }
    fgets(buf,BUFSIZ,fp);
  }
  fclose(fp);

	/* sort aout in descending order */
	qsort(aouts,nout,sizeof(float),fcompare);
}


void read_subhalos_host_id()
/* Read in subhalos' host id's.  Assign host id & FoF mass to its subhalos. */
{
	int		o,e;							// index over # of outputs, subhalo id
	int		ngot;							// temp for readin check
	int		nobj;							// # of subhalos read in at a given output
	int		*ehalos;					// temp array to store halo ID's
	char	fname[BUFSIZ];
	FILE	*fp;

  for (o=0; o < nout; o++) {
    sprintf(fname,"%s/%s/%s/tpmsph_%.4f.par",
						DirSim,DirOutput,DirSubHost,aouts[o]);
    if ( (fp=fopen(fname,"r"))==NULL ) {
      printf("! Error opening %s\n",fname); exit(1);
    }

    ngot = fread(&nobj,sizeof(int),1,fp);
    if (ngot != 1) {
			printf("! ngot = %d in %s\n",ngot,fname); exit(1);
		}

		S[o] = mycalloc(nobj,sizeof(struct SubGrp));
		nsubs[o] = nobj;

    ehalos = mycalloc(nobj,sizeof(int));
    ngot = fread(ehalos,sizeof(int),nobj,fp);
    if (ngot != nobj) {perror("fread"); exit(1);}

		for (e=1; e < nsubs[o]; e++) S[o][e].ehalo = ehalos[e];

		fclose(fp);
    free(ehalos);
  }
}


void read_halos_central_id()
/* Read in halos' central subhalo id.  Assign central sub id & mass to halo. */
{
	int		o,e;							// index over number of outputs
	int		ngot;							// temp for readin check
	int		nobj;							// temp for # of subhalos read in at each output
	int		*ecens;
	char	fname[BUFSIZ];
	FILE	*fp;

  for (o=0; o < nout; o++) {
    sprintf(fname,"%s/%s/%s/tpmsph_%.4f.cen",
						DirSim,DirOutput,DirHaloCen,aouts[o]);
    if ( (fp=fopen(fname,"r"))==NULL) {
      printf("! Error opening %s\n",fname); exit(1);
    }

    /* read number of objects */
		ngot = fread(&nobj,sizeof(int),1,fp);
    if (ngot != 1) {
			printf("! ngot = %d in %s\n",ngot,fname); exit(1);
		}

    ecens = mycalloc(nobj,sizeof(int));
    G[o] = mycalloc(nobj,sizeof(struct FoFGrp));

		ngot = fread(ecens,sizeof(int),nobj,fp);
    if (ngot != nobj) {perror("fread"); exit(1);}

		/* assign to halo structure */
		for (e=1; e < nobj; e++) G[o][e].ecen = ecens[e];

		fclose(fp);
		free(ecens);
  }
}


void read_subhalo_child()
/* Read & assign children of subhalos.  Use to assign subhalo parents. */
{
	int		o,otemp,e;				// index over # of outputs, subhalo id
	int		ngot;							// temp for readin check
	int		nobj;							// temp for number of subhalos read in at each output
	int		ec,t;
	int		*Ltail,*Ttail;
	long	indx,totgrp,*ncum;
	char	fname[BUFSIZ];
	FILE	*fp;
	int		*echilds;
	float	*achilds;
	struct SubGrp *St;

	for (o=0; o < nout; o++)
		for (e=1; e < nsubs[o]; e++)
    	S[o][e].ep = S[o][e].op = S[o][e].el = S[o][e].ol = -1;

	/* Calculate cumulative group counts */
  ncum = mycalloc(nout,sizeof(long));
  for (totgrp=nsubs[0], ncum[0]=0, t=1; t < nout; t++) {
    ncum[t] = ncum[t-1] + nsubs[t-1];
    totgrp += nsubs[t];
  }

  /* Set up auxiliary arrays for linked list construction */
  Ltail = mycalloc(totgrp,sizeof(int));
  Ttail = mycalloc(totgrp,sizeof(int));

	for (o=1; o < nout; ++o) {
		/* Load child id's & outputs */
		sprintf(fname,"%s/%s/%s/tpmsph_%.4f.child",
						DirSim,DirOutput,DirSubChild,aouts[o]);
		if ( (fp=fopen(fname,"r"))==NULL) {
			printf("! Error opening %s\n",fname); exit(1);
		}

		ngot = fread(&nobj,sizeof(int),1,fp);
		if (ngot != 1) {
			printf("! ngot = %d in %s\n",ngot,fname); exit(1);
		}
		if (nobj != nsubs[o]) {
      printf("! Nsub mismatch in %s\n",fname);
      printf("  Read ns = %d, expecting nsubs[%d] = %d\n",nobj,o,nsubs[o]);
      exit(1);
    }

		echilds = mycalloc(nobj,sizeof(int));
		achilds = mycalloc(nobj,sizeof(float));

		ngot = fread(echilds,sizeof(int),nobj,fp);
		if (ngot != nobj) {perror("fread"); exit(1);}
		ngot = fread(achilds,sizeof(float),nobj,fp);
		if (ngot != nobj) {perror("fread"); exit(1);}

		for (e=1; e < nobj; e++) {
			S[o][e].ec = echilds[e];
			if (S[o][e].ec < 0)
				S[o][e].oc = -1;
			else {
				for (otemp = 0; otemp < nout; otemp++) {
					if (aouts[otemp] == achilds[e]) {
						S[o][e].oc = otemp;
						break;
					}
				}
			}
		}
		fclose(fp);

		/* Assign parents */
	  for (e=1; e < nsubs[o]; e++) {
      if ( (ec=echilds[e]) > 0 ) {
        /* find the time this child refers to */
        t = o-1;
        while (t > 0 && fabs(aouts[t]-achilds[e]) > 0.01*aouts[t]) t--;
        if (fabs(aouts[t]-achilds[e]) > 0.01*aouts[t]) {
          printf("! Unable to find aout = %.4f starting at iout = %d\n",
								 achilds[e],o);
          exit(1);
        } else
          St = S[t];
        if (ec >= nsubs[t] || ec < 0) {
          printf("! ec = %d at %d out of range [0,%d)\n",ec,t,nsubs[t]);
					exit(1);
        }
        indx = ncum[t] + ec;
        if (St[ec].ep < 0) {
					/* child has no previously assigned parents */
          St[ec].ep     = e;
          St[ec].op     = o;
          Ltail[indx]  = e;
          Ttail[indx]  = o;
        } else {
          St = S[Ttail[indx]];
          St[Ltail[indx]].el = e;
          St[Ltail[indx]].ol = o;
          Ltail[indx] = e;
          Ttail[indx] = o;
        }
      }
		}
		free(echilds);
		free(achilds);
  }

  free(Ttail);
  free(Ltail);
  free(ncum);
}


/** PROCESSING ****************************************************************/
int	get_sub_type(int o,int e)
/* Import output & subhalo id.  Get subhalo stype:
1 = central, 0 = satellite. */
{
	if (S[o][e].ehalo <= 0) {
		printf("! (%d,%d) is subhalo without halo\n",o,e); exit(1);
	} else if (G[o][S[o][e].ehalo].ecen == 0)
		return 0;
	else if (G[o][S[o][e].ehalo].ecen == e)
		return 1;
	else
		return 0;
}


void fix_tracking_bug_switch()
/* Fix tracking bug (during switch) where a satellite & its "child" central at
	 next output are both assigned as parents to satellite in following output. */
{
	int o,e,op,ep,optemp;
	int ocen,ecen,osat,esat;
	int nfixed = 0;

	for (o=0; o < nout-2; o++) {
		for (e=1; e < nsubs[o]; e++) {
			if (get_sub_type(o,e) <= 0 && S[o][e].ep > 0) {
				ocen = ecen = osat = esat = -1;
				op = S[o][e].op;
				ep = S[o][e].ep;
				while (op < nout && ep > 0) {
					if (get_sub_type(op,ep) >= 1 && op == (o+1) && S[op][ep].ep <= 0) {
						ocen = op;
						ecen = ep;
					} else if (get_sub_type(op,ep) <= 0 && op > (o+1)) {
						osat = op;
						esat = ep;
					}
					optemp = S[op][ep].ol;
					ep = S[op][ep].el;
					op = optemp;
				}
				/* if find buggy parents, re-do parent & child assignments */
				if (ecen > 0 && esat > 0) {
					S[osat][esat].oc = ocen;
					S[osat][esat].ec = ecen;
					nfixed += 1;
				}
			}
		}
	}
	printf("  subs fixed in switches: %d\n",nfixed);
}


void fix_tracking_bug_fof6d()
/* Fix FoF6D tracking bug where a subhalo's most bound particles get split into
one or more Minf=0 satellites, and its child skips an output.  If gets split
into two or more Minf = 0 satellites, randomly pick one to use for fix. */
{
	int o,e,op,ep,optemp;
	int ocen,ecen,osat,esat;
	int nfixed = 0;

	for (o=0; o < nout-2; o++) {
		for (e=1; e < nsubs[o]; e++) {
			if (S[o][e].ep > 0) {
				ocen = ecen = osat = esat = -1;
				op = S[o][e].op;
				ep = S[o][e].ep;
				while (op < nout && ep > 0) {
					/* find if has Minf = 0 satellite parent */
					if (get_sub_type(op,ep) <= 0 && op == (o+1) && S[op][ep].ep <= 0) {
						osat = op;
						esat = ep;
					/* find if has central parent which skips to output */
					} else if (get_sub_type(op,ep) >= 1 && op > (o+1)) {
						ocen = op;
						ecen = ep;
					}
					optemp = S[op][ep].ol;
					ep = S[op][ep].el;
					op = optemp;
				}
				/* if find buggy parents, re-do parent & child assignments */
				if (ecen > 0 && esat > 0) {
					S[ocen][ecen].oc = osat;
					S[ocen][ecen].ec = esat;
					nfixed += 1;
				}
			}
		}
	}
	printf("  subs fixed in fof6d: %d\n",nfixed);
}


/** OUTPUT ********************************************************************/
void print_child()
/* Print subhalo child id's to binary files, one per output. */
{
	int o,e,ngot,*ibuf;
	float *fbuf;
	char fname[128];
	FILE *fp;

	free(S[0]);

	for (o=1; o < nout; ++o) {
		sprintf(fname,"%s/%s/%s/tpmsph_%.4f.child",
						DirSim,DirOutput,DirSubChildFix,aouts[o]);
		if ( (fp=fopen(fname,"w"))==NULL) {
			printf("! Error opening: %s\n",fname); exit(1);
		}

		ngot = fwrite(&nsubs[o],sizeof(int),1,fp);
		if (ngot != 1) {perror("fwrite"); exit(1);}

		ibuf = malloc(nsubs[o]*sizeof(int));
		if (ibuf==NULL){perror("malloc");exit(1);}
		for (e=0; e < nsubs[o]; e++) ibuf[e] = S[o][e].ec;
		ngot = fwrite(ibuf,sizeof(int),nsubs[o],fp);
		if (ngot != nsubs[o]) {perror("fwrite"); exit(1);}
	  free(ibuf);

		fbuf = malloc(nsubs[o]*sizeof(float));
   	if (fbuf==NULL) {perror("malloc"); exit(1);}

		for (e=0; e < nsubs[o]; e++) {
     if (S[o][e].oc < 0)
       fbuf[e] = -1.0;
     else
       fbuf[e] = aouts[S[o][e].oc];
		}
		ngot = fwrite(fbuf,sizeof(float),nsubs[o],fp);
		if (ngot != nsubs[o]) {perror("fwrite"); exit(1);}
   	free(fbuf);
		fclose(fp);
		free(S[o]);
	}
}


/** MAIN **********************************************************************/
int main(int argc,char **argv)
{
  if (argc != 2) {
    printf("! Usage: a.out <LBox>\n"); exit(1);
  }
	LBox = atof(argv[1]);

	sprintf(DirSim,"%s/lcdm%.0f",DirTreePM,LBox);

  /* Load tree into memory. */
  nsubs = mycalloc(MaxOut,sizeof(int));
	aouts = mycalloc(MaxOut,sizeof(float));

	/* Load output scale factors & sort in descending order */
	puts("# load output scale factors"); read_aout();

	/* Load halo IDs for subhalos & assign to subhalos */
	puts("# load subhhalo's halo id"); read_subhalos_host_id();

	/* Load central subhalos of halos & assign to halos */
	puts("# load halo's central id"); read_halos_central_id();

	/* Load subhalo child outputs & id's, assign subhalo parent linked list */
	puts("# load subhalo children"); read_subhalo_child();

	/* Fix tracking bug during switches */
	puts("# fix tracking bug - switches"); fix_tracking_bug_switch();

#ifndef SUBFIND
	/* Fix FoF6D tracking bug */
	puts("# fix tracking bug - fof6d"); fix_tracking_bug_fof6d();
#endif

	puts("# print new child files"); print_child();

	return 0;
}
